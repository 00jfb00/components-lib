import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";
import KIcon from "../KIcon";

class KChip extends Component {
  static defaultProps = {
    brand: "default",
    state: "default",
    hasRadius: false,
    leftIcon: false,
    outlined: false,
    colorText: "#0072CE",
    chipText: undefined,
    subText: undefined,
    colorBorder: undefined,
    id: undefined,
    ClickChip: undefined
  }

  static propTypes = {
    /** O estado do botão é baseado no tema. Os valores aceitos são: 'primary', 'secondary', 'ternary', 'gradient', 'white', 'black', 'dark', 'light', 'grey', 'grey-dark', 'grey-lighter', 'success', 'warning', 'info', 'danger' * */
    state: PropTypes.string,
    /** O estado do botão é baseado na marca. Os valores aceitos são: 'default', 'anhanguera' * */
    brand: PropTypes.string,
    /** Define se o chip tem borda arredondada ou não aceita os valores: true, false * */
    hasRadius: PropTypes.bool,
    /** Define se o chip possui apenas borda. Aceita os valores: true e false * */
    outlined: PropTypes.bool,
    /** Define onde o icone ficará dentro do chip. Aceita os valores: true e false * */
    leftIcon: PropTypes.bool,
    /** Define a cor do texto do chip * */
    colorText: PropTypes.string,
    /** Define o texto dentro chip * */
    chipText: PropTypes.any,
    /** Define o sub titulo dentro chip * */
    subText: PropTypes.any,
    /** Define a cor da borda do chip * */
    colorBorder: PropTypes.string,
    /** Define o id unico do chip * */
    id: PropTypes.any,
    /** Função de clique sobre o chip * */
    ClickChip: PropTypes.func
  };

  constructor(props) {
    super(props);
    this.chip = React.createRef();
    this.handleClick = this.handleClick.bind(this);
    this.handleClickChip = this.handleClickChip.bind(this);
  }

  handleClick = () => {
    if (this.props.onClick) {
      this.props.onClick(this.chip.current.innerHTML);
    }
  };

  handleClickChip = () => {
    if (this.props.ClickChip) {
      this.props.ClickChip();
    }
  };

  render() {
    const {
      id, colorText, colorBorder, fontSize, state, brand, hasRadius, outlined, leftIcon, icon, colorIcon, sizeIcon, chipText, subText
    } = this.props;

    return (
      <Fragment>
        <span
          key={id}
          id={`container-chip-${id}`}
          style={{ color: colorText, borderColor: colorBorder }}
          className={`k-chips chip ${fontSize} is-${state}-${brand} ${state === "default" ? "default" : ""} ${this.props.ClickChip ? "hover" : ""}`}
        >
          <span
            id={`body-chip-${id}`}
            style={{ borderColor: colorBorder }}
            className={`chip-value ${hasRadius ? "hasRadius" : ""} ${outlined ? "outlined" : ""} ${colorBorder ? "colorBorder" : ""}`}
          >
            {
              leftIcon && icon ? (
                <div id={`group-icon-left-chip-${id}`} className="icon-left">
                  <Fragment>
                    <KIcon id={`icon-chip-${id}`} onClick={this.handleClick} icon={icon} color={colorIcon} size={sizeIcon} />
                  </Fragment>
                </div>
              )
                : null
            }
            <div id={`group-label-chip-${id}`} className={`${subText ? "labels" : ""}`} onClick={() => { this.handleClickChip(this.props.ClickChip); }}>
              <div ref={this.chip} id={`label-chip-${id}`} className="text">{chipText}</div>
              {
                subText && (
                  <div id={`sub-text-chip-${id}`} className="sub-title">{subText}</div>
                )
              }
            </div>
            {
              leftIcon === false && icon ? (
                <div id={`group-icon-right-chip-${id}`} className="icon">
                  <Fragment>
                    <KIcon id={`icon-chip-${id}`} onClick={this.handleClick} icon={icon} color={colorIcon} size={sizeIcon} />
                  </Fragment>
                </div>
              )
                : null
            }
          </span>
        </span>
      </Fragment>
    );
  }
}
KChip.description = "Componente de Chip";
KChip.released = "v.0.1.72";
KChip.url = "Componentes/Átomos/KChip";
export default KChip;
