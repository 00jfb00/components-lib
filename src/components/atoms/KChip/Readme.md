Default:
```
<section>
    <KChip fontSize='p3' chipText={["Default Chip"]}/><br/><br/>
    <KChip ClickChip={(value) => console.log(value)} outlined state="primary" chipText={["lorem-ipsum-dolor.jpeg"]} icon={ "times"} colorIcon="primary" sizeIcon="xs"  subText="2 mb"/><br/><br/>
</section>
```
Tipos:
```
<section>
<div style={{maxWidth: "300px", border: "1px solid"}}>
    <KChip icon={["kci", "close"]} leftIcon outlined hasRadius state="primary" chipText={["Default Chiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiip"]} /><br/><br/>
</div>    
<KChip hasRadius state="primary" chipText={["Default Chip"]} /><br/><br/>
    <KChip outlined state="primary" chipText={["Default Chip"]} /><br/><br/>
    <KChip state="primary" chipText={["Default Chip"]} />
</section>
```

Colors:
```
<section>
    <KChip state="primary" placeholder="Add a tag..." max="10" chipText={["olaaaaaaaaa"]}/>
    <KChip state="secondary" placeholder="Add a tag..." max="10" chipText={["olaaaaaaaaa"]}/>
    <KChip state="ternary" placeholder="Add a tag..." max="10" chipText={["olaaaaaaaaa"]}/>
    <KChip state="gradient" placeholder="Add a tag..." max="10" chipText={["olaaaaaaaaa"]}/>
    <KChip state="success" placeholder="Add a tag..." max="10" chipText={["olaaaaaaaaa"]}/>
    <KChip state="warning" placeholder="Add a tag..." max="10" chipText={["olaaaaaaaaa"]}/>
    <KChip state="info" placeholder="Add a tag..." max="10" chipText={["olaaaaaaaaa"]}/>
    <KChip state="danger" placeholder="Add a tag..." max="10" chipText={["olaaaaaaaaa"]}/>
    <KChip state="dark" placeholder="Add a tag..." max="10" chipText={["olaaaaaaaaa"]}/>
</section>
```
Outlined default, com borda arredondada e com icone:
```
initialState = {
    chips: [
        { key: 0, label: 'Chip', outlined: true, colorBorder:'#ddd', colorText: '#ddd', fontSize:'p1' },
        { key: 1, label: 'Chip', state:"primary", hasRadius: true, outlined: true },
        { key: 2, label: 'Chip', state:"primary", icon:["kci", "close"], colorIcon:"primary", sizeIcon:"xs", hasRadius: true, outlined: true, leftIcon: true },
        { key: 3, label: 'Chip' },
        { key: 4, label: 'Chip' },
    ]
};

handleDelete = chipToDelete => () => {
    setState({chips: state.chips.filter(chip => chip.key !== chipToDelete.key)})
};

<section>
    {
         state.chips.map((chip, index) => 
        <div key={index}>         
            <KChip key={chip.key} id={chip.key} ClickChip={handleDelete(chip)} fontSize={chip.fontSize} chipText={chip.label} colorText={chip.colorText} colorBorder={chip.colorBorder} key={chip.key} onClick={handleDelete(chip)} state={chip.state} icon={chip.icon} colorIcon={chip.colorIcon} leftIcon={chip.leftIcon} sizeIcon={chip.sizeIcon} hasRadius={chip.hasRadius} outlined={chip.outlined}/>
            <br/><br/>
        </div>
        )
    }
</section>
```
Tamanho de fonte:
```
<section>
    <KChip fontSize='h1' state="primary" chipText={["Chip font"]}/><br/><br/>
    <KChip fontSize='h2' state="secondary" chipText={["Chip font"]}/><br/><br/>
    <KChip fontSize='h3' state="ternary" chipText={["Chip font"]}/><br/><br/>
    <KChip fontSize='h4' state="gradient" chipText={["Chip font"]}/><br/><br/>
    <KChip fontSize='h5' state="success" chipText={["Chip font"]}/><br/><br/>
    <KChip fontSize='p1' state="warning" chipText={["Chip font"]}/><br/><br/>
    <KChip fontSize='p2' state="info" chipText={["Chip font"]}/><br/><br/>
    <KChip fontSize='p3' state="danger" chipText={["Chip font"]}/><br/><br/>
    <KChip fontSize='p4' state="dark" chipText={["Chip font"]}/><br/><br/>
    <KChip fontSize='p5' state="dark" chipText={["Chip font"]}/><br/><br/>
</section>
```
