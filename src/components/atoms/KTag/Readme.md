Estados:

```
<section>
    <KTag textColor="#FF001A" bgColor="#FFEFF0" title="Importante"/>
    <KTag textColor="#E5A000" bgColor="#FFF7E6" title="Vídeo"/>
    <KTag textColor="#3E75C8" bgColor="#EDF4FF" title="Exercícios"/>
    <KTag textColor="#00A82F" bgColor="#F3FFF1" title="Desejável"/>
    <KTag textColor="#009AC4" bgColor="#E1FBFF" title="Fórum"/>
</section>
```

Tamanhos:

```
<section>
    <KTag fontSize="7px" textColor="#FF001A" bgColor="#FFEFF0" title="Importante"/>
</section>
<section>
    <KTag fontSize="15px" textColor="#E5A000" bgColor="#FFF7E6" title="Aula Digital"/>
</section>
<section>
    <KTag fontSize="30px" textColor="#3E75C8" bgColor="#EDF4FF" title="Exercícios"/>
</section>
<section>
    <KTag fontSize="60px" textColor="#00A82F" bgColor="#F3FFF1" title="Desejável"/>
</section>
<section>
    <KTag fontSize="90px" textColor="#009AC4" bgColor="#E1FBFF" title="Fórum"/>
</section>
```
