import React, { PureComponent } from "react";
import PropTypes from "prop-types";

class KTag extends PureComponent {
  static defaultProps = {
    title: "",
    textColor: "#9A9A9A",
    bgColor: "#DDDDDD",
    fontSize: "12px",
    borderRadius: "3px"
  };

  static propTypes = {
    /** Texto da tag - Será exibido dentro da tag */
    title: PropTypes.string,
    /** Cor do texto - Será exibido dentro da tag, Color Codes:(Color Name, Hex Code RGB e Decimal Code RGB) */
    textColor: PropTypes.string,
    /** Cor do backgroud texto - Será exibido dentro da tag, Color Codes:(Color Name, Hex Code RGB e Decimal Code RGB) */
    bgColor: PropTypes.string,
    /** Tamanho do texto - Será exibido dentro da tag */
    fontSize: PropTypes.string,
    /** borderRadius */
    borderRadius: PropTypes.string
  };

  render() {
    const {
      title, textColor, bgColor, fontSize, borderRadius
    } = this.props;

    return (
      <div
        className="k-tag"
        style={{
          color: textColor,
          backgroundColor: bgColor,
          fontSize,
          borderRadius
        }}
      >
        <b>{title}</b>
      </div>
    );
  }
}

KTag.description = "Componente de tag padrão";
KTag.released = "v.0.1.24";
KTag.url = "Componentes/Átomos/KTag";
export default KTag;
