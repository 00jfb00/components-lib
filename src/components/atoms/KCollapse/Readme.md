Default:  
Recebe expanded = true via props  
O conteúdo do collapse é passado como elemento filho
```

<section>
  <KCollapse styleContent={{minWidth: '288px'}}>
    <h1>Conteúdo do Collapse</h1>
    <ul>
        <li>Item 1</li>
        <li>Item 2</li>
        <li>Item 3</li>
        <li>Item 4</li>
    </ul>
  </KCollapse>
</section>
```
Expanded false adiciona uma classe que esconde o elemento
```
<section>
    <KCollapse
      expanded={false}>
    
    <div>
        <h1>Conteúdo do Dropdown</h1>
        <p>Lorem Ipsum is simply dummy text of the printing 
        and typesetting industry. Lorem Ipsum has been the 
        industry's standard dummy text ever since the 1500s, 
        when an unknown printer took a galley of type and 
        scrambled it to make a type specimen book.</p>
    </div>
    </KCollapse>
</section>
```
