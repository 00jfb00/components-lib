import React, { PureComponent } from "react";
import PropTypes from "prop-types";

class KCollapse extends PureComponent {
  static defaultProps = {
    styleContent: {},
    expanded: true,
    velocity: "0s"
  };

  static propTypes = {
    /** Objeto que permite customizar o estilo do conteúdo * */
    styleContent: PropTypes.object,
    /** Controla a exibição do componente */
    expanded: PropTypes.bool,
    /** Controla a velocidade de exibição do componente (usa transition) */
    velocity: PropTypes.string
  };

  render() {
    const {
      styleContent, children, expanded, velocity, ...rest
    } = this.props;

    return (
      <div className="k-collapse" {...rest}>

        <div
          style={{ transition: velocity }}
          className={`k-collapse__content-wrapper ${expanded ? "expanded" : ""}`}
        >
          <div className="k-collapse__content" style={styleContent}>
            {children}
          </div>
        </div>
      </div>
    );
  }
}

KCollapse.description = "Componente collapse padrão";
KCollapse.released = "v.0.1.40";
KCollapse.url = "Componentes/Átomos/KCollapse";
export default KCollapse;
