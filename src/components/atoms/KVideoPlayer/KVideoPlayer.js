import React, { PureComponent, Fragment } from "react";
import PropTypes from "prop-types";
import ReactPlayer from "react-player";

class KVideoPlayer extends PureComponent {
  static defaultProps = {
    controls: false,
    width: "100%",
    height: "100%",
    url: undefined,
    onProgress: () => {},
    onDuration: () => {},
    onEnded: () => {},
    seek: 0
  };

  static propTypes = {
    /** Controls configura a exibição de controles do player * */
    controls: PropTypes.bool,
    /** Recebe a url do vídeo * */
    url: PropTypes.string,
    /** Recebe o width do player de vídeo * */
    width: PropTypes.string,
    /** Recebe o height do player de vídeo * */
    height: PropTypes.string,
    /** Configura o callback de progresso do vídeo * */
    onProgress: PropTypes.func,
    /** Configura o callback da duração do vídeo * */
    onDuration: PropTypes.func,
    /** Configura o callback ao fim da execução do vídeo * */
    onEnded: PropTypes.func,
    /** Configura o tempo que o vídeo foi assistido * */
    seek: PropTypes.number
  };

  constructor(props) {
    super(props);
    this.handleProgress = this.handleProgress.bind(this);
    this.handleDuration = this.handleDuration.bind(this);
    this.handleEnded = this.handleEnded.bind(this);
  }

  handleProgress = evt => {
    this.props.onProgress(evt);
  };

  handleDuration = evt => {
    this.player.seekTo(this.props.seek);
    this.props.onDuration(evt);
  };

  handleEnded = evt => {
    this.props.onEnded(evt);
  };

  render() {
    const {
      controls, url, width, height, onProgress, onDuration, onEnded, seek, ...rest
    } = this.props;

    return (
      <Fragment>
        <ReactPlayer
          className="k-video-player"
          ref={ref => { this.player = ref; }}
          url={url}
          width={width}
          height={height}
          controls={controls}
          pip={false}
          {...rest}
          onEnded={this.handleEnded}
          onDuration={this.handleDuration}
          onProgress={this.handleProgress}
        />

      </Fragment>
    );
  }
}

KVideoPlayer.description = "Componente de player padrão da Kroton";
KVideoPlayer.released = "v.0.1.24";
KVideoPlayer.url = "Componentes/Átomos/KVideoPlayer";
export default KVideoPlayer;
