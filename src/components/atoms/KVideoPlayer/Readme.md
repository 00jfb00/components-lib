Default youtube player (Set width):
```
<section>
    <KVideoPlayer
      url='https://www.youtube.com/watch?v=jssO8-5qmag'
      controls={false}
      width={'640px'}
      height={'360px'}
      />
</section>
```

Default stream player (Responsive player):
```
<section>
    <KVideoPlayer
      url='https://bitdash-a.akamaihd.net/content/MI201109210084_1/m3u8s/f08e80da-bf1d-4e3d-8899-f0f6155f6efa.m3u8'
      controls={true}
      progressInterval={10000}
      onProgress={(progress)=> console.log(progress)}
      onDuration={(duration)=> console.log(duration)}
      onEnded={(ended)=> console.log(ended)}
      seek={35} />
</section>
```
