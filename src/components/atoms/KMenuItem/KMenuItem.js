import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";
import KIcon from "../KIcon";

class KMenuItem extends Component {
  static defaultProps = {
    active: false,
    icon: undefined,
    onClick: () => {
    },
    iconState: "primary",
    brand: "default",
    subtitle: undefined,
    iconColor: undefined,
    hasBorder: undefined,
    highlightTitle: false
  };

  static propTypes = {
    /** Configura título do item de menu * */
    title: PropTypes.string.isRequired,
    /** Configura o estado do ícone * */
    iconState: PropTypes.string,
    /** Configura a marca * */
    brand: PropTypes.string,
    /** Configura subtítulo do item de menu * */
    subtitle: PropTypes.string,
    /** Configura se o item de menu está ativo * */
    active: PropTypes.bool,
    /** Configura se o titulo de menu será destacado * */
    highlightTitle: PropTypes.bool,
    /** Configura a ação do item de menu * */
    onClick: PropTypes.func,
    /** Define a cor do icone * */
    iconColor: PropTypes.string,
    /** Define se o icone possui bordar ao redor* */
    hasBorder: PropTypes.bool,
    /** Define o icone que ficará dentro do bloco* */
    icon: PropTypes.any
  };

  handleClick = event => {
    this.props.onClick(event);
  };

  render() {
    const {
      title, subtitle, active, highlightTitle, brand, iconState, iconColor, hasBorder, icon
    } = this.props;
    const { handleClick } = this;
    const isActive = (active) ? "is-active" : "";
    const highlight = (highlightTitle) ? "is-highlight" : "";

    return (
      <div className={`k-menu-item ${isActive} ${highlight} is-primary-${brand}`} onClick={handleClick}>
        <div className={`k-menu-item__title-wrapper is-primary-${brand} ${isActive}`}>
          <h1 className={`${isActive}`}>{title}</h1>
          {subtitle && <h2>{subtitle}</h2>}
        </div>
        <div className={`${icon ? "k-menu-item__icon-wrapper" : ""} ${hasBorder ? "hasBorder" : ""}`}>
          {active ? (
            <Fragment>
              {icon && (
                <KIcon
                  color={iconColor}
                  state="success"
                  brand={brand}
                  icon={icon}
                />
              )}
            </Fragment>
          ) : (
            <Fragment>
              {icon && (
                <KIcon
                  state={iconState}
                  brand={highlightTitle ? brand : undefined}
                  icon={icon}
                />
              )}
            </Fragment>
          )}
        </div>
      </div>
    );
  }
}

KMenuItem.description = "Item do menu padrão";
KMenuItem.released = "v.0.1.22";
KMenuItem.url = "Componentes/Átomos/KMenuItem";
export default KMenuItem;
