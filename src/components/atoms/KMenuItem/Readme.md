Default:
```
<section style={{maxWidth:"280px"}}>
    <KMenuItem title="Ciência da Computação" />
</section>
```
Com borda:
```
<section style={{maxWidth:"280px"}}>
    <KMenuItem title="Ciência da Computação" hasBorder/>
</section>
```
Com marca:
```
<section style={{maxWidth:"280px"}}>
    <KMenuItem brand='anhanguera' title="Ciência da Computação" />
    <KMenuItem brand='anhanguera' title="Ciência da Computação" active icon='check-circle'/>
</section>
```
With title and subtitle:
```
<section style={{maxWidth:"280px"}}>
    <KMenuItem title="Ciência da Computação" subtitle="RA: 0291830390" />
</section>
```

With title and subtitle and active:
```
<section style={{maxWidth:"280px"}}>
    <KMenuItem title="Análise e Desenvolvimento de Sistemas - Tecnólogo" subtitle="RA: 0291830390" active={true} icon='check-circle'/>
</section>
```

With onClick callback:
```
<section style={{maxWidth:"280px"}}>
    <KMenuItem 
        title="Análise e Desenvolvimento de Sistemas - Tecnólogo" 
        subtitle="RA: 0291830390" 
        onClick={() => {alert('Item clicado')}} />
</section>
```
