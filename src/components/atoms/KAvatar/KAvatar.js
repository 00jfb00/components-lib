import React, { PureComponent, Fragment } from "react";
import PropTypes from "prop-types";
import KImage from "../KImage";

class KAvatar extends PureComponent {
  static propTypes = {
    /** Define a url da imagem * */
    src: PropTypes.string,
    /** Define se exibe o ícone indicador * */
    showIndicator: PropTypes.bool,
    /** Define o tamanho do indicador * */
    indicatorSize: PropTypes.string,
    /** Define a cor de fundo do indicator * */
    indicatorBackground: PropTypes.string,
    /** Define a largura do componente * */
    width: PropTypes.string,
    /** Define a altura do componente * */
    height: PropTypes.string
  };

  static defaultProps = {
    src: undefined,
    showIndicator: false,
    indicatorSize: "sm",
    width: "50px",
    height: "50px",
    indicatorBackground: "green"
  };

  render() {
    const {
      src, showIndicator, indicatorBackground, width, height, indicatorSize
    } = this.props;
    const customStyleKAvatar = { width, height };
    const customStyleIndicator = { backgroundColor: indicatorBackground };

    return (
      <div className="k-avatar" style={customStyleKAvatar}>
        {showIndicator
        && (
          <div
            className={`k-avatar__indicator ${indicatorSize}`}
            style={customStyleIndicator}
          >
            <i className={`k-avatar__arrow ${indicatorSize}`} />
          </div>
        )
        }
        <div className="k-avatar__img">
          {src && <Fragment><KImage src={src} responsive /></Fragment>}
        </div>
      </div>
    );
  }
}

KAvatar.description = "Renderiza a foto do perfil";
KAvatar.released = "v.0.1.22";
KAvatar.url = "Componentes/Átomos/KAvatar";

export default KAvatar;
