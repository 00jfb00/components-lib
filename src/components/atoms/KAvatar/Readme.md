Default:
```
<section>
    <KAvatar />
</section>
```

Avatar with src image:
```
<section>
    <KAvatar src="https://content-static.upwork.com/uploads/2014/10/01073427/profilephoto1.jpg" />
</section>
```

Avatar with indicator:
```
<section>
    <KAvatar showIndicator={true} />
</section>
```

Avatar with indicator and indicator Background:
```
<section>
    <KAvatar showIndicator={true} indicatorBackground="green" />
</section>
```

Avatar with Width, Height and indicatorSize:  
Indicator sizes: sm, md, lg
```
<section>
    <KAvatar showIndicator={true} indicatorSize="md" width="100px" height="100px" />
</section>
```