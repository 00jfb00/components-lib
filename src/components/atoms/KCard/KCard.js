import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import Card from "react-bootstrap/Card";

class KCard extends PureComponent {
  static defaultProps = {
    style: {},
    shadow: true,
    border: true,
    zoom: false,
    hover: false,
    hasMicroContent: false,
    radius: undefined
  };

  static propTypes = {
    /** O estilo para o Card * */
    style: PropTypes.object,

    /** Define se o card possui sombra * */
    shadow: PropTypes.bool,

    /** Define se o card possui borda * */
    border: PropTypes.bool,

    /** Define se o card possui zoom no hover* */
    zoom: PropTypes.bool,

    /** Define se o card possui o hover* */
    hover: PropTypes.bool,

    /** Define se o border radius do card* */
    radius: PropTypes.number,

    /** Define se o card esta na versão com micro contents, se for verdadeira a variavel então o card não possui padding* */
    hasMicroContent: PropTypes.bool
  }

  render() {
    const {
      children,
      title,
      subtitle,
      shadow,
      border,
      style,
      zoom,
      hover,
      radius,
      className,
      hasMicroContent,
      ...rest
    } = this.props;

    let localStyle = {};
    if (radius && typeof radius === "number") {
      localStyle = { ...localStyle, borderRadius: radius };
    }

    return (
      <Card
        className={[
          "k-card",
          shadow ? "has-shadow" : "",
          !border ? "hide-border" : "",
          hover ? "has-hover" : "",
          zoom ? "has-zoom" : "",
          hasMicroContent ? "hasMicroContent" : "",
          className
        ]}
        {...rest}
        style={{ ...style, ...localStyle }}
      >
        <Card.Body className={`${hasMicroContent ? "hasMicroContent" : ""}`}>
          {title && <Card.Title>{title}</Card.Title>}
          {subtitle && (
            <Card.Subtitle className="subtitle">{subtitle}</Card.Subtitle>
          )}
          {children}
        </Card.Body>
      </Card>
    );
  }
}

KCard.description = "Card padrão do projeto";
KCard.released = "v.0.1.22";
KCard.url = "Componentes/Átomos/KCard";
export default KCard;
