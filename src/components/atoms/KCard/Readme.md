Estilo e children:
```
<section>
    <KCard style={{ backgroundColor: 'yellow' }}>
        <div style={{ height: '50px' }}>
            <p>Teste</p>
        </div>
    </KCard> 
</section>
```

Borda:
```
<section>
    <KCard border={false} style={{ backgroundColor: 'yellow' }}>
        <div style={{ height: '50px' }}>
            <p>Teste</p>
        </div>
    </KCard> 
</section>
```

Sombra:
```
<section>
    <KCard shadow={false} style={{ backgroundColor: 'yellow' }}>
        <div style={{ height: '50px' }}>
            <p>Teste</p>
        </div>
    </KCard> 
</section>
```

Efeito hover (aumentando a sombra):
```
<section>
    <KCard hover style={{ backgroundColor: 'yellow' }}>
        <div style={{ height: '50px' }}>
            <p>Teste</p>
        </div>
    </KCard> 
</section>
```

Efeito Zoom:
```
<section>
    <KCard zoom style={{ backgroundColor: 'yellow' }}>
        <div style={{ height: '50px' }}>
            <p>Teste</p>
        </div>
    </KCard> 
</section>
```

Configurando border radius:
```
<section>
    <KCard radius={40} style={{ backgroundColor: 'yellow' }}>
        <div style={{ height: '50px' }}>
            <p>Teste</p>
        </div>
    </KCard> 
</section>
```
