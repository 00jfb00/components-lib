import React, { PureComponent } from "react";
import PropTypes from "prop-types";

class KUrlView extends PureComponent {
  static defaultProps = {
    src: undefined,
    containerStyle: undefined,
    contentStyle: undefined
  };

  static propTypes = {
    /** Define a url do conteúdo a ser exibido * */
    src: PropTypes.string,
    /** Define estilos customizados para container de conteúdo * */
    containerStyle: PropTypes.object,
    /** Define estilos customizados para o embed de conteúdo* */
    contentStyle: PropTypes.object
  };

  render() {
    const { src, containerStyle, contentStyle } = this.props;

    return (
      <div className="k-url-view" style={containerStyle}>
        {src ? (
          <iframe
            title="k-url-view"
            className="k-url-view__content"
            src={src}
            style={contentStyle}
          />
        ) : (
          <span>prop src not defined!</span>
        )}
      </div>
    );
  }
}

KUrlView.description = "Container para renderizar o iframe";
KUrlView.released = "v.0.1.22";
KUrlView.url = "Componentes/Átomos/KUrlView";
export default KUrlView;
