PDF:
```
<section>
    <KUrlView src="https://s3.amazonaws.com/cm-kls-content/201502/INTERATIVAS_2_0/CIENCIAS_MOLECULARES_E_CELULARES/U1/LIVRO_UNICO.pdf" />
</section>
```

HTML:
```
<section>
    <KUrlView src="https://s3.amazonaws.com/cm-kls-content/201502/INTERATIVAS_2_0/CIENCIAS_MOLECULARES_E_CELULARES/U1/HIB_APRESENTACAO_DA_DISCIPLINA/index.html" />
</section>
```

HTML:
```
<section>
    <KUrlView src="https://s3.amazonaws.com/cm-kls-content/201502/INTERATIVAS_2_0/CIENCIAS_MOLECULARES_E_CELULARES/LIVRO_DIGITAL/index.html" />
</section>
```