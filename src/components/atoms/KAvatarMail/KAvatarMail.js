import React, { PureComponent } from "react";
import PropTypes from "prop-types";

class KAvatarMail extends PureComponent {
  static propTypes = {
    /** Define a largura do componente * */
    width: PropTypes.string,
    /** Define a altura do componente * */
    height: PropTypes.string,
    /** Define o tamanho da fonte do componente * */
    fontSize: PropTypes.string,
    /** Define o state * */
    state: PropTypes.string,
    /** Define o brand * */
    brand: PropTypes.string,
    /** Define fullname * */
    fullName: PropTypes.string.isRequired
  };

  state = {
    initialsName: this.props.fullName ? this.props.fullName : ""
  }

  static defaultProps = {
    width: "30px",
    height: "30px",
    fontSize: "12px",
    state: "primary",
    brand: "default"
  };

   getInitials = fullName => {
     if (fullName !== "") {
     // Remove os de,da, dos,das.
       fullName = fullName.replace(/\s(de|da|dos|das)\s/g, " ");
       // iniciais de cada parte do nome.
       const initials = fullName.match(/\b(\w)/gi);

       const firstLetterName = initials.splice(0, 1).join("").toUpperCase();
       const firstLetterLastName = initials.splice(initials.length - 1).join("").toUpperCase();
       // Initials
       const initialsName = firstLetterName + firstLetterLastName;

       this.setState({ initialsName });
     }
   }

   componentDidMount = () => {
     this.getInitials(this.state.initialsName);
   }

   render() {
     const {
       width, height, fontSize, state, brand
     } = this.props;
     const customStyleKAvatarMail = { width, height, fontSize };

     return (
       <div
         className={`k-avatar_mail is-${state}-${brand}`}
         style={customStyleKAvatarMail}
       >
         {this.state.initialsName}
       </div>
     );
   }
}

KAvatarMail.description = "Renderiza a foto do perfil para Mensageria";
KAvatarMail.released = "v.0.1.22";
KAvatarMail.url = "Componentes/Átomos/KAvatarMail";

export default KAvatarMail;
