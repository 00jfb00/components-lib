Estados:
```
<section>
    <KProgressBar animated percentage={30}/>
    <br/>
    <KProgressBar striped percentage={50}/>
    <br/>
    <KProgressBar percentage={30} state="primary"/>
    <br/>
    <KProgressBar percentage={30} state="secondary"/>
    <br/>
    <KProgressBar percentage={30} state="success" />
    <br/>
    <KProgressBar percentage={30} state="danger" />
    <br/>
    <KProgressBar percentage={30} state="warning" />
    <br/>
    <KProgressBar percentage={30} state="info" />
    <br/>
    <KProgressBar percentage={30} state="light" />
    <br/>
    <KProgressBar percentage={30} state="dark" />
    <br/>
    <KProgressBar percentage={30} brand="anhanguera" state="primary" round />
</section>
```

Marcas:
```
<section>
  <KProgressBar brand="anhanguera" state="primary" percentage={30}/> 
  <br/>
  <KProgressBar brand="anhanguera" state="secondary" percentage={30}/>
  <br/>
  <KProgressBar brand="anhanguera" state="ternary" percentage={30}/>
  <br/>
  <KProgressBar brand="anhanguera" state="gradient" percentage={30} />

</section>
```

Tamanhos:
```
<section>
  <KProgressBar animated percentage={30} size='sm'/>
  <br/>
  <KProgressBar animated percentage={30} size='medium'/>
  <br/>
  <KProgressBar animated percentage={30} size='lg'/>
</section>
```

