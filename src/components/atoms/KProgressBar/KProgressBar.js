import React, { PureComponent, Fragment } from "react";
import PropTypes from "prop-types";
import ProgressBar from "react-bootstrap/ProgressBar";

class KProgressBar extends PureComponent {
  static defaultProps = {
    id: "",
    striped: false,
    state: "primary",
    brand: "default",
    animated: false,
    size: "sm",
    round: false
  };

  static propTypes = {
    /** Um id padrão para o componente* */
    id: PropTypes.string,
    /** A percentage exibe o progresso da barra * */
    percentage: PropTypes.number.isRequired,
    /** O striped  permite que a barra tenha listras * */
    striped: PropTypes.bool,
    /** O animated  permite que as listras da barra tenha movimento * */
    animated: PropTypes.bool,
    /** O estado do progressBar é baseado no tema. Os valores aceitos são: 'primary', 'secondary', 'ternary', 'gradient', 'white', 'black', 'dark', 'light', 'grey', 'grey-dark', 'grey-lighter', 'success', 'warning', 'info', 'danger' * */
    state: PropTypes.string,
    /** Define um tamanho para o progressBar. Tamanhos aceitos: sm, medium e lg* */
    size: PropTypes.string,
    /** O estado do progressBar é baseado na marca. Os valores aceitos são: 'default', 'anhanguera' * */
    brand: PropTypes.string,
    /** O round do progressBar permite que as bordar sejam aredondadas * */
    round: PropTypes.bool
  };

  render() {
    const {
      size, round, state, brand, striped, animated, percentage, id
    } = this.props;

    return (
      <Fragment>
        <ProgressBar
          id={id}
          className={`k-Progress-Bar --${size} ${round ? "--is-round" : ""} is-${state}-${brand}`}
          striped={striped}
          animated={animated}
          now={percentage}
          variant={state}
        />
      </Fragment>
    );
  }
}

KProgressBar.description = "Progress bar padrão";
KProgressBar.released = "v.0.1.22";
KProgressBar.url = "Componentes/Átomos/KProgressBar";
export default KProgressBar;
