import React, { PureComponent } from "react";
import PropTypes from "prop-types";

class KBadge extends PureComponent {
  static defaultProps = {
    size: 12,
    color: "#F69021",
    hide: false,
    border: true,
    borderWidth: 2,
    borderColor: "#FFFFFF",
    rounded: true,
    position: "topRight"
  };

  static propTypes = {
    /** Define se o badge deve ser exibido * */
    hide: PropTypes.bool,

    /** Define o tamanho do badge * */
    size: PropTypes.number,

    /** Define a cor do badge * */
    color: PropTypes.string,

    /** Define a posição do badge (topLeft, topRight, bottomLeft, bottomRight) * */
    position: PropTypes.oneOf(["topLeft", "topRight", "bottomLeft", "bottomRight"]),

    /** Define se o badge será redondo * */
    rounded: PropTypes.bool,

    /** Define se o badge possui borda * */
    border: PropTypes.bool,

    /** Define a cor da borda * */
    borderColor: PropTypes.string,

    /** Define a espessura da borda * */
    borderWidth: PropTypes.number
  };

  render() {
    const {
      children, hide, color, borderWidth, size, border, borderColor, position, rounded
    } = this.props;

    return (
      <div
        className="k-badge"
      >
        {children}
        {!hide && (
          <div
            name="badge"
            className={`${position}`}
            style={{
              backgroundColor: color,
              width: size,
              height: size,
              borderStyle: "solid",
              borderWidth: border ? borderWidth : 0,
              borderColor,
              position: "absolute",
              borderRadius: !rounded ? 0 : (size / 2)
            }}
          />
        )}
      </div>
    );
  }
}

KBadge.description = "Adicionar uma marcação no canto de um componente";
KBadge.released = "v.0.1.22";
KBadge.url = "Componentes/Átomos/KBadge";
export default KBadge;
