Estado
```
<section>
    <KBadge>
        <div style={{ height: '50px', width: '50px', backgroundColor: 'blue' }} />
    </KBadge> 
</section>
```

Hide
```
<section>
    <KBadge hide={true}>
        <div style={{ height: '50px', width: '50px', backgroundColor: 'blue' }} />
    </KBadge> 
</section>
```

Tamanho
```
<section>
    <KBadge size={20}>
        <div style={{ height: '50px', width: '50px', backgroundColor: 'blue' }} />
    </KBadge> 
    <br />
    <KBadge size={15}>
        <div style={{ height: '50px', width: '50px', backgroundColor: 'blue' }} />
    </KBadge> 
</section>
```

Posição
```
<section>
    <KBadge position="topLeft">
        <div style={{ height: '50px', width: '50px', backgroundColor: 'blue' }} />
    </KBadge> 
    <br />
    <KBadge position="topRight">
        <div style={{ height: '50px', width: '50px', backgroundColor: 'blue' }} />
    </KBadge> 
    <br />
    <KBadge position="bottomLeft">
        <div style={{ height: '50px', width: '50px', backgroundColor: 'blue' }} />
    </KBadge> 
    <br />
    <KBadge position="bottomRight">
        <div style={{ height: '50px', width: '50px', backgroundColor: 'blue' }} />
    </KBadge> 
</section>
```

Cores
```
<section>
    <KBadge color="#FFFFFF">
        <div style={{ height: '50px', width: '50px', backgroundColor: 'blue' }} />
    </KBadge> 
    <br />
    <KBadge color="red">
        <div style={{ height: '50px', width: '50px', backgroundColor: 'blue' }} />
    </KBadge> 
</section>
```

Rounded
```
<section>
    <KBadge>
        <div style={{ height: '50px', width: '50px', backgroundColor: 'blue' }} />
    </KBadge> 
    <br />
    <KBadge rounded={false}>
        <div style={{ height: '50px', width: '50px', backgroundColor: 'blue' }} />
    </KBadge> 
</section>
```

Borda
```
<section>
    <KBadge>
        <div style={{ height: '50px', width: '50px', backgroundColor: 'blue' }} />
    </KBadge> 
    <br />
    <KBadge border={false}>
        <div style={{ height: '50px', width: '50px', backgroundColor: 'blue' }} />
    </KBadge> 
</section>
```

Cores de borda
```
<section>
    <KBadge borderColor="#FFFFFF">
        <div style={{ height: '50px', width: '50px', backgroundColor: 'blue' }} />
    </KBadge> 
    <br />
    <KBadge borderColor="red">
        <div style={{ height: '50px', width: '50px', backgroundColor: 'blue' }} />
    </KBadge> 
</section>
```

Espessura de borda
```
<section>
    <KBadge borderWidth={1}>
        <div style={{ height: '50px', width: '50px', backgroundColor: 'blue' }} />
    </KBadge> 
    <br />
    <KBadge borderWidth={3}>
        <div style={{ height: '50px', width: '50px', backgroundColor: 'blue' }} />
    </KBadge> 
</section>
```
