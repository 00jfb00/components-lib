import React, { Fragment, PureComponent } from "react";
import PropTypes from "prop-types";
import Spinner from "react-bootstrap/Spinner";

class KSpinner extends PureComponent {
  static defaultProps = {
    animation: "border",
    role: "status",
    size: "md",
    state: "primary",
    brand: "default",
    backdrop: false
  };

  static propTypes = {
    animation: PropTypes.string,
    role: PropTypes.string,
    size: PropTypes.string,
    state: PropTypes.string,
    brand: PropTypes.string,
    backdrop: PropTypes.bool
  };

  render() {
    return (
      <Fragment>
        <div
          className={`k-spinner ${
            this.props.backdrop ? "--spinner-backdrop" : ""
          }`}
        >
          <Spinner
            className={[
              "k-spinner",
              `is-${this.props.state}-${this.props.brand}`
            ]}
            animation={this.props.animation}
            role={this.props.role}
            size={this.props.size}
            variant={this.props.state}
          />
        </div>
      </Fragment>
    );
  }
}

KSpinner.description = "Componente de loading";
KSpinner.released = "v.0.1.22";
KSpinner.url = "Componentes/Átomos/KSpinner";
export default KSpinner;
