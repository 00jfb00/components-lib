import React, { PureComponent } from "react";
import PropTypes from "prop-types";

class KMessageriaBadge extends PureComponent {
  static defaultProps = {
    size: 12,
    bgColor: undefined,
    fontColor: undefined,
    hide: false,
    state: "primary",
    brand: "default",
    border: true,
    borderWidth: 2,
    borderColor: "#FFFFFF",
    rounded: true,
    content: undefined,
    position: "center"
  };

  static propTypes = {
    /** Define se o badge deve ser exibido * */
    hide: PropTypes.bool,

    /** Define o tamanho do badge * */
    size: PropTypes.number,

    /** Define a cor do background do badge * */
    bgColor: PropTypes.string,

    /** Define a cor da fonte do badge * */
    fontColor: PropTypes.string,

    /** Define a posição do badge (topLeft, topRight, bottomLeft, bottomRight, center) * */
    position: PropTypes.oneOf(["topLeft", "topRight", "bottomLeft", "bottomRight", "center"]),

    /** Define se o badge será redondo * */
    rounded: PropTypes.bool,

    /** Marca a ser utilizada */
    brand: PropTypes.string,

    /** O estado é baseado no tema. Os valores aceitos são: 'primary', 'secondary', 'ternary', 'gradient', 'white', 'black', 'dark', 'light', 'grey', 'grey-dark', 'grey-lighter', 'success', 'warning', 'info', 'danger'  */
    state: PropTypes.string,

    /** Define se o badge possui borda * */
    border: PropTypes.bool,

    /** Define a cor da borda * */
    borderColor: PropTypes.string,

    /** Define se exibirá um conteúdo (number, string, kicon) dentro do badge, específico para messageria * */
    content: PropTypes.any,

    /** Define a espessura da borda * */
    borderWidth: PropTypes.number
  };

  render() {
    const {
      children, hide, bgColor, fontColor, state, brand, borderWidth, size, border, borderColor, content, position, rounded
    } = this.props;

    return (
      <div
        className="k-messageria-badge"
      >
        {children}
        {!hide && (
          <div
            name="badge"
            className={`item ${position} is-${state}-${brand}`}
            style={{
              backgroundColor: bgColor,
              width: size,
              height: size,
              borderStyle: "solid",
              borderWidth: border ? borderWidth : 0,
              borderColor,
              position: content ? "relative" : "absolute",
              borderRadius: !rounded ? 0 : (size / 2)
            }}
          >
            {content
              ? (
                <p
                  className={`internalText is-${state}-${brand}`}
                  style={{ color: fontColor }}
                >
                  {content}
                </p>
              )
              : null}
          </div>
        )}

      </div>
    );
  }
}

KMessageriaBadge.description = "Adicionar um badge ao lado de algum outro componente, é possível ter um número dentro do badge";
KMessageriaBadge.released = "v.0.1.87";
KMessageriaBadge.url = "Componentes/Átomos/KMessageriaBadge";
export default KMessageriaBadge;
