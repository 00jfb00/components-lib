Com Estado
```
<section>
    <KMessageriaBadge border={false} state='primary' content={'9+'} size={30} />
</section>
```

Com Estado e Marca
```
<section>
    <KMessageriaBadge border={false} state='secondary' brand='anhanguera' content={'9+'} size={30} />
    <KMessageriaBadge border={false} state='primary' content={'9+'} size={30} />
    <KMessageriaBadge border={false} brand='anhanguera' state='primary' content={'9+'} size={30} />
    <KMessageriaBadge border={false} brand='anhanguera' state='secondary' content={'9+'} size={30} />
</section>
```

Caso haja Color será o preferencial
```
<section>
    <KMessageriaBadge border={false} state='secondary' brand='anhanguera' content={'9+'} size={30} />
    <KMessageriaBadge border={false} state='primary' content={'9+'} size={30} bgColor="#CCC" fontColor="#000" />
    <KMessageriaBadge border={false} brand='anhanguera' state='primary' content={'9+'} size={30} bgColor="#E0F4FF" fontColor="#AAA" />
    <KMessageriaBadge border={false} brand='anhanguera' state='secondary' content={'9+'} size={30} bgColor="#E0F4FF" fontColor="#DDD" />
</section>
```

Hide
```
<section>
    <KMessageriaBadge hide={true}/> 
</section>
```

Tamanho
```
<section>
    <KMessageriaBadge size={40}/>
    <br /><br /><br />
    <KMessageriaBadge bgColor='#F00' size={15}/>
</section>
```

Posição
```
<section>
    <KMessageriaBadge position="center">
        <div style={{ height: '50px', width: '50px', backgroundColor: 'blue' }} />
    </KMessageriaBadge> 
    <br />
    <KMessageriaBadge position="topLeft">
        <div style={{ height: '50px', width: '50px', backgroundColor: 'blue' }} />
    </KMessageriaBadge> 
    <br />
    <KMessageriaBadge position="topRight">
        <div style={{ height: '50px', width: '50px', backgroundColor: 'blue' }} />
    </KMessageriaBadge> 
    <br />
    <KMessageriaBadge position="bottomLeft">
        <div style={{ height: '50px', width: '50px', backgroundColor: 'blue' }} />
    </KMessageriaBadge> 
    <br />
    <KMessageriaBadge position="bottomRight">
        <div style={{ height: '50px', width: '50px', backgroundColor: 'blue' }} />
    </KMessageriaBadge> 
</section>
```

Cores
```
<section>
    <KMessageriaBadge bgColor="#FFFFFF">
        <div style={{ height: '50px', width: '50px', backgroundColor: 'blue' }} />
    </KMessageriaBadge> 
    <br />
    <KMessageriaBadge bgColor="red">
        <div style={{ height: '50px', width: '50px', backgroundColor: 'blue' }} />
    </KMessageriaBadge> 
</section>
```

Rounded
```
<section>
    <KMessageriaBadge>
        <div style={{ height: '50px', width: '50px', backgroundColor: 'blue' }} />
    </KMessageriaBadge> 
    <br />
    <KMessageriaBadge rounded={false}>
        <div style={{ height: '50px', width: '50px', backgroundColor: 'blue' }} />
    </KMessageriaBadge> 
</section>
```

Borda
```
<section>
    <KMessageriaBadge>
        <div style={{ height: '50px', width: '50px', backgroundColor: 'blue' }} />
    </KMessageriaBadge> 
    <br />
    <KMessageriaBadge border={false}>
        <div style={{ height: '50px', width: '50px', backgroundColor: 'blue' }} />
    </KMessageriaBadge> 
</section>
```

Cores de borda
```
<section>
    <KMessageriaBadge borderColor="#FFFFFF">
        <div style={{ height: '50px', width: '50px', backgroundColor: 'blue' }} />
    </KMessageriaBadge> 
    <br />
    <KMessageriaBadge borderColor="red">
        <div style={{ height: '50px', width: '50px', backgroundColor: 'blue' }} />
    </KMessageriaBadge> 
</section>
```

Espessura de borda
```
<section>
    <KMessageriaBadge borderWidth={1}>
        <div style={{ height: '50px', width: '50px', backgroundColor: 'blue' }} />
    </KMessageriaBadge> 
    <br />
    <KMessageriaBadge borderWidth={3}>
        <div style={{ height: '50px', width: '50px', backgroundColor: 'blue' }} />
    </KMessageriaBadge> 
</section>
```
