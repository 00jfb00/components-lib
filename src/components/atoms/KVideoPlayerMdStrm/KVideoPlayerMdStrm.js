import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";

import "./mdstrm_player_api";

class KVideoPlayerMdStrm extends Component {
  static defaultProps = {
    controls: true,
    type: "media",
    progressInterval: 1000,
    width: document.activeElement.clientWidth,
    height: document.activeElement.clientHeight,
    onProgress: () => {},
    onDuration: () => {},
    onEnded: () => {},
    seek: 0,
    autoPlay: false,
    videoDuration: 0
  };

  static propTypes = {
    /** Controls configura a exibição de controles do player * */
    controls: PropTypes.bool,
    /** autoPlay define se o video irá executar automaticamente * */
    autoPlay: PropTypes.bool,
    /** Recebe o ID do vídeo * */
    mediaId: PropTypes.string.isRequired,
    /** Recebe o tipo do vídeo * */
    type: PropTypes.oneOf(["media", "live"]),
    /** Recebe o intervalo de atualizaçao do progresso * */
    // eslint-disable-next-line react/no-unused-prop-types
    progressInterval: PropTypes.number,
    /** Recebe o width do player de vídeo * */
    width: PropTypes.number,
    /** Recebe o height do player de vídeo * */
    height: PropTypes.number,
    /** Configura o callback de progresso do vídeo * */
    onProgress: PropTypes.func,
    /** Configura o callback da duração do vídeo * */
    onDuration: PropTypes.func,
    /** Configura o callback ao fim da execução do vídeo * */
    // eslint-disable-next-line react/no-unused-prop-types
    onEnded: PropTypes.func,
    /** Configura o tempo que o vídeo foi assistido * */
    // eslint-disable-next-line react/no-unused-prop-types
    seek: PropTypes.number,
    /** Recebe a duração do vídeo em segundos, resolvendo a limitação do Player da MediaStream, que não tem esse dado * */
    videoDuration: PropTypes.number
  };

  constructor(props) {
    super(props);
    this.state = {
      // eslint-disable-next-line react/no-unused-state
      isPlayerPlaying: false,
      // eslint-disable-next-line react/no-unused-state
      lastProgressCallback: null,
      // eslint-disable-next-line react/no-unused-state
      seeked: false
    };
    this.player = undefined;
  }

  componentDidMount() {
    this.handleScriptLoad();
  }

  componentDidUpdate(prevProps) {
    /* istanbul ignore next */ if (prevProps.width !== this.props.width || prevProps.height !== this.props.height) {
      this.handleScriptLoad();
    }
  }

  handleOnPlay = () => {
    const _this = this;
    this.player.getCurrentTime(/* istanbul ignore next */ time => {
      if (Math.floor(time) === 0 && Math.floor(time) !== Math.floor(_this.props.seek) && !_this.state.seeked) {
        _this.player.seekTo(_this.props.seek);
        _this.setState({ seeked: true });
      }
    });
  };

  handleOnVideoEnd = () => {
    const _this = this;
    this.player.getCurrentTime(/* istanbul ignore next */ time => {
      _this.props.onEnded({ playedSeconds: Math.floor(time) });
      _this.setState({ isPlayerPlaying: false });
    });
  };

  handleOnSeek = time => {
    this.props.onDuration(this.props.videoDuration);
    this.setState({ lastProgressCallback: Math.floor(time) });
  };

  handleOnTimeUpdate = time => {
    if (!this.player.isPlaying()) return;
    if (Math.floor(time % (this.props.progressInterval / 1000)) === 0 && Math.floor(time) !== this.state.lastProgressCallback) {
      this.setState({ lastProgressCallback: Math.floor(time) });
      this.props.onProgress({ playedSeconds: Math.floor(time) });
    }
  };

  handleScriptLoad = () => {
    const _this = this;
    const playerOptions = {
      width: this.props.width,
      height: this.props.height,
      type: this.props.type,
      id: this.props.mediaId,
      controls: this.props.controls,
      autoplay: this.props.autoPlay,
      events: {
        onPlay() {
          _this.handleOnPlay();
        },
        onVideoEnd() {
          _this.handleOnVideoEnd();
        },
        onSeeked(time) {
          _this.handleOnSeek(time);
        },
        onTimeUpdate(time) {
          _this.handleOnTimeUpdate(time);
        }
      }
    };

    try {
      // eslint-disable-next-line no-undef
      this.player = new MediastreamPlayer(`k-mdstrm-player-${this.props.mediaId}`, playerOptions);
      this.player.videoPlay();
    } catch (e) {
      this.player = undefined;
    }
  };

  render() {
    return (
      <Fragment>
        <div id={`k-mdstrm-player-${this.props.mediaId}`} />
      </Fragment>
    );
  }
}

KVideoPlayerMdStrm.description = "Componente de player de vídeo da Media Stream";
KVideoPlayerMdStrm.released = "v.0.3.19";
KVideoPlayerMdStrm.url = "Componentes/Átomos/KVideoPlayerMdStrm";
export default KVideoPlayerMdStrm;
