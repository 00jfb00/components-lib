(function () {
  const b = (function () {
    function b(c, a) {
      let b; let k; a == null && (a = {}); this._isPlaying = this.playerDestroyed = this._isReady = !1; const f = []; document.getElementById(c) || f.push("ID must be a valid DOM Element"); /^[0-9a-fA-F]{24}$/.test(a.id) || this._getYouTubeID(a.id) || f.push("Options.id must be a valid Platform Video ID or a YouTube Video ID"); (b = a.type) !== "media" && b !== "live" && b !== "dvr" && f.push("Options.type must be \"media\", \"live\" or \"dvr\""); isNaN(parseInt(a.width)) && f.push("Options.width must be a positive integer");
      isNaN(parseInt(a.height)) && f.push("Options.height must be a positive integer"); if ((k = a.protocol) === "http" || k === "https") this._protocol = a.protocol; f.length > 0 ? this._error(f) : this._init(c, a);
    }b.prototype._init = function (c, a) {
      let b; let k; let f; let p; let q; let r; let n; let t; let u; let v; let w; let x; let y; let z; let A; let B; let C; let D; let E; let F; let l; let G; let H; let I; let J; let m; let K; let L; let M; let N; let O; let P; let Q; this._callbacks = {}; this._bindEvent(window, "message", (function (a) { return function (c) { if (!a.playerDestroyed && (c != null ? c.source : void 0) === a._IFrameContent) return a._receiveMessage(c); }; }(this))); this._events = {}; let d = "onAdsAdBreakReady onAdsAdMetadata onAdsAllAdsCompleted onAdsClick onAdsComplete onAdsContentPauseRequested onAdsContentResumeRequested onAdsDurationChange onAdsFirstQuartile onAdsImpression onAdsLinearChanged onAdsLoaded onAdsLog onAdsMidpoint onAdsPaused onAdsResumed onAdsSkippableStateChanged onAdsSkipped onAdsStarted onAdsThirdQuartile onAdsUserClose onAdsVolumeChanged onAdsVolumeMuted".split(" ");
      let h = 0; for (k = d.length; h < k; h++) { var e = d[h]; typeof ((f = a.events) != null ? f[e] : void 0) === "function" && (this._events[e] = a.events[e]); } typeof ((p = a.events) != null ? p.onPlayerMounted : void 0) === "function" && (this._events.onPlayerMounted = a.events.onPlayerMounted); typeof ((A = a.events) != null ? A.onPlayerReady : void 0) === "function" && (this._events.onPlayerReady = a.events.onPlayerReady); typeof ((G = a.events) != null ? G.onVideoEnd : void 0) === "function" && (this._events.onVideoEnd = a.events.onVideoEnd); typeof ((L = a.events) != null
        ? L.onVideoStop : void 0) === "function" && (this._events.onVideoStop = a.events.onVideoStop); typeof ((M = a.events) != null ? M.onVideoError : void 0) === "function" && (this._events.onVideoError = a.events.onVideoError); typeof ((N = a.events) != null ? N.onVolumeChange : void 0) === "function" && (this._events.onVolumeChange = a.events.onVolumeChange); typeof ((O = a.events) != null ? O.onPlay : void 0) === "function" && (this._events.onPlay = a.events.onPlay); typeof ((P = a.events) != null ? P.onSeeked : void 0) === "function" && (this._events.onSeeked = a.events.onSeeked); typeof ((Q = a.events) != null ? Q.onSeeking : void 0)
=== "function" && (this._events.onSeeking = a.events.onSeeking); typeof ((q = a.events) != null ? q.onReplay : void 0) === "function" && (this._events.onReplay = a.events.onReplay); typeof ((r = a.events) != null ? r.onProgramDateTime : void 0) === "function" && (this._events.onProgramDateTime = a.events.onProgramDateTime); typeof ((n = a.events) != null ? n.onTimeUpdate : void 0) === "function" && (this._events.onTimeUpdate = a.events.onTimeUpdate); typeof ((t = a.events) != null ? t.onFullscreenChange : void 0) === "function"
&& (this._events.onFullscreenChange = a.events.onFullscreenChange); typeof ((u = a.events) != null ? u.onBuffering : void 0) === "function" && (this._events.onBuffering = a.events.onBuffering); typeof ((v = a.events) != null ? v.onFragChanged : void 0) === "function" && (this._events.onFragChanged = a.events.onFragChanged); typeof ((w = a.events) != null ? w.onCastConnected : void 0) === "function" && (this._events.onCastConnected = a.events.onCastConnected); typeof ((x = a.events) != null ? x.onCastDisconnected : void 0) === "function" && (this._events.onCastDisconnected = a.events.onCastDisconnected); typeof ((y = a.events) != null ? y.onCastRemoteDevicePaused : void 0) === "function" && (this._events.onCastRemoteDevicePaused = a.events.onCastRemoteDevicePaused); typeof ((z = a.events) != null ? z.oncastRemoteDevicePlaying : void 0) === "function" && (this._events.oncastRemoteDevicePlaying = a.events.oncastRemoteDevicePlaying); typeof ((B = a.events) != null ? B.onPlaylistStatusChange : void 0) === "function" && (this._events.onPlaylistStatusChange = a.events.onPlaylistStatusChange); typeof ((C = a.events) != null ? C.onChangeLocation : void 0) === "function" && (this._events.onChangeLocation = a.events.onChangeLocation); typeof ((D = a.events) != null ? D.custom : void 0) === "function" && (this._events.custom = a.events.custom); this._IFrameURLOptions = []; this._IFrameURLOptions.push("jsapi\x3dtrue"); a.loop != null && this._IFrameURLOptions.push(`loop\x3d${encodeURIComponent(a.loop)}`); a.pause_ad_on_click != null && this._IFrameURLOptions.push(`pause_ad_on_click\x3d${encodeURIComponent(a.pause_ad_on_click)}`); a.skip_ad_on_touch != null && this._IFrameURLOptions.push(`skip_ad_on_touch\x3d${
        encodeURIComponent(a.skip_ad_on_touch)}`); a.pause_on_screen_click != null && this._IFrameURLOptions.push(`pause_on_screen_click\x3d${encodeURIComponent(a.pause_on_screen_click)}`); a.autoplay != null && this._IFrameURLOptions.push(`autoplay\x3d${encodeURIComponent(a.autoplay)}`); a.pip != null && this._IFrameURLOptions.push(`pip\x3d${encodeURIComponent(a.pip)}`); a.controls != null && this._IFrameURLOptions.push(`controls\x3d${encodeURIComponent(a.controls)}`); a.useMobileNativeControls != null && this._IFrameURLOptions.push(`useMobileNativeControls\x3d${
        encodeURIComponent(a.useMobileNativeControls)}`); a.volume != null && this._IFrameURLOptions.push(`volume\x3d${encodeURIComponent(a.volume)}`); a.player && this._IFrameURLOptions.push(`player\x3d${encodeURIComponent(a.player)}`); a.skin && this._IFrameURLOptions.push(`skin\x3d${encodeURIComponent(a.skin)}`); a.ref && this._IFrameURLOptions.push(`ref\x3d${encodeURIComponent(a.ref)}`); a.startTime != null && this._IFrameURLOptions.push(`starttime\x3d${encodeURIComponent(a.startTime)}`); a.endTime != null && this._IFrameURLOptions.push(`endtime\x3d${
        encodeURIComponent(a.endTime)}`); a.title != null && this._IFrameURLOptions.push(`title\x3d${encodeURIComponent(a.title)}`); a.show_title != null && this._IFrameURLOptions.push(`show_title\x3d${encodeURIComponent(a.show_title)}`); a.show_timeline_time != null && this._IFrameURLOptions.push(`show_timeline_time\x3d${encodeURIComponent(a.show_timeline_time)}`); a.access_token != null && this._IFrameURLOptions.push(`access_token\x3d${encodeURIComponent(a.access_token)}`); a.admin_token != null && this._IFrameURLOptions.push(`admin_token\x3d${
        encodeURIComponent(a.admin_token)}`); a.acc_token != null && this._IFrameURLOptions.push(`acc_token\x3d${encodeURIComponent(a.acc_token)}`); a.source != null && this._IFrameURLOptions.push(`source\x3d${encodeURIComponent(a.source)}`); a.poster != null && this._IFrameURLOptions.push(`poster\x3d${encodeURIComponent(a.poster)}`); a.customer != null && this._IFrameURLOptions.push(`c\x3d${encodeURIComponent(a.customer)}`); a.distributor != null && this._IFrameURLOptions.push(`ds\x3d${encodeURIComponent(a.distributor)}`); a.analyticsCustom != null
&& this._IFrameURLOptions.push(`ac\x3d${encodeURIComponent(a.analyticsCustom)}`); a.rendition_rule != null && this._IFrameURLOptions.push(`rendition_rule\x3d${encodeURIComponent(a.rendition_rule)}`); a.dnt != null && this._IFrameURLOptions.push(`dnt\x3d${encodeURIComponent(a.dnt)}`); a.show_previews != null && this._IFrameURLOptions.push(`show_previews\x3d${encodeURIComponent(a.show_previews)}`); a.playlistId != null && this._IFrameURLOptions.push(`playlistId\x3d${encodeURIComponent(a.playlistId)}`); a.player_skin != null && this._IFrameURLOptions.push(`player_skin\x3d${
        encodeURIComponent(a.player_skin)}`); a.subtitles != null && this._IFrameURLOptions.push(`subtitles\x3d${encodeURIComponent(a.subtitles)}`); a.mse_buffer_length != null && this._IFrameURLOptions.push(`mse_buffer_length\x3d${encodeURIComponent(a.mse_buffer_length)}`); a.mse_buffer_size != null && this._IFrameURLOptions.push(`mse_buffer_size\x3d${encodeURIComponent(a.mse_buffer_size)}`); a.maxProfile != null && this._IFrameURLOptions.push(`max_profile\x3d${encodeURIComponent(a.maxProfile)}`); a.firstProfile != null && this._IFrameURLOptions.push(`first_profile\x3d${
        encodeURIComponent(a.firstProfile)}`); ((E = a.style) != null ? E.basecolor : void 0) != null && this._IFrameURLOptions.push(`style[basecolor]\x3d${encodeURIComponent(a.style.basecolor)}`); ((F = a.style) != null ? F.backgroundcolor : void 0) != null && this._IFrameURLOptions.push(`style[backgroundcolor]\x3d${encodeURIComponent(a.style.backgroundcolor)}`); !0 === a.mse && this._IFrameURLOptions.push("mse\x3dtrue"); !0 === a.rtsp && this._IFrameURLOptions.push("rtsp\x3dtrue"); !0 === a.debug && this._IFrameURLOptions.push("debug\x3dtrue"); !0
=== a.ima_debug && this._IFrameURLOptions.push("ima_debug\x3dtrue"); !0 === a.no_ad && this._IFrameURLOptions.push("no_ad\x3dtrue"); a.type === "dvr" && this._IFrameURLOptions.push("type\x3ddvr"); a.start != null && this._IFrameURLOptions.push(`start\x3d${(new Date(a.start)).toISOString()}`); a.end != null && this._IFrameURLOptions.push(`end\x3d${(new Date(a.end)).toISOString()}`); h = a.style || {}; for (b in h) {
        if (d = h[b], typeof d !== "object") this._IFrameURLOptions.push(`style[${b}]\x3d${encodeURIComponent(d)}`); else {
          for (g in d) {
            e = d[g],
            this._IFrameURLOptions.push(`style[${b}][${g}]\x3d${encodeURIComponent(e)}`);
          }
        }
      }e = a.ads || {}; for (b in e)d = e[b], this._IFrameURLOptions.push(`ads[${b}]\x3d${encodeURIComponent(d)}`); e = a.custom || {}; for (b in e)d = e[b], this._IFrameURLOptions.push(`custom.${b}\x3d${encodeURIComponent(d)}`); e = ((l = a.youbora) != null ? l.extra : void 0) || {}; for (b in e)d = e[b], this._IFrameURLOptions.push(`youbora[extra][${b}]\x3d${encodeURIComponent(d)}`); ((H = a.playlist) != null ? H.template : void 0) != null && this._IFrameURLOptions.push(`playlist[template]\x3d${
        encodeURIComponent(a.playlist.template)}`); ((I = a.playlist) != null ? I.showControlBarButton : void 0) != null && this._IFrameURLOptions.push(`playlist[showControlBarButton]\x3d${encodeURIComponent(a.playlist.showControlBarButton)}`); ((J = a.playlist) != null ? J.autoplay : void 0) != null && this._IFrameURLOptions.push(`playlist[autoplay]\x3d${encodeURIComponent(a.playlist.autoplay)}`); l = ((m = a.playlist) != null ? m.items : void 0) || []; for (b in l) {
        for (g in m = (d = l[b]) || {}, m) {
          e = m[g], this._IFrameURLOptions.push(`playlist[items][${b}][${
            g}]\x3d${encodeURIComponent(e)}`);
        }
      } var g = a.custom_related || []; for (b in g)d = g[b], this._IFrameURLOptions.push(`customrelated[][${b}]\x3d${encodeURIComponent(d)}`); this._IFrameURL = []; this._IFrameURL.push(`${this.protocol()}//${window.EMBED_HOST || "mdstrm.com"}/`); this._IFrameURL.push((K = a.type) === "live" || K === "dvr" ? "live-stream" : "embed"); this._IFrameURL.push("/"); this._getYouTubeID(a.id) && this._IFrameURL.push("y/"); this._IFrameURL.push(this._getYouTubeID(a.id) || a.id); this._IFrameURL.push("?"); this._IFrameURL.push(this._IFrameURLOptions.join("\x26"));
      this._playerContainer = document.getElementById(c); this._IFrameContainer = document.createElement("IFRAME"); a.className && (this._IFrameContainer.class = [].concat.apply([], [a.className]).join(" ")); this._IFrameContainer.src = this._IFrameURL.join(""); a.width && (this._IFrameContainer.width = parseInt(a.width)); a.height && (this._IFrameContainer.height = parseInt(a.height)); this._IFrameContainer.frameBorder = 0; this._IFrameContainer.setAttribute("allow", "autoplay; fullscreen; encrypted-media"); this._IFrameContainer.setAttribute("allowfullscreen",
        ""); this._IFrameContainer.setAttribute("allowscriptaccess", "always"); this._IFrameContainer.setAttribute("scrolling", "no"); this._playerContainer.innerHTML = ""; this._playerContainer.appendChild(this._IFrameContainer);

      return this._IFrameContent = this._IFrameContainer.contentWindow;
    }; b.prototype.protocol = function () { return this._protocol ? `${this._protocol}:` : /^http/i.test(location.protocol) ? location.protocol : "https:"; }; b.prototype._bindEvent = function (c, a, b) {
      if (c != null) {
        return c.addEventListener ? c.addEventListener(a,
          b, !1) : c.attachEvent(`on${a}`, b);
      }
    }; b.prototype.destroy = function () {
      this._playerContainer.innerHTML = "";

      return this.playerDestroyed = !0;
    }; b.prototype._receiveMessage = function (c) {
      let a; let b; let k; let f; let p; let q; let r; let n; let t; let u; let v; let w; let x; let y; let z; let A; let B; let C; let D; let E; let F; let l; let G; let H; let I; let J; let m; let K; let L; let M; let N; let O; let P; let Q; let d; let h; let e; let g; let S; let T; let U; let V; let W; let X; let Y; let Z; let aa; let ba; let ca; let da; let ea; let fa; let ha; let ia; let ja; let ka; let la; let ma; let na; let oa; let pa; let qa; let ra; let sa; let ta; c = c != null ? c.data : void 0; switch (c != null ? c.eventName : void 0) {
        case "onPlayerMounted": return this._isReady = !0, (f = this._events) != null ? (p = f.onPlayerMounted) != null ? p.apply(this, [(z = c.eventData) != null ? z : {}]) : void 0 : void 0; case "onPlayerReady": return this._isReady = !0, (J = this._events) != null ? (e = J.onPlayerReady) != null ? e.apply(this, [(ba = c.eventData) != null ? ba : {}]) : void 0 : void 0; case "onVideoEnd": return this._isPlaying = !1, (na = this._events) != null ? (ra = na.onVideoEnd) != null ? ra.apply(this, [(sa = c.eventData) != null ? sa : {}]) : void 0 : void 0; case "onVideoStop": return this._isPlaying = !1, (ta = this._events) != null ? (q = ta.onVideoStop) != null ? q.apply(this, [(r = c.eventData) != null ? r : {}]) : void 0 : void 0; case "onVideoError": var R = (b = c.eventData) != null ? b : {}; try { R = JSON.parse(R); } catch (va) {} this._isPlaying = !1;

          return (n = this._events) != null ? (t = n.onVideoError) != null ? t.apply(this, [R]) : void 0 : void 0; case "onVolumeChange": return (u = this._events) != null ? (v = u.onVolumeChange) != null ? v.apply(this, [(w = c.eventData) != null ? w : {}]) : void 0 : void 0; case "onPlay": return this._isPlaying = !0, (x = this._events) != null ? (y = x.onPlay) != null ? y.apply(this, [(A = c.eventData) != null ? A : {}]) : void 0 : void 0; case "onSeeked": return (B = this._events) != null ? (C = B.onSeeked) != null
          ? C.apply(this, [(D = c.eventData) != null ? D : {}]) : void 0 : void 0; case "onSeeking": return (E = this._events) != null ? (F = E.onSeeking) != null ? F.apply(this, [(l = c.eventData) != null ? l : {}]) : void 0 : void 0; case "onReplay": return (G = this._events) != null ? (H = G.onReplay) != null ? H.apply(this, [(I = c.eventData) != null ? I : {}]) : void 0 : void 0; case "onProgramDateTime": return (m = this._events) != null ? (K = m.onProgramDateTime) != null ? K.apply(this, [(L = c.eventData) != null ? L : {}]) : void 0 : void 0; case "onTimeUpdate": return (M = this._events) != null ? (N = M.onTimeUpdate)
!= null ? N.apply(this, [(O = c.eventData) != null ? O : {}]) : void 0 : void 0; case "onFullscreenChange": return (P = this._events) != null ? (Q = P.onFullscreenChange) != null ? Q.apply(this, [(d = c.eventData) != null ? d : {}]) : void 0 : void 0; case "onBuffering": return (h = this._events) != null ? (g = h.onBuffering) != null ? g.apply(this, [(S = c.eventData) != null ? S : {}]) : void 0 : void 0; case "onFragChanged": return (T = this._events) != null ? (U = T.onFragChanged) != null ? U.apply(this, [(V = c.eventData) != null ? V : {}]) : void 0 : void 0; case "onCastConnected": return (W = this._events)
!= null ? (X = W.onCastConnected) != null ? X.apply(this, [(Y = c.eventData) != null ? Y : {}]) : void 0 : void 0; case "onCastDisconnected": return (Z = this._events) != null ? (aa = Z.onCastDisconnected) != null ? aa.apply(this, [(ca = c.eventData) != null ? ca : {}]) : void 0 : void 0; case "onCastRemoteDevicePaused": return (da = this._events) != null ? (ea = da.onCastRemoteDevicePaused) != null ? ea.apply(this, [(fa = c.eventData) != null ? fa : {}]) : void 0 : void 0; case "onCastRemoteDevicePlaying": return (ha = this._events) != null ? (ia = ha.onCastRemoteDevicePlaying) != null
          ? ia.apply(this, [(ja = c.eventData) != null ? ja : {}]) : void 0 : void 0; case "onPlaylistStatusChange": return (ka = this._events) != null ? (la = ka.onPlaylistStatusChange) != null ? la.apply(this, [(ma = c.eventData) != null ? ma : {}]) : void 0 : void 0; case "onChangeLocation": return b = c.eventData, (n = this._events) != null && (R = n.onChangeLocation) != null && R.apply(this, [b != null ? b : {}]), b != null && b.parent ? window.parent.location = b.url : this._IFrameContainer.src = b.url; default: return (oa = this._events) != null && oa[c != null ? c.eventName : void 0] ? this._events[c.eventName].apply(this,
          [(pa = c.eventData) != null ? pa : {}]) : typeof (a = this._callbacks)[k = c != null ? c.eventName : void 0] === "function" ? a[k]((qa = c.eventData) != null ? qa : null) : void 0;
      }
    }; b.prototype._postMessage = function (b) {
      let a;

      return this._isReady && !this.playerDestroyed ? (a = this._IFrameContent) != null ? a.postMessage(b, `${this.protocol()}//${window.EMBED_HOST || "mdstrm.com"}`) : void 0 : this._error(["Player is not ready yet, please wait for the onPlayerReady event"]);
    }; b.prototype._getMessage = function (b, a) {
      for (var c; c == null || this._callbacks[c] != null;) {
        c = Math.random().toString(32).substr(2);
      } this._callbacks[c] = (function (b) {
        return function () {
          typeof a === "function" && a.apply(null, arguments);

          return delete b._callbacks[c];
        };
      }(this));

      return this._postMessage({ cb: c, eventName: b.action || b, data: b.data });
    }; b.prototype._getYouTubeID = function (b) {
      let a; b == null && (b = "");

      return (a = b.match(/^youtube:(.+)/i)) != null ? a[1] : void 0;
    }; b.prototype._log = function (b) {
      b == null && (b = []);

      return typeof console !== "undefined" && console !== null ? console.log(`[MDSTRM.Player] ${b.join(". ")}`) : void 0;
    };
    b.prototype._error = function (b) {
      b == null && (b = []);

      return typeof console !== "undefined" && console !== null ? console.error(`[MDSTRM.Player] ${b.join(". ")}`) : void 0;
    }; b.prototype.isReady = function () { return this._isReady && !this.playerDestroyed; }; b.prototype.isPlaying = function () { return this._isPlaying; }; b.prototype.requestAds = function (b) { return b ? (this._postMessage({ action: "requestAds", data: b }), !0) : !1; }; b.prototype.clickCustomPlaylist = function (b, a) {
      a == null && (a = function () {});

      return this._getMessage({
        action: "clickCustomPlaylist",
        data: b
      }, a);
    }; b.prototype.seekTo = function (b, a) {
      b == null && (b = 0); a == null && (a = function () {});

      return this._getMessage({ action: "seekTo", data: b }, a);
    }; b.prototype.videoPlay = function (b) {
      b == null && (b = function () {});

      return this._getMessage("videoPlay", b);
    }; b.prototype.videoStop = function (b) {
      b == null && (b = function () {});

      return this._getMessage("videoStop", b);
    }; b.prototype.getCurrentTime = function (b) { return this._getMessage("getCurrentTime", b); }; b.prototype.getVideoMetrics = function (b) {
      return this._getMessage("getVideoMetrics",
        b);
    }; b.prototype.setVolume = function (b, a) {
      a == null && (a = function () {});

      return this._getMessage({ action: "setVolume", data: b }, a);
    }; b.prototype.setSrc = function (b, a) {
      a == null && (a = function () {});

      return this._getMessage({ action: "setSrc", data: b }, a);
    }; b.prototype.setAdsVolume = function (b, a) {
      a == null && (a = function () {});

      return this._getMessage({ action: "setAdsVolume", data: b }, a);
    }; b.prototype.getDuration = function (b) { return this._getMessage("getDuration", b); }; b.prototype.getSnapshot = function (b, a, ua) {
      return this._getMessage({
        action: "getSnapshot",
        data: { height: a, width: b }
      }, ua);
    }; b.prototype.playbackLevel = function (b, a) {
      a == null && (a = function () {});

      return this._getMessage({ action: "playbackLevel", data: b }, a);
    }; b.prototype.playbackLevels = function (b) { return this._getMessage("playbackLevels", b); }; b.prototype.getPlaylist = function (b) { return this._getMessage("getPlaylist", b); }; b.prototype.toggleFullScreen = function (b) { return this._getMessage("toggleFullScreen", b); }; b.prototype.addMarker = function (b, a) {
      a == null && (a = function () {});

      return this._getMessage({
        action: "addMarker",
        data: b
      }, a);
    }; b.prototype.stopCast = function (b) {
      b == null && (b = function () {});

      return this._getMessage("stopCast", b);
    };

    return b;
  }()); window.MediastreamPlayer = b; b.prototype.isReady = b.prototype.isReady; b.prototype.isPlaying = b.prototype.isPlaying; b.prototype.requestAds = b.prototype.requestAds; b.prototype.seekTo = b.prototype.seekTo; b.prototype.videoPlay = b.prototype.videoPlay; b.prototype.videoStop = b.prototype.videoStop; b.prototype.getCurrentTime = b.prototype.getCurrentTime; b.prototype.getVideoMetrics = b.prototype.getVideoMetrics;
  b.prototype.setVolume = b.prototype.setVolume; b.prototype.setSrc = b.prototype.setSrc; b.prototype.setAdsVolume = b.prototype.setAdsVolume; b.prototype.getDuration = b.prototype.getDuration; b.prototype.getSnapshot = b.prototype.getSnapshot; b.prototype.destroy = b.prototype.destroy; b.prototype.playbackLevel = b.prototype.playbackLevel; b.prototype.playbackLevels = b.prototype.playbackLevels; b.prototype.toggleFullScreen = b.prototype.toggleFullScreen; b.prototype.getPlaylist = b.prototype.getPlaylist; b.prototype.addMarker = b.prototype.addMarker; b.prototype.clickCustomPlaylist = b.prototype.clickCustomPlaylist; b.prototype.stopCast = b.prototype.stopCast;
}).call(this);
