Default media player:
```
<section>
    <KVideoPlayerMdStrm
      mediaId="5ed538951a5483074fda0173"
      width={640}
      height={360}
      progressInterval={10000}
      onProgress={(progress)=> console.log(progress)}
      onDuration={(duration)=> console.log(duration)}
      onEnded={(ended)=> console.log(ended)}
      seek={35} 
    />
</section>
```

Default youtube player:
```
<section>
    <KVideoPlayerMdStrm
      mediaId="youtube:dQw4w9WgXcQ"
      controls={true}
      progressInterval={10000}
      onProgress={(progress)=> console.log(progress)}
      onDuration={(duration)=> console.log(duration)}
      onEnded={(ended)=> console.log(ended)}
      seek={35} />
</section>
```
