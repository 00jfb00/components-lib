import React, { Component } from "react";
import PropTypes from "prop-types";

class KImage extends Component {
  static defaultProps = {
    id: "image",
    responsive: false,
    responsiveHeight: false,
    maxWidth: undefined,
    customClass: undefined,
    maxHeight: undefined,
    onClick: () => {}
  };

  static propTypes = {
    /** ID do elemento * */
    id: PropTypes.string,
    /** Configura o caminho da imagem * */
    src: PropTypes.string.isRequired,
    /** Define a responsividade com base na largura * */
    responsive: PropTypes.bool,
    /** Define a responsividade com base na altura * */
    responsiveHeight: PropTypes.bool,
    /** Configura classes customizadas * */
    customClass: PropTypes.string,
    /** Configura definir largura máxima * */
    maxWidth: PropTypes.number,
    /** Configura definir altura máxima * */
    maxHeight: PropTypes.number,
    /** Evento de click da imagem * */
    onClick: PropTypes.func
  };

  constructor(props) {
    super(props);
    const { responsive, responsiveHeight, customClass } = this.props;
    this.state = {
      propsClasses: [
        { propClass: "is-responsive", propValue: responsive },
        { propClass: "is-responsive-height", propValue: responsiveHeight },
        { propClass: customClass, propValue: customClass }
      ]
    };
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick = evt => {
    this.props.onClick(evt);
  };

  render() {
    const {
      id, src, maxWidth, maxHeight
    } = this.props;
    const { propsClasses } = this.state;
    const customStyle = { maxWidth, maxHeight };
    let aditionalClasses = "";

    propsClasses.map(propItem => {
      if (propItem.propValue) {
        aditionalClasses += ` ${propItem.propClass}`;
      }

      return true;
    });

    return (
      <img
        id={id}
        alt=""
        src={src}
        className={`k-image${aditionalClasses}`}
        style={customStyle}
        onClick={this.handleClick}
      />
    );
  }
}

KImage.description = "Imagem padrão do projeto";
KImage.released = "v.0.1.22";
KImage.url = "Componentes/Átomos/KImage";
export default KImage;
