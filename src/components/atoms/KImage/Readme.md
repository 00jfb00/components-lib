Default:
```
<section>
    <KImage src="https://s3-us-west-2.amazonaws.com/estudante-production/app/public/ckeditor_assets/pictures/128/original/anhanguera_full.svg" />
</section>
```

Responsive Width:
```
<section>
    <KImage src="https://s3-us-west-2.amazonaws.com/estudante-production/app/public/ckeditor_assets/pictures/128/original/anhanguera_full.svg" responsive={true} />
</section>
```

Responsive Height:
```
<section style={{height:"50px"}}>
    <KImage src="https://s3-us-west-2.amazonaws.com/estudante-production/app/public/ckeditor_assets/pictures/128/original/anhanguera_full.svg" responsiveHeight={true} />
</section>
```

Custom class:
```
<section>
    <KImage src="https://s3-us-west-2.amazonaws.com/estudante-production/app/public/ckeditor_assets/pictures/128/original/anhanguera_full.svg" customClass="my-class" />
</section>
```

Max Width:
```
<section>
    <KImage src="https://s3-us-west-2.amazonaws.com/estudante-production/app/public/ckeditor_assets/pictures/128/original/anhanguera_full.svg" responsive={true} maxWidth={100} />
</section>
```

Max Height:
```
<section>
    <KImage src="https://s3-us-west-2.amazonaws.com/estudante-production/app/public/ckeditor_assets/pictures/128/original/anhanguera_full.svg" maxHeight={20} />
</section>
```
