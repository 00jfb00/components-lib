import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";
import NavLink from "react-bootstrap/NavLink";
import KIcon from "../KIcon";

class KOption extends Component {
  static defaultProps = {
    selected: false,
    disable: false,
    highlight: true,
    showBorder: true,
    size: "1x",
    state: "primary",
    brand: "default",
    radio: false,
    onChangeValue: undefined,
    block: false,
    borderColor: undefined,
    content: "",
    color: "#000000",
    contentColor: "#000000",
    ignoreState: false,
    fontSize: "p3",
    colorSelected: undefined,
    hoverBackground: undefined
  };

  static propTypes = {
    /** Conteúdo do radio* */
    content: PropTypes.any,

    /** Highlight selected content do radio* */
    highlight: PropTypes.bool,

    /** Define radio como selecionado ou não* */
    selected: PropTypes.bool,

    /** Define cor do radio* */
    color: PropTypes.string,

    /** Define cor do conteúdo do radio* */
    contentColor: PropTypes.string,

    /** Option é radio ou checkbox* */
    radio: PropTypes.bool,

    /** Define se o radio deve exibir borda* */
    showBorder: PropTypes.bool,

    /** Define a cor do da borda do radio* */
    borderColor: PropTypes.string,

    /** Radio preenche toda a linha* */
    block: PropTypes.bool,

    /** Tamanho do radio* */
    size: PropTypes.string,

    /** O estado do radio é baseado no tema. Os valores aceitos são: 'primary', 'secondary', 'ternary', 'gradient', 'white', 'black', 'dark', 'light', 'grey', 'grey-dark', 'grey-lighter', 'success', 'warning', 'info', 'danger' * */
    state: PropTypes.string,

    /** Função onChange* */
    onChangeValue: PropTypes.func,

    /** O estado do radio é baseado na marca. Os valores aceitos são: 'default', 'anhanguera' * */
    brand: PropTypes.string,

    /** O Radio é desabilitado não sendo possivel clicar para altera-lo* */
    disable: PropTypes.bool,

    /** Define o estilo da caixa de ignoreState* */
    ignoreState: PropTypes.bool,

    /** Define o tamanho da fonte do texto do KOption* */
    fontSize: PropTypes.string,
    /** Define a cor do KOption quando selecionado* */
    colorSelected: PropTypes.string,
    /** Define a cor do KOption quando estiver com hover* */
    hoverBackground: PropTypes.string

  };

  constructor(props) {
    super(props);
    this.state = {
      selected: props.selected,
      disable: props.disable
    };
    this.handleClick = this.handleClick.bind(this);
    this.getStateIcon = this.getStateIcon.bind(this);
  }

  handleClick = () => {
    if (!this.state.disable) {
      this.setState(
        prevState => ({ selected: !prevState.selected }),
        () => this.props.onChangeValue
          && this.props.onChangeValue(this.state.selected),
      );
    }
  };

  getStateIcon = () => {
    switch (this.props.state) {
      case "success":
        return "check-circle";
      case "info":
        return "info-circle";
      case "warning":
        return "exclamation-triangle";
      case "danger":
        return "times-circle";
      default:
        return "";
    }
  };

  changeBackground = e => {
    e.currentTarget.style.backgroundColor = this.props.hoverBackground;
    e.currentTarget.style.color = this.props.colorSelected;
    e.currentTarget.children[0].children[0].setAttribute("color", this.props.colorSelected);
  };

  OutBackground = e => {
    if (!this.state.selected) {
      e.currentTarget.style.backgroundColor = "transparent";
      e.currentTarget.style.color = this.props.contentColor;
      e.currentTarget.children[0].children[0].setAttribute("color", this.props.color);
    }
  };

  UNSAFE_componentWillReceiveProps(nextProps) {
    this.setState({ selected: nextProps.selected });
  }

  render() {
    const {
      id,
      content,
      highlight,
      contentColor,
      showBorder,
      borderColor,
      block,
      disable,
      color,
      size,
      state,
      brand,
      radio,
      ignoreState,
      fontSize,
      colorSelected,
      hoverBackground
    } = this.props;

    if (!ignoreState && (["success", "warning", "info", "danger"].indexOf(state) > -1)) {
      return (
        <NavLink
          id={id}
          name="navLink"
          disabled={disable}
          className={`
            k-option 
            ${block ? "is-block" : ""} 
            ${showBorder ? "has-border" : ""} 
            ${this.state.selected && !colorSelected ? `is-${state}-${brand}` : ""} 
            ${this.state.selected && highlight && !colorSelected ? "is-bold" : ""}
          `}
          onClick={this.handleClick}
          style={{
            color: this.state.selected && !colorSelected ? undefined : contentColor,
            borderColor: this.state.selected && !colorSelected ? undefined : borderColor
          }}
        >
          <div className="k-option-content">
            <Fragment>
              <KIcon
                state={!ignoreState && this.state.selected && !colorSelected ? state : undefined}
                brand={!ignoreState && this.state.selected && !colorSelected ? brand : undefined}
                icon={
                  this.state.selected
                    ? this.getStateIcon()
                    : radio ? ["far", "circle"] : ["far", "square"]
                }
                color={
                  this.state.selected && colorSelected
                    ? colorSelected
                    : color
                }
                size={size}
              />
            </Fragment>
            &nbsp;
            <div className="radio-content">
              {content}
            </div>
          </div>
        </NavLink>
      );
    }

    return (
      <NavLink
        id={id}
        disabled={disable}
        onMouseOver={hoverBackground ? this.changeBackground : undefined}
        onFocus={hoverBackground ? this.changeBackground : undefined}
        onMouseOut={hoverBackground ? this.OutBackground : undefined}
        onBlur={hoverBackground ? this.OutBackground : undefined}
        name="teste"
        className={`k-option 
          ${fontSize} 
          ${block ? "is-block" : ""} 
          ${radio ? "is-radio" : ""} 
          ${showBorder ? "has-border" : ""} 
          ${this.state.selected && !colorSelected && (!ignoreState || radio) ? `is-${radio ? "primary" : state}-${brand}` : ""} 
          ${this.state.selected && !colorSelected && highlight ? "is-bold" : ""}
        `}
        onClick={this.handleClick}
        style={{ color: this.state.selected && colorSelected ? colorSelected : contentColor, borderColor, backgroundColor: this.state.selected && hoverBackground ? hoverBackground : undefined }}
      >

        <div className="k-option-content">
          <Fragment>
            <KIcon
              id={`icon-check-${id}`}
              state={!ignoreState && this.state.selected ? state : undefined}
              brand={radio || (!ignoreState && this.state.selected) ? brand : undefined}
              icon={this.state.selected ? ["fas", radio ? "dot-circle" : "check-square"] : ["far", radio ? "circle" : "square"]}
              color={this.state.selected && colorSelected ? colorSelected : color}
              size={size}
            />
          </Fragment>
          &nbsp;
          <div className="radio-content">
            {content}
          </div>
        </div>
      </NavLink>
    );
  }
}

KOption.description = "Componente radio padrão";
KOption.released = "v.0.1.22";
KOption.url = "Componentes/Átomos/KOption";
export default KOption;
