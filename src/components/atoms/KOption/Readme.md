Conteúdo:
```
<section>
    <KOption hoverBackground={"#FFFAF4"} showBorder={false} colorSelected={"#F69120"} content="Opção 1" color={"#F69120"} contentColor={"#F69120"}/> 
    <KOption content="Opção 1" /> 
    <KOption content={<a>Opção <strong>2</strong></a>} /> 
    <KOption content="Opção 3" /> 
</section>
```

Bordas:
```
<section>
   <KOption showBorder={false} content={<a>Opção 1</a>} state="primary" /> 
   <KOption showBorder={true} state="secondary" content={<a>Opção 2</a>} />
</section>
```

Radio:
```
<section>
   <KOption radio showBorder={false} content={<a>Opção 1</a>} state="primary" /> 
   <KOption radio showBorder={true} state="secondary" content={<a>Opção 2</a>} />
</section>
```

Cores de Bordas:
```
<section>
   <KOption borderColor="red" content={<a>Opção 1</a>} state="primary" /> 
   <KOption borderColor="#999999" state="secondary" content={<a>Opção 2</a>} />
</section>
```

Highlight conteúdo selecionado:
```
<section>
   <KOption highlight={false} content={<a>Opção 1</a>} state="primary" /> 
   <KOption state="secondary" content={<a>Opção 2</a>} />
   <KOption highlight={false} content={<a>Opção 3</a>} state="success" />
   <KOption state="danger" content={<a>Opção 4</a>} />
</section>
```

Cores:
```
<section>
   <KOption color="red" content={<a>Opção 1</a>} state="primary" /> 
   <KOption color="#335212" content={<a>Opção 2</a>} state="secondary" />
</section>
```

Cores do Conteúdo:
```
<section>
   <KOption contentColor="red" content={<a>Opção 1</a>} state="primary" /> 
   <KOption contentColor="#335212" content={<a>Opção 2</a>} state="secondary" />
</section>
```

States:
```
<section>
   <KOption content={<a>Opção 1</a>} state="success" /> 
   <KOption content={<a>Opção 2</a>} state="info" />
   <KOption content={<a>Opção 3</a>} state="warning" />
   <KOption content={<a>Opção 4</a>} state="danger" />
</section>
```

Tamanhos:
```
<section>
   <KOption size="1x" />
   <KOption size="2x" />
   <KOption size="5x" />
</section>
```


Desabilitado:
```
<section>
   <KOption state="primary" content={<a>Opção 1</a>} disable/> 
   <KOption state="secondary" content={<a>Opção 2</a>} disable />
</section>
```
