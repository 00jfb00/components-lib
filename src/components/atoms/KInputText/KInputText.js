import React, {
  Component, Fragment
} from "react";
import PropTypes from "prop-types";
import KIcon from "../KIcon";

class KInputText extends Component {
  static defaultProps = {
    errorMessage: "Campo obrigatório",
    required: false,
    type: undefined,
    textTransform: "none",
    placeholder: "Digite...",
    backgroundColor: "#EEF2F6",
    borderWidth: 0,
    borderColor: "gray",
    rounded: true,
    iconSize: "1x",
    size: "md",
    defaultValue: undefined,
    color: undefined,
    leftIcon: undefined,
    onLeftIconClick: undefined,
    rightIcon: undefined,
    onRightIconClick: undefined,
    iconColor: undefined,
    onChangeValue: () => {},
    onError: () => {},
    sizeText: "p2",
    disabled: false,
    getRefInpput: () => {}
  };

  static propTypes = {
    /** Define a mesnsagem de retorno para quando o campo é obrigatório * */
    errorMessage: PropTypes.string,
    /** Dispara um evento quando o campo é obrigatório * */
    onError: PropTypes.func,
    /** Define se campo é obrigatório * */
    required: PropTypes.bool,
    /** Tipo de capitalização de texto ['none', 'capitalize', 'uppercase', 'lowercase'] * */
    textTransform: PropTypes.string,
    /** Tipo de input ['default', 'underline'] * */
    type: PropTypes.string,
    /** Placeholder para o input * */
    placeholder: PropTypes.string,
    /** Valor default para o input * */
    defaultValue: PropTypes.string,
    /** Cor do texto do input * */
    color: PropTypes.string,
    /** Input arredondado * */
    rounded: PropTypes.bool,
    /** Ícone para se exibido do lado esquerdo * */
    leftIcon: PropTypes.any,
    /** Função para o clique do icone esquerdo * */
    onLeftIconClick: PropTypes.func,
    /** Ícone para se exibido do lado direito * */
    rightIcon: PropTypes.any,
    /** Função para o clique do icone direito * */
    onRightIconClick: PropTypes.func,
    /** Cor do icone do input * */
    iconColor: PropTypes.string,
    /** Tamanho do icone do input * */
    iconSize: PropTypes.string,
    /** Placeholder para o input * */
    backgroundColor: PropTypes.string,
    /** Espessura da borda do input * */
    borderWidth: PropTypes.number,
    /** Cor da borda do input * */
    borderColor: PropTypes.string,
    /** Função onChange* */
    onChangeValue: PropTypes.func,
    /** Define o tamanho do input* */
    size: PropTypes.string,
    /** Define o tamanho das letras dentro do input* */
    sizeText: PropTypes.string,
    /** Desabilita o input* */
    disabled: PropTypes.bool,
    /** Obtém a referência do elemento input* */
    getRefInpput: PropTypes.func
  };

  constructor(props) {
    super(props);
    this.state = {
      focus: false,
      highlightBorder: false,
      value: "",
      isInputEmpty: false
    };
    this.inputElement = React.createRef();
    this.handleChange = this.handleChange.bind(this);
    this.handleLeftIconClick = this.handleLeftIconClick.bind(this);
    this.handleRightIconClick = this.handleRightIconClick.bind(this);
    this.handleFocus = this.handleFocus.bind(this);
    this.handleBlur = this.handleBlur.bind(this);
  }

  componentDidMount() {
    this.props.getRefInpput(this.inputElement);
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.state.isInputEmpty !== prevState.isInputEmpty) {
      this.props.onError(this.state.isInputEmpty);
    }
  }

  handleLeftIconClick = evt => {
    this.props.onLeftIconClick(evt);
  }

  handleRightIconClick = evt => {
    this.props.onRightIconClick(evt);
  }

  handleChange = evt => {
    this.props.onChangeValue(evt.target.value);
    this.setState({
      value: evt.target.value,
      isInputEmpty: this.props.required && evt.target.value === 0
    });
  }

  handleFocus = () => {
    if (this.props.type !== "underline") return;
    this.setState({
      focus: true,
      highlightBorder: true
    });
  }

  handleBlur = () => {
    this.setState(prevState => ({
      isInputEmpty: this.props.required && prevState.value.length === 0
    }));
    if (this.props.type !== "underline") return;
    this.setState({
      highlightBorder: false
    });
    if (this.state.value.length > 0) return;
    this.setState({
      focus: false
    });
  }

  render() {
    const {
      placeholder,
      sizeText,
      leftIcon,
      type,
      defaultValue,
      iconSize,
      borderWidth,
      iconColor,
      color,
      errorMessage,
      borderColor,
      rounded,
      rightIcon,
      backgroundColor,
      onLeftIconClick,
      onRightIconClick,
      textTransform,
      size,
      disabled
    } = this.props;

    return (
      <Fragment>
        <div
          className="k-inputtext-container"
          style={{
            backgroundColor, paddingLeft: type === "underline" ? 10 : undefined, paddingRight: type === "underline" ? 10 : undefined, borderRadius: rounded ? 20 : 0
          }}
        >
          <p className={`k-inputtext-container-underline-placeholder ${this.state.focus ? "focused" : ""}`} style={{ color: "#979797", fontSize: 11 }}>{placeholder}</p>
          <div
            style={type === "underline" ? {
              padding: 0, borderBottomWidth: this.state.isInputEmpty || this.state.highlightBorder ? 2 : borderWidth, borderBottomStyle: "solid", borderBottomColor: this.state.isInputEmpty ? "red" : this.state.highlightBorder ? "#0185CD" : borderColor
            } : {
              borderWidth: this.state.isInputEmpty ? 2 : borderWidth, borderStyle: "solid", borderColor: this.state.isInputEmpty ? "red" : borderColor, borderRadius: rounded ? 20 : 0
            }}
            className={`k-inputtext col-lg-12 ${size}`}
          >
            <div className="input-group">
              {leftIcon !== undefined && (
                <div
                  onClick={this.handleLeftIconClick}
                  className={`input-group-prepend ${onLeftIconClick !== undefined && "is-clickable"}`}
                >
                  <Fragment><KIcon color={iconColor} icon={leftIcon} size={iconSize} /></Fragment>
                </div>
              )}
              <input
                ref={this.inputElement}
                onFocus={this.handleFocus}
                onBlur={this.handleBlur}
                defaultValue={defaultValue}
                style={{
                  textTransform, backgroundColor, color, padding: type === "underline" ? 0 : undefined
                }}
                type="text"
                disabled={disabled}
                onChange={this.handleChange}
                className={`k-inputtext form-control ${sizeText}`}
                placeholder={this.state.focus ? "" : placeholder}
              />
              {rightIcon !== undefined && (
                <div
                  onClick={this.handleRightIconClick}
                  className={`input-group-append ${onRightIconClick !== undefined && "is-clickable"}`}
                >
                  <Fragment><KIcon color={iconColor} icon={rightIcon} size={iconSize} /></Fragment>
                </div>
              )}
              {this.state.isInputEmpty && (
                <div className="input-group-append error">
                  <Fragment><KIcon color="red" icon="times" size="1x" /></Fragment>
                </div>
              )}
            </div>
          </div>
        </div>
        {this.state.isInputEmpty && (
          <p style={{
            paddingLeft: 10, color: "red", fontSize: 11, marginTop: 5, width: "100%", textAlign: "left"
          }}
          >
            {errorMessage}
          </p>
        )}
      </Fragment>
    );
  }
}

KInputText.description = "Text field padrão do projeto";
KInputText.released = "v.0.1.22";
KInputText.url = "Componentes/Átomos/KInputText";
export default KInputText;
