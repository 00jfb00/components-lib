Recebendo o ref do input:
```
<section>
  <div>
    <KInputText
      placeholder=""
      getRefInpput={inputElement => console.log(inputElement.current)}
    />
  </div>
</section>
```

Estado:
```
<section>
    <div style={{ width: '300px'}}>
        <KInputText /> 
    </div>
    <div>
        <KInputText /> 
    </div>
</section>
```

Tipo:
```
<section>
    <div>
        <KInputText type="underline" borderColor='red' borderWidth={1} /> 
    </div>
</section>
```

Icones:
```
<section>
    <KInputText leftIcon='phone' /> 
    <KInputText rightIcon={['fab','github-alt']} /> 
    <KInputText leftIcon={['far','square']} rightIcon={['fab','github-alt']} /> 
</section>
```

Cores de icones:
```
<section>
    <KInputText iconColor='red' leftIcon='phone' /> 
    <KInputText iconColor='#D4B3A5' rightIcon={['fab','github-alt']} /> 
</section>
```

Required:
```
<section>
    <KInputText required /> 
    <KInputText required type="underline" /> 
</section>
```

Tamanho de icones:
```
<section>
    <KInputText iconSize='1x' rightIcon={['fab','github-alt']} /> 
    <KInputText iconSize='lg' rightIcon={['fab','github-alt']} /> 
    <KInputText iconSize='3x' rightIcon={['fab','github-alt']} /> 
</section>
```

Eventos:
```
<section>
    <KInputText onChangeValue={(value) => alert(value)} /> 
    <KInputText onRightIconClick={() => alert('Clicou no ícone direito')} rightIcon={['fab','github-alt']} /> 
    <KInputText onLeftIconClick={() => alert('Clicou no ícone esquerdo')} leftIcon={['fab','github-alt']} /> 
</section>
```

Capitalização:
```
<section>
    <KInputText textTransform="uppercase" /> 
    <KInputText textTransform="lowercase" /> 
    <KInputText textTransform="capitalize" /> 
    <KInputText textTransform="unset" /> 
</section>
```

Background:
```
<section>
    <KInputText backgroundColor='red' /> 
    <KInputText backgroundColor='#A9BC3A' /> 
</section>
```

Bordas:
```
<section>
    <KInputText borderColor='red' borderWidth={1} /> 
    <KInputText borderWidth={2} /> 
</section>
```

Cor do texto:
```
<section>
    <KInputText color='red' defaultValue='Texto' /> 
    <KInputText color='#A9BC3A' defaultValue='Texto' /> 
</section>
```

Placeholder:
```
<section>
    <KInputText placeholder='Novo placeholder' /> 
    <KInputText placeholder='Outro novo placeholder' /> 
</section>
```
