import React, {
  Component, Fragment
} from "react";
import PropTypes from "prop-types";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import KButton from "../KButton";

class KTextArea extends Component {
  static defaultProps = {
    text: undefined,
    options: undefined
  };

  static propTypes = {
    /** Define o text que irá dentro do text area */
    text: PropTypes.string,
    /** Função de clique * */
    options: PropTypes.array
  };

  constructor(props) {
    super(props);
    this.state = {
      rows: 1,
      minRows: 1,
      isMobile: false,
      text: this.props.text
    };
    this.text_area = React.createRef();
    this.handleClick = this.handleClick.bind(this);
    this.handleWindowSizeChange = this.handleWindowSizeChange.bind(this);
  }

  componentDidMount() {
    if (this.text_area && this.text_area.current && this.text_area.current.value) {
      this.setRow(~~(this.text_area.current.scrollHeight / 24));
    }
    this.setState({ isMobile: window.innerWidth < 768 });
    window.addEventListener("resize", this.handleWindowSizeChange);
  }

  componentWillUnmount() {
    this.setState({ isMobile: window.innerWidth < 768 });
    window.removeEventListener("resize", this.handleWindowSizeChange);
  }

  setRow(rows) {
    this.setState({ rows });
  }

  handleWindowSizeChange = () => {
    this.setState({ isMobile: window.innerWidth < 768 });
  };

  handleChange = event => {
    const textareaLineHeight = 24;
    const { minRows } = this.state;
    const previousRows = event.target.rows;
    event.target.rows = minRows; // reset number of rows in textarea
    let currentRows;
    if (this.state.isMobile) {
      currentRows = ((event.target.scrollHeight / 24).toPrecision(2) <= 2.3 ? 1 : (~~(event.target.scrollHeight / textareaLineHeight)));
    } else {
      currentRows = ~~(event.target.scrollHeight / textareaLineHeight) <= 1.5 ? 1 : ~~(event.target.scrollHeight / textareaLineHeight);
    }

    if (currentRows === previousRows) {
      event.target.rows = currentRows;
    }
    this.setState({
      rows: currentRows,
      text: this.text_area.current ? this.text_area.current.value : undefined
    });
  };

  handleClick = func => {
    if (this.text_area && !this.text_area.current) {
      this.text_area.current = {};
    }
    func(this.text_area.current.value);
  };

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps.text !== undefined) {
      if (this.text_area && this.text_area.current) {
        nextProps.text = this.text_area.current.value;
        this.setState({ text: this.text_area.current.value });
      }
    }
  }

  render() {
    const {
      text, id, options, placeholder
    } = this.props;

    return (
      <div className={`k-text-area ${this.state.rows === 1 ? "--one-line" : ""}`}>
        <Container className="group-text-area">
          <Row className="row-text-area">
            <span className="group-text">
              <textarea
                ref={this.text_area}
                placeholder={placeholder}
                id={`textarea-content-${id}`}
                defaultValue={text}
                onChange={this.handleChange}
                className={`text ${this.state.rows === 1 ? "--one-line" : ""}`}
                rows={this.state.rows}
              />
            </span>
            {
              options
                ? (
                  <Fragment>
                    <div className="container-buttons">
                      {
                        options.map(option => (
                          <div key={`group-button-${option.id}`} className={`${!option.border ? option.emptyLockedButton ? (this.state.text === "" || this.state.text === undefined) ? "disabled" : "" : "" : ""} group-icon ${this.state.rows > 1 ? "--more-one-line" : ""}`}>
                            <Fragment>
                              <KButton
                                size="sm"
                                id={`confirm-send-${option.id}`}
                                fontSize={option.fontSizeButton}
                                state={option.state}
                                brand={option.brand}
                                border={option.border}
                                onClick={() => this.handleClick(option.handleClickButton)}
                                outline={option.outline}
                                title={option.titleButton}
                                disabled={option.emptyLockedButton ? this.state.text === "" || this.state.text === undefined : false}
                              />
                            </Fragment>
                          </div>
                        ))
                      }
                    </div>
                  </Fragment>
                )
                : null
            }
          </Row>
        </Container>
      </div>
    );
  }
}

KTextArea.description = "Componente de TextArea";
KTextArea.released = "v.0.1.62";
KTextArea.url = "Componentes/Átomos/KTextArea";
export default KTextArea;
