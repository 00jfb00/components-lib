Default:
```
<section>
    <KTextArea text="Pois é amiga, a proposta é tentadora, né? Principalmente porque a sua mãe parece ser uma boa pessoa e merecedora  oportunidade. Mas pense que o candidato"/>
</section>
```
Com botão de enviar:
```
<section>
    <KTextArea id={1} onClick={() => alert('Mensagem enviada!')}
    options={ [ {
                    id:5,
                    titleButton: 'Cancelar',
                    handleClickButton: () => alert('item cancelado!')
                   } ]}
    />
</section>
```
Com botão de enviar e texto default:
```
<section>
    <KTextArea onClick={() => alert('Mensagem enviada!')} text="Pois é amiga, a proposta é tentadora, né? Principalmente porque a sua mãe parece ser uma boa pessoa e merecedora  oportunidade. Mas pense que o candidato"
    titleButton='Enviar'
 options={ [ {
                    id:5,
                    titleButton: 'Cancelar',
                    handleClickButton: (text) => alert(text),
                    emptyLockedButton: true,
                    border:false,
outline: true,
                   }]}/>
</section>
```
