Estados:
```
import KCard from '../KCard';
<section>
    <KBookmark show={true}>
        <KCard style={{ backgroundColor: 'white' }}>
            <div style={{ height: '20px' }}/>
        </KCard>
    </KBookmark> 
</section>
```

Marcas/State:
```
import KCard from '../KCard';
<section>
    <KBookmark show={true}>
        <KCard style={{ backgroundColor: 'white' }}>
            <div style={{ height: '20px' }}/>
        </KCard>
    </KBookmark>
    <br/>
    <KBookmark state={'primary'} show={true}>
        <KCard style={{ backgroundColor: 'white' }}>
            <div style={{ height: '20px' }}/>
        </KCard>
    </KBookmark>
    <br/>
    <KBookmark state={'primary'} brand={'anhanguera'} show={true}>
        <KCard style={{ backgroundColor: 'white' }}>
            <div style={{ height: '20px' }}/>
        </KCard>
    </KBookmark>        
    
</section>
```

Genérico:
```
import KCard from '../KCard';
import KButton from '../KButton';
<section>
    <KBookmark state={'success'} show={true}>
        <KButton title={'Lorem ipsum'} block/>
    </KBookmark>  
    <br/>
    <KBookmark state={'success'} show={true}>
        <KCard style={{ backgroundColor: 'white' }}>
            <div style={{ height: '20px' }}/>
        </KCard>
    </KBookmark>
    <br/>  
</section>
```
