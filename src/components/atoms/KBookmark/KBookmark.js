import React, { PureComponent, Fragment } from "react";
import PropTypes from "prop-types";
import KIcon from "../KIcon";

class KBookmark extends PureComponent {
  static defaultProps = {
    show: false,
    changeIcon: false,
    state: "success",
    brand: "default",
    icon: "bookmark"
  };

  static propTypes = {
    /** Um ícone é exibido no lugar da imagem padrão* */
    changeIcon: PropTypes.bool,

    /** Informa o ícone que será exibido no lugar da imagem padrão* */
    icon: PropTypes.string,

    /** Exibe o bookmark* */
    show: PropTypes.bool,

    /** O estado do bookmark é baseado no tema. Só ativado quando passar um ícone customizado. Os valores aceitos são: 'primary', 'secondary', 'ternary', 'gradient', 'white', 'black', 'dark', 'light', 'grey', 'grey-dark', 'grey-lighter', 'success', 'warning', 'info', 'danger' * */
    state: PropTypes.string,

    /** O estado do bookmark é baseado na marca. Os valores aceitos são: 'default', 'anhanguera' * */
    brand: PropTypes.string
  };

  render() {
    const {
      show, changeIcon, icon, state, brand, children, className, ...rest
    } = this.props;

    return (
      <Fragment>
        <div {...rest} className={`k-bookmark ${(className) || ""}`}>
          {children}
          <div className={`bookmark-icon ${(!show) ? "-is-hide" : ""}`}>
            <Fragment>
              {
                (changeIcon)
                  ? <KIcon brand={brand} state={state} icon={icon} size="2x" />
                  : (
                    <img
                      alt="bookmark"
                      src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSI0NS42NzIiIGhlaWdodD0iNTcuMzk4IiB2aWV3Qm94PSIwIDAgNDUuNjcyIDU3LjM5OCI+CiAgICA8ZyBpZD0iR3J1cG9fMjM5MyIgZGF0YS1uYW1lPSJHcnVwbyAyMzkzIiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgtMjg1Ljk2OCAtMTczMikiPgogICAgICAgIDxnIGlkPSJHcnVwb18yMzkyIiBkYXRhLW5hbWU9IkdydXBvIDIzOTIiPgogICAgICAgICAgICA8cGF0aCBpZD0iQ2FtaW5ob183NTUiIGRhdGEtbmFtZT0iQ2FtaW5obyA3NTUiIGQ9Ik0xNDgxNi45NjYsMTc0Mi42NzRsOC4yMjktOC42NjQsNC4yNzYsNi45MzkuMTMzLDEuNzI1WiIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoLTE0NTMwLjk5OCAtMi4wMSkiIGZpbGw9IiMwMGI0NjgiLz4KICAgICAgICAgICAgPGcgaWQ9IkdydXBvXzIzOTEiIGRhdGEtbmFtZT0iR3J1cG8gMjM5MSI+CiAgICAgICAgICAgICAgICA8cGF0aCBpZD0iQ2FtaW5ob183NTQiIGRhdGEtbmFtZT0iQ2FtaW5obyA3NTQiIGQ9Ik0wLTJIMzcuNjcyVjU1LjRMMTguODI4LDQzLjE1MSwwLDU1LjRaIiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgyOTMuOTY4IDE3MzQpIiBmaWxsPSIjMDBjNzczIi8+CiAgICAgICAgICAgICAgICA8cGF0aCBpZD0iQ2FtaW5ob183NTgiIGRhdGEtbmFtZT0iQ2FtaW5obyA3NTgiIGQ9Ik02LjQ1My0uMzE5YS45NS45NSwwLDAsMCwxLjM0MywwTDE4LjcyMi0xMS4yNDRhLjk1Ljk1LDAsMCwwLDAtMS4zNDRsLTEuMzQzLTEuMzQ0YS45NS45NSwwLDAsMC0xLjM0NCwwbC04LjkxLDguOTEtNC4xNi00LjE2YS45NS45NSwwLDAsMC0xLjM0NCwwTC4yNzgtNy44MzdhLjk1Ljk1LDAsMCwwLDAsMS4zNDRaIiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgzMDMuOTY4IDE3NjMuMzM5KSIgZmlsbD0iI2ZmZiIvPgogICAgICAgICAgICA8L2c+CiAgICAgICAgPC9nPgogICAgPC9nPgo8L3N2Zz4K"
                    />
                  )
              }
            </Fragment>
          </div>
        </div>
      </Fragment>
    );
  }
}

KBookmark.description = "Renderiza um marcador no componente filho";
KBookmark.released = "v.0.1.35";
KBookmark.url = "Componentes/Átomos/KBookmark";
export default KBookmark;
