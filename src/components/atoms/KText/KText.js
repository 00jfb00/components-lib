import React, { PureComponent } from "react";
import PropTypes from "prop-types";

class KText extends PureComponent {
  static defaultProps = {
    state: "dark",
    brand: "default",
    fontSize: "20px",
    font: "Nunito Sans",
    fontWeight: "regular",
    textColor: undefined
  };

  static propTypes = {
    /** Texto que será exibido */
    text: PropTypes.string.isRequired,
    /** Cor do texto - Será exibido dentro da tag, Color Codes:(Color Name, Hex Code RGB e Decimal Code RGB) */
    textColor: PropTypes.string,
    /** Tamanho do texto que será exibido */
    fontSize: PropTypes.string,
    /** Fonte do texto que será exibido */
    font: PropTypes.string,
    /** Especifica o peso ou a intensidade da fonte */
    fontWeight: PropTypes.string,
    /** O estado do texto é baseado no tema. Os valores aceitos são: 'primary', 'secondary', 'ternary', 'white', 'black', 'dark', 'light', 'grey', 'grey-dark', 'grey-lighter', 'success', 'warning', 'info', 'danger' * */
    state: PropTypes.string,
    /** O estado do texto é baseado na marca. Os valores aceitos são: 'default', 'anhanguera' * */
    brand: PropTypes.string
  };

  render() {
    const {
      text, textColor, fontSize, fontWeight, font, state, brand
    } = this.props;

    return (
      <span
        className={`k-text is-${state}-${brand}`}
        style={{
          color: textColor, fontSize, fontFamily: [font], fontWeight, margin: "1px"
        }}
      >
        {text}
      </span>
    );
  }
}

KText.description = "Componente de texto padrão";
KText.released = "v.0.1.23";
KText.url = "Componentes/Átomos/KText";
export default KText;
