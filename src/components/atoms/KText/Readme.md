tipos de fontes:
```
<section>
    <KText font="Nunito Sans" fontWeight="regular" text="Texto"/>
    <KText font="Open Sans" fontWeight="regular" text="Texto"/>
    <KText font="Nunito Sans" fontWeight="bold" text="Texto"/>
    <KText font="Open Sans" fontWeight="bold" text="Texto"/>
</section>
```

estados:
```
<section>
    <KText state="primary" text="Texto"/> 
    <KText state="secondary" text="Texto"/>
    <KText state="ternary" text="Texto"/>
    <KText state="success" text="Texto"/>
    <KText state="danger" text="Texto"/>
</section>
```
brand:
```
<section>
    <KText state="primary" brand="default" text="Texto"/>
    <KText state="success" brand="default" text="Texto"/> 
    <KText state="primary" brand="anhanguera" text="Texto"/>
    <KText state="secondary" brand="anhanguera" text="Texto"/>
    <KText state="ternary" brand="anhanguera" text="Texto"/>
</section>
```

tamanho das fontes:
```
<section>
    <KText fontSize="15px" text="Texto"/> 
    <KText fontSize="25px" text="Texto"/>
    <KText fontSize="40px" text="Texto"/>
    <KText fontSize="50px" text="Texto"/>
    <KText fontSize="65px" text="Texto"/>
</section>
```
cores das fontes:
```
<section>
    <KText textColor="red" fontSize="15px" font="Arial" text="Texto"/> 
    <KText textColor="green" fontSize="25px" font="Proxima Nova" text="Texto"/>
    <KText textColor="blue" fontSize="40px" font="Nunito Sans" text="Texto"/>
    <KText textColor="yellow" fontSize="50px" font="Open Sans" text="Texto"/>
    <KText textColor="black" fontSize="65px" font="fantasy" text="Texto"/>
</section>
```