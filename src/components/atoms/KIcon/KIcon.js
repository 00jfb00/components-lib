import React, { Component } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { library } from "@fortawesome/fontawesome-svg-core";
import { fas } from "@fortawesome/free-solid-svg-icons";
import { fab } from "@fortawesome/free-brands-svg-icons";
import { far } from "@fortawesome/free-regular-svg-icons";
import PropTypes from "prop-types";
import { kci } from "../../../assets/customIcons";

library.add(fas, fab, far, kci);

class KIcon extends Component {
  static defaultProps = {
    style: {},
    id: "k-icon",
    spin: false,
    state: "primary",
    brand: undefined,
    pulse: false,
    size: "1x",
    color: undefined,
    onClick: undefined,
    rotate: false,
    rotate180: false,
    transform: undefined,
    hover: undefined
  }

  static propTypes = {
    /** ID do elemento * */
    id: PropTypes.string,

    /** O estilo para o 'Icon' * */
    style: PropTypes.object,

    /** Nome do ícone FontAwesome * */
    icon: PropTypes.any.isRequired,

    /** Cor para o 'Icon' * */
    color: PropTypes.string,

    /** Ícone deve girar * */
    spin: PropTypes.bool,

    /** Ícone pulsante * */
    pulse: PropTypes.bool,

    /** Função de clique * */
    onClick: PropTypes.func,

    /** Tamanho do ícone * */
    size: PropTypes.string,

    /** Rotacionar icone em 90deg com animação * */
    rotate: PropTypes.bool,

    /** Rotacionar icone em 180deg com animação * */
    rotate180: PropTypes.bool,

    /** Parametro para aplicar funções transform no icone * */
    transform: PropTypes.any,

    /** O estado do icone é baseado no tema. Os valores aceitos são: 'primary', 'secondary', 'ternary', 'gradient', 'white', 'black', 'dark', 'light', 'grey', 'grey-dark', 'grey-lighter', 'success', 'warning', 'info', 'danger' * */
    state: PropTypes.string,

    /** O estado do icone é baseado na marca. Os valores aceitos são: 'default', 'anhanguera' * */
    brand: PropTypes.string,

    /** Evento de hover no ícone * */
    hover: PropTypes.bool
  }

  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick = () => {
    if (this.props.onClick) {
      this.props.onClick();
    }
  }

  render() {
    const {
      id,
      style,
      transform,
      icon,
      color,
      spin,
      rotate,
      rotate180,
      pulse,
      state,
      brand,
      hover,
      size,
      onClick
    } = this.props;

    const _color = color === undefined && brand === undefined ? "#707070" : color;

    return (
      <FontAwesomeIcon
        id={id}
        onClick={this.handleClick}
        transform={transform}
        className={`k-icon ${
          _color !== undefined ? "has-color" : ""
        } is-${state}-${brand} ${hover !== undefined && hover ? "is-hover" : ""} ${
          onClick !== undefined ? "is-clickable" : ""
        } ${rotate180 ? "rotate-icon-180" : ""} ${rotate ? "rotate-icon" : ""}`}
        icon={icon}
        size={size}
        color={_color}
        spin={spin}
        pulse={pulse}
        style={style}
      />
    );
  }
}

KIcon.description = "Utiliza ícones customizáveis e do fontawesome";
KIcon.released = "v.0.1.22";
KIcon.url = "Componentes/Átomos/KIcon";
export default KIcon;
