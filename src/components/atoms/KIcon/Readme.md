Size:
```
<section>
    <KIcon icon="home" size="1x" /> 
    <KIcon icon="home" size="2x" /> 
    <KIcon icon="home" size="3x" /> 
    <KIcon icon="home" size="4x" /> 
    <KIcon icon="home" size="5x" /> 
    <KIcon icon="home" size="10x" /> 
</section>
```

Eventos:
```
<section>
   <KIcon icon="phone" onClick={() => alert(1)} />
</section>
```

Spin:
```
<section>
    <KIcon icon="spinner" spin={true} /> 
    <KIcon icon="spinner" /> 
</section>
```

Pulse:
```
<section>
    <KIcon icon="square" pulse={true} /> 
    <KIcon icon="square" /> 
</section>
```

Color:
```
<section>
    <KIcon icon="circle" color={"purple"} /> 
    <KIcon icon="circle" brand={"default"} color={"gray"} /> 
    <KIcon icon="circle" color={"cyan"} /> 
    <KIcon icon="circle" brand={"anhanguera"} color={"green"} /> 
</section>
```

Marcas:
```
<section>
    <KIcon icon="circle" brand={"default"} /> 
    <KIcon icon="circle" brand={"anhanguera"} /> 
</section>
```

Estados:
```
<section>
    <KIcon icon="circle" state='success' brand={"default"} /> 
    <KIcon icon="circle" state='secondary' brand={"default"} /> 
    <KIcon icon="circle" state='danger' brand={"default"} /> 
    <KIcon icon="circle" state='warning' brand={"default"} /> 
</section>
```

Rotacionar:
```
<section>
    <KIcon icon="arrow-right" color={"purple"} /> 
    <KIcon icon="arrow-right" rotate color={"purple"} /> 
</section>
```

Fontes:
```
<section>
    <KIcon icon={["kci", "forum"]} color={"purple"} /> 
    <KIcon icon={["kci", "menu"]} color={"purple"} /> 
    <KIcon icon={["kci", "close"]} color={"purple"} /> 
    <KIcon icon={["fab", "youtube"]} color={"purple"} /> 
    <KIcon icon={["fas", "square"]} color={"purple"} /> 
    <KIcon icon={["far", "square"]} color={"purple"} /> 
    <KIcon icon="square" color={"gray"} /> 
</section>
```

Hover:
```
<section>
    <KIcon icon="phone" color={"purple"} brand={"anhanguera"} state={"primary"} hover={true} /> 
    <KIcon icon="phone" color={"purple"} state={"primary"} brand={'default'} hover={true} /> 
    <KIcon icon="phone" color={"purple"} state={"secondary"} brand={'default'} hover={true} /> 
    <KIcon icon="phone" state={"success"} brand={'default'} hover={true} /> 
    <KIcon icon="phone" state={"secondary"} brand={'default'} hover={true} /> 
</section>
```
