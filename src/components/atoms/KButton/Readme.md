Estados:
```
<section>
    <KButton state="primary" title="primary"/> 
    <KButton state="secondary" title="secondary"/>
    <KButton state="ternary" title="ternary"/>
    <KButton state="gradient" title="gradient"/>
    <KButton state="success" title="success"/>
    <KButton state="warning" title="warning"/>
    <KButton state="info" title="info"/>
    <KButton state="danger" title="danger"/>
</section>
```

Marcas:
```
<section>
    <KButton brand="anhanguera" state="primary" title="primary"/> 
    <KButton brand="anhanguera" state="secondary" title="secondary"/>
    <KButton brand="anhanguera" state="ternary" title="ternary"/>
    <KButton brand="anhanguera" state="gradient" title="gradient"/>
</section>
```

Tamanhos:
```
<section>
   <KButton title="Normal"/>
</section>
```

Block:
```
<section>
    <KButton block state="success" title="Block"/>
</section>
```

Outlined buttons:
```
<section>
    <KButton outline state="success" title="Outline"/>
</section>
```

Ícones:
```
<section>
   <KButton leftIcon="home" title="Normal"/>
   <KButton rightIcon="home" title="Normal"/>
   <KButton leftIcon="home" rightIcon="chevron-right" title="Normal"/>
</section>
```

Tamanho dos Ícones:
```
<section>
   <KButton leftIcon="home" iconSize="1x" title="Normal"/>
   <KButton rightIcon="home" iconSize="2x" title="Normal"/>
   <KButton leftIcon="home" iconSize="5x" rightIcon="chevron-right" title="Normal"/>
</section>
```

Botão para Mensageria:
```
<section style={{maxWidth:200}}>
   <KButton block leftIcon="plus" title="Nova Mensagem" noRadius="true" fontSize="p1"/>
</section>
```

Botão Header para Mensageria:
```
<section>
   <KButton iconBackground leftIcon="trash-alt" title="Descartar"  iconSize="1x"/>
</section>
```
