import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";
import Button from "react-bootstrap/Button";
import KIcon from "../KIcon";

class KButton extends Component {
  static defaultProps = {
    block: false,
    radius: undefined,
    outline: false,
    loading: false,
    menu: false,
    success: false,
    rotate: false,
    rotate180: false,
    leftIcon: undefined,
    rightIcon: undefined,
    iconSize: "1x",
    size: "medium",
    disabled: false,
    title: "Title",
    state: "primary",
    brand: "default",
    stateIcon: "white",
    brandIcon: "default",
    iconOnly: false,
    border: true,
    id: undefined,
    fontSize: "p3",
    noRadius: false,
    iconBackground: false
  };

  static propTypes = {
    /** Define o radius da borda* */
    radius: PropTypes.number,

    /** Expande o button para 100% do espaço disponível* */
    block: PropTypes.bool,

    /** Exibe apenas on ícone* */
    iconOnly: PropTypes.bool,

    /** Adiciona estilo deleniado* */
    outline: PropTypes.bool,

    /** Adciona classe do estilo do botão do menu */
    menu: PropTypes.bool,

    /** Ativa modo loading de carregamento* */
    loading: PropTypes.bool,

    /** Exibe um ícone de check no local do 'title'. Geralmente usado após o carregamento do 'loading'* */
    success: PropTypes.bool,

    /** Define o ícone FontAwesome a ser exibido ao lado esquerdo do 'title'* */
    leftIcon: PropTypes.any,

    /** Define o ícone FontAwesome a ser exibido ao lado direito do 'title'* */
    rightIcon: PropTypes.any,

    /** Define o estado do ícone* */
    stateIcon: PropTypes.string,

    /** Define um tamanho para o ícone. Tamanhos aceitos: sm, medium, lg, 1x e 2x* */
    iconSize: PropTypes.string,

    /** Define um tamanho para o botão. Tamanhos aceitos: sm, medium e lg* */
    size: PropTypes.string,

    /** Desabilita o botão* */
    disabled: PropTypes.bool,

    /** Título do botão - Será exibido dentro do botão* */
    title: PropTypes.string,

    /** O estado do botão é baseado no tema. Os valores aceitos são: 'primary', 'secondary', 'ternary', 'gradient', 'white', 'black', 'dark', 'light', 'grey', 'grey-dark', 'grey-lighter', 'success', 'warning', 'info', 'danger' * */
    state: PropTypes.string,

    /** O estado do botão é baseado na marca. Os valores aceitos são: 'default', 'anhanguera' * */
    brand: PropTypes.string,

    /** O estado do botão do ícone baseado na marca. Os valores aceitos são: 'default', 'anhanguera' * */
    brandIcon: PropTypes.string,

    /** Rotação do icone do botão em 90deg * */
    rotate: PropTypes.bool,

    /** Rotação do icone do botão em 180deg * */
    rotate180: PropTypes.bool,

    /** Define se o botão possui borda ou não * */
    border: PropTypes.bool,

    /** ID do elemento * */
    id: PropTypes.string,

    /** Define o tamanho da font do texto do botão * */
    fontSize: PropTypes.string,

    /** Define o tamanho da font do texto do botão * */
    noRadius: PropTypes.bool,

    /** Define o botão com background no icone * */
    iconBackground: PropTypes.bool

  };

  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick = () => {
    this.props.onClick();
  };

  render() {
    const {
      block,
      outline,
      loading,
      stateIcon,
      brandIcon,
      success,
      leftIcon,
      rightIcon,
      radius,
      iconSize,
      size,
      disabled,
      title,
      state,
      brand,
      iconOnly,
      rotate,
      rotate180,
      menu,
      border,
      id,
      fontSize,
      noRadius,
      iconBackground,
      ...rest
    } = this.props;

    return (
      <Fragment>
        <Button
          id={`button-${id}`}
          onClick={this.handleClick}
          className={[
            `${fontSize}`,
            "k-button",
            `is-${state}-${brand}`,
            outline ? "is-outlined" : "",
            iconBackground ? "is-iconBackground" : "",
            menu ? "is-menu" : "",
            !border ? "not-border" : "",
            disabled ? "is-disabled" : "",
            noRadius ? "not-radius" : ""
          ]}
          style={{ borderRadius: radius }}
          variant={state}
          block={block}
          disabled={disabled}
          size={size}
          {...rest}
        >
          {leftIcon !== undefined && (
            <Fragment>
              <KIcon
                id={id}
                state={stateIcon}
                brand={brandIcon}
                rotate={rotate}
                rotate180={rotate180}
                icon={leftIcon}
                size={iconSize}
                style={iconOnly ? {} : { marginRight: "10px" }}
              />
            </Fragment>
          )}
          {title}
          {rightIcon !== undefined && (
            <Fragment>
              <KIcon
                id={id}
                state={stateIcon}
                brand={brandIcon}
                icon={rightIcon}
                rotate={rotate}
                rotate180={rotate180}
                size={iconSize}
                style={iconOnly ? {} : { marginLeft: "10px" }}
              />
            </Fragment>
          )}
        </Button>
      </Fragment>
    );
  }
}

KButton.description = "Renderiza um botão de acordo com o tema";
KButton.released = "v.0.1.23";
KButton.url = "Componentes/Átomos/KButton";
export default KButton;
