Default: NOVO - nunca visualizado
```
<section>
    <KMessageList 
    isNew
    user='Nome do Usuário'
    date= '17/05'
    title= 'Titulo do email'
    />
    <KMessageList 
    isNew
    brand="anhaguera"
    user='Nome do Usuário'
    date= '17/05'
    title= 'Titulo do email'
    />
</section>
```

Já visualizado
```
<section>
    <KMessageList 
    user='Nome do Usuário'
    date= '17/05'
    title= 'Titulo do email'   
    preview='o atual status da atividade de avaliação.Prezado Aluno? o atual status da atividade de avaliação.Prezado Aluno? o atual status da atividade de avaliação.Prezado Aluno?'
    />
</section>
```

Já visualizado - com imagem do usuario
```
<section>
    <KMessageList 
    user='Nome do Usuário'
    date= '17/05'
    title= 'Titulo do email'   
    preview='o atual status da atividade de avaliação.Prezado Aluno? o atual status da atividade de avaliação.Prezado Aluno? o atual status da atividade de avaliação.Prezado Aluno?'
    src="https://content-static.upwork.com/uploads/2014/10/01073427/profilephoto1.jpg"
    />
</section>
```


Active: open no momento - com imagem do usuario
```
<section>
    <KMessageList
    isNew
    isActive
    user='Nome do Usuário'
    date= '17/05'
    title= 'Titulo do email ss ssssss ssssss'   
    src="https://content-static.upwork.com/uploads/2014/10/01073427/profilephoto1.jpg"
    onClick={() => alert(1)}
    />
</section>
```

Active: open no momento - com imagem do usuario - brand
```
<section>
    <KMessageList
    brand='anhanguera'
    isNew
    isActive
    user='Nome do Usuário'
    date= '17/05 minutos'
    title= 'Titulo do email ss ssssss ssssss'   
    src="https://content-static.upwork.com/uploads/2014/10/01073427/profilephoto1.jpg"
    onClick={() => alert(1)}
    />
</section>
```

Sem o Kavatar
```
<section>
    <KMessageList
    showImage={false}
    user='Nome do Usuário'
    date= '17/05'
    title= 'Titulo do email'
    />
</section>





