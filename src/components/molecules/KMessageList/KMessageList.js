import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";
import KAvatarMail from "../../atoms/KAvatarMail";
import KMessageriaBadge from "../../atoms/KMessageriaBadge";
import KIcon from "../../atoms/KIcon";

class KMessageList extends Component {
    static propTypes = {
      /** user nome do remetente da mensagem * */
      user: PropTypes.string.isRequired,
      /** data de envio da mensagem * */
      date: PropTypes.string.isRequired,
      /** title titulo da mensagem * */
      title: PropTypes.string,
      /** preview parte do texto da mensagem * */
      preview: PropTypes.string,
      /** isNew  true marca a mensagem como Nova (ainda não vizualizada pelo usuario) * */
      isNew: PropTypes.bool,
      /** isActive  true marca a mensagem que está sendo visualizada pelo usuario * */
      isActive: PropTypes.bool,
      /** show image true exibe a div que contém o KAvatarMail * */
      showImage: PropTypes.bool,
      /** define o tamanho do KAvatarMail * */
      width: PropTypes.string,
      /** define o tamanho do KAvatarMail * */
      height: PropTypes.string,
      /** define o tamanho do icone do KAvatarMail* */
      size: PropTypes.number,
      /** define o alinhamento do KMessageriaBadge * */
      position: PropTypes.string,
      /** define a cor do componente KMessageriaBadge * */
      bgColor: PropTypes.string,
      /** Função de clique * */
      onClick: PropTypes.func,
      /** O estado do item é baseado no tema. Os valores aceitos são: 'primary', 'secondary', 'ternary', 'gradient', 'white', 'black', 'dark', 'light', 'grey', 'grey-dark', 'grey-lighter', 'success', 'warning', 'info', 'danger' * */
      state: PropTypes.string,
      /** O estado do item é baseado na marca. Os valores aceitos são: 'default', 'anhanguera' * */
      brand: PropTypes.string,
      /** Define se tem anexos ou não * */
      hasAttachment: PropTypes.bool
    };

    static defaultProps = {
      width: "28px",
      height: "28px",
      size: 6,
      bgColor: "#F69120",
      position: "center",
      showImage: true,
      title: undefined,
      preview: undefined,
      isNew: false,
      isActive: false,
      state: "primary",
      brand: "default",
      hasAttachment: false,
      onClick: () => {}
    };

    constructor(props) {
      super(props);
      this.handleClick = this.handleClick.bind(this);
    }

    handleClick = () => {
      this.props.onClick();
    }

    render() {
      const {
        id,
        width,
        height,
        size,
        position,
        showImage,
        user,
        date,
        title,
        preview,
        isNew,
        isActive,
        bgColor,
        state,
        brand,
        outline
      } = this.props;

      return (

        <div id={`k-message-list-${id}`} className="k-message-list">
          <div
            id={`k-message-list-container-${id}`}
            className={
            `k-message-list__container 
            ${isActive ? "is_active" : ""} 
            is-${state}-${brand} 
            is-${outline}
          `}
            onClick={this.handleClick}
          >

            <div id={`k-message-list-avatar-${id}`} className="k-message-list__avatar">

              {showImage
                ? (
                  <Fragment>
                    <KAvatarMail
                      width={width}
                      height={height}
                      fullName={user}
                    />
                  </Fragment>
                )
                : null}
            </div>

            <div id={`k-message-list-content-${id}`} className="k-message-list__content">

              <h3>{user}</h3>

              <div id={`k-message-list-middle-${id}`} className="k-message-list__middle">

                <div id={`k-message-list-title-${id}`} className={`msg-title ${isNew ? "is_new" : ""}`}>{title}</div>

                {isNew
                  ? (
                    <Fragment>
                      <div id={`k-message-list-badge-${id}`} className="badge">
                        <KMessageriaBadge
                          border={false}
                          size={size}
                          bgColor={bgColor}
                        />
                      </div>
                    </Fragment>
                  )
                  : null }

              </div>

              <div id={`k-message-list-preview-${id}`} className="k-message-list__preview">

                <div id={`k-message-list-previewMessage-${id}`} className="previewMessage">{preview}</div>

              </div>

            </div>

            <div id={`k-message-list-date-${id}`} className="k-message-list__date">
              {this.props.hasAttachment && (
                <KIcon icon="paperclip" size="sm" />
              )
              }
              <h3>{date}</h3>
              {isNew
                ? (
                  <Fragment>
                    <div id={`k-message-list-badgeMobile-${id}`} className="badgeMobile">
                      <KMessageriaBadge
                        border={false}
                        position={position}
                        size={size}
                        bgColor={bgColor}
                      />
                    </div>
                  </Fragment>
                )
                : null }

            </div>
          </div>
        </div>
      );
    }
}

KMessageList.description = "componente MessageList";
KMessageList.released = "v.0.1.22";
KMessageList.url = "Componentes/Moléculas/KMessageList";

export default KMessageList;
