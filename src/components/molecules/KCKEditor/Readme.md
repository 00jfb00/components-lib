Default:
```
<section>
    <KCKEditor />
</section>
```

Com conteúdo:
```
<section>
    <KCKEditor initialContent={'<p>Teste</p>'} />
</section>
```

Evento de onChange:
```
<section>
    <KCKEditor initialContent={'<p>Teste</p>'} onChange={(content) => console.log(content)} />
</section>
```
