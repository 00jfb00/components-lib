Default:
```
<section>
    <KTreeSubItem title="Porque pensar sobre ética?" />
</section>
```

With title and indicator:
```
<section>
    <KTreeSubItem title="Porque pensar sobre ética?" indicator="1.1" />
</section>
```

With title and indicator and percentage:
```
<section style={{maxWidth:"280px"}}>
    <KTreeSubItem title="Porque pensar sobre ética?" indicator="1.1" percentage={70} />
</section>
```

With title, indicator, percentage and state:
```
<section style={{maxWidth:"280px"}}>
    <KTreeSubItem title="Porque pensar sobre ética?" state={"gradient"} indicator="1.1" percentage={70} />
</section>
```

With title, indicator, percentage and brand:
```
<section style={{maxWidth:"280px"}}>
    <KTreeSubItem title="Porque pensar sobre ética?" brand={"anhanguera"} indicator="1.1" percentage={70} />
</section>
```

With title and subtitle and active:
```
<section style={{maxWidth:"280px"}}>
    <KTreeSubItem title="Porque pensar sobre ética?" indicator="1.1" percentage={70} />
</section>
```

With title, indicator, percentage and state:
```
<section>
    <KTreeSubItem title="Porque pensar sobre ética?" state={"gradient"} indicator="1.1" percentage={70} />
</section>
```

With title, indicator, percentage and brand:
```
<section>
    <KTreeSubItem title="Porque pensar sobre ética?" brand={"anhanguera"} indicator="1.1" percentage={70} />
</section>
```

With title, subtitle and active:
```
<section>
    <KTreeSubItem 
      title="Porque pensar sobre ética?" 
      indicator="1.1" 
      active={true}
      percentage={70}
      onClick={() => {alert('Item clicado')}}
      onClickIcon={() => {alert('Ícone clicado')}} />
</section>
```

With onClick callback:
```
<section>
    <KTreeSubItem 
        title="Porque pensar sobre ética?" 
        onClick={() => {alert('Item clicado')}} />
</section>
```