import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";
import NavLink from "react-bootstrap/NavLink";
import KIcon from "../../atoms/KIcon";
import KProgressBar from "../../atoms/KProgressBar";

class KTreeSubItem extends Component {
  static defaultProps = {
    active: false,
    state: "primary",
    brand: "default",
    icon: false,
    disabled: false,
    lefticon: false,
    iconColor: "#9A9A9A",
    width: "160px",
    minWidth: "135px",
    onClick: () => {
    },
    onClickIcon: () => {
    },
    id: "k-tree-subitem",
    indicator: undefined,
    percentage: undefined
  };

  static propTypes = {
    /** Configura título do sub-item * */
    title: PropTypes.string.isRequired,
    /** Configura indicador do sub-item * */
    indicator: PropTypes.string,
    /** Configura se o sub-item está ativo * */
    active: PropTypes.bool,
    /** Define se o elemento esta inativo * */
    disabled: PropTypes.bool,
    /** Configura a ação do sub-item * */
    onClick: PropTypes.func,
    /** Configura a ação do icone a direita sub-item * */
    onClickIcon: PropTypes.func,
    /** Configura a exibição de porcentagem em um KProgressBar */
    percentage: PropTypes.number,
    /** O estado é baseado no tema. Os valores aceitos são: 'primary', 'secondary', 'ternary', 'gradient', 'white', 'black', 'dark', 'light', 'grey', 'grey-dark', 'grey-lighter', 'success', 'warning', 'info', 'danger' * */
    state: PropTypes.string,
    /** Brand é baseado na marca. Os valores aceitos são: 'default', 'anhanguera' * */
    brand: PropTypes.string,
    /** Icon configura a exibição de ícone no sub-item */
    icon: PropTypes.any,
    /** LeftIcon configura a exibição de ícone a esquerda no sub-item */
    lefticon: PropTypes.any,
    /** LeftIcon configura a exibição de ícone a esquerda no sub-item */
    iconColor: PropTypes.string,
    /** Id do componente */
    id: PropTypes.string,
    /** Tamanho da div do progress bar */
    width: PropTypes.string,
    /** Tamanho mínimo da progress bar */
    minWidth: PropTypes.string
  };

  constructor(props) {
    super(props);
    this.state = {};
  }

  handleClick = event => {
    this.props.onClick(event);
  };

  handleClickIcon = event => {
    this.props.onClickIcon(event);
  };

  render() {
    const {
      id,
      title,
      indicator,
      active,
      percentage,
      state,
      brand,
      disabled,
      icon,
      lefticon,
      iconColor,
      width,
      minWidth
    } = this.props;
    const isActive = active ? "is-active" : "";

    return (
      <NavLink
        id={id}
        disabled={disabled}
        className={`k-tree-subitem ${isActive} is-${state}-${brand}`}
        onClick={!isActive ? this.handleClick : null}
      >
        <div className="k-tree-subitem__container">
          {indicator && !lefticon && (
            <div
              className="k-tree-subitem__indicator-wrapper"
              onClick={isActive ? this.handleClick : null}
            >
              {indicator && (
                <h3 className={`is-primary-${brand} ${isActive}`}>
                  {indicator}
                </h3>
              )}
            </div>
          )}

          {lefticon && (
            <div
              className="k-tree-subitem__lefticon-wrapper"
              onClick={isActive ? this.handleClick : null}
            >
              {lefticon && (
                <Fragment>
                  <KIcon id={`${id}-lefticon`} hover icon={lefticon} color={iconColor} />
                </Fragment>
              )}
            </div>
          )}

          <div
            className="k-tree-subitem__title-wrapper"
            onClick={isActive ? this.handleClick : null}
          >
            <h1
              style={{ fontWeight: !isActive ? "normal" : "" }}
              className={`is-primary-${brand} ${isActive}`}
            >
              {title}
            </h1>
            {percentage >= 0 && (
              <div
                className="k-tree-subitem__percent-inline"
                style={{ width }}
              >
                <div
                  className="k-tree-subitem__percent-bar"
                  style={{ minWidth }}
                >
                  <Fragment>
                    <KProgressBar
                      id={`${id}-progressbar`}
                      state={state}
                      brand={brand}
                      percentage={percentage}
                    />
                  </Fragment>
                </div>
                <div className="k-tree-subitem__percent-item-text">
                  <p>
                    {percentage}
                    %
                  </p>
                </div>
              </div>
            )}
          </div>
          {icon && (
            <div
              className="k-tree-subitem__icon-wrapper"
              onClick={this.handleClickIcon}
            >
              <Fragment>
                <KIcon id={`${id}-righticon`} hover icon={icon} brand={brand} color="#7D97AD" />
              </Fragment>
            </div>
          )}
        </div>
      </NavLink>
    );
  }
}

KTreeSubItem.description = "Componente subitem para o KTreeItem";
KTreeSubItem.released = "v.0.1.22";
KTreeSubItem.url = "Componentes/Moléculas/KTreeSubItem";
export default KTreeSubItem;
