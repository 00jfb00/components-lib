import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";
import KTreeItem from "../KTreeItem";

class KTree extends Component {
  static defaultProps = {
    items: [],
    brand: "default"
  }

  static propTypes = {
    /** Opções a serem exibidas* */
    items: PropTypes.array,

    /** Marca selecionada. Os valores aceitos são: 'default', 'anhanguera' * */
    brand: PropTypes.string
  }

  constructor(props) {
    super(props);
    this.state = {
      data: props.items.map(element => {
        element.active = element.active || false;

        return element;
      })
    };
    this.handleClick = this.handleClick.bind(this);
  }

  componentDidUpdate(prevProps) {
    if (this.props.items !== prevProps.items) {
      this.setState({
        data: this.props.items.map(element => {
          element.active = element.active || false;

          return element;
        })
      });
    }
  }

  handleClick = (selectedIndex, onClick) => {
    this.setState(
      {
        data: this.props.items.map((element, index) => {
          element.active = index === selectedIndex;

          return element;
        })
      },
      () => onClick && onClick(),
    );
  }

  render() {
    const { brand } = this.props;

    return (
      <div className="k-tree">
        {this.state.data.map((element, index) => (
          <Fragment key={`ID${index}`}>
            <KTreeItem
              id={`${element.id ? element.id : `tree-item${index}`}`}
              indexID={`item${index}`}
              key={`ID${index}`}
              onClick={() => this.handleClick(index, element.onClick)}
              active={element.active}
              title={element.title}
              subitems={element.subitems}
              icon={element.icon}
              iconActive={element.iconActive}
              subtitle={element.subtitle}
              brand={brand}
            />
          </Fragment>
        ))}
      </div>
    );
  }
}

KTree.description = "Componente de árvore para menu lateral";
KTree.released = "v.0.1.22";
KTree.url = "Componentes/Moléculas/KTree";
export default KTree;
