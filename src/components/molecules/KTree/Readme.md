Estado:
```
<section>
<div style={{width: 294}}>
    <KTree
     items={[
        {
            title: 'Unidade 1',
            icon: ["fab", "youtube"],
            subitems: [{id:1, title:'Introdução e importância da atuação estratégica da área de Gestão de Pessoas', onClick: () => alert(1), indicator:'1.0', percentage:90},{id:2, title:'Porque pensar sobre ética?', indicator:'1.1', percentage:90}],
            subtitle: 'Ética e Política Brasileira'
        },
        {
            title: 'Unidade 2',
            icon: ["fab", "google"],
            subitems: [{id:1, title:'Why?', indicator:'1.0', percentage:90},{id:2, title:'Porque pensar sobre ética?', indicator:'1.1', percentage:90}],
            subtitle: 'Ética e Política Brasileira'
        },
         {
             title: 'Unidade 3',
             icon: ["fab", "apple"],
             subtitle: 'Ética e Política Brasileira',
             onClick: () => alert('Unidade 3')
         },
         {
            title:'Unidade 1' ,
            icon: ['fas','caret-down'],
            iconActive: ['fas','caret-up'],
            subtitle:'Título da Unidade em Questão, Seguindo esse Padrão',
            subitems: [
                        {
                           id:1,
                           title:'PRAZO expira em 17/07',
                           icon: ['fas','caret-down'],
                           iconActive: ['fas','caret-up'],
                           subtitle: 'Título da Sessão',
                           indicator:'1.1',
                           percentage:0, 
                            children: [
                                    {
                                    title: "Conteúdo",
                                    subtitle: "2 minutos de conteúdo",
                                    icon: ['fas', 'file-alt'],
                                    iconColor: "#HEX",
                                    indicator:'1.1',                                     
                                    onClick:()=> {alert('Conteúdo')}
                                  },{
                                    title: "Avaliação",
                                    subtitle: "Prazo ate 24/07",
                                    icon: ['fas', 'calendar'],
                                    iconColor: "#HEX",
                                    indicator:'1.1', 
                                    onClick:()=> {alert('Avaliação')}
                                  }
                            
                             ]
                        },
                        {
                            id:2,
                            title:'Porque pensar sobre ética?',
                            indicator:'1.1',
                            percentage:90,
                            onClick: () => alert('Porque pensar sobre ética?')
                        }
            ],
            subtitle: 'Ética e Política Brasileira',
        },
     ]}/>
</div>
</section>
```

Marcas:
```
<section>
    <KTree
     brand='anhanguera'
     items={[
        {
            title: 'Unidade 1 Anhanguera',
            icon: ["fab", "youtube"],
            subitems: [{id:1, title:'Why?', indicator:'1.0', percentage:90},{id:2, title:'Porque pensar sobre ética?', indicator:'1.1', percentage:90}],
            subtitle: 'Ética e Política Brasileira'
        },
        {
            title: 'Unidade 2 Anhanguera',
            icon: ["fab", "google"],
            subitems: [{id:1, title:'Why?', indicator:'1.0', percentage:90},{id:2, title:'Porque pensar sobre ética?', indicator:'1.1', percentage:90}],
            subtitle: 'Ética e Política Brasileira'
        },
         {
             title: 'Unidade 3 Anhanguera',
             icon: ["fab", "apple"],
             subtitle: 'Ética e Política Brasileira',
             onClick: () => alert('Unidade 3')
         }
     ]}/>
    <KTree
     items={[
        {
            title: 'Unidade 1 Default',
            icon: ["fab", "youtube"],
            subitems: [{id:1, title:'Why?', indicator:'1.0', percentage:90},{id:2, title:'Porque pensar sobre ética?', indicator:'1.1', percentage:90}],
            subtitle: 'Ética e Política Brasileira'
        },
        {
            title: 'Unidade 2 Default',
            icon: ["fab", "google"],
            subitems: [{id:1, title:'Why?', indicator:'1.0', percentage:90},{id:2, title:'Porque pensar sobre ética?', indicator:'1.1', percentage:90}],
            subtitle: 'Ética e Política Brasileira'
        },
         {
             title: 'Unidade 3 Default',
             icon: ["fab", "apple"],
             subtitle: 'Ética e Política Brasileira',
             onClick: () => alert('Unidade 3')
         }
     ]}/>
</section>
