import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";
import KOption from "../../atoms/KOption";

class KOptionGroup extends Component {
  static defaultProps = {
    horizontal: false,
    highlight: true,
    showBorder: true,
    size: "1x",
    id: "k-option-group",
    radio: false,
    color: undefined,
    contentColor: undefined,
    borderColor: undefined,
    brand: "default",
    onChangeValue: () => {},
    disable: false,
    ignoreState: false,
    multipleSelected: false,
    fontSize: "p3",
    listStyleType: "none"
  };

  static propTypes = {
    /** Id do component */
    id: PropTypes.string,

    /** Exibe os radios na mesma linha* */
    horizontal: PropTypes.bool,

    /** Option é radio ou checkbox* */
    radio: PropTypes.bool,

    /** Opções a serem exibidas* */
    options: PropTypes.any.isRequired,

    /** Highlight selected content do radio* */
    highlight: PropTypes.bool,

    /** Define cor do radio* */
    color: PropTypes.string,

    /** Define cor do conteúdo do radio* */
    contentColor: PropTypes.string,

    /** Define se o radio deve exibir borda* */
    showBorder: PropTypes.bool,

    /** Define a cor do da borda do radio* */
    borderColor: PropTypes.string,

    /** Tamanho do radio* */
    size: PropTypes.string,

    /** Função onChange* */
    onChangeValue: PropTypes.func,

    /** O estado do radio é baseado na marca. Os valores aceitos são: 'default', 'anhanguera' * */
    brand: PropTypes.string,

    /** O Radio é desabilitado não sendo possivel clicar para altera-lo* */
    disable: PropTypes.bool,

    /** Define se é do tipo messsage* */
    ignoreState: PropTypes.bool,

    /** Define se o option group pode selecionar mais de um option* */
    multipleSelected: PropTypes.bool,

    /** Define o tamanho de fonte* */
    fontSize: PropTypes.string,
    /** Define tipo de listagem das options */
    listStyleType: PropTypes.string
  };

  constructor(props) {
    super(props);
    this.state = {
      data: props.options.map(element => {
        element.selected = element.selected || false;

        return element;
      })
    };
    this.onChangeSelected = this.onChangeSelected.bind(this);
  }

  componentDidUpdate(prevState) {
    prevState.options.forEach((element, index) => {
      if (this.props.options[index].selected !== element.selected) {
        this.setState({
          data: this.props.options.map(element => {
            element.selected = element.selected || false;

            return element;
          })
        });
      }
    });
  }

  onChangeSelected = selectedIndex => {
    this.setState(
      {
        data: this.props.multipleSelected
          ? this.props.options.map((element, index) => {
            if (index === selectedIndex) {
              element.selected = !element.selected;
            }

            return element;
          })
          : this.props.options.map((element, index) => {
            element.selected = index === selectedIndex;

            return element;
          })
      },
      () => {
        const selectedOptions = [];
        this.state.data.map((element, index) => {
          if (element.selected) {
            selectedOptions.push(index);
          }

          return element;
        });
        if (this.props.multipleSelected) {
          this.props.onChangeValue(selectedOptions);
        } else {
          this.props.onChangeValue(selectedIndex);
        }
      }
    );
  };

  render() {
    const {
      horizontal,
      highlight,
      contentColor,
      showBorder,
      borderColor,
      color,
      size,
      radio,
      brand,
      id,
      disable,
      ignoreState,
      fontSize,
      listStyleType
    } = this.props;

    const styleOl = listStyleType === "none"
      ? { listStyleType, padding: "0" }
      : { listStyleType, padding: "0 20px" };

    return (
      <ol
        className="k-option-group"
        style={styleOl}
      >
        {this.state.data.map((element, index) => (
          <Fragment key={`ID${index}`}>
            <KOption
              id={`radio-option-${id}-${index + 1}`}
              key={`ID${index}`}
              name={
                element.id === undefined
                  ? `radio-group-radio${index}`
                  : `radio-group-radio${element.id}`
              }
              fontSize={fontSize}
              selected={element.selected}
              size={size}
              content={element.content}
              color={color}
              radio={radio}
              contentColor={contentColor}
              block={!horizontal}
              highlight={highlight}
              showBorder={showBorder}
              borderColor={borderColor}
              onChangeValue={() => this.onChangeSelected(index)}
              state={element.state}
              brand={brand}
              disable={disable}
              ignoreState={ignoreState}
            />
          </Fragment>
        ))}
      </ol>
    );
  }
}

KOptionGroup.description = "OptionGroup para gerenciar o KOption";
KOptionGroup.released = "v.0.1.22";
KOptionGroup.url = "Componentes/Moléculas/KOptionGroup";
export default KOptionGroup;
