Estados:
```
<section>
    <KOptionGroup 
        brand="anhanguera"
        listStyleType="upper-alpha"
        options={[
            {content: <a>Opção <strong>1</strong></a>},
            {content: 'b', state: 'success', selected: true},
            {content: 'c', state: 'info'},
            {content: <a>Opção <strong>2</strong></a>, state: 'warning'},
            {content: 'e', state: 'danger'},
        ]}
        onChangeValue={val => console.log(val)}
    />
</section>
```
Type message:
```
<section>
    <KOptionGroup 
        message 
        listStyleType="decimal"
        highlight={false}
        brand="anhanguera"
        showBorder={false}
        multipleSelected={true}
        options={[
            {content: <a>Opção <strong>1</strong></a>},
            {content: 'b', state: 'success'},
            {content: 'c', state: 'info'},
            {content: <a>Opção <strong>2</strong></a>, state: 'warning'},
            {content: 'e', state: 'danger'},
        ]}
        onChangeValue={val => console.log(val)}
    />
</section>
```

Direção:
```
<section>
    <KOptionGroup 
        brand="anhanguera"
        options={[
            {content: <a>Vertical <strong>1</strong></a>},
            {content: 'Vertical 2', state: 'success'},
            {content: 'Vertical 3', state: 'info'},
            {content: <a>Vertical <strong>4</strong></a>, state: 'warning'},
            {content: 'Vertical 5', state: 'danger'},
        ]} />
        <br />
    <KOptionGroup 
        brand="anhanguera"
        horizontal={false}
        options={[
            {content: <a>Vertical <strong>1</strong></a>},
            {content: 'Vertical 2', state: 'success'},
            {content: 'Vertical 3', state: 'info'},
            {content: <a>Vertical <strong>4</strong></a>, state: 'warning'},
            {content: 'Vertical 5', state: 'danger'},
        ]} />
        <br />
    <KOptionGroup 
        brand="anhanguera"
        horizontal={true}
        options={[
            {content: <a>Horizontal <strong>1</strong></a>},
            {content: 'Horizontal 2', state: 'success'},
            {content: 'Horizontal 3', state: 'info'},
            {content: <a>Horizontal <strong>4</strong></a>, state: 'warning'},
            {content: 'Horizontal 5', state: 'danger'},
        ]} />
</section>
```

Eventos:
```
<section>
   <KOptionGroup 
       brand="anhanguera"
       onChangeValue={(index) => alert(index)}
       options={[
           {content: <a>Opção <strong>1</strong></a>},
           {content: 'b', state: 'success'},
           {content: 'c', state: 'info'},
           {content: <a>Opção <strong>2</strong></a>, state: 'warning'},
           {content: 'e', state: 'danger'},
       ]} />
</section>
```

Bordas:
```
<section>
    <KOptionGroup 
       brand="anhanguera"
       showBorder={false}
       options={[
           {content: <a>Opção <strong>1</strong></a>},
           {content: 'b', state: 'success'},
           {content: 'c', state: 'info'},
           {content: <a>Opção <strong>2</strong></a>, state: 'warning'},
           {content: 'e', state: 'danger'},
       ]} />
    <KOptionGroup 
       brand="anhanguera"
       showBorder={true}
       options={[
           {content: <a>Opção <strong>1</strong></a>},
           {content: 'b', state: 'success'},
           {content: 'c', state: 'info'},
           {content: <a>Opção <strong>2</strong></a>, state: 'warning'},
           {content: 'e', state: 'danger'},
       ]} />
</section>
```

Cores de Bordas:
```
<section>
    <KOptionGroup 
       brand="anhanguera"
       borderColor="red"
       options={[
           {content: <a>Opção <strong>1</strong></a>},
           {content: 'b', state: 'success'},
           {content: 'c', state: 'info'},
           {content: <a>Opção <strong>2</strong></a>, state: 'warning'},
           {content: 'e', state: 'danger'},
       ]} />
    <KOptionGroup 
       brand="anhanguera"
       borderColor="#999999"
       options={[
           {content: <a>Opção <strong>1</strong></a>},
           {content: 'b', state: 'success'},
           {content: 'c', state: 'info'},
           {content: <a>Opção <strong>2</strong></a>, state: 'warning'},
           {content: 'e', state: 'danger'},
       ]} />
</section>
```

Highlight conteúdo selecionado:
```
<section>
    <KOptionGroup 
       brand="anhanguera"
       highlight={false}
       options={[
           {content: <a>Opção <strong>1</strong></a>},
           {content: 'b', state: 'success'},
           {content: 'c', state: 'info'},
           {content: <a>Opção <strong>2</strong></a>, state: 'warning'},
           {content: 'e', state: 'danger'},
       ]} />
    <KOptionGroup 
       brand="anhanguera"
       options={[
           {content: <a>Opção <strong>1</strong></a>},
           {content: 'b', state: 'success'},
           {content: 'c', state: 'info'},
           {content: <a>Opção <strong>2</strong></a>, state: 'warning'},
           {content: 'e', state: 'danger'},
       ]} />
</section>
```

Cores:
```
<section>
    <KOptionGroup 
       brand="anhanguera"
       color="red"
       options={[
           {content: <a>Opção <strong>1</strong></a>},
           {content: 'b', state: 'success'},
           {content: 'c', state: 'info'},
           {content: <a>Opção <strong>2</strong></a>, state: 'warning'},
           {content: 'e', state: 'danger'},
       ]} />
    <KOptionGroup 
       brand="anhanguera"
       color="#335212"
       options={[
           {content: <a>Opção <strong>1</strong></a>},
           {content: 'b', state: 'success'},
           {content: 'c', state: 'info'},
           {content: <a>Opção <strong>2</strong></a>, state: 'warning'},
           {content: 'e', state: 'danger'},
       ]} />
</section>
```

Cores do Conteúdo:
```
<section>
    <KOptionGroup 
       brand="anhanguera"
       contentColor="red"
       options={[
           {content: <a>Opção <strong>1</strong></a>},
           {content: 'b', state: 'success'},
           {content: 'c', state: 'info'},
           {content: <a>Opção <strong>2</strong></a>, state: 'warning'},
           {content: 'e', state: 'danger'},
       ]} />
    <KOptionGroup 
       brand="anhanguera"
       contentColor="#335212"
       options={[
           {content: <a>Opção <strong>1</strong></a>},
           {content: 'b', state: 'success'},
           {content: 'c', state: 'info'},
           {content: <a>Opção <strong>2</strong></a>, state: 'warning'},
           {content: 'e', state: 'danger'},
       ]} />
</section>
```

Tamanhos:
```
<section>
    <KOptionGroup 
       brand="anhanguera"
       size="1x"
       options={[
           {content: <a>Opção <strong>1</strong></a>},
           {content: 'b', state: 'success'},
           {content: 'c', state: 'info'},
           {content: <a>Opção <strong>2</strong></a>, state: 'warning'},
           {content: 'e', state: 'danger'},
       ]} />
    <KOptionGroup 
       brand="anhanguera"
       size="2x"
       options={[
           {content: <a>Opção <strong>1</strong></a>},
           {content: 'b', state: 'success'},
           {content: 'c', state: 'info'},
           {content: <a>Opção <strong>2</strong></a>, state: 'warning'},
           {content: 'e', state: 'danger'},
       ]} />
    <KOptionGroup 
       brand="anhanguera"
       size="5x"
       options={[
           {content: <a>Opção <strong>1</strong></a>},
           {content: 'b', state: 'success'},
           {content: 'c', state: 'info'},
           {content: <a>Opção <strong>2</strong></a>, state: 'warning'},
           {content: 'e', state: 'danger'},
       ]} />
</section>
```

Desabilitado para gabaritos e revisões
```
<section>
    <KOptionGroup disable
        brand="anhanguera"
        options={[
            {content: <a>Opção <strong>1</strong></a>},
            {content: 'b', state: 'success', selected: true},
            {content: 'c', state: 'info'},
            {content: <a>Opção <strong>2</strong></a>, state: 'warning'},
            {content: 'e', state: 'danger' , selected: true},
        ]} />
</section>
