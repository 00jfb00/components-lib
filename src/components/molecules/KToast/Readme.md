Estados:
```
import KButton from '../../atoms/KButton';
initialState = { 
    show1: false,
    show2: false,
    show3: false,
    show4: false,
};

<section>
    <KButton onClick={() => setState({ show1: !state.show1, show2: false, show3: false, show4: false })} title="Toast1" />
    <KButton onClick={() => setState({ show2: !state.show2, show1: false, show3: false, show4: false })} title="Toast2" />
    <KButton onClick={() => setState({ show3: !state.show3, show1: false, show2: false, show4: false })} title="Toast3" />
    <KButton onClick={() => setState({ show4: !state.show4, show1: false, show3: false, show2: false })} title="Toast4" />
    <KToast message="Toast1" onClose={() => setState({ show1: false })} show={state.show1} autoHide />
    <KToast message="Toast2" onClose={() => setState({ show2: false })} state="primary" show={state.show2} autoHide />
    <KToast message="Toast3" onClose={() => setState({ show3: false })} state="gradient" show={state.show3} autoHide />
    <KToast message="Toast4" onClose={() => setState({ show4: false })} state="primary" brand="anhanguera" show={state.show4} autoHide />
</section>
```

Ícones:
```
import KButton from '../../atoms/KButton';
initialState = { 
    show1: false,
    show2: false,
};

<section>
    <KButton onClick={() => setState({ show1: !state.show1, show2: false })} title="Toast1" />
    <KButton onClick={() => setState({ show2: !state.show2, show1: false })} title="Toast2" />
    <KToast message="Toast1" icon="home" onClose={() => setState({ show1: false })} show={state.show1} autoHide />
    <KToast message="Toast2" icon={['kci', 'forum']} onClose={() => setState({ show2: false })} state="primary" show={state.show2} autoHide />
</section>
```
