import React, {
  Component, Fragment
} from "react";
import PropTypes from "prop-types";
import KIcon from "../../atoms/KIcon";

class KToast extends Component {
  static defaultProps = {
    show: false,
    icon: "check-circle",
    state: "success",
    brand: "default",
    onClose: () => {},
    onPrintScreen: undefined,
    onCopyRequest: undefined,
    autoHide: false,
    delay: 3000
  }

  static propTypes = {
    /** Define se o toast esta visível ou não* */
    show: PropTypes.bool,

    /** Define o ícone à ser exibido* */
    icon: PropTypes.any,

    /** Mensagem do toast* */
    message: PropTypes.string.isRequired,

    /** Callback para quando o toast se fecha * */
    onClose: PropTypes.func,

    /** Callback para quando clica no icone de printscreen * */
    onPrintScreen: PropTypes.func,

    /** Callback para quando clica no icone de copia de requisição * */
    onCopyRequest: PropTypes.func,

    /** Define se o toast se fechará sozinho* */
    autoHide: PropTypes.bool,

    /** Delay para o toast se fechar caso possua autoHide=true * */
    delay: PropTypes.number,

    /** Estado do toast. Os valores aceitos são: 'primary', 'secondary', 'ternary', 'gradient', 'white', 'black', 'dark', 'light', 'grey', 'grey-dark', 'grey-lighter', 'success', 'warning', 'info', 'danger' * */
    state: PropTypes.string,

    /** Marca do toast. Os valores aceitos são: 'default', 'anhanguera' * */
    brand: PropTypes.string
  }

  constructor(props) {
    super(props);
    this.state = {
      show: props.show,
      display: props.show
    };
    this.handleClose = this.handleClose.bind(this);
    this.interval = {};
    if (props.show && props.autoHide) {
      this.interval = setTimeout(() => {
        this.handleClose();
      }, props.delay);
    }
  }

  componentDidUpdate(prevProps) {
    if (this.props !== prevProps) {
      this.setState(prevState => ({
        display: this.props.show ? true : prevState.display
      }), () => setTimeout(() => {
        this.setState({
          show: this.props.show
        },
        () => {
          if (!this.props.show) {
            clearTimeout(this.interval);
          }
          if (this.props.show && this.props.autoHide) {
            this.interval = setTimeout(() => {
              this.handleClose();
            }, this.props.delay);
          }
        });
      }, 500));
    }
  }

  handlePrintScreen = () => {
    this.props.onPrintScreen();
  }

  handleCopyRequest = () => {
    this.props.onCopyRequest();
  }

  handleClose() {
    setTimeout(() => {
      this.setState({ display: false });
    }, 1000);
    this.setState(
      {
        show: false
      },
      () => this.props.onClose(),
    );
  }

  render() {
    const {
      message, state, brand, icon, onPrintScreen, onCopyRequest
    } = this.props;
    const { show, display } = this.state;

    return (
      <>
        <Fragment>
          <div id="k-toast" className={`k-toast is-${state}-${brand}`} style={{ display: display ? "flex" : "none" }}>
            <div className={`fade toast ${show ? "show" : ""}`}>
              <div className="toast-body">
                <div className="toast-body-container">
                  <div className="toast-body-content-left">
                    <KIcon icon={icon} size="lg" />
                  </div>
                  <div className="toast-body-content">
                    <span>{message}</span>
                  </div>
                  <div className="toast-body-content-right">
                    {onCopyRequest && (
                      <Fragment>
                        <KIcon icon="copy" onClick={this.handleCopyRequest} size="1x" />
                        <span style={{ marginLeft: 10 }} />
                      </Fragment>
                    )}
                    {onPrintScreen && (
                      <Fragment>
                        <KIcon icon="camera-retro" onClick={this.handlePrintScreen} size="1x" />
                        <span style={{ marginLeft: 10 }} />
                      </Fragment>
                    )}
                    <KIcon icon="times" onClick={this.handleClose} size="1x" />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </Fragment>
      </>
    );
  }
}

KToast.description = "Component KToast";
KToast.released = "v.0.1.63";
KToast.url = "Componentes/Moléculas/KToast";
export default KToast;
