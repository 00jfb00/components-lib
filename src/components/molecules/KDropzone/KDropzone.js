import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";
import Dropzone from "react-dropzone";
import { Alert } from "react-bootstrap";
import NavLink from "react-bootstrap/NavLink";
import KIcon from "../../atoms/KIcon";

class KDropzone extends Component {
  static defaultProps = {
    accept: "",
    multiple: false,
    onSelectacceptedFiles: () => { },
    maxSize: 20000000,
    minSize: 0,
    backgroundColor: "#edf1f5",
    borderColor: "#999",
    icon: "paperclip",
    colorIcon: "#888",
    title: "Clique para selecionar",
    subTitle: "o arquivo ou arraste-o aqui!",
    subTitleBold: undefined,
    borderStyle: "dashed",
    errorBottom: false,
    messageError: undefined,
    messageAccept: undefined,
    typeTeacher: false,
    deleteFiles: false
  };

  static propTypes = {
    /** Define os tipos de arquivos permitidos, ex: ".pdf,.doc,.docx"  * */
    accept: PropTypes.string,
    /** Define a mensagem de arquivos aceitos  * */
    messageAccept: PropTypes.string,
    /** Permite seleção de multiplos arquivos * */
    multiple: PropTypes.bool,
    /** Função para obter os arquivos selecionados * */
    onSelectacceptedFiles: PropTypes.func,
    /** Define o tamanho máximo permitido do arquivo para envio (em bytes), ex: 20000000 (20mb)  * */
    maxSize: PropTypes.number,
    /** Define o tamanho mínimo permitido do arquivo para envio (em bytes), ex: 1000000 (1mb)  * */
    minSize: PropTypes.number,
    /** Define a cor de fundo do dropzone  * */
    backgroundColor: PropTypes.any,
    /** Define a cor da borda do dropzone  * */
    borderColor: PropTypes.any,
    /** Define o icone a ser exibido no dropzone  * */
    icon: PropTypes.any,
    /** Define a cor do icone a ser exibido no dropzone  * */
    colorIcon: PropTypes.any,
    /** Define a ser exibido dentro do dropzone  * */
    title: PropTypes.any,
    /** Define  o subtitulo do dropzone  * */
    subTitle: PropTypes.any,
    /** Define que o subtitulo é em negrito  * */
    subTitleBold: PropTypes.bool,
    /** Define o estilo da borda  * */
    borderStyle: PropTypes.string,
    /** Exibe mensagem de erro abaixo do dropzone */
    errorBottom: PropTypes.bool,
    /** Define a mensagem de erro quando o arquivo ultrapassar o tamanho minimo */
    messageError: PropTypes.string,
    /** Define o tipo que ira aparecer os arquivos adicionados */
    typeTeacher: PropTypes.bool,
    /** Define se os arquivos adicionados devem ser apagados */
    deleteFiles: PropTypes.bool
  };

  constructor(props) {
    super(props);
    this.state = {
      files: [],
      dragActive: "",
      overwritingFiles: false,
      invalidFiles: false
    };
  }

  onDragEnter = () => {
    this.setState({ dragActive: " is-active" });
  };

  onDragLeave = () => {
    this.setState({ dragActive: "" });
  };

  onDrop = (acceptedFiles = [], rejectedFiles = []) => {
    if (rejectedFiles.length > 0) {
      this.setState({
        invalidFiles: true,
        dragActive: ""
      });
    } else {
      this.setState({
        invalidFiles: false,
        files: acceptedFiles,
        overwritingFiles: false,
        dragActive: ""
      }, () => {
        this.props.onSelectacceptedFiles(this.state.files);
      });
    }
  };

  resetFiles = () => {
    this.setState({
      overwritingFiles: false,
      files: []
    }, () => {
      this.props.onSelectacceptedFiles(this.state.files);
    });
  };

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps.deleteFiles) {
      this.setState({ files: [] });
    }
  }

  render() {
    const {
      accept, multiple, maxSize, minSize, backgroundColor,
      borderColor, icon, colorIcon, title, subTitle,
      subTitleBold, borderStyle, errorBottom, messageAccept,
      messageError, typeTeacher
    } = this.props;
    const { files, invalidFiles, overwritingFiles } = this.state;
    const hasFiles = files.length > 0;

    return (
      <Dropzone
        onDrop={this.onDrop}
        onDragEnter={this.onDragEnter}
        onDragLeave={this.onDragLeave}
        multiple={multiple}
        accept={accept}
        maxSize={maxSize}
        minSize={minSize}
      >
        {({ getRootProps, getInputProps }) => (
          <section className="k-dropzone">
            {invalidFiles && !errorBottom && (
              <div className="k-dropzone__invalid-files">
                <Alert
                  variant="danger"
                  onClose={() => this.setState({ invalidFiles: false })}
                  dismissible
                >
                  <KIcon icon="exclamation-circle" size="1x" style={{ color: "#890512" }} />
                  {" Seleção incorreta, tipo ou tamanho de arquivo inválido, tente novamente."}
                </Alert>
              </div>
            )
            }
            {!hasFiles
              && (
                <div {...getRootProps()}>
                  <input {...getInputProps()} />
                  <div
                    className={`k-dropzone__drop-area${this.state.dragActive}`}
                    style={{
                      backgroundColor: `${backgroundColor}`,
                      borderColor: `${invalidFiles && errorBottom ? "#BC3206" : borderColor}`,
                      borderStyle: ` ${invalidFiles && errorBottom ? borderStyle : "dashed"}`
                    }}
                  >
                    <p className="k-dropzone__description">
                      <KIcon icon={icon} size="2x" style={{ color: `${colorIcon}` }} />
                      <span className={`${!subTitleBold ? "k-dropzone__highlight" : "k-dropzone__titleNoBold"} `}>{title}</span>
                      <span className={`${subTitleBold ? "k-dropzone__highlight" : ""}`}>{subTitle}</span>
                    </p>
                  </div>
                </div>
              )
            }
            {hasFiles && !typeTeacher
              && (
                <Fragment>
                  <h2 className="k-dropzone__title">A seleção atual é:</h2>
                  <div className="k-dropzone__file-list">
                    {files.map((file, index) => (
                      <div className="k-dropzone__file" key={index}>
                        <div className="k-dropzone__file-name">
                          {file.name}
                          <div className="k-dropzone__close-button">
                            {/* eslint-disable-next-line react/button-has-type */}
                            <button onClick={this.resetFiles}>
                              <KIcon icon="times" style={{ color: "#888" }} />
                            </button>
                          </div>
                        </div>
                        <NavLink className="k-dropzone__status-text" onClick={() => this.setState({ overwritingFiles: true })}>
                          Desejo substituir meu arquivo para avaliação
                        </NavLink>
                      </div>
                    ))}
                  </div>
                </Fragment>
              )
            }
            {
              hasFiles && typeTeacher && (
                <div
                  className={`k-dropzone__drop-area-file${this.state.dragActive}`}
                  style={{
                    backgroundColor: `${backgroundColor}`,
                    borderColor: `${invalidFiles && errorBottom ? "#BC3206" : borderColor}`,
                    borderStyle: ` ${invalidFiles && errorBottom ? borderStyle : "dashed"}`
                  }}
                >
                  {files.map((file, index) => (
                    <div className="k-dropzone__file-teacher" key={index}>
                      <div className="k-dropzone__file-name-teacher">
                        {file.name}
                      </div>
                      <div className="k-dropzone__close">
                        <KIcon icon="trash-alt" onClick={this.resetFiles} style={{ color: "#BC3206" }} />
                      </div>
                    </div>
                  ))}
                </div>
              )
            }
            {overwritingFiles && (
              <div {...getRootProps()}>
                <input {...getInputProps()} />
                <div className={`k-dropzone__drop-area${this.state.dragActive}`}>
                  <p className="k-dropzone__description">
                    <KIcon icon={icon} size="2x" style={{ color: `${colorIcon}` }} />
                    <strong className="k-dropzone__highlight">Clique para selecionar</strong>
                    <span>o arquivo ou arraste-o aqui!</span>
                  </p>
                </div>
              </div>
            )}
            {
              messageAccept && (
                <div className="k-dropzone__messageAccept">{messageAccept}</div>
              )
            }
            {invalidFiles && errorBottom && (
              <div className="k-dropzone__error">
                <KIcon icon="exclamation-triangle" brand="default" size="1x" style={{ color: "#BC3206" }} />
                <div className="text-error">{messageError}</div>
              </div>
            )
            }
          </section>
        )}
      </Dropzone>
    );
  }
}

KDropzone.description = "Componente de dropzone para selecionar arquivos";
KDropzone.released = "v.0.2.47";
KDropzone.url = "Componentes/Moléculas/KDropzone";
export default KDropzone;
