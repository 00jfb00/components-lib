Simples dropzone para selecionar arquivos para upload:

```
<KDropzone
        icon={"cloud-upload-alt"}
        colorIcon="#2a88d9"
        borderColor="#2a88d9"
        backgroundColor="#F6F8FA"
        title="Arraste e solte um arquivo ou"
        subTitle="clique aqui"
        subTitleBold={true}
/>
```

Exemplo de dropzone com mensagem de erro embaixo do dropzone
```
<KDropzone
        icon={"cloud-upload-alt"}
        colorIcon="#2a88d9"
        borderColor="#2a88d9"
        backgroundColor="#F6F8FA"
        title="Arraste e solte um arquivo ou"
        subTitle="clique aqui"
        subTitleBold={true}
        maxSize={15}
        borderStyle="solid"
        errorBottom
        typeTeacher
        messageAccept="PDF, PNG, JPG ou MP4 de no máximo 15MB"
/>
```

Dropzone com função para receber arquivos selecionados:

```
<KDropzone
  onSelectacceptedFiles={(acceptedFiles)=>console.log(acceptedFiles)}
/>
```
Dropzone com função multiplos arquivos selecionados:

```
<KDropzone
  multiple
  onSelectacceptedFiles={(acceptedFiles)=>console.log(acceptedFiles)}
/>
```
Dropzone com bloqueio de tipo de arquivo:

```
<KDropzone accept=".pdf,.doc,.docx" />
```
