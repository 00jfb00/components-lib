Placeholder:

```
<section>
   <KChipInput placeholder="Selecione uma sala digital"/>
</section>
```
Default:

```
<section>
   <KChipInput chips = {[
                          {
                            key: 1,
                            label: 'Chip',
                            state: 'primary',
                            icon: ['kci', 'close'],
                            colorIcon: 'primary',
                            sizeIcon: 'xs',
                            hasRadius: true,
                            outlined: true,
                            leftIcon: true,
                            onclickChip: true,
                            onDeleteChip: true,
                          },]}/>
</section>
```
Type message:

```
<section>
   <KChipInput message onClickIcon={() => alert(1)} chips = {[
                          {
                            key: 1,
                            label: 'Chip',
                            state: 'primary',
                            sizeIcon: 'xs',
                            hasRadius: true,
                            outlined: true,
                            leftIcon: true,
                            onclickChip: true,
                            onDeleteChip: true,
                          },]}/>
</section>
```
Type message disabled:

```
<section>
   <KChipInput message disabled onClickIcon={() => alert(1)} chips = {[
                          {
                            key: 1,
                            label: 'Chip',
                            state: 'primary',
                            sizeIcon: 'xs',
                            hasRadius: true,
                            outlined: true,
                            leftIcon: true,
                            onclickChip: true,
                            onDeleteChip: true,
                          },]}/>
</section>
```

Definindo número de linhas:

```
<section>
   <KChipInput rows='3' chips = {[
                          {
                            key: 1,
                            label: 'Chip',
                            state: 'primary',
                            icon: ['kci', 'close'],
                            colorIcon: 'primary',
                            sizeIcon: 'xs',
                            hasRadius: true,
                            outlined: true,
                            leftIcon: true,
                            onclickChip: true,
                            onDeleteChip: true,
                          },]}/>
</section>
```

Recebendo um texto normal como default e recebendo chips:
```
<section>
   <KChipInput placeholder='Teste de texto default'/>
    <KChipInput  chips = {[{ key: 1, fontSize: 'p3', label: 'Indiara Beltrane'},]} />
</section>
```
Input com botões dentro e fora do corpo:
```
<section>
   <KChipInput options={ [
                 {
                    id:1,
                   title: 'Cancelar',
                   border: true,
                   handleClickButton: () => alert('item cancelado!'),
                 },
                {
                     id:2,
                   title: 'Enviar',
                   border: true,
                   handleClickButton: () => alert('item Enviado!'),
                 },
               ]}
               id={1} buttonIsOff onChangeValue={(value) => console.log(value)} />
   <KChipInput options={ [
                 {
                     id:3,
                   title: 'Cancelar',
                   border: true,
                   handleClickButton: () => alert('item cancelado!'),
                 },
                {
                     id:4,
                   title: 'Enviar',
                   border: true,
                   handleClickButton: () => alert('item Enviado!'),
                 },
               ]}
               id={2} onChangeValue={(value) => console.log(value)} />
</section>
```
Input com propriedade para que o botão fique disabled quando o input estiver vazio:
```
<section>
   <KChipInput options={ [
                 {
                     id:5,
                   title: 'Cancelar',
                   border: true,
                   handleClickButton: () => alert('item cancelado!'),
                 },
                {
                     id:6,
                   title: 'Enviar',
                   emptyLockedButton: true,
                   border: true,
                   handleClickButton: () => alert('item Enviado!'),
                 },
                 {
                     id:50,
                   title: 'Cancelar',
                   border: true,
                   handleClickButton: () => alert('item cancelado!'),
                 },
               ]}
               id={1} buttonIsOff  buttonLeft onChangeValue={(value) => console.log(value)} />
   <KChipInput chips = {[
                          {
                            key: 1,
                            label: 'Indiara Beltrane'
                          }]}
            options={ [
                 {
                    id:7,
                   title: 'Cancelar',
                   border: true,
                   handleClickButton: () => alert('item cancelado!'),
                 },
                {
                    id:8,
                   title: 'Enviar',
                   emptyLockedButton: true,
                   border: true,
                   handleClickButton: () => alert('item Enviado!'),
                 },
               ]}
               id={2} onChangeValue={(value) => console.log(value)} />
</section>
```
Input com botões block outline:
```
<section>
   <KChipInput options={ [
                 {
                     id:5,
                   title: 'Cancelar',
                   border: true,
                    block: true,
                    outline: true,
                   handleClickButton: () => alert('item cancelado!'),
                 },
                {
                     id:30,
                   title: 'Enviar',
                   emptyLockedButton: true,
                   border: true,
                    block: true,
                   handleClickButton: () => alert('item Enviado!'),
                 },
                   {
                     id:40,
                   title: 'Enviar',
                   emptyLockedButton: true,
                   border: false,
                    block: true,
                   handleClickButton: () => alert('item Enviado!'),
                 },
               ]}
               id={1} buttonIsOff onChangeValue={(value) => console.log(value)} />
</section>
```
