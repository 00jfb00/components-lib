import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";
import { Container, Row } from "react-bootstrap";
import KIcon from "../../atoms/KIcon";
import KButton from "../../atoms/KButton";
import KChip from "../../atoms/KChip";

class KChipInput extends Component {
  static defaultProps = {
    chips: [],
    options: undefined,
    id: undefined,
    buttonIsOff: false,
    buttonLeft: false,
    fontSize: "p3",
    defaultValue: undefined,
    rows: "1",
    message: false,
    disabled: false,
    deleteCallback: undefined,
    onChangeValue: undefined,
    placeholder: undefined,
    getRefInpput: () => {}
  };

  static propTypes = {
    /** Define o array de chips que serão exibidos dentro do input */
    chips: PropTypes.array,
    /** Define o valor que está sendo inserido no input */
    defaultValue: PropTypes.string,
    /** Define o array de botões disponíveis no input */
    options: PropTypes.array,
    /** Define o id do input */
    id: PropTypes.any,
    /** Define se o botão está fora do input ou não * */
    buttonIsOff: PropTypes.bool,
    /** Define a posição onde os botões vão ficar. Esquerda ou Direita * */
    buttonLeft: PropTypes.bool,
    /** Define o tamanho de font do texto que vai dentro do input * */
    fontSize: PropTypes.string,
    /** Define a quantidade de linhas do input * */
    rows: PropTypes.string,
    /** Define se o componente é do tipo de mensagem e aí redefine ele para este layout * */
    message: PropTypes.bool,
    /** Define se o componente do tipo de mensagem é desativado * */
    disabled: PropTypes.bool,
    /** Funçaõ de retorno do novo array ao executar o delete Chip * */
    deleteCallback: PropTypes.func,
    /** Funçaõ de retorno do valor do chipinput * */
    onChangeValue: PropTypes.func,
    /** Define o placeholder do kchipinput* */
    placeholder: PropTypes.string,
    /** Obtém a referência do elemento input* */
    getRefInpput: PropTypes.func
  };

  constructor(props) {
    super(props);
    this.state = {
      chips: this.props.chips,
      defaultValue: this.props.defaultValue,
      options: this.props.options,
      oneLine: true
    };
    this.input = React.createRef();
    this.container = React.createRef();
  }

  componentDidMount() {
    if (this.container.current) {
      this.setState({ oneLine: this.container.current.offsetHeight > 51 });
    }
    this.props.getRefInpput(this.input);
  }

  handleChange = evt => {
    this.setState({
      defaultValue: evt.target.innerText,
      oneLine: this.input.current.offsetHeight === 19
    });
    if (this.props.onChangeValue) {
      this.props.onChangeValue(`<p>${evt.target.innerText}</p>`);
    }
  };

  onClickDeleteChip = chipToDelete => {
    this.setState(prevState => ({
      chips: prevState.chips.filter(chip => chip.key !== chipToDelete.key)
    }));
    if (this.props.deleteCallback) {
      this.props.deleteCallback(
        this.state.chips.filter(chip => chip.key !== chipToDelete.key)
      );
    }
  };

  onKeyDown = evt => {
    if (
      evt.keyCode === 8
      && (this.state.defaultValue === "" || this.state.defaultValue === undefined)
    ) {
      this.setState(prevState => ({ chips: prevState.chips.slice(0, -1) }));
    }
  };

  handleClick = func => {
    func(this.state.defaultValue);
  };

  handleClickDelete = func => {
    if (func)func(this.state.defaultValue);
    this.input.current.innerText = "";
    this.setState({ defaultValue: "" });
  };

  handleClickIcon = () => {
    this.props.onClickIcon();
  };

  UNSAFE_componentWillReceiveProps(nextProps) {
    this.setState({ chips: nextProps.chips, defaultValue: this.props.defaultValue });
  }

  render() {
    const { chips, options, oneLine } = this.state;
    const {
      id,
      buttonIsOff,
      buttonLeft,
      fontSize,
      rows,
      message,
      disabled,
      placeholder
    } = this.props;

    return (
      <Container
        id={`container-input-${id}`}
        className={`k-chip-input ${!buttonIsOff ? "buttonIsOff" : ""}`}
      >
        <Row
          id={`group-input-${id}`}
          ref={this.container}
          className={`content-editable ${buttonIsOff ? "buttonIsOff" : ""}`}
        >
          <div
            contentEditable={!message}
            style={{ minHeight: `${rows * 24}px` }}
            id={`label-input-${id}`}
            onKeyDown={this.onKeyDown}
            value={this.props.defaultValue}
            placeholder={placeholder}
            onInput={this.handleChange}
            ref={this.input}
            suppressContentEditableWarning
            className={`col-text ${fontSize} ${oneLine ? "more-one-line" : ""} ${message ? "type-message" : "message"}`}
          >
            {chips.length > 0
              ? chips.map(chip => (
                <span
                  id={`group-chips-${chip.key}`}
                  key={`group-chips-${chip.key}`}
                  className="group-chip"
                  contentEditable="false"
                  suppressContentEditableWarning
                >
                  <Fragment>
                    <KChip
                      contentEditable="false"
                      suppressContentEditableWarning
                      ClickChip={
                        message ? () => this.onClickDeleteChip(chip) : null
                      }
                      key={`chips-${chip.key}`}
                      id={`${chip.key}`}
                      fontSize={chip.fontSize}
                      chipText={chip.label}
                      colorText={chip.colorText}
                      colorBorder={chip.colorBorder}
                      state={chip.state}
                      icon={chip.icon}
                      colorIcon={chip.colorIcon}
                      leftIcon={chip.leftIcon}
                      sizeIcon={chip.sizeIcon}
                      hasRadius={chip.hasRadius}
                      outlined={chip.outlined}
                      onClick={
                        chip.onDeleteChip
                          ? () => this.onClickDeleteChip(chip)
                          : null
                      }
                    />
                  </Fragment>
                </span>
              ))
              : null}
          </div>
          {message ? (
            <div
              id="container-icon"
              className={`col-button ${
                !this.state.oneLine ? "icon-bottom" : ""
              } ${disabled ? "icon-disabled" : ""}`}
              onClick={() => (!disabled ? this.handleClickIcon() : null)}
            >
              <KIcon icon="user-plus" size="1x" style={{ color: "#0185CD" }} />
            </div>
          ) : null}
        </Row>
        {options ? (
          <Container
            id={`container-buttons-${id}`}
            className={`container-buttons ${
              buttonLeft ? "buttonLeft" : "buttonRight"
            }`}
          >
            <Row
              id={`row-buttons-${id}`}
              className={`row-button ${
                buttonLeft ? "buttonLeft" : "buttonRight"
              }`}
            >
              {options.map(option => (
                <div
                  id={`group-buttons-${option.id}`}
                  key={`group-buttons-${option.id}`}
                  className={`group-buttons ${
                    option.block
                      ? "block"
                      : option.emptyLockedButton
                        ? (this.state.defaultValue === "" && chips.length === 0
                          ? "disabled"
                          : "")
                        || (this.state.defaultValue === undefined
                        && chips.length === 0
                          ? "disabled"
                          : "")
                        : false
                  }`}
                >
                  <Fragment>
                    <KButton
                      id={`option-${option.id}`}
                      disabled={
                        option.emptyLockedButton
                          ? (this.state.defaultValue === ""
                              && chips.length === 0)
                            || (this.state.defaultValue === undefined
                              && chips.length === 0)
                          : false
                      }
                      outline={option.outline}
                      block={option.block}
                      fontSize={option.fontSizeButton}
                      state={option.state}
                      brand={option.brand}
                      title={option.title}
                      border={option.border}
                      onClick={() => (option.delete
                        ? this.handleClickDelete(option.handleClickButton, this.props.id)
                        : this.handleClick(option.handleClickButton))
                      }
                    />
                  </Fragment>
                </div>
              ))}
            </Row>
          </Container>
        ) : null}
      </Container>
    );
  }
}

KChipInput.description = "Componente de input que pode conter chips";
KChipInput.released = "v.0.1.72";
KChipInput.url = "Componentes/Moléculas/KChipInput";
export default KChipInput;
