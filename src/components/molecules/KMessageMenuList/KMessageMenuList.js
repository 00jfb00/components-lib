import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";
import ListGroup from "react-bootstrap/ListGroup";
import KDropdown from "../KDropdown";
import KIcon from "../../atoms/KIcon";

class KMessageMenuList extends Component {
  static defaultProps = {
    list: [],
    funcClick: () => {},
    id: "k-menu-list",
    title: "Filtrar: ",
    rightIcon: "chevron-down",
    iconSize: "1x",
    state: "primary",
    brand: "default"
  };

  static propTypes = {
    /** Id que será exibido na div * */
    id: PropTypes.string,
    /** Nome a ser exibido no botão do menu * */
    title: PropTypes.string,
    /** Lista que será exibido no menu * */
    list: PropTypes.any,
    /** Função de callback do click * */
    funcClick: PropTypes.func,
    /** Icone a ser utilizada * */
    rightIcon: PropTypes.string,
    /** Tamanho do Icone a ser utilizada * */
    iconSize: PropTypes.string,
    /** O estado do item é baseado no tema. Os valores aceitos são: 'primary', 'secondary', 'ternary', 'gradient', 'white', 'black', 'dark', 'light', 'grey', 'grey-dark', 'grey-lighter', 'success', 'warning', 'info', 'danger' * */
    state: PropTypes.string,
    /** O estado do item é baseado na marca. Os valores aceitos são: 'default', 'anhanguera' * */
    brand: PropTypes.string
  };

  constructor(props) {
    super(props);
    this.state = {
      click: undefined,
      index: 0
    };
    this.handleClick = this.handleClick.bind(this);
    this.handleFuncClick = this.handleFuncClick.bind(this);
  }

  handleClick = () => {
    this.setState(prevState => ({ click: !prevState.click }));
  };

  handleFuncClick = index => {
    this.setState({ index }, () => this.props.funcClick(index));
  };

  render() {
    const {
      id, title, list, brand, rightIcon, iconSize, state
    } = this.props;

    return (
      <div id={id} className="k-menu-list">
        <Fragment>
          <KDropdown
            contentPositionLeft="auto"
            styleContent={{ width: "90px", fontSize: "12px" }}
            changeProps={this.handleClick}
            triggerComponent={(
              <div
                className={`k-filter_button
                  is-${state}-${brand}`}
                onClick={this.handleClick}
              >
                <div className="k-filter_button__mobile">
                  <span>
                    {title}
                  </span>
                </div>
                <div className="k-filter_button__text">
                  <span>
                    <b>{title}</b>
                    {" "}
                    {list[this.state.index]}
                  </span>
                </div>
                <div className="k-filter_button__icon">
                  <Fragment>
                    <KIcon
                      icon={rightIcon}
                      rotate180={this.state.click}
                      size={iconSize}
                      brand={brand}
                      style={{ marginLeft: "10px", marginTop: "5px" }}
                    />
                  </Fragment>
                </div>
              </div>
            )}
          >
            {" "}
            <ListGroup>
              <div style={{ textAlign: "center" }}>
                <div style={{
                  backgroundColor: "#FFF",
                  zIndex: 10,
                  content: "",
                  position: "absolute",
                  left: "50px",
                  top: "-4px",
                  width: "8px",
                  height: "8px",
                  borderLeft: "1px solid rgba(10, 10, 10, 0.125)",
                  borderBottom: "1px solid rgba(10, 10, 10, 0.125)",
                  transform: "rotate(135deg)"
                }}
                />
                {list.map((element, index) => (
                  <ListGroup.Item
                    key={`${id}-${index}`}
                    id={`${id}-${index}`}
                    action
                    onClick={() => this.handleFuncClick(index)}
                    style={
                    index === 0
                      ? { borderRadius: "10px 10px 0px 0px", padding: "0.35rem 0.25rem" }
                      : index === list.length - 1
                        ? { borderRadius: "0px 0px 10px 10px", padding: "0.35rem 0.25rem" }
                        : { padding: "0.35rem 0.25rem" }
                  }
                  >
                    {element}
                  </ListGroup.Item>
                ))}
              </div>
            </ListGroup>
          </KDropdown>
        </Fragment>
      </div>
    );
  }
}

KMessageMenuList.description = "Componente Message menu list";
KMessageMenuList.released = "v.0.1.60";
KMessageMenuList.url = "Componentes/Moléculas/KMessageMenuList";
export default KMessageMenuList;

