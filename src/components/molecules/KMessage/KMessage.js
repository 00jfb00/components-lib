import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";
import KAvatarMail from "../../atoms/KAvatarMail";
import KChip from "../../atoms/KChip";

class KMessage extends Component {
  static defaultProps = {
    id: "k-mail-message",
    title: "",
    author: "",
    discipline: "",
    date: "",
    message: <div />,
    onClick: undefined,
    listAttachments: []
  };

  static propTypes = {
    /** Id que será exibido na div * */
    id: PropTypes.string,
    /** Nome a ser exibido no botão do menu * */
    title: PropTypes.string,
    /** Autor da mensagem * */
    author: PropTypes.string,
    /** Disciplina da origim da mensagem * */
    discipline: PropTypes.string,
    /** Data da origim da mensagem * */
    date: PropTypes.string,
    message: PropTypes.element,
    /** Função de clique sobre o chip * */
    onClick: PropTypes.func,
    /** Define o array de chips * */
    listAttachments: PropTypes.array
  };

  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const {
      id,
      title,
      author,
      discipline,
      date,
      message,
      listAttachments
    } = this.props;

    return (
      <div id={id} className="k-mail-message">
        <Fragment>
          <div className="k-mail-message-header">
            <p className="k-mail-message-header-title h4 is-primary-default">
              {title}
            </p>
            <div className="k-mail-message-header-info">
              { author && (
                <div className="k-mail-message-header-info-avatar">
                  <KAvatarMail fullName={author} width="20px" height="20px" fontSize="9px" />
                  <p className="k-mail-message-header-info-avatar-author is-primary-default p4">
                    {author}
                  </p>
                </div>
              )}
              <div className="k-mail-message-header-info-disciplina p4">
                <b className="p4">Disciplina:</b>
                {discipline}
              </div>
              <div className="k-mail-message-header-info-date p4">{date}</div>
            </div>
          </div>
          <div className="k-mail-message-divider">
            <div className="upload">
              {
              listAttachments && (listAttachments.map((attachment, index) => (
                <KChip
                  key={index}
                  outlined
                  state="primary"
                  chipText={attachment.fileName}
                  onClick={this.props.onClick}
                  subText={attachment.size && (`${attachment.size} bytes`)}
                  icon="download"
                  colorIcon="primary"
                  sizeIcon="sm"
                />
              ))
              )
            }
            </div>
            <hr />
          </div>
          <div className="k-mail-message-content">
            <p className="content">
              <span
                dangerouslySetInnerHTML={{ __html: `<span class="p2">${message}</span>` }}
              />
            </p>
          </div>
        </Fragment>
      </div>
    );
  }
}

KMessage.description = "Componemte de visualização de mensagem";
KMessage.released = "v.0.1.87";
KMessage.url = "Componentes/Moléculas/KMessage";
export default KMessage;
