Mail View:

```
<section>
    <KMessage
        title={"Sejam bem-vindos ao Estudos Dirigidos!"}
        src={`https://api.adorable.io/avatars/225/abost${new Date()}@addrable.png`}
        author={"Hal Kemp Ferreira"}
        discipline={"Construindo uma Carreira de Sucesso: Saúde (ED_CSS)"}
        date={"feira, 35 de janeiro de 2, 25:72"}
        message={"<div>deve aceitar html aqui....<ul><li>test</li><li>test</li></ul></div>"}
    />
</section>
```
Mail View:

```
<section>
    <KMessage
        onClick={()=> console.log("ola")}
        listAttachments={[{_id: "5eb177555537670012445b91", fileName: "274eron%20(13)%20(1).pdf", status: "success"},{_id: "5eb177555537670012445b91", fileName: "teste2.pdf", status: "success"}]}
        title={"Sejam bem-vindos ao Estudos Dirigidos!"}
        src={`https://api.adorable.io/avatars/225/abost${new Date()}@addrable.png`}
        author={"Hal Kemp Ferreira"}
        discipline={"Construindo uma Carreira de Sucesso: Saúde (ED_CSS)"}
        date={"feira, 35 de janeiro de 2, 25:72"}
        message={"<div>deve aceitar html aqui....<ul><li>test</li><li>test</li></ul></div>"}
    />
</section>
```
