Default:
```
import KButton from '../../atoms/KButton';
import KTree from '../KTree';

initialState = { isOpen: false };
    
<section>
    <div style={{ width: '100%', height: '100vh', position: 'relative' }}>
        <KDrawer toDrawerRight showDrawer={state.isOpen} checkClosed={() => {
            setState({isOpen: false})
        }}>
            <KTree
                 items={[
                    {
                        title: 'Unidade 1',
                        icon: ["fab", "youtube"],
                        subitems: [{id:1, title:'Why?', onClick: () => alert(1), indicator:'1.0', percentage:90},{id:2, title:'Porque pensar sobre ética?', indicator:'1.1', percentage:90}],
                        subtitle: 'Ética e Política Brasileira'
                    },
                    {
                        title: 'Unidade 2',
                        icon: ["fab", "google"],
                        subitems: [{id:1, title:'Why?', indicator:'1.0', percentage:90},{id:2, title:'Porque pensar sobre ética?', indicator:'1.1', percentage:90}],
                        subtitle: 'Ética e Política Brasileira'
                    },
                     {
                         title: 'Unidade 3',
                         icon: ["fab", "apple"],
                         subtitle: 'Ética e Política Brasileira',
                         onClick: () => alert('Unidade 3')
                     }
                 ]}/>
        </KDrawer> 
        <KButton state="primary" title="Exibir Drawer" onClick={() => {
            setState({isOpen: true})
        }}/> 
    </div>
</section>
```
