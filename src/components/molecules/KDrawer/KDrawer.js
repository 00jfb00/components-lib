import React, { Component } from "react";
import PropTypes from "prop-types";

class KDrawer extends Component {
  static defaultProps = {
    showDrawer: false,
    toDrawerRight: false,
    onlyMobile: false,
    width: 300,
    state: "primary",
    brand: "default"
  };

  static propTypes = {
    /** Define um id após o nome definido do componente* */
    id: PropTypes.string.isRequired,
    /** Exibe/Oculta o menu* */
    showDrawer: PropTypes.bool,
    /** Exibe apenas para sistemas mobile* */
    onlyMobile: PropTypes.bool,
    /** Ativa o drawer na direita da tela* */
    toDrawerRight: PropTypes.bool,
    /** Função callback para fechar o drawer* */
    checkClosed: PropTypes.func.isRequired,
    /** Largura padrão do drawer* */
    width: PropTypes.number,
    /** Marca do drawer* */
    brand: PropTypes.string,
    /** Estado do drawer* */
    state: PropTypes.string
  };

  constructor(props) {
    super(props);
    this.state = {
      isMobile: false
    };
    this.handleClick = this.handleClick.bind(this);
    this.handleWindowSizeChange = this.handleWindowSizeChange.bind(this);
  }

  componentDidMount() {
    this.setState({ isMobile: window.innerWidth <= 992 });
    window.addEventListener("resize", this.handleWindowSizeChange);
  }

  componentWillUnmount() {
    this.setState({ isMobile: window.innerWidth <= 992 });
    window.removeEventListener("resize", this.handleWindowSizeChange);
  }

  handleWindowSizeChange = () => {
    this.setState({ isMobile: window.innerWidth <= 992 });
  };

  handleClick = e => {
    if (e.target.id === `k-drawer-${this.props.id}`) this.props.checkClosed();
  };

  render() {
    const {
      id,
      children,
      toDrawerRight,
      showDrawer,
      onlyMobile,
      width,
      checkClosed,
      state,
      brand,
      ...rest
    } = this.props;
    const { isMobile } = this.state;

    return (
      <div
        id={`k-drawer-${id}`}
        className={`
          k-drawer
          ${toDrawerRight ? "-to-right" : "-to-left"}
          ${showDrawer && (onlyMobile && isMobile) ? "-is-showDrawer" : ""}
          ${showDrawer && !onlyMobile ? "-is-showDrawer" : ""}
        `}
        onClick={this.handleClick}
        {...rest}
      >
        <div
          className={`
              is-${state}-${brand} k-drawer-container 
              ${toDrawerRight ? "-to-right" : "-to-left"}
              ${!showDrawer && isMobile ? "-is-hide" : ""}
            `}
          id="k-drawer-container"
          style={{ width }}
        >
          <div className="k-drawer-content" id="k-drawer-content">
            {children}
          </div>
        </div>
      </div>
    );
  }
}

KDrawer.description = "Componente de drawer responsivo";
KDrawer.released = "v.0.1.22";
KDrawer.url = "Componentes/Moléculas/KDrawer";
KDrawer.element = "KDrawer";
export default KDrawer;
