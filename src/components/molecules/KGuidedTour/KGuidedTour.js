import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import Tour from "reactour";
import { disableBodyScroll, enableBodyScroll } from "body-scroll-lock";
import NavLink from "react-bootstrap/NavLink";
import KIcon from "../../atoms/KIcon";

class KGuidedTour extends PureComponent {
  static defaultProps = {
    isTourOpen: false,
    hideStepIndicator: false,
    hidePrevButton: false,
    closeButtonText: "Concluir",
    rounded: 0,
    steps: [],
    onClose: () => {},
    onOpen: () => {},
    onTourConcluded: () => {},
    onChange: () => {}
  };

  static propTypes = {
    isTourOpen: PropTypes.bool,
    hideStepIndicator: PropTypes.bool,
    hidePrevButton: PropTypes.bool,
    closeButtonText: PropTypes.string,
    rounded: PropTypes.number,
    steps: PropTypes.array,
    onClose: PropTypes.func,
    onOpen: PropTypes.func,
    onTourConcluded: PropTypes.func,
    onChange: PropTypes.func
  };

  state = {
    steps: [],
    active: 0
  };

  componentDidMount() {
    this.setState({
      steps: this.props.steps.map((step, index) => {
        step = {
          position: "bottom",
          ...step,
          content: ({ goTo }) => (
            <div className="kguided-tour-float-baloon-container">
              {!step.hideCloseButton && (
                <NavLink data-hideacess="true" onClick={this.handleClose} id={`kguided-tour-float-baloon-close-btn-${index}`} className="kguided-tour-float-baloon-close-btn-container">
                  <KIcon icon="times" color="#FFF" />
                </NavLink>
              )}
              <p className="kguided-tour-float-baloon-title">{step.title}</p>
              <p className="kguided-tour-float-baloon-description">{step.description}</p>
              <div className="kguided-tour-float-baloon-close-footer-container">
                {!this.props.hideStepIndicator && (
                  <div className="kguided-tour-float-baloon-step-indicator">
                    {index + 1}
                    /
                    {this.props.steps.length}
                  </div>
                )}
                <div className="kguided-tour-float-baloon-controls-container">
                  {!this.props.hidePrevButton && (
                    <NavLink data-hideacess="true" className={`kguided-tour-float-baloon-prev-btn ${index === 0 ? "disabled" : ""}`} onClick={() => (index > 0 ? this.handleGoTo(index - 1, goTo) : undefined)}>Anterior</NavLink>
                  )}
                  <NavLink data-hideacess="true" className="kguided-tour-float-baloon-next-btn" onClick={() => (index < (this.props.steps.length - 1) ? this.handleGoTo(index + 1, goTo) : this.handleClose())}>{index === (this.props.steps.length - 1) ? this.props.closeButtonText : "Próximo"}</NavLink>
                </div>
              </div>
            </div>
          )
        };

        return step;
      })
    });
  }

  handleGoTo = (index, goTo) => {
    this.props.onChange(index);
    this.setState({
      active: index
    });
    goTo(index);
  };

  handleClose = evt => {
    this.props.onClose(evt);
    if (this.state.active === this.props.steps.length - 1) {
      this.handleConcludedTour();
    }
    this.setState({ active: 0 }, this.forceUpdate);
  };

  handleConcludedTour = evt => {
    this.props.onTourConcluded(evt);
  };

  handleOpen = evt => {
    this.props.onOpen(evt);
  };

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps.isTourOpen && !this.props.isTourOpen) {
      this.handleOpen();
    }
  }

  render() {
    return (
      <Tour
        rounded={this.props.rounded}
        showButtons={false}
        startAt={0}
        className="kguided-tour-float-baloon"
        disableInteraction
        showNavigationNumber={false}
        showCloseButton={false}
        showNumber={false}
        scrollOffset={-100}
        showNavigation={false}
        maskSpace={0}
        steps={this.state.steps}
        isOpen={this.props.isTourOpen}
        onRequestClose={this.handleClose}
        onAfterOpen={disableBodyScroll}
        onBeforeClose={enableBodyScroll}
        disableKeyboardNavigation={ ["right", "left" ] }
      />
    );
  }
}

KGuidedTour.description = "Componente de modal";
KGuidedTour.released = "v.0.2.99";
KGuidedTour.url = "Componentes/Moléculas/KGuidedTour";
export default KGuidedTour;
