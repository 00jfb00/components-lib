Default: Recebe o conteudo por children

```
import KGuidedTour from '../../molecules/KGuidedTour';
import KButton from '../../atoms/KButton';

initialState = { isOpen: false };

<section>
    <KButton title="Abra o tour" onClick={() => setState({ isOpen: true })} />
    <KGuidedTour
      rounded={10}
      steps={[
        {
          selector: ".rsg--sidebar-4",
          title: "Titulo",
          position: "right",
          description: "Em \"Disciplinas\" você poderá ver a listagem de suas disciplinas e iniciar seus estudos clicando em Acessar."
        },
        {
          selector: ".k-button",
          title: "Titulo 2",
          position: "bottom",
          hideCloseButton: true,
          description: <span>Descrição <strong>2</strong></span>
        }
      ]}
      isTourOpen={state.isOpen}
      onTourConcluded={() => alert("Tour concluído")}
      onChange={index => alert(index)}
      onOpen={() => alert("Tour iniciado")}
      onClose={() => setState({ isOpen: false })}
    />
</section>
```
