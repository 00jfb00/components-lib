import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";
import KIcon from "../../atoms/KIcon";
import KProgressBar from "../../atoms/KProgressBar";

class KRatingBar extends Component {
  static propTypes = {

    /** Pontos maximo na disciplina * */
    maxPoints: PropTypes.number,

    /** Pontos minimo a ser alçado pelo aluno para aprovação* */
    miniPoints: PropTypes.number,

    /** Pontos obtido pelo aluno * */
    studentPoints: PropTypes.number,

    /** cor da Progress Bar de acordo com brand do aluno * */
    brand: PropTypes.string

  };

  static defaultProps = {
    maxPoints: 0,
    miniPoints: 0,
    studentPoints: 0,
    brand: "default"
  };

  constructor(props) {
    super(props);
    this.state = {
      round: true
    };
  }

  getIndicator = () => {
    const { maxPoints, miniPoints, studentPoints } = this.props;

    let indicatorMini = Math.round((miniPoints * 100) / maxPoints);

    let total = 0;
    let successIcon = false;
    let active = false;
    let studentMaxPoints = studentPoints;
    let state = false;
    let display = true;

    if (studentPoints && studentPoints > 0) {
      total = Math.round((studentPoints * 100) / maxPoints);
    }

    if (maxPoints > 0) {
      active = true;
    }

    if (studentPoints >= miniPoints && studentPoints !== 0) {
      successIcon = true;
      state = true;
    }

    if (studentPoints > maxPoints) {
      studentMaxPoints = maxPoints;
    }

    if (miniPoints > maxPoints) {
      indicatorMini = 0;
    }

    if (miniPoints < 1) {
      display = false;
    }

    this.setState({
      indicator: indicatorMini,
      alunoProgressBar: total,
      successIcon,
      active,
      studentPoints: studentMaxPoints,
      state,
      display
    });
  };

  UNSAFE_componentWillMount() {
    this.getIndicator();
  }

  render() {
    const { maxPoints, miniPoints, brand } = this.props;

    const {
      alunoProgressBar,
      successIcon,
      active,
      studentPoints,
      indicator,
      round,
      state,
      display
    } = this.state;

    return (

      <div className="k-rating-bar wrap">

        <div className="k-rating-bar__circle">
          <div className="k-rating-bar__icon">

            <Fragment>
              {miniPoints > 0 && (
              <KIcon
                icon={successIcon ? "unlock" : "lock"}
                color={active ? "#000000" : "#C9C9C9"}
                size="1x"
              />
              )}
            </Fragment>
            <p>{studentPoints}</p>

          </div>
        </div>

        <div className="k-rating-bar__progress">
          <div
            className={display ? "k-rating-bar__indicator" : "k-rating-bar__none"}
            style={{ left: `${indicator}%` }}
          />
          <Fragment>
            <KProgressBar
              round={round}
              percentage={alunoProgressBar}
              state={state ? "success" : "primary"}
              brand={brand}
            />
          </Fragment>
          {miniPoints > 0 ? (
            <p>{`Vale ${maxPoints} pontos (mínimo ${miniPoints} pontos)`}</p>
          ) : (
            <p>{`Vale ${maxPoints} pontos`}</p>
          )}
        </div>

      </div>

    );
  }
}

KRatingBar.description = "Teste de componente";
KRatingBar.released = "v.0.1.1";
KRatingBar.url = "Componentes/Moléculas/KRatingBar";

export default KRatingBar;
