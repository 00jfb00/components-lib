Default:
```
<section>
  <KRatingBar />
</section>
```

Rating Bar Ativo - Necessário os  três parametros: miniPoints:(número minimo de pontos para obter a aprovação), maxPoints:(número máximo de pontos possiveis na disciplina), studentPoints(pontos obtidos pelo estudante)
```
<section>
  <KRatingBar
    miniPoints={2500}
    maxPoints={5000}
    studentPoints={1000}
  />
</section>

<section>
  <KRatingBar
    miniPoints={2500}
    maxPoints={5000}
    studentPoints={1000}
    brand='anhanguera'
  /> 
</section>

```
Rating Bar Aprovado
```
<section>
    <KRatingBar
    miniPoints={8000} 
    maxPoints={14000} 
    studentPoints={9000} />
</section>

```
Rating Bar  - Se o número de pontos do Estudante (studentPoints) for maior que o número  máximo de pontos (maxPoints) - é apresentado para o Estudante o numero máximo de Pontos.
```
<section>
  <KRatingBar
    miniPoints={8000}
    maxPoints={14000}
    studentPoints={19000}
    state="success"
  />
</section>

```
Rating Bar - Se o maximo dos pontos (maxPoints) for igual a zero - O grafico é apresentado com o estado desativado.
```
<section>
  <KRatingBar 
    miniPoints={1500}
    maxPoints={5000}
    studentPoints={1350}
  />
</section>

```

Rating Bar  - Sem miniPoints não terá indicador e indicador de lock
```
<section>
    <KRatingBar
      maxPoints={2500}
      studentPoints={2000}
    />
</section>

```
