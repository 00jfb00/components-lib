import React, { Component } from "react";
import PropTypes from "prop-types";
import ReactDOM from "react-dom";
import KImage from "../../atoms/KImage";
import KIcon from "../../atoms/KIcon";

class KThumbnail extends Component {
  static defaultProps = {
    src:
      "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAACVBMVEW6sq+8tLG4sK3Rd1v+AAACF0lEQVR4nO3c606DQBRFYeD9H1onpkYNQhnOmTl7s74/mrSls8AOl2CX5WX7tKRYm5xFF2EfWMYz/pa8C4F32X8YDD7uR+NXb/tCob4HFHY/CABXrMvMSWXEYdPcwhGHvpMLB/AvbPwLAQAAAAAAAMzSe5X13wvc5e6KCC8sd2dLfGE1/oVNV2H4KOCs2twVrtz+BwAAIJv/fVMU6vMvbO4X2q8j/5O1+oV3T5k1Cu8MslxhuQGFo1CfcuGbQ+8oLLJS8jZOlc1OYcUlX+NfeMj/PIlCffUL+0a37vxWVMdsVn+r/ULh3ksWCutQGmsfCvX5Fzb+hTDgf7vag+7IO0k1mHhPtqVB4QkKPXQX2q8a/8maQn3+hQAAAAAAYKpqFx/ix0PhaBT+fGro04a5MJ5qGycehfr8CwEUdWX2iZ+pRsx9V46JEgoHJFLY99SJi7zzHqKF1fjfhEWhvicUbhSiOvtd5GZY+ITZk0J1FQrXPz/z3mGWNr1t1vM4hfqUC3sHrRNLYfTrxvMv7P0qHp3APvaBFBrwL0xS4TQqF4X6sgtrrL3UwhqJiSjU5184H3sLfRR68C8EkMH/WIlCfdMLg97/5OszZwo4VCp+tEXhmEVksi1cl6oji0KhPgqPH/Iw/WgrHYX6xArFhrvvFbHbQqGEhxda7L7z/2UAQCSLqfUQhfr8CzV3mpKDvsT7ulJDob7vwg+c5wHsD8iPrgAAAABJRU5ErkJggg==",
    responsive: false,
    iconSize: "2x",
    icon: ["fas", "play"],
    iconColor: "#FFFFFF",
    iconBackgroundColor: "rgba(0, 0, 0, .73)",
    hover: undefined,
    onClick: undefined,
    maxWidth: undefined,
    maxHeight: undefined
  }

  static propTypes = {
    /** Configura o caminho da imagem * */
    src: PropTypes.string,

    /** Define a responsividade com base na largura * */
    responsive: PropTypes.bool,

    /** Define se haverá animação no hover * */
    hover: PropTypes.bool,

    /** Ícone para ser exibido sobre a imagem * */
    icon: PropTypes.any,

    /** Cor do ícone para ser exibido sobre a imagem * */
    iconColor: PropTypes.string,

    /** Tamanho do ícone para ser exibido sobre a imagem * */
    iconSize: PropTypes.string,

    /** Cor do fundo do ícone para ser exibido sobre a imagem * */
    iconBackgroundColor: PropTypes.string,

    /** Evento a ser realizado ao clicar no ícone exibido sobre a imagem * */
    onClick: PropTypes.func,

    /** Configura definir largura máxima * */
    maxWidth: PropTypes.number,

    /** Configura definir altura máxima * */
    maxHeight: PropTypes.number
  }

  constructor(props) {
    super(props);
    this.state = {
      height: 0,
      width: 0
    };
    this.handleClick = this.handleClick.bind(this);
    this.handleImageSizeChange = this.handleImageSizeChange.bind(this);
  }

  componentDidMount() {
    const imageDOM = ReactDOM.findDOMNode(this.refs.ref_image);
    if (typeof imageDOM.addEventListener === "function") {
      imageDOM.addEventListener("load", this.handleImageSizeChange);
      window.addEventListener("resize", this.handleImageSizeChange);
    }
    this.setState({
      height: imageDOM.offsetHeight || 30,
      width: imageDOM.offsetWidth || 30
    });
  }

  componentDidUpdate(prevProps) {
    if (this.props !== prevProps) {
      const imageDOM = ReactDOM.findDOMNode(this.refs.ref_image);
      this.setState({
        height: imageDOM.offsetHeight || 30,
        width: imageDOM.offsetWidth || 30
      });
    }
  }

  handleImageSizeChange = () => {
    const imageDOM = ReactDOM.findDOMNode(this.refs.ref_image);
    this.setState({
      height: imageDOM.offsetHeight || 30,
      width: imageDOM.offsetWidth || 30
    });
  }

  handleClick = evt => {
    this.props.onClick(evt);
  }

  render() {
    const {
      src,
      maxWidth,
      maxHeight,
      hover,
      responsive,
      iconSize,
      icon,
      iconBackgroundColor,
      iconColor,
      onClick
    } = this.props;
    const _responsiveHeight = !responsive;
    const _hover = onClick && hover === undefined ? true : hover;

    return (
      <div
        className={`k-thumbnail ${
          _responsiveHeight ? "is-responsive-height" : ""
        } ${responsive ? "is-responsive" : ""}`}
      >
        <div
          className="k-thumbnail-button-container"
          style={{ height: this.state.height, width: this.state.width }}
        >
          <div
            onClick={this.handleClick}
            className={`k-thumbnail-button ${_hover ? "has-animation" : ""} ${
              onClick ? "clickable" : ""
            }`}
            style={{
              backgroundColor: iconBackgroundColor,
              maxHeight: this.state.height / 2,
              height: this.state.height / 2,
              maxWidth: this.state.width / 2,
              width: this.state.height / 2,
              borderRadius: this.state.height / 4
            }}
          >
            <KIcon icon={icon} color={iconColor} size={iconSize} />
          </div>
        </div>
        {React.cloneElement(
          <KImage
            src={src}
            responsive={responsive}
            responsiveHeight={_responsiveHeight}
            maxWidth={maxWidth}
            maxHeight={maxHeight}
          />,
          { ref: "ref_image" },
        )}
      </div>
    );
  }
}

KThumbnail.description = "Componente de thumbnail";
KThumbnail.released = "v.0.1.24";
KThumbnail.url = "Componentes/Moléculas/KThumbnail";
export default KThumbnail;
