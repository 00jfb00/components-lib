Sem imagem definida:

```
<section >
    <KThumbnail />
</section>
```

Com thumb definida:

```
<section >
    <KThumbnail src='https://i.ytimg.com/vi/XcbrQp7TOiU/hqdefault.jpg?sqp=-oaymwEYCNIBEHZIVfKriqkDCwgBFQAAiEIYAXAB&rs=AOn4CLD6MkLNZlcoNTLsGMyH9Nmaqcg3zg' />
</section>
```

Com imagem definida:

```
<section >
    <KThumbnail src='https://www.citiplumbing.com.au/wp-content/uploads/2013/11/lightning-storm-400x300.jpg' />
    
</section>
```

Hover:

```
<section >
    <KThumbnail hover />
</section>
```

Eventos:

```
<section >
    <KThumbnail onClick={() => alert('Clicou')} />
    <KThumbnail onClick={() => alert('Clicou')} hover={false} />
</section>
```

Tamanhos máximos:

```
<section >
    <KThumbnail maxHeight={80} src='https://static.wixstatic.com/media/e28979_c7c66c9ae404417199d77b5e10f9c702~mv2.jpg' />
    <KThumbnail maxWidth={900} src='https://static.wixstatic.com/media/e28979_c7c66c9ae404417199d77b5e10f9c702~mv2.jpg' />
    <KThumbnail maxHeight={80} maxWidth={80} src='https://static.wixstatic.com/media/e28979_c7c66c9ae404417199d77b5e10f9c702~mv2.jpg' />
</section>
```

Responsivo:

```
<section >
    <KThumbnail responsive />
</section>
```

Responsive Height:

```
<section style={{ height: 80 }}>
    <KThumbnail />
</section>
```

Ícones:

```
<section >
    <KThumbnail icon={['fab', 'youtube']} />
    <KThumbnail icon={['fas', 'home']} />
    <KThumbnail icon={['kci', 'menu']} />
    <KThumbnail icon='pause' />
</section>
```

Tamanhos de ícones:

```
<section >
    <KThumbnail iconSize='sm' />
    <KThumbnail iconSize='4x' />
</section>
```

Cores de ícones:

```
<section >
    <KThumbnail iconColor='red' />
    <KThumbnail iconColor='#F7B1AB' />
    <KThumbnail iconColor='rgb(49, 197, 61)' />
    <KThumbnail iconColor='rgba(255, 255, 255, .1)' />
</section>
```

Cores de fundo de ícones:

```
<section >
    <KThumbnail iconBackgroundColor='red' />
    <KThumbnail iconBackgroundColor='#F7B1AB' />
    <KThumbnail iconBackgroundColor='rgb(49, 197, 61)' />
    <KThumbnail iconBackgroundColor='rgba(255, 255, 255, .1)' />
</section>
```
