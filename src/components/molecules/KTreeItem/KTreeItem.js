import React, { Component, Fragment, Suspense } from "react";
import PropTypes from "prop-types";
import NavLink from "react-bootstrap/NavLink";
import KIcon from "../../atoms/KIcon";
import KMessageriaBadge from "../../atoms/KMessageriaBadge";
import KProgressBar from "../../atoms/KProgressBar";
import KSpinner from "../../atoms/KSpinner";

class KTreeItem extends Component {
  static defaultProps = {
    subitems: [],
    active: false,
    cockpit: false,
    mail: false,
    badgeContent: false,
    icon: undefined,
    iconActive: undefined,
    iconColor: ["white", "#7D97AD"],
    state: "gradient",
    brand: "default",
    indexID: undefined,
    onClick: undefined,
    title: undefined,
    subtitle: undefined,
    id: "k-tree-item",
    bgColor: undefined,
    fontColor: undefined,
    percentage: undefined,
    width: "160px",
    minWidth: "135px",
    session: false,
    disabled: false
  };

  static propTypes = {
    /** Lista de subítens */
    subitems: PropTypes.array,

    /** Ícone a ser exibido */
    icon: PropTypes.any,

    /** Ícone a ser exibido quando o item for clicado */
    iconActive: PropTypes.any,

    /** Cor do ícone a ser exibido */
    iconColor: PropTypes.array,

    /** Evento de clique */
    onClick: PropTypes.func,

    /** Título do item */
    title: PropTypes.string,

    /** Subtítulo do item */
    subtitle: PropTypes.string,

    /** Define se o ítem esta ativo */
    active: PropTypes.bool,

    /** Define se o layout do cockpit */
    cockpit: PropTypes.bool,

    /** Define se o item está inativo */
    disabled: PropTypes.bool,

    /** Define se utiliza o layout da messageria */
    mail: PropTypes.bool,

    /** Define se o item da messageria possui conteúdo dentro do badge */
    badgeContent: PropTypes.any,

    /** Define a cor do background do badge * */
    bgColor: PropTypes.string,

    /** Define a cor da fonte do badge * */
    fontColor: PropTypes.string,

    /** O estado do item é baseado no tema. Os valores aceitos são: 'primary', 'secondary', 'ternary', 'gradient', 'white', 'black', 'dark', 'light', 'grey', 'grey-dark', 'grey-lighter', 'success', 'warning', 'info', 'danger' * */
    state: PropTypes.string,

    /** O estado do item é baseado na marca. Os valores aceitos são: 'default', 'anhanguera' * */
    brand: PropTypes.string,

    /** Id do componente */
    id: PropTypes.string,

    /** Propriedade de controle para identificação */
    indexID: PropTypes.string,

    /** Tamanho da div do progress bar */
    width: PropTypes.string,

    /** Tamanho mínimo da progress bar */
    minWidth: PropTypes.string,

    /** Configura a exibição de porcentagem em um KProgressBar */
    percentage: PropTypes.number,

    /** Define se o item é um subItem */
    session: PropTypes.bool

  }

  constructor(props) {
    super(props);
    this.state = {
      collapse: false,
      active: false,
      data: props.subitems.map(element => {
        element.active = element.active || false;

        return element;
      })
    };

    this.handleClick = this.handleClick.bind(this);
    this.handleClickSubItems = this.handleClickSubItems.bind(this);
  }

  componentDidUpdate(prevProps) {
    if (this.props.active !== prevProps.active) {
      this.setState({
        collapse: this.props.subitems.length > 0 ? this.props.active : false,
        data: this.props.subitems.map(element => {
          element.active = element.active || false;

          return element;
        })
      });
    }
  }

  static getDerivedStateFromProps(props, state) {
    if (state.active === props.active) {
      return state;
    }
    state.collapse = props.subitems.length > 0 ? props.active : false;
    state.active = props.active;
    state.data = props.subitems.map(element => {
      element.active = element.active || false;

      return element;
    });
  }

  handleClickSubItems = (selectedIndex, onClick) => {
    if (this.props.subitems[selectedIndex].children) {
      this.setState(
        {
          data: this.props.subitems.map((element, index) => {
            element.active = index === selectedIndex;

            return element;
          })
        },
        () => onClick && onClick(),
      );
    } else {
      this.setState(
        {
          ...this.props.state
        },
        () => onClick && onClick(),
      );
    }
  }

  handleClick(evt) {
    if (
      (!this.props.active || this.props.subitems.length === 0)
      && this.props.onClick
    ) {
      this.props.onClick(evt);
    }
    this.setState(prevState => ({ collapse: !prevState.collapse }));
  }

  render() {
    const {
      id,
      active,
      cockpit,
      mail,
      badgeContent,
      icon,
      iconColor,
      state,
      brand,
      subitems,
      title,
      subtitle,
      indexID,
      disabled,
      bgColor,
      fontColor,
      percentage,
      width,
      minWidth,
      iconActive,
      session,
      indicator
    } = this.props;

    return (
      <div
        id={id}
        className={`k-tree-item list-group-item is-primary-${brand} ${!active && cockpit ? "cockpit-li" : ""} ${cockpit ? "cockpit" : ""} ${!active && mail ? "messageria-li" : ""} ${mail ? "messageria" : ""}`}
        style={{ borderLeftWidth: !session ? active && cockpit ? 3 : active && mail ? 0 : active ? 4 : 0 : 0 }}
      >
        <NavLink
          id={`${id}-action`}
          disabled={disabled}
          onClick={this.handleClick}
          className={`k-tree-item-block ${session && !icon ? "baseline" : ""} ${session ? "session" : ""} ${cockpit ? "cockpit" : ""} ${mail ? "messageria" : ""} ${
            active && !this.state.collapse ? "is-active" : ""
          } is-${state}-${brand} ${active && cockpit ? "cockpit-block-active" : ""} ${active && mail ? "messageria-block-active" : ""}`}
          style={{
            borderBottomWidth:
              subitems.length > 0 && !this.state.collapse ? 2 : 1
          }}
        >
          {indicator && session && !icon && (
            <div
              className="k-tree-session indicator-wrapper"
            >
              {indicator && (
                <h3 className={`is-primary-${brand} ${active}`}>
                  {indicator}
                </h3>
              )}
            </div>
          )}
          {
            icon && (
              <div
                className={`k-tree-item-icon ${
                  active && !session ? `is-${state}-${brand} ${cockpit ? "cockpit-icon" : ""} ${mail ? "messageria-icon" : ""}` : ""
                }`}
              >
                <Fragment>
                  <KIcon
                    icon={iconActive ? (!cockpit && active && this.state.collapse && subitems.length > 0 ? iconActive : icon) : icon}
                    rotate={!iconActive ? !cockpit && active && this.state.collapse && subitems.length > 0 : undefined}
                    color={!session ? (active ? iconColor[0] : iconColor[1]) : iconColor[1]}
                    size={`${cockpit ? "xs" : mail || session ? "sm" : "lg"}`}
                  />
                </Fragment>
              </div>
            )
          }
          <div className={`title-container ${session ? "start" : ""} ${icon && session ? "pipe" : ""} ${cockpit ? "cockpit-title" : ""} ${mail ? "messageria-title" : ""}`}>
            <p name="title-sessão" className={`${active && mail ? `is-primary-${brand} ${active}` : ""} ${mail ? "center-align" : ""} ${active && session ? `is-primary-${brand} ${active}` : ""} ${session ? `has-primary-${brand}` : ""} ${session && !icon ? "title-session" : ""}`}>{title}</p>
            {mail && badgeContent
              ? (
                <div style={{ justifyContent: "flex-end", display: "inline-flex", width: "10%" }}>
                  <Fragment>
                    <KMessageriaBadge border={false} content={badgeContent} size={30} brand={brand} state={state} bgColor={bgColor} fontColor={fontColor} />
                  </Fragment>
                </div>
              )
              : null }
            <p
              className={active && !session ? `is-primary-${brand}` : ""}
              style={{ fontWeight: "bold" }}
            >
              {subtitle}
            </p>
            {percentage >= 0 && (
              <div
                className="k-tree-subitem__percent-inline"
                style={{ width }}
              >
                <div
                  className="k-tree-subitem__percent-bar"
                  style={{ minWidth }}
                >
                  <Suspense fallback={<KSpinner />}>
                    <KProgressBar
                      state={state}
                      brand={brand}
                      percentage={percentage}
                    />
                  </Suspense>
                </div>
                <div className="k-tree-subitem__percent-item-text">
                  <p>
                    {percentage}
                    %
                  </p>
                </div>
              </div>
            )}
          </div>
          {cockpit && subitems.length > 0
            ? (
              <div style={{ right: "0px", position: "absolute", width: "30px" }}>
                <Suspense fallback={<KSpinner />}>
                  <KIcon
                    icon="chevron-right"
                    rotate={this.state.collapse && active && subitems.length > 0}
                    color="#000"
                    size="xs"
                  />
                </Suspense>
              </div>
            )
            : null}
        </NavLink>
        <div className={`k-tree-item-list${active && cockpit ? " cockpit-block-active" : ""}`}>
          <div
            className={`k-tree-item-list-expand ${
              (active || this.state.collapse) && this.state.collapse && subitems.length > 0 ? "expanded" : ""
            }`}
          >
            {this.state.data.map((element, index) => (
              <Suspense key={`ID${index}`} fallback={<KSpinner />}>
                <KTreeItem
                  id={`${element.idSubItem || `tree-${indexID ? `${indexID}-` : ""}subitem${index}`}`}
                  key={`ID${index}`}
                  title={element.title}
                  subtitle={element.subtitle}
                  onClick={() => this.handleClickSubItems(index, element.onClick)}
                  disabled={!(active && this.state.collapse && subitems.length > 0)}
                  state="gradient"
                  brand={brand}
                  session
                  active={element.active}
                  indicator={element.indicator}
                  percentage={element.percentage}
                  icon={element.icon}
                  iconActive={element.iconActive}
                  subitems={element.children}
                />
              </Suspense>
            ))}
          </div>
        </div>
      </div>
    );
  }
}

KTreeItem.description = "Componente item para o KTree";
KTreeItem.released = "v.0.1.22";
KTreeItem.url = "Componentes/Moléculas/KTreeItem";
export default KTreeItem;
