Mensageria menu:
```
<section>
    <KTreeItem mail icon={['fas', 'inbox']} badgeContent='3' iconColor={['#0185CD', '#000']} active title='Caixa de Entrada' />
    <KTreeItem state='primary' brand='anhanguera' mail badgeContent='3' active icon={['fas', 'paper-plane']} iconColor={['#0185CD', '#000']} title='Enviados'/> 
    <KTreeItem brand='anhanguera' mail badgeContent='3' icon={['fas', 'paper-plane']} iconColor={['#0185CD', '#000']} title='Enviados'/> 
    <KTreeItem state='primary' brand='anhanguera' mail badgeContent='3' icon={['fas', 'paper-plane']} iconColor={['#0185CD', '#000']} title='Enviados'/> 
</section>
```
Menu com 3 granularidades:
```
<section>
<KTreeItem 
        title='Unidade 1' 
        icon={["kci", "arrow-circle-right"]}
        subtitle='Título da Unidade em Questão, Seguindo esse Padrão'
        subitems={[
                    {id:1, title:'PRAZO expira em 17/07', icon: ['fas','caret-down'], iconActive: ['fas','caret-up'], subtitle: 'Título da Sessão',indicator:'1.1', percentage:0, 
                         children: [
                                    {
                                    title: "Conteúdo",
                                    subtitle: "2 minutos de conteúdo",
                                    icon: ['fas', 'file-alt'],
                                    iconColor: "#HEX",
                                    indicator:'1.1',                                     
                                    onClick:()=> {alert('esta clicavel01')}
                                  },{
                                    title: "Avaliação",
                                    subtitle: "Prazo ate 24/07",
                                    icon: ['fas', 'calendar'],
                                    iconColor: "#HEX",
                                    indicator:'1.1', 
                                    onClick:()=> {alert('esta clicavel')}
                                  }
                            
                                ]
                    }
                ]} active/>
</section>
```
Home:
```
<section>
    <KTreeItem cockpit icon={['fas', 'home']} iconColor={['#0185CD', '#000']} active title='Meu AVA' />
    <KTreeItem cockpit icon={['fas', 'user-graduate']} iconColor={['#0185CD', '#000']} title='Secretaria Digital'/>    
    <KTreeItem cockpit icon={['kci', 'conecta']} iconColor={['#F04F23', '#000']} active brand='anhanguera' title='Canal Conecta'/>    
    <KTreeItem cockpit active icon={['fas', 'university']} title='Biblioteca Virtual' 
    iconColor={['#0185CD', '#000']} subitems={[{id:1, title:'Biblioteca Virtual'},{id:2, title:'Acervo Virtual'}]} />    
    <KTreeItem cockpit icon={['fas', 'book-reader']} title='Sala de Aula'/>     
    <KTreeItem cockpit icon={['far', 'question-circle']} title='Precisa de Ajuda'/>
</section>
```

Estados:
```
<section>
    <KTreeItem title='Unidade 1' active subtitle='Ética e Política Brasileira' />
    <KTreeItem title='Unidade 2' subtitle='Cidadania e Direitos Humanos' />
</section>
```

Com Subitens:
```
<section>
    <KTreeItem title='Unidade 1' subitems={[{id:1, title:'Why?', indicator:'1.0', percentage:90},{id:2, title:'Porque pensar sobre ética?', indicator:'1.1', percentage:90}]} active subtitle='Ética e Política Brasileira' />
    <KTreeItem title='Unidade 2' active subtitle='Cidadania e Direitos Humanos' />
    <KTreeItem title='Unidade 2' subitems={[{id:1, title:'Porque pensar sobre ética?'}]} subtitle='Cidadania e Direitos Humanos' />
</section>
```

Ícones:
```
<section>
    <KTreeItem title='Unidade 1' icon={["fab", "youtube"]} subitems={[{id:1, title:'Why?', indicator:'1.0', percentage:90, onClick:()=> {alert('onClick Event')}},{id:2, title:'Porque pensar sobre ética?', indicator:'1.1'}]} active subtitle='Ética e Política Brasileira' />
    <KTreeItem title='Unidade 2' icon={["kci", "forum"]} active subtitle='Cidadania e Direitos Humanos' />
    <KTreeItem title='Unidade 2' icon='home' subtitle='Cidadania e Direitos Humanos' />
</section>
```

Eventos:
```
<section>
    <KTreeItem title='Unidade 1' onClick={() => alert(1)} subitems={[{id:1, title:'Porque pensar sobre ética?'}]} active subtitle='Ética e Política Brasileira' />
    <KTreeItem title='Unidade 2' onClick={() => alert(1)} active subtitle='Cidadania e Direitos Humanos' />
    <KTreeItem title='Unidade 2' onClick={() => alert(1)} subitems={[{id:1, title:'Porque pensar sobre ética?'}]} subtitle='Cidadania e Direitos Humanos' />
    <KTreeItem title='Unidade 2' onClick={() => alert(1)} subtitle='Cidadania e Direitos Humanos' />
</section>
```
