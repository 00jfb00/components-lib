import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";
import NavLink from "react-bootstrap/NavLink";
import KDropdown from "../KDropdown";
import KButton from "../../atoms/KButton";

class KMenuList extends Component {
  static defaultProps = {
    list: [],
    funcClick: () => {},
    brand: "default",
    id: "k-menu-list",
    title: undefined
  };

  static propTypes = {
    /** Id que será exibido na div * */
    id: PropTypes.string,
    /** Nome a ser exibido no botão do menu * */
    title: PropTypes.string,
    /** Lista que será exibido no menu * */
    list: PropTypes.any,
    /** Função de callback do click * */
    funcClick: PropTypes.func,
    /** Marca a ser utilizada * */
    brand: PropTypes.string
  };

  constructor(props) {
    super(props);
    this.state = {
      click: false
    };
    this.handleClick = this.handleClick.bind(this);
    this.handleFuncClick = this.handleFuncClick.bind(this);
  }

  handleClick = () => {
    this.setState(prevState => ({ click: !prevState.click }));
  };

  handleFuncClick = index => {
    this.props.funcClick(index);
  };

  render() {
    const {
      id, title, list, brand
    } = this.props;

    return (
      <div id={id} className="k-menu-list">
        <Fragment>
          <KDropdown
            changeProps={this.handleClick}
            triggerComponent={(
              <KButton
                id="k-menu-list-btn"
                outline
                menu
                rightIcon="chevron-down"
                title={title}
                brand={brand}
                stateIcon="Primary"
                rotate180={this.state.click}
                onClick={this.handleClick}
              />
            )}
          >
            <ul className="k-menu-list-group list-group">
              {list.map((element, index) => (
                <NavLink className="list-group-item list-group-item-action" onClick={() => this.handleFuncClick(index)} key={index}>
                  <li
                    key={`${id}-${index}`}
                    id={`${id}-${index}`}

                  >
                    {element}
                  </li>
                </NavLink>
              ))}
            </ul>
          </KDropdown>
        </Fragment>
      </div>
    );
  }
}

KMenuList.description = "Componemte menu list";
KMenuList.released = "v.0.1.60";
KMenuList.url = "Componentes/Moléculas/KMenuList";
export default KMenuList;
