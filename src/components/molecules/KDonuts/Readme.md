Default:
```
<section>
    <KDonuts />
</section>
```

ProgressBar Donuts Ativo - Necessário os três parametros: miniPoints:(número minimo de pontos para obter a aprovação), maxPoints:(número máximo de pontos possiveis na disciplina), studentPoints(pontos obtidos pelo estudante)
```
<section>
    <KDonuts
        miniPoints={8000}
        maxPoints={14000}
        studentPoints={7000} />

</section>
```
ProgressBar Donuts Aprovado
```
<section>
    <KDonuts
        miniPoints={8000}
        maxPoints={14000}
        studentPoints={9000} />
</section>

```
ProgressBar Donuts - Se o número de pontos do Estudante (studentPoints) for maior que o número  máximo de pontos (maxPoints) - é apresentado para o Estudante o numero máximo de Pontos.
```
<section>
    <KDonuts
        miniPoints={8000}
        maxPoints={14000}
        studentPoints={19000} />
</section>

```
ProgressBar Donuts - Se o maximo dos pontos (maxPoints) for igual a zero - O grafico é apresentado com o estado desativado.
```
<section>
    <KDonuts
        miniPoints={20}
        maxPoints={0}
        studentPoints={1} />
</section>

```
ProgressBar Donuts - Com autoApproved false
```
<section>
    <KDonuts
        miniPoints={8000}
        maxPoints={14000}
        studentPoints={9000}
        autoApproved={false} />
</section>

```

ProgressBar Donuts - Com autoApproved false e approved true
```
<section>
    <KDonuts
        miniPoints={8000}
        maxPoints={14000}
        studentPoints={6000}
        autoApproved={false}
        approved />
</section>

```
