import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";
import KIcon from "../../atoms/KIcon";

class KDonuts extends Component {
  static propTypes = {

    /** Pontos maximo na disciplina * */
    maxPoints: PropTypes.number,

    /** Pontos minimo a ser alçado pelo aluno para aprovação* */
    miniPoints: PropTypes.number,

    /** Pontos obtido pelo aluno * */
    studentPoints: PropTypes.number,

    /** Define se aprovação será automática * */
    autoApproved: PropTypes.bool,

    /** Define o status de aprovação * */
    approved: PropTypes.bool

  };

  static defaultProps = {
    maxPoints: 0,
    miniPoints: 0,
    studentPoints: 0,
    autoApproved: true,
    approved: false
  };

  constructor(props) {
    super(props);
    this.state = {
      viewBox: "3 3 36 36",
      preserveAspectRatio: "xMinYMin",
      rotateDeg: 0,
      sizeIcon: "lg"
    };
  }

  componentDidMount() {
    this.getRotate();
  }

  getRotate = () => {
    const {
      maxPoints,
      miniPoints,
      studentPoints,
      autoApproved,
      approved
    } = this.props;
    let total = 0;
    let totalstr = "0 100";
    let successIcon = false;
    let active = false;
    let rotacao = (miniPoints * 360) / maxPoints;
    let studentMaxPoints = studentPoints;
    let strokeFull = "#fb9678";
    const ProgressBarSucessColor = "#3ac7b1";

    if (studentPoints && studentPoints > 0) {
      total = Math.round((studentPoints * 100) / maxPoints);
      totalstr = `${total} ${100 - total}`;
    }

    if (autoApproved) {
      if (studentPoints >= miniPoints && studentPoints !== 0) {
        successIcon = true;
        strokeFull = ProgressBarSucessColor;
      }
    } else if (approved) {
      successIcon = true;
      strokeFull = ProgressBarSucessColor;
    }

    if (maxPoints > 0) {
      active = true;
    } else {
      strokeFull = "#d2d3d4";
    }

    if (studentPoints > maxPoints) {
      studentMaxPoints = maxPoints;
    }

    if (miniPoints > maxPoints) {
      rotacao = 360;
    }

    this.setState({
      rotateDeg: rotacao,
      alunoProgressBar: totalstr,
      successIcon,
      active,
      studentPoints: studentMaxPoints,
      strokeFull
    });
  }

  render() {
    const { maxPoints } = this.props;

    const {
      viewBox,
      preserveAspectRatio,
      alunoProgressBar,
      sizeIcon,
      successIcon,
      active,
      rotateDeg,
      strokeFull,
      studentPoints
    } = this.state;

    return (

      <div className="k-donut">

        <svg
          viewBox={viewBox}
          preserveAspectRatio={preserveAspectRatio}
        >
          <circle className="k-donut__hole" />
          <circle className="k-donut__ring" />
          <circle
            className="k-donut__segment"
            stroke={strokeFull}
            strokeDasharray={alunoProgressBar}
          />
        </svg>
        <div className="k-donut__center">
          <Fragment>
            <KIcon
              icon={successIcon ? "unlock" : "lock"}
              color={active ? "#212121" : "#707070"}
              size={`${sizeIcon}`}
            />
          </Fragment>
          <div className="k-donut__points">
            <p>{`${studentPoints} / ${maxPoints}`}</p>
            <p>pontos</p>
          </div>
        </div>
        <div className="k-donut__indicator" style={{ transform: `rotate(${rotateDeg}deg)` }} />

      </div>
    );
  }
}

KDonuts.description = "Componente radial progress com indicador de mínimo";
KDonuts.released = "v.0.1.2";
KDonuts.url = "Componentes/Moléculas/KDonuts";

export default KDonuts;
