import React, { PureComponent, Fragment } from "react";
import PropTypes from "prop-types";
import { TransformWrapper, TransformComponent } from "react-zoom-pan-pinch";

import NavLink from "react-bootstrap/NavLink";
import KOptionGroup from "../KOptionGroup";
import KModal from "../KModal";
import KCard from "../../atoms/KCard";
import KButton from "../../atoms/KButton";
import KIcon from "../../atoms/KIcon";

class KQuestion extends PureComponent {
  static defaultProps = {
    subtitle: "",
    options: [],
    formulas: [],
    attachments: [],
    body: "",
    radio: false,
    iconSize: undefined,
    onChangeValue: undefined,
    ignoreState: false,
    listStyleType: "none"
  };

  static propTypes = {
    /** Titulo do card * */
    title: PropTypes.any.isRequired,
    /** Sub titulo do card * */
    subtitle: PropTypes.any,
    /** Opções a serem exibidas* */
    options: PropTypes.any,
    /** Formulas a serem exibidas* */
    formulas: PropTypes.any,
    /** Option é radio ou checkbox* */
    radio: PropTypes.bool,
    /** Anexos a serem exibidas* */
    attachments: PropTypes.any,
    /** O corpo da pergunta * */
    body: PropTypes.any,
    /** O corpo da pergunta * */
    onChangeValue: PropTypes.func,
    /** Ignore o estado * */
    ignoreState: PropTypes.bool,
    /** Define tipo de listagem das options */
    listStyleType: PropTypes.string,
    /** Tamanho do icone* */
    iconSize: PropTypes.string
  };

  accessibilityAuxiliars = {
    auxA: "",
    auxButton: ""
  };

  constructor(props) {
    super(props);
    this.state = {
      isGalleryOpen: false,
      activeItem: 0,
      selectedGallery: undefined,
      limitToBounds: true,
      panningEnabled: true,
      transformEnabled: true,
      pinchEnabled: true,
      limitToWrapper: false,
      disabled: false,
      dbClickEnabled: true,
      lockAxisX: false,
      lockAxisY: false,
      velocityEqualToMove: true,
      enableWheel: false,
      enableTouchPadPinch: true,
      enableVelocity: true,
      limitsOnWheel: false
    };
  }

  ignoreBackgroundActions = () => {
    if (!this.state.isGalleryOpen) {
      this.accessibilityAuxiliars.auxA = "";
      this.accessibilityAuxiliars.auxButton = "";
      Array.prototype.slice.call(document.getElementsByTagName("button")).forEach(value => {
        if (!value.getAttribute("data-hideacess")) {
          if (!value.disabled) {
            value.setAttribute("tabIndex", "-1");
          } else {
            this.accessibilityAuxiliars.auxButton = `${this.accessibilityAuxiliars.auxButton},${value.id}`;
          }
        }
      });
      Array.prototype.slice.call(document.getElementsByTagName("a")).forEach(value => {
        if (!value.getAttribute("data-hideacess")) {
          if (value.tabIndex >= 0) {
            this.accessibilityAuxiliars.auxA = `${this.accessibilityAuxiliars.auxA},${value.id}`;
          }
          value.setAttribute("tabIndex", "-1");
        }
      });
    } else if (this.state.isGalleryOpen) {
      Array.prototype.slice.call(document.getElementsByTagName("button")).forEach(value => {
        if (!this.accessibilityAuxiliars.auxButton.split(",").find(id => id === value.id) && (!value.getAttribute("data-hideacess"))) {
          value.setAttribute("tabIndex", "1");
        }
      });

      Array.prototype.slice.call(document.getElementsByTagName("a")).forEach(value => {
        if (this.accessibilityAuxiliars.auxA.split(",").find(id => id === value.id) !== undefined && (!value.getAttribute("data-hideacess"))) {
          value.setAttribute("tabIndex", "0");
        }
      });
    }
  };

  render() {
    const {
      title, subtitle, options, body, iconSize, radio, onChangeValue, id, disable, formulas, attachments, ignoreState, listStyleType
    } = this.props;

    const {
      limitToBounds,
      panningEnabled,
      transformEnabled,
      pinchEnabled,
      limitToWrapper,
      disabled,
      dbClickEnabled,
      lockAxisX,
      lockAxisY,
      velocityEqualToMove,
      enableWheel,
      enableTouchPadPinch,
      enableVelocity,
      limitsOnWheel
    } = this.state;

    return (
      <Fragment>
        <Fragment>
          <KCard
            id={id || "k-question"}
            className="k-question"
            style={{ padding: 0 }}
            title={title}
            shadow={false}
            border={false}
            subtitle={subtitle}
          >
            <div>
              {body}
            </div>
            {formulas.length > 0 && (
              <div className="image-container-button-action">
                <p>Esta questão possui uma fórmula</p>
                <KButton
                  block
                  id={`${id}-formulas`}
                  onClick={() => {
                    this.ignoreBackgroundActions();
                    this.setState({ isGalleryOpen: true, activeItem: 0, selectedGallery: "formulas" });
                  }}
                  title="Ver Fórmula"
                  outline
                />
              </div>
            )}
            {attachments.length > 0 && (
              <div className="image-container-button-action">
                <p>Esta questão possui um anexo</p>
                <KButton
                  block
                  id={`${id}-attachments`}
                  onClick={() => {
                    this.ignoreBackgroundActions();
                    this.setState({ isGalleryOpen: true, activeItem: 0, selectedGallery: "attachments" });
                  }}
                  title="Ver Anexo"
                  outline
                />
              </div>
            )}
            <KOptionGroup
              id={id || "k-question"}
              borderColor="#999"
              contentColor="#777"
              color="#999"
              options={options}
              size={iconSize}
              radio={radio}
              onChangeValue={onChangeValue}
              disable={!!disable}
              ignoreState={ignoreState}
              listStyleType={listStyleType}
            />
          </KCard>
        </Fragment>
        <KModal
          openModal={this.state.isGalleryOpen}
          id="k-question"
          onHide={() => {
            this.ignoreBackgroundActions();
            this.setState({ isGalleryOpen: false });
          }}
        >
          <div className="k-question-zoom-image-modal">
            <TransformWrapper
              options={{
                limitToBounds,
                transformEnabled,
                disabled,
                limitToWrapper
              }}
              pan={{
                disabled: !panningEnabled,
                lockAxisX,
                lockAxisY,
                velocityEqualToMove,
                velocity: enableVelocity
              }}
              pinch={{ disabled: !pinchEnabled }}
              doubleClick={{ disabled: !dbClickEnabled }}
              wheel={{
                wheelEnabled: enableWheel,
                touchPadEnabled: enableTouchPadPinch,
                limitsOnWheel
              }}
            >
              {({
                zoomIn,
                zoomOut,
                resetTransform,
                scale
              }) => (
                <div className="k-question-zoom-image-modal-container">
                  <TransformComponent>
                    {this.props[this.state.selectedGallery].map((image, index) => (
                      <div key={`key-${index}`} className={`k-question-zoom-image-modal-image ${index === this.state.activeItem ? "is-active" : ""}`}>
                        <img src={image} alt={this.state.selectedGallery} />
                      </div>
                    ))}
                  </TransformComponent>
                  <NavLink disabled={this.state.activeItem <= 0} onClick={this.state.activeItem <= 0 ? undefined : () => this.setState(prevState => ({ activeItem: prevState.activeItem - 1 }), resetTransform)} className="k-question-zoom-image-modal-image-paginator-prev">
                    <KIcon id="k-question-zoom-image-modal-image-paginator-prev" style={{ opacity: this.state.activeItem <= 0 ? 0.4 : 1 }} size="4x" icon="angle-left" />
                  </NavLink>
                  <NavLink disabled={this.state.activeItem >= (this.props[this.state.selectedGallery].length - 1)} onClick={this.state.activeItem >= (this.props[this.state.selectedGallery].length - 1) ? undefined : () => this.setState(prevState => ({ activeItem: prevState.activeItem + 1 }), resetTransform)} className="k-question-zoom-image-modal-image-paginator-next">
                    <KIcon id="k-question-zoom-image-modal-image-paginator-next" style={{ opacity: this.state.activeItem >= (this.props[this.state.selectedGallery].length - 1) ? 0.4 : 1 }} size="4x" icon="angle-right" />
                  </NavLink>
                  <div className="k-question-zoom-image-modal-controls">
                    <KButton disabled={scale === 1} iconOnly outline id="k-question-zoom-image-modal-controls-zoomout" title="" leftIcon={["kci", "minus-circle"]} brandIcon="default" stateIcon="primary" iconSize="2x" onClick={zoomOut} />
                    <KIcon size="1x" brand="default" icon="search" />
                    <KButton disabled={scale >= 3} iconOnly id="k-question-zoom-image-modal-controls-zoomin" title="" leftIcon={["fas", "plus-circle"]} brandIcon="default" stateIcon="primary" iconSize="2x" onClick={zoomIn} />
                  </div>
                </div>
              )}
            </TransformWrapper>
          </div>
        </KModal>
      </Fragment>
    );
  }
}

KQuestion.description = "Componente de questões";
KQuestion.released = "v.0.1.22";
KQuestion.url = "Componentes/Moléculas/KQuestion";
export default KQuestion;
