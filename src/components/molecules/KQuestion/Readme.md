Estado:
```
<section>
    <KQuestion 
        title={<p style={{ color: '#3F9BD6', fontWeight: 'bold', fontSize: 18 }}>Questão 1</p>}
        subtitle={<p style={{ color: '#3F9BD6' }}>Ética e Responsabilidade</p>}
        listStyleType="upper-alpha"
        ignoreState={false}
        body={<span><span className="ava-quiz-html"><p>Analise a imagem a seguir:</p><p><img data-attachment="true" style={{ width: 216 }} src="https://cdn.unoparead.com.br/upload/provaead/imagens/1518808270577.png" alt="" data-attechment="true" data-attechment-id="3868"/><br/>Anexo - Consulte a imagem em melhor resolução no final do cadernos de questões.</p><p>Fonte: disponível em </p><p>Julgue as sentenças a seguir:</p><p>I) Na representação temos um mobiliário representado em perspectiva isométrica explodida.</p><p>PORQUE</p><p>II) Nesse tipo de representação a perspectiva explodida é adequada e permite o entendimento da montagem do objeto.</p><p>Assinale a alternativa correta:</p></span></span>}
        options={[
            {content: 'Este ava aqui 1', state: 'warning'},
            {content: 'Este ava aqui 2', state: 'info'},
            {content: 'O ava que vamos construir juntinhos', state: 'danger'},
            {content: <div><p>A ferramenta de qualidade escolhida pelos gestores do Hospital das Dores foi o Diagrama de Causa e Efeito ou também chamado de Espinha de Peixe.</p><p>O diagrama de Causa e Efeito é a representação gráfica das causas de um</p><p>fenômeno e é um instrumento muito usado para estudar:</p><li> Os fatores que determinam resultados que se deseja obter (processo, desempenho, oportunidade).</li><li> As causas de problemas que precisam ser evitados (defeitos, falhas, variabilidade).</li><p>Portanto, é uma ferramenta que pode ser utilizada para a identificação dos fatores que potencialmente causam efeitos indesejáveis na realização de um processo.</p></div>, selected: true, state: 'success'}
        ]}
        onChangeValue={val => console.log(val)}
    />
</section>
```

Disable:
```
<section>
    <KQuestion 
        disable
        title={<p style={{ color: '#3F9BD6', fontWeight: 'bold', fontSize: 18 }}>Questão 1</p>}
        subtitle={<p style={{ color: '#3F9BD6' }}>Ética e Responsabilidade</p>}
        body={<p style={{ color: '#777' }}>Qual o melhor ava do mundo?</p>}
        options={[
            {content: 'Este ava aqui 1', state: 'warning'},
            {content: 'Este ava aqui 2', state: 'info'},
            {content: 'O ava que vamos construir juntinhos', selected: true, state: 'danger'},
            {content: 'O ava com tempo de resposta em 200 milisegundos', selected: true, state: 'success'}
          ]}/>
</section>
```

Ignorar Estado:
```
<section>
    <KQuestion 
        title={<p style={{ color: '#3F9BD6', fontWeight: 'bold', fontSize: 18 }}>Questão 1</p>}
        subtitle={<p style={{ color: '#3F9BD6' }}>Ética e Responsabilidade</p>}
        listStyleType="upper-alpha"
        ignoreState
        formulas={["https://cdn.unoparead.com.br/upload/provaead/imagens/1491330976361.png", "https://cdn.unoparead.com.br/upload/provaead/imagens/1535498847762.png"]}
        attachments={["https://cdn.unoparead.com.br/upload/provaead/imagens/1518808270577.png"]}
        body={<span><span className="ava-quiz-html"><p>Analise a imagem a seguir:</p><p><img data-attachment="true" style={{ width: 216 }} src="https://cdn.unoparead.com.br/upload/provaead/imagens/1518808270577.png" alt="" data-attechment="true" data-attechment-id="3868"/><br/>Anexo - Consulte a imagem em melhor resolução no final do cadernos de questões.</p><p>Fonte: disponível em </p><p>Julgue as sentenças a seguir:</p><p>I) Na representação temos um mobiliário representado em perspectiva isométrica explodida.</p><p>PORQUE</p><p>II) Nesse tipo de representação a perspectiva explodida é adequada e permite o entendimento da montagem do objeto.</p><p>Assinale a alternativa correta:</p></span></span>}
        options={[
            {content: 'Este ava aqui 1'},
            {content: 'Este ava aqui 2'},
            {content: 'O ava que vamos construir juntinhos'},
            {content: <div><p>A ferramenta de qualidade escolhida pelos gestores do Hospital das Dores foi o Diagrama de Causa e Efeito ou também chamado de Espinha de Peixe.</p><p>O diagrama de Causa e Efeito é a representação gráfica das causas de um</p><p>fenômeno e é um instrumento muito usado para estudar:</p><li> Os fatores que determinam resultados que se deseja obter (processo, desempenho, oportunidade).</li><li> As causas de problemas que precisam ser evitados (defeitos, falhas, variabilidade).</li><p>Portanto, é uma ferramenta que pode ser utilizada para a identificação dos fatores que potencialmente causam efeitos indesejáveis na realização de um processo.</p></div>, selected: true, state: 'success'}
        ]}
        onChangeValue={val => console.log(val)}
    />
</section>
```
