import React, { Component } from "react";
import PropTypes from "prop-types";
import KAvatar from "../../atoms/KAvatar";

// eslint-disable-next-line react/prefer-stateless-function
class KBalloon extends Component {
  static defaultProps = {
    src: undefined,
    width: "40px",
    height: "40px",
    children: "Mensagem padrão aqui :)",
    contentWidth: "auto"
  };

  static propTypes = {
    /** Define a url da imagem * */
    src: PropTypes.string,
    /** Define a largura do avatar * */
    width: PropTypes.string,
    /** Define a altura do avatar * */
    height: PropTypes.string,
    /** Define conteudo do container * */
    children: PropTypes.any,
    /** Define a largura do container * */
    contentWidth: PropTypes.string
  };

  render() {
    const {
      src, width, height, children, contentWidth
    } = this.props;
    const contentStyle = {
      width: contentWidth
    };

    return (
      <div className="k-balloon">
        <div className="k-balloon__avatar" style={{ minWidth: width }}>
          <KAvatar width={width} height={height} src={src} />
        </div>
        <div className="k-balloon__content" style={contentStyle}>
          {children}
        </div>
      </div>
    );
  }
}

KBalloon.description = "Componente KBalloon";
KBalloon.released = "v.0.1.1";
KBalloon.url = "Componentes/Moléculas/KBalloon";
export default KBalloon;
