Padrão:
```
<section>
    <KBalloon /> 
</section>
```

Com conteúdo (JSX ou não)
```
<section>
    <KBalloon>
     <strong>Nome: </strong> Bart Simpson
     <p>lorem ipsum dollor sit ay caramba!</p>
    </KBalloon> 
</section>
```


Com imagem e tamanho customizados
```
<section>
    <KBalloon src="https://randomuser.me/api/portraits/women/31.jpg" width="30px" height="30px" /> 
</section>
```
