import React, {
  Component, Fragment
} from "react";
import PropTypes from "prop-types";
import NavLink from "react-bootstrap/NavLink";
import Container from "react-bootstrap/Container";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import KIcon from "../../atoms/KIcon";
import KImage from "../../atoms/KImage";

class KHeader extends Component {
  static defaultProps = {
    id: "1",
    backgroundColor: "#EEF2F6",
    colorIcon: "black",
    iconMobile: ["kci", "menu"],
    sizeIcon: "lg",
    fixed: false,
    onClickMenu: undefined,
    logo:
      "https://s3-us-west-2.amazonaws.com/estudante-production/app/public/ckeditor_assets/pictures/128/original/anhanguera_full.svg",
    responsiveHeight: false,
    bodyMiddle: undefined,
    bodyRight: undefined,
    onClickLogo: () => { }
  };

  static propTypes = {
    /** ID do elemento * */
    id: PropTypes.string,
    /** O icon é o icone que aparecerá quando a página estiver sendo renderizada em mobile * */
    iconMobile: PropTypes.any,
    /** Define a cor do icon que aparecerá quando a página estiver sendo renderizada em mobile * */
    colorIcon: PropTypes.string,
    /** Define o tamanho do icon que aparecerá quando a página estiver sendo renderizada em mobile * */
    sizeIcon: PropTypes.string,
    /** Evento de click na logo da marca * */
    onClickLogo: PropTypes.func,
    /** Define a logo da marca que está cadastrada * */
    logo: PropTypes.string,
    /** Define a logo da marca a ser responsivo height * */
    responsiveHeight: PropTypes.bool,
    /** Define o conteúdo central do header * */
    bodyMiddle: PropTypes.any,
    /** Define o conteúdo do lado esquerdo do header * */
    bodyRight: PropTypes.any,
    /** Define a cor do fundo do header * */
    backgroundColor: PropTypes.any,
    /** Define se a div é fixa no topo da página ou não * */
    fixed: PropTypes.bool,
    /** Define um clique sobre o icone de hamburguer que é exibido quando a página estiver sendo renderizada em mobile * */
    onClickMenu: PropTypes.func
  }

  constructor(props) {
    super(props);
    this.state = {
      isMobile: window.innerWidth <= 992
    };
  }

  componentDidMount() {
    window.addEventListener("resize", this.handleWindowSizeChange);
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.handleWindowSizeChange);
  }

  handleWindowSizeChange = () => {
    this.setState({ isMobile: window.innerWidth <= 992 });
  };

  handleClickLogo = () => {
    this.props.onClickLogo();
  };

  render() {
    const { isMobile } = this.state;

    const {
      id,
      fixed,
      colorIcon,
      iconMobile,
      sizeIcon,
      logo,
      responsiveHeight,
      bodyRight,
      bodyMiddle,
      backgroundColor,
      onClickMenu
    } = this.props;

    return (
      <div
        className={`k-header ${fixed ? "--fixed" : ""}`}
        style={{ backgroundColor }}
      >
        <Fragment>
          <Container>
            <Row>
              {isMobile ? (
                <Col xs={2} md={1} className="icon-menu">
                  <KIcon
                    id="menu-toggler"
                    onClick={onClickMenu}
                    icon={iconMobile}
                    color={colorIcon}
                    size={sizeIcon}
                  />
                </Col>
              ) : null}
              {!isMobile ? (
                <Fragment>
                  <Col sm={4} md={3} lg={2} className="logo">
                    <NavLink id={`nav-logo-${id}`} className="nav-logo" onClick={this.handleClickLogo}>
                      <KImage
                        id={`logo-${id}`}
                        src={logo}
                        responsiveHeight={responsiveHeight}
                        maxHeight={40}
                      />
                    </NavLink>
                  </Col>
                  {bodyMiddle ? (
                    <Col sm={6} md={4} lg={5} className="bodyMiddle">
                      {bodyMiddle}
                    </Col>
                  ) : null}
                </Fragment>
              ) : (
                <Fragment>
                  <Col className="logoMobile">

                    <KImage
                      id={`logo-mobile-${id}`}
                      src={logo}
                      responsiveHeight={responsiveHeight}
                      maxHeight={30}
                      onClick={this.handleClickLogo}
                    />
                  </Col>
                </Fragment>
              )}
              {bodyRight ? <div className="bodyRight">{bodyRight}</div> : null}
            </Row>
          </Container>
        </Fragment>
      </div>
    );
  }
}

KHeader.description = "Header responsivo";
KHeader.released = "v.0.1.22";
KHeader.url = "Componentes/Moléculas/KHeader";
KHeader.element = "KHeader";
export default KHeader;
