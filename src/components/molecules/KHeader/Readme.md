Estado:
```
<section>
<KHeader fixed={false}
         iconMobile={["kci", "menu"]}
         colorIcon="white"
         sizeIcon="lg"
         expand="lg"
         onClickLogo={() => console.log('onClickLogo')}
         variant="light"    
         logo="https://s3-us-west-2.amazonaws.com/estudante-production/app/public/ckeditor_assets/pictures/128/original/anhanguera_full.svg"
         state="primary"
         responsiveHeight
         backgroundColor={"#dddd"}
         />
</section>
```

Com conteúdo:
```
<section>
<KHeader fixed={false}
         logo="https://s3-us-west-2.amazonaws.com/estudante-production/app/public/ckeditor_assets/pictures/128/original/anhanguera_full.svg"         
         bodyMiddle={<div>Conteúdo de input</div>}
         bodyRight={<div>Dados pessoais</div>}
         />
</section>
```
Com onclick no hamburguer mobile:
```
<section>
<KHeader onClick={() => alert(1)}
         logo="https://s3-us-west-2.amazonaws.com/estudante-production/app/public/ckeditor_assets/pictures/128/original/anhanguera_full.svg"         
         bodyMiddle={<div>Conteúdo de input</div>}
         bodyRight={<div>Dados pessoais</div>}
         />
</section>
```
