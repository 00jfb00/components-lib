import React, { Component } from "react";

/** Molécula que deve ser usada para rederizar o conteúdo dentro do k-tabs.* */
class KTabsItem extends Component {
  render() {
    const {
      children, className, id, ...rest
    } = this.props;

    return (
      <div
        id={id}
        ref="k_tab_item"
        className={`k-tab-item ${className}`}
        {...rest}
      >
        {children}
      </div>
    );
  }
}

KTabsItem.description = "Deve ser usado dentro do k-tabs para que o conteúdo dele possa ser renderizado";
KTabsItem.released = "v.0.1.42";
KTabsItem.url = "Componentes/Moléculas/KTabsItem";
KTabsItem.element = "KTabsItem";
KTabsItem.orderElement = 1;

export default KTabsItem;
