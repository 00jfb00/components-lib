Default:

```
import KButton from '../../atoms/KButton';

initialState = { isOpen: false };

<section>
    <KConfirm
      openModal={state.isOpen}
      onHide={() => {
            setState({isOpen: false})
        }}
      agree={() => { console.log('Concordo plenamente.')}}
      span='Atenção'
      msg='Editando sua resposta, você perderá seus likes.'
      question='Tem certeza de que quer continuar?'
      btnAccept='Sim'
      btnRecuse='Voltar'
      />

    <KButton
      state="primary"
      title="Modal!"
      onClick={() => { setState({isOpen: true}) } }/>
</section>
```

Change props:

```
import KButton from '../../atoms/KButton';

initialState = { isOpen: false };

<section>
    <KConfirm
      openModal={state.isOpen}
      onHide={() => {
            setState({isOpen: false})
        }}
      agree={() => { console.log('Concordo plenamente.')}}
      icon={'smile'}
      iconColor={'black'}
      sizeIcon={'2x'}
      span='Sorria'
      msg='Você esta sendo filmado.'
      question='Concorda com nossas filmagens?'
      />

    <KButton
      state="primary"
      title="Modal!"
      onClick={() => { setState({isOpen: true}) } }/>
</section>
```
