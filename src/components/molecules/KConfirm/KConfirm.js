import React, { PureComponent } from "react";
import Modal from "react-bootstrap/Modal";
import PropTypes from "prop-types";
import NavLink from "react-bootstrap/NavLink";
import KIcon from "../../atoms/KIcon";

class KConfirm extends PureComponent {
  static defaultProps = {
    id: "",
    btnAccept: "Sim",
    btnRecuse: "Não",
    openModal: false,
    onHide: undefined,
    agree: undefined,
    icon: undefined,
    iconColor: undefined,
    sizeIcon: undefined,
    span: undefined,
    msg: undefined,
    question: undefined,
    brand: "default"
  }

  static propTypes = {
    /** Id do component modal */
    id: PropTypes.string,
    /** Boolean de abertura do modal */
    openModal: PropTypes.bool,
    /** Função de ocultar modal  */
    onHide: PropTypes.func,
    /** Função de aceite do modal  */
    agree: PropTypes.func,
    /** Icone do modal */
    icon: PropTypes.any,
    /** Cor do icone do modal */
    iconColor: PropTypes.string,
    /** Tamanho do icone do modal */
    sizeIcon: PropTypes.string,
    /** Tag do icone do modal */
    span: PropTypes.string,
    /** Messagem do modal */
    msg: PropTypes.string,
    /** Pergunta do modal */
    question: PropTypes.string,
    /** Botão de aceite do modal */
    btnAccept: PropTypes.string,
    /** Botão de recusa do modal */
    btnRecuse: PropTypes.string,
    /** Marca a ser utilizada */
    brand: PropTypes.string
  }

  render() {
    const {
      id,
      openModal,
      onHide,
      agree,
      icon,
      iconColor,
      sizeIcon,
      span,
      msg,
      question,
      btnAccept,
      btnRecuse,
      brand
    } = this.props;

    return (
      <>
        <Modal
          id={`k-confirm-id-${id}`}
          show={openModal}
          onHide={onHide}
          centered
          backdropClassName="k-confir-modal-backdrop"
          dialogClassName="k-confirm-modal"
        >
          <Modal.Body bsPrefix="k-confirm">
            <div className="k-confirm-body">
              <div className="k-confirm-content">
                <div className="k-confirm-icon">
                  {icon ? (
                    <KIcon icon={icon} color={iconColor} size={sizeIcon} />
                  ) : (
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="39.525"
                      height="33.475"
                      viewBox="0 0 39.525 33.475"
                    >
                      <g
                        id="Grupo_2474"
                        data-name="Grupo 2474"
                        transform="translate(0 0)"
                      >
                        <path
                          id="Caminho_784"
                          data-name="Caminho 784"
                          d="M247.618,134.422a4.54,4.54,0,0,0-7.71,0l-15.225,24.4a4.456,4.456,0,0,0-.683,2.4,4.543,4.543,0,0,0,4.537,4.537h30.45a4.543,4.543,0,0,0,4.537-4.537,4.46,4.46,0,0,0-.682-2.4Zm11.37,28.308h-30.45a1.514,1.514,0,0,1-1.512-1.512,1.461,1.461,0,0,1,.224-.794l15.225-24.4a1.515,1.515,0,0,1,2.577,0l15.225,24.4a1.458,1.458,0,0,1,.224.794,1.514,1.514,0,0,1-1.512,1.512Zm0,0"
                          transform="translate(-224 -132.28)"
                          fill="#bebebe"
                        />
                        <rect
                          id="Retângulo_17167"
                          data-name="Retângulo 17167"
                          width="3.232"
                          height="11.31"
                          transform="translate(18.147 10.275)"
                          fill="#bebebe"
                        />
                        <rect
                          id="Retângulo_17168"
                          data-name="Retângulo 17168"
                          width="3.232"
                          height="3.232"
                          transform="translate(18.147 24.009)"
                          fill="#bebebe"
                        />
                      </g>
                    </svg>
                  )}
                </div>
                <span className="k-confirm-span">{span}</span>
                <p className="k-confirm-msg">{msg}</p>
                <span className="k-confirm-question">{question}</span>
              </div>
              <div className="k-confirm-footer">
                <NavLink
                  id="confirm-modal"
                  className={`k-confirm-yes is-primary-${brand}`}
                  onClick={agree}
                >
                  <div>{btnAccept}</div>
                </NavLink>
                <NavLink
                  id="cancel-modal"
                  className={`k-confirm-back is-primary-${brand}`}
                  onClick={onHide}
                >
                  <div>{btnRecuse}</div>
                </NavLink>
              </div>
            </div>
          </Modal.Body>
        </Modal>
      </>
    );
  }
}

KConfirm.description = "Componente de alerta de confirmação";
KConfirm.released = "v.0.1.62";
KConfirm.url = "Componentes/Moléculas/KConfirm";
export default KConfirm;
