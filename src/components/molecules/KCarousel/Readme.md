O conteúdo do Carousel é passado como elemento filho.
```
<KCarousel>
    <img src={'https://i.imgur.com/Pnx3SeU.png'} alt={'teste'} />
    <img src={'https://i.imgur.com/Pnx3SeU.png'} alt={'teste'} />
    <img src={'https://i.imgur.com/Pnx3SeU.png'} alt={'teste'} />
    <img src={'https://i.imgur.com/Pnx3SeU.png'} alt={'teste'} />
    <img src={'https://i.imgur.com/Pnx3SeU.png'} alt={'teste'} />
    <img src={'https://i.imgur.com/Pnx3SeU.png'} alt={'teste'} />
    <img src={'https://i.imgur.com/Pnx3SeU.png'} alt={'teste'} />
    <img src={'https://i.imgur.com/Pnx3SeU.png'} alt={'teste'} />
</KCarousel>
```

Com cor dos botões next e prev
```
<KCarousel arrowColor="coral">
    <img src={'https://i.imgur.com/Pnx3SeU.png'} alt={'teste'} />
    <img src={'https://i.imgur.com/Pnx3SeU.png'} alt={'teste'} />
    <img src={'https://i.imgur.com/Pnx3SeU.png'} alt={'teste'} />
    <img src={'https://i.imgur.com/Pnx3SeU.png'} alt={'teste'} />
</KCarousel>
```

Com posição dos pontos
```
<KCarousel dotsPosition="left">
    <img src={'https://i.imgur.com/Pnx3SeU.png'} alt={'teste'} />
    <img src={'https://i.imgur.com/Pnx3SeU.png'} alt={'teste'} />
    <img src={'https://i.imgur.com/Pnx3SeU.png'} alt={'teste'} />
    <img src={'https://i.imgur.com/Pnx3SeU.png'} alt={'teste'} />
</KCarousel>
```

Sem os pontos
```
<KCarousel showDots={ false }>
    <img src={'https://i.imgur.com/Pnx3SeU.png'} alt={'teste'} />
    <img src={'https://i.imgur.com/Pnx3SeU.png'} alt={'teste'} />
    <img src={'https://i.imgur.com/Pnx3SeU.png'} alt={'teste'} />
    <img src={'https://i.imgur.com/Pnx3SeU.png'} alt={'teste'} />
</KCarousel>
```

Sem os botões de next e prev
```
<KCarousel showArrows={ false }>
    <img src={'https://i.imgur.com/Pnx3SeU.png'} alt={'teste'} />
    <img src={'https://i.imgur.com/Pnx3SeU.png'} alt={'teste'} />
    <img src={'https://i.imgur.com/Pnx3SeU.png'} alt={'teste'} />
    <img src={'https://i.imgur.com/Pnx3SeU.png'} alt={'teste'} />
</KCarousel>
```
