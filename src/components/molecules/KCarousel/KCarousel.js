import React, { Component } from "react";
import PropTypes from "prop-types";
import KIcon from "../../atoms/KIcon";

class KCarousel extends Component {
    static defaultProps = {
      showArrows: true,
      arrowColor: "#DDDDDD",
      showDots: true,
      dotsPosition: "right"
    };

    static propTypes = {
      /** Boolean para indicar se os controles serão visíveis */
      showArrows: PropTypes.bool,
      /** Cor dos controles */
      arrowColor: PropTypes.string,
      /** Boolean para indicar se os contadores serão visíveis  */
      showDots: PropTypes.bool,
      /** Posição dos indicadores no componente (right, left) * */
      dotsPosition: PropTypes.string
    };

    constructor(props) {
      super(props);
      this.state = {
        activeItem: 0
      };

      this.nextItem = this.nextItem.bind(this);
      this.prevItem = this.prevItem.bind(this);
    }

    nextItem() {
      this.setState(prevState => {
        const newIndex = (prevState.activeItem === this.props.children.length)
          ? 0 : prevState.activeItem + 1;

        return {
          activeItem: newIndex
        };
      });
    }

    prevItem() {
      this.setState(prevState => {
        const newIndex = (prevState.activeItem === 0)
          ? this.props.children.length : prevState.activeItem - 1;

        return {
          activeItem: newIndex
        };
      });
    }

    render() {
      const carouselItems = this.props.children ? this.props.children.length === undefined ? [this.props.children] : this.props.children : [];
      const { activeItem } = this.state;
      const {
        showArrows,
        arrowColor,
        showDots,
        dotsPosition
      } = this.props;

      return (
        <div className="k-carousel">
          {
            showDots ? (
              <section className={`k-carousel-dots k-carousel-dots-${dotsPosition}`}>
                {
                  carouselItems && React.Children.map(carouselItems, (dot, index) => (
                    <span
                      key={index}
                      className={`k-carousel-dot${
                        index === activeItem ? " k-carousel-dot-active" : ""}`
                      }
                    />
                  ))
                }
              </section>
            ) : null
          }
          {
            showArrows ? (
              <>
                <button
                  className={`k-carousel-btn k-carousel-btn-left${
                    activeItem === carouselItems.length - 1 ? " d-none" : " d-inline-block"}`
                        }
                  onClick={() => this.nextItem()}
                  type="button"
                >
                  <KIcon
                    icon={["fas", "chevron-left"]}
                    size="2x"
                    color={arrowColor}
                  />
                </button>

                <button
                  className={`k-carousel-btn k-carousel-btn-right${
                    activeItem === 0 ? " d-none" : " d-inline-block"}`
                        }
                  onClick={() => this.prevItem()}
                  type="button"
                >
                  <KIcon
                    icon={["fas", "chevron-right"]}
                    size="2x"
                    color={arrowColor}
                  />
                </button>
              </>
            ) : null
          }

          <div className="k-carousel-col">
            <div className="k-carousel-cards-slider">
              <div
                className="k-carousel-cards-slider-wrapper"
                style={{
                  transform: `translateX(-${activeItem * (100 / carouselItems.length)}%)`
                }}
              >
                {
                  carouselItems && React.Children.map(carouselItems, (item, index) => (
                    <div key={index} className="k-carousel-card">
                      { item }
                    </div>
                  ))
                }
              </div>
            </div>
          </div>
        </div>
      );
    }
}

KCarousel.description = "Componente de Carousel";
KCarousel.released = "v.0.2.47";
KCarousel.url = "Componentes/Moléculas/KCarousel";
export default KCarousel;
