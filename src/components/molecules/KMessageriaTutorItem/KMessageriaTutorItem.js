import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";
import KIcon from "../../atoms/KIcon";
import KAvatar from "../../atoms/KAvatar";

class KMessageriaTutorItem extends Component {
  static defaultProps = {
    icon: ["fas", "mail"],
    title: "Daniel",
    subtitle: [],
    state: "primary",
    brand: "default",
    id: "k-messageria-tutor-item",
    image: undefined,
    onClick: () => {
    },
    cursor: undefined
  }

  static propTypes = {

    /** Nome do Tutor */
    title: PropTypes.string,

    /** Disciplinas do Tutor */
    subtitle: PropTypes.array,

    /** Configura a ação do sub-item * */
    onClick: PropTypes.func,

    /** Ícone do tutor */
    icon: PropTypes.any,

    /** Marca a ser utilizada */
    brand: PropTypes.string,

    /** O estado do botão é baseado no tema. Os valores aceitos são: 'primary', 'secondary', 'ternary', 'gradient', 'white', 'black', 'dark', 'light', 'grey', 'grey-dark', 'grey-lighter', 'success', 'warning', 'info', 'danger'  */
    state: PropTypes.string,

    /** Define o id único do componente */
    id: PropTypes.string,

    /** Define a imagem do componente */
    image: PropTypes.string,

    /** Define o style do componente */
    cursor: PropTypes.string
  };

  constructor(props) {
    super(props);
    this.state = {};
  }

  handleClick = event => {
    this.props.onClick(event);
  };

  render() {
    const {
      title, icon, subtitle, state, brand, active, id, image, cursor
    } = this.props;
    const isActive = active ? "is-active" : "";

    return (
      <div
        id={`k-messageria-tutor-${id}`}
        className="k-messageria-tutor"
        style={{ cursor }}
        onClick={this.handleClick}
      >

        <div
          id={`k-messageria-tutor-container-${id}`}
          className={`k-messageria-tutor__container ${isActive} is-${state}-${brand}`}
        >
          <div
            id={`k-messageria-tutor-left-wrapper-${id}`}
            className="k-messageria-tutor__left-wrapper"
            onClick={isActive ? this.handleClick : null}
          >
            <Fragment>
              <KAvatar
                width="30px"
                height="30px"
                src={image}
              />
            </Fragment>
          </div>

          <div
            id={`k-messageria-tutor-title-wrapper-${id}`}
            className="k-messageria-tutor__title-wrapper"
            onClick={isActive ? this.handleClick : null}
          >
            <h1 id={`k-messageria-tutor-title-${id}`} className={`is-${state}-${brand} ${isActive}`}>
              {title}
            </h1>
            <h2 id={`k-messageria-tutor-subtitle-${id}`} className="k-messageria-tutor__subtitle">
              {subtitle.map(element => (
                `${element} `
              ))}

            </h2>
          </div>
          <div
            id={`k-messageria-tutor-icon-wrapper-${id}`}
            className="k-messageria-tutor__icon-wrapper"
            onClick={this.handleClickIcon}
          >
            <div className="iconposition">
              <Fragment>
                <KIcon hover icon={icon} brand={brand} />
              </Fragment>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

KMessageriaTutorItem.description = "Componente para exibir os tutores no menu da messageria";
KMessageriaTutorItem.released = "v.0.1.88";
KMessageriaTutorItem.url = "Componentes/Moléculas/KMessageriaTutorItem";
export default KMessageriaTutorItem;
