Default:
```
<section>
    <KMessageriaTutorItem
      title='Daniel Aber Almeida'
      icon={['fas', 'envelope']}
      subtitle={['Sociedade e cidadania', 'Metodologias Ativas', 'Gestão Escolar']}
      />
</section>
```

Default with state and brand:
```
<section>
    <KMessageriaTutorItem
      state='secondary'
      brand='anhanguera'
      title='Daniel Aber Almeida'
      icon={['fas', 'envelope']}
      subtitle={['Aprendizado das Ciências Naturais']}/>
</section>
```

Default with left image:
```
<section>
    <KMessageriaTutorItem
      title='Daniel Aber Almeida'
      icon={['fas', 'envelope']}
      subtitle={['Democracia e Cidadania']}
      image='https://placeimg.com/120/120/people'/>
</section>
```

Default with onClick event:
```
<section>
    <KMessageriaTutorItem
      title='Daniel Aber Almeida'
      icon={['fas', 'envelope']}
      subtitle={['Democracia e Cidadania']}
      onClick={() => {alert('Item clicado')}}/>
</section>
```

Default with cursor type:
```
<section>
    <KMessageriaTutorItem
      cursor='pointer'
      title='Daniel Aber Almeida'
      icon={['fas', 'envelope']}
      subtitle={['Democracia e Cidadania']}/>
</section>
```