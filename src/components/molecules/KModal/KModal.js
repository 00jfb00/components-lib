import React, { PureComponent } from "react";
import Modal from "react-bootstrap/Modal";
import PropTypes from "prop-types";

class KModal extends PureComponent {
  static defaultProps = {
    id: "",
    openModal: false,
    onHide: undefined,
    size: "lg"
  };

  static propTypes = {
    /** Id do component modal */
    id: PropTypes.string,
    /** Boolean de abertura do modal */
    openModal: PropTypes.bool,
    /** Função de ocultar modal  */
    onHide: PropTypes.func,
    /** Tamanho do Modal (sm, lg, xl) * */
    size: PropTypes.string
  };

  render() {
    const {
      id,
      openModal,
      onHide,
      size
    } = this.props;

    return (
      <>
        <Modal
          id={`k-modal-${id}`}
          className="k-modal"
          show={openModal}
          onHide={onHide}
          centered
          size={size}
        >
          <div className="modal-content-container">
            {this.props.children}
          </div>
        </Modal>
      </>
    );
  }
}

KModal.description = "Componente de modal";
KModal.released = "v.0.1.62";
KModal.url = "Componentes/Moléculas/KModal";
export default KModal;
