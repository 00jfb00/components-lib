Default: Recebe o conteudo por children

```
import KButton from '../../atoms/KButton';

initialState = { isOpen: false };

<section>
    <KModal
      openModal={state.isOpen}
      onHide={() => {
            setState({isOpen: false})
        }}
    >
  <div style={{ padding: "20px", display: "flex"}}> 
  <p>Mussum ipsum cacilds, vidis litro abertis. Consetis adipiscings elitis. Pra lá , depois divoltis porris, paradis. Paisis, filhis, espiritis santis. Mé faiz elementum girarzis, nisi eros vermeio, in elementis mé pra quem é amistosis quis leo. Manduma pindureta quium dia nois paga. Sapien in monti palavris qui num significa nadis i pareci latim. Interessantiss quisso pudia ce receita de bolis, mais bolis eu num gostis.</p>
   </div>   
</ KModal>

    <KButton
      title="Modal!"
      onClick={() => { setState({isOpen: true}) } }/>

</section>
```
Tamanhos SM

```
import KButton from '../../atoms/KButton';

initialState = { isOpen: false };

<section>
    <KModal
      size="sm"
      openModal={state.isOpen}
      onHide={() => {
            setState({isOpen: false})
        }}
    >
    </ KModal>

    <KButton
      title="Modal SM!"
      onClick={() => { setState({isOpen: true}) } }/>

</section>

```
Tamanhos LG

```
import KButton from '../../atoms/KButton';

initialState = { isOpen: false };

<section>
    <KModal
      size="lg"
      openModal={state.isOpen}
      onHide={() => {
            setState({isOpen: false})
        }}
    >
    </ KModal>

    <KButton
      title="Modal LG!"
      onClick={() => { setState({isOpen: true}) } }/>

</section>

```
Tamanhos XL

```
import KButton from '../../atoms/KButton';

initialState = { isOpen: false };

<section>
    <KModal
      size="xl"
      openModal={state.isOpen}
      onHide={() => {
            setState({isOpen: false})
        }}
    >
    </ KModal>

    <KButton
      title="Modal XL!"
      onClick={() => { setState({isOpen: true}) } }/>

</section>

```
exmplo de aplicação de conteúdo

```
import KButton from '../../atoms/KButton';
import KIcon from "../../atoms/KIcon";
import KTag from "../../atoms/KTag";
import KRadio from "../../atoms/KOption";



initialState = { isOpen: false };

<section>
    <KModal
      openModal={state.isOpen}
      onHide={() => {
            setState({isOpen: false})
        }}
    >
  <div style={{ padding: "20px", display: "flex"}}>  
       <div style={{backgroundColor:"#EEF7FB", borderRadius:"100px", width:"30px",height:"30px", alignItems:"center",display:"flex",justifyContent:"center"}}> 
      <KIcon icon ="user-plus"  color="#0085CD"/>
      </div>
      

      <div>
         <div style={{color:"#0085CD",marginLeft:"10px",fontWeight:"bolder" }}>
           <h4>Selecione seus destinatários</h4>
           <p>Sociedade Brasileira e Cidadania</p>
         </div>
        <div style={{padding: "20px 10px"}}>
          <p><KTag textColor="#009AC4" bgColor="#E1FBFF" title="Tutores"/></p>
          <p><KRadio showBorder={false} /> Power Ranger Verde (dragon@powerranger.com.br)</p>
          <hr/>
          <p><KTag textColor="#E5A000" bgColor="#FFF7E6" title="Professores"/></p>
          <p><KRadio showBorder={false} /> Power Ranger Vermelho (tiranossauro@pr.com.br)</p>
          <p><KRadio showBorder={false} /> Power Ranger Amarelo (tigredsabre@pr.com.br)</p>
          <p><KRadio showBorder={false} /> Power Ranger Azul (triceratopis@por.com.br)</p>
        </div> 
      <hr />
        <div style={{display:"flex",flexDirection:"row", justifyContent:"space-between"}}>
         <p><KIcon icon ="check"  color="#00C773"/> 3 contatos selecionados</p>
         <KButton title="Confirmar"  onClick={() => { setState({isOpen: false}) }} />
        </div>
      </div> 
   </div>   
</ KModal>

    <KButton
      state="primary"
      title="Modal!"
      onClick={() => { setState({isOpen: true}) } }/>

</section>
```





