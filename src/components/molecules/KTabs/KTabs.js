import React, { Component } from "react";
import PropTypes from "prop-types";

/** As tabs organizam e permitem a navegação entre grupos de conteúdo relacionados ao mesmo nível de hierarquia. <br/>
 * Aceita como 'children' apenas 'k-tabs-item' */
class KTabs extends Component {
  static defaultProps = {
    index: 1
  }

  static propTypes = {
    /** Índice que será exibido* */
    index: PropTypes.number,
    /** Os filhos que serão renderizados* */
    children: PropTypes.any.isRequired
  }

  constructor(props) {
    super(props);

    this.state = {
      allowed: [{ name: "KTabsItem", order: 1 }],
      previousIndex: 1
    };
  }

  componentDidUpdate(prevProps) {
    if (prevProps.index !== this.props.index) {
      this.setState({ previousIndex: prevProps.index });
    }
  }

  prepareChildren = () => {
    const array = Array.isArray(this.props.children)
      ? this.props.children
      : [this.props.children];

    return array.filter(i => this.state.allowed.find(elm => elm.name === i.type.element));
  }

  render() {
    const { index, id } = this.props;
    const { previousIndex } = this.state;
    const filter = this.prepareChildren();

    return (
      <div id={id} className="k-tabs">
        {filter.map((item, ind) => React.cloneElement(item, {
          key: ind,
          id: `${id ? `${id}-${ind + 1}` : `k-tab-item-${ind + 1}`}`,
          className: `
                ${
                  ind + 1 === index ? "-item-show" : "-item-hide"
                } tab-child-item-${ind}
                ${
                  previousIndex !== index && index > previousIndex
                    ? "-next"
                    : "-previous"
                }
              `
        }))}
      </div>
    );
  }
}

KTabs.description = "As tabs organizam e permitem a navegação entre grupos de conteúdo relacionados ao mesmo nível de hierarquia.";
KTabs.released = "v.0.1.42";
KTabs.url = "Componentes/Moléculas/KTabs";
export default KTabs;
