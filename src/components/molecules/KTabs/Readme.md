Uso:
```
import KQuestion from '../KQuestion';
import KTabsItem from '../KTabsItem';
import KButton from '../../atoms/KButton';

initialState = { index: 1 };

<section>
    <KButton state="gradient" disabled={state.index === 1} title="Primeira" onClick={() => {
        setState({index: 1})
    }}/>&emsp;
    <KButton state="gradient" disabled={state.index === 1} title="Anterior" onClick={() => {
        setState({index: state.index - 1})
    }}/>&emsp;
    <KButton state="gradient" disabled={state.index === 4} title="Proximo" onClick={() => {
        setState({index: state.index + 1})
    }}/>&emsp;
    <KButton state="gradient" disabled={state.index === 4} title="Última" onClick={() => {
        setState({index: 4})
    }}/>
        
    <KTabs index={state.index}>
        <KTabsItem>
             <KQuestion 
                title={<p style={{ color: '#3F9BD6', fontWeight: 'bold', fontSize: 18 }}>Questão 1</p>}
                subtitle={<p style={{ color: '#3F9BD6' }}>Ética e Responsabilidade</p>}
                body={<p style={{ color: '#777' }}>Qual o melhor ava do mundo?</p>}
                options={[
                    {content: 'Este ava aqui', state: 'warning'},
                    {content: 'Este ava aqui'},
                    {content: 'O ava que vamos construir juntinhos', state: 'danger'},
                    {content: 'O ava com tempo de resposta em 200 milisegundos. O ava com tempo de resposta em 200 milisegundos. O ava com tempo de resposta em 200 milisegundos', selected: true, state: 'success'}
                ]}
             />
        </KTabsItem>
        <KTabsItem>
             <KQuestion 
                title={<p style={{ color: '#3F9BD6', fontWeight: 'bold', fontSize: 18 }}>Questão 2</p>}
                subtitle={<p style={{ color: '#3F9BD6' }}>Ética e Responsabilidade</p>}
                body={<p style={{ color: '#777' }}>Qual o melhor ava do mundo?</p>}
                options={[
                    {content: 'Este ava aqui', state: 'warning'},
                    {content: 'Este ava aqui'},
                    {content: 'O ava que vamos construir juntinhos', state: 'danger'},
                    {content: 'O ava com tempo de resposta em 200 milisegundos', selected: true, state: 'success'}
                ]}
             />
        </KTabsItem>
        <KTabsItem>
            <KQuestion 
                title={<p style={{ color: '#3F9BD6', fontWeight: 'bold', fontSize: 18 }}>Questão 3</p>}
                subtitle={<p style={{ color: '#3F9BD6' }}>Ética e Responsabilidade</p>}
                body={<p style={{ color: '#777' }}>Qual o melhor ava do mundo?</p>}
                options={[
                    {content: 'Este ava aqui', state: 'warning'},
                    {content: 'Este ava aqui'},
                    {content: 'O ava que vamos construir juntinhos', state: 'danger'},
                    {content: 'O ava com tempo de resposta em 200 milisegundos', selected: true, state: 'success'}
                ]}
             />
        </KTabsItem>
        <KTabsItem>
            <KQuestion 
                title={<p style={{ color: '#3F9BD6', fontWeight: 'bold', fontSize: 18 }}>Questão 4</p>}
                subtitle={<p style={{ color: '#3F9BD6' }}>Ética e Responsabilidade</p>}
                body={<p style={{ color: '#777' }}>Qual o melhor ava do mundo?</p>}
                options={[
                    {content: 'Este ava aqui', state: 'warning'},
                    {content: 'Este ava aqui'},
                    {content: 'O ava que vamos construir juntinhos', state: 'danger'},
                    {content: 'O ava com tempo de resposta em 200 milisegundos', selected: true, state: 'success'}
                ]}
             />
        </KTabsItem>        
    </KTabs>
</section>
```
