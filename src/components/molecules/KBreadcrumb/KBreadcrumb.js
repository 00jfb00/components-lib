import React, { Component } from "react";
import PropTypes from "prop-types";
import KIcon from "../../atoms/KIcon";
import KText from "../../atoms/KText";

class KBreadcrumb extends Component {
  static defaultProps = {
    icon: "circle",
    fontSize: "20px",
    iconSize: 8,
    font: "Nunito Sans",
    fontWeight: "regular",
    state: "dark",
    brand: "default",
    isMobile: false,
    color: undefined
  }

  static propTypes = {
    /** Lista de subítens */
    routes: PropTypes.array.isRequired,
    /** Ícone a ser exibido */
    icon: PropTypes.string,
    /** Tamanho do texto que será exibido */
    fontSize: PropTypes.string,
    /** Tamanho do icon que será exibido */
    iconSize: PropTypes.number,
    /** Tamanho do texto que será exibido */
    font: PropTypes.string,
    /** Especifica o peso ou a intensidade da fonte */
    fontWeight: PropTypes.string,
    /** Cor do breadcrumb que será exibido, Color Codes:(Color Name, Hex Code RGB e Decimal Code RGB) */
    color: PropTypes.string,
    /** O estado do texto é baseado no tema. Os valores aceitos são: 'primary', 'secondary', 'ternary', 'white', 'black', 'dark', 'light', 'grey', 'grey-dark', 'grey-lighter', 'success', 'warning', 'info', 'danger' * */
    state: PropTypes.string,
    /** O estado do texto é baseado na marca. Os valores aceitos são: 'default', 'anhanguera' * */
    brand: PropTypes.string,
    /** O estado do texto é baseado na marca. Os valores aceitos são: 'default', 'anhanguera' * */
    isMobile: PropTypes.bool
  }

  constructor(props) {
    super(props);
    this.state = {
      breadcrumb: []
    };
    this.createBreadcrumb = this.createBreadcrumb.bind(this);
  }

  componentDidMount() {
    this.createBreadcrumb(this.props);
  }

  componentDidUpdate(prevProps) {
    if (this.props !== prevProps) {
      this.createBreadcrumb(this.props);
    }
  }

  createBreadcrumb = () => {
    const {
      routes,
      icon,
      fontSize,
      fontWeight,
      color,
      font,
      state,
      brand,
      iconSize,
      isMobile
    } = this.props;
    const array = [];
    for (let i = 0; i < routes.length; i++) {
      array.push(
        <KText
          key={i}
          state={state}
          brand={brand}
          text={routes[i]}
          font={font}
          textColor={color}
          fontSize={fontSize}
          fontWeight={fontWeight}
        />,
      );
      if (i < routes.length - 1 && !isMobile) {
        array.push(
          <KIcon
            key={`${i}icon`}
            state={state}
            brand={brand}
            icon={icon}
            color={color}
            size="lg"
            style={{ width: `${iconSize}px` }}
          />,
        );
      }
    }
    this.setState({ breadcrumb: array });
  }

  render() {
    const { isMobile } = this.props;

    return (
      <div
        style={isMobile ? { flexDirection: "column", alignItems: "end" } : {}}
      >
        {this.state.breadcrumb}
      </div>
    );
  }
}

KBreadcrumb.description = "Componente Breadcrumb para o header";
KBreadcrumb.released = "v.0.1.23";
KBreadcrumb.url = "Componentes/Moléculas/KBreadcrumb";
export default KBreadcrumb;
