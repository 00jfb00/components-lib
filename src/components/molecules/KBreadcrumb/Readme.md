Rotas:

```
<section>
    <KBreadcrumb iconSize={5} fontSize="13px" font="Open Sans" icon="circle" routes={['Sala Digital','Sociedade Brasileira e Cidadania']}/>
    <KBreadcrumb iconSize={5} fontSize="22px" font="Nunito Sans" fontWeight="bold" icon="chevron-right" routes={['Unidade 1. Ética e Política Brasileira','1.1 Porque pensar em ética?']}/>
</section>
```

estados:

```
<section>
    <KBreadcrumb state="primary" brand="default" fontSize="13px" font="Open Sans" icon="circle" routes={['Sala Digital','Sociedade Brasileira e Cidadania']}/>
    <KBreadcrumb state="primary" brand="anhanguera" fontSize="22px" font="Nunito Sans" fontWeight="bold" icon="chevron-right" routes={['Unidade 1. Ética e Política Brasileira','1.1 Porque pensar em ética?']}/>
</section>
```

mobile:

```
<section>
    <KBreadcrumb isMobile state="primary" brand="default" fontSize="13px" font="Open Sans" icon="circle" routes={['Sala Digital','Sociedade Brasileira e Cidadania']}/>
    <KBreadcrumb isMobile state="primary" brand="anhanguera" fontSize="22px" font="Nunito Sans" fontWeight="bold" icon="chevron-right" routes={['Unidade 1. Ética e Política Brasileira','1.1 Porque pensar em ética?']}/>
</section>
```
