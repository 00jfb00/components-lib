import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import KIcon from "../../atoms/KIcon";

class KChat extends PureComponent {
  static propTypes = {
    /** Define se exibe o contador */
    showCounter: PropTypes.bool,
    /** Define largura do componente */
    width: PropTypes.string,
    /** Define altira do componente */
    height: PropTypes.string
  }

  static defaultProps = {
    showCounter: false,
    width: "60px",
    height: "60px"
  }

  render() {
    const { showCounter, width, height } = this.props;
    const customStyleKChat = { width, height };

    return (
      <div className="k-chat" style={customStyleKChat}>
        { showCounter && (
        <div className="k-chat__counter">
              2
        </div>
        )}
        <KIcon
          icon={["far", "comment-alt"]}
          color="#fff"
          brand="default"
          size="2x"
          className="k-chat__img"
        />
      </div>
    );
  }
}

KChat.description = "Componente para o chat flutuante";
KChat.released = "v.0.1.1";
KChat.url = "Componentes/Moléculas/KChat";

export default KChat;
