Default:  
Recebe um Elemento ou Componente acionador  
O conteúdo do dropdown é passado como elemento filho
```

<KDropdown 
    triggerComponent={(<button>Elemento acionador do Dropdown</button>)}
    styleContent={{minWidth: '288px'}}>
    <div style={{
        backgroundColor: '#fff', 
        padding: '10px', 
        borderRadius: '4px',
        boxShadow: '5px 5px 25px 0px rgba(46, 61, 73, 0.2)'}}>
        <h1>Conteúdo do Dropdown</h1>
        <ul>
            <li>Item 1</li>
            <li>Item 2</li>
            <li>Item 3</li>
            <li>Item 4</li>
        </ul>
    </div>
</KDropdown>

```

Elemento acionador diferente:  
Recebe um Elemento ou Componente acionador  
O conteúdo do dropdown é passado como elemento filho
```

<KDropdown 
    triggerComponent={(
        <img src="https://s3-us-west-2.amazonaws.com/estudante-production/app/public/ckeditor_assets/pictures/128/original/anhanguera_full.svg"
        style={{maxWidth: '150px'}} />
    )}
    styleContent={{minWidth: '288px'}}
    contentPositionTop='50px'>
    
    <div style={{
        backgroundColor: '#fff', 
        padding: '10px', 
        borderRadius: '4px',
        boxShadow: '5px 5px 25px 0px rgba(46, 61, 73, 0.2)'}}>
        <h1>Conteúdo do Dropdown</h1>
        <p>Lorem Ipsum is simply dummy text of the printing 
        and typesetting industry. Lorem Ipsum has been the 
        industry's standard dummy text ever since the 1500s, 
        when an unknown printer took a galley of type and 
        scrambled it to make a type specimen book.</p>
    </div>
</KDropdown>

```
