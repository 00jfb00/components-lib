import React, { Component } from "react";
import PropTypes from "prop-types";
import NavLink from "react-bootstrap/NavLink";

class KDropdown extends Component {
  static defaultProps = {
    styleContent: {},
    contentPositionLeft: "0",
    contentPositionRight: "0",
    numberOfItems: 0,
    changeProps: undefined,
    contentPositionTop: undefined,
    id: "k-dropdown"
  }

  static propTypes = {
    /** Permite adicionar um id para o Componente * */
    id: PropTypes.string,
    /** Elemento/Componente que aciona o conteúdo dropdown * */
    triggerComponent: PropTypes.object.isRequired,
    /** Número de ítens visíveis * */
    numberOfItems: PropTypes.number,
    /** Objeto que permite customizar o estilo do conteúdo * */
    styleContent: PropTypes.object,
    /** Determina a posição do container de conteúdo à partir do topo * */
    contentPositionTop: PropTypes.string,
    /** Determina a posição do container de conteúdo à partir da esquerda * */
    contentPositionLeft: PropTypes.string,
    /** Determina a posição do container de conteúdo à partir da direita * */
    contentPositionRight: PropTypes.string,
    /** Função de callback para alterar props * */
    changeProps: PropTypes.func
  }

  constructor(props) {
    super(props);
    this.state = {
      expanded: false,
      contentPositionTop: "30px"
    };
    this.handleClick = this.handleClick.bind(this);
    this.handleDocumentClick = this.handleDocumentClick.bind(this);
    this.triggerElement = React.createRef();
  }

  componentDidMount() {
    document.addEventListener("click", this.handleDocumentClick);
    const { contentPositionTop } = this.props;
    let triggerElementHeight = 30;
    if (this.triggerElement.current) {
      triggerElementHeight = this.triggerElement.current.offsetHeight;
    }
    this.setState({
      contentPositionTop: contentPositionTop || `${triggerElementHeight}px`
    });
  }

  componentWillUnmount() {
    document.removeEventListener("click", this.handleDocumentClick);
  }

  handleClick() {
    this.setState(prevState => ({
      expanded: !prevState.expanded
    }));
  }

  handleDocumentClick(event) {
    const { changeProps } = this.props;
    let targetElement = event.target;
    let clickTrigger = false;
    do {
      if (targetElement.className === "k-dropdown__trigger") {
        clickTrigger = true;
        break;
      }
      if (targetElement.className === "k-dropdown nav-link") {
        clickTrigger = true;
        break;
      }
      targetElement = targetElement.parentNode;
    } while (targetElement);
    if (!clickTrigger && this.state.expanded) {
      this.setState({
        expanded: false
      });
      if (changeProps !== undefined) {
        changeProps();
      }
    }
  }

  render() {
    const { expanded, contentPositionTop } = this.state;
    const {
      id,
      styleContent,
      children,
      triggerComponent,
      contentPositionLeft,
      contentPositionRight,
      numberOfItems
    } = this.props;

    return (
      <NavLink id={id} className="k-dropdown" onClick={this.handleClick}>
        <div className="k-dropdown__trigger" ref={this.triggerElement}>
          {triggerComponent}
        </div>
        <div
          className={`k-dropdown__content-wrapper${
            expanded ? " expanded" : ""
          }`}
          style={{
            top: contentPositionTop,
            left: contentPositionLeft,
            right: contentPositionRight,
            maxHeight: numberOfItems < 1 ? "none" : 50 * numberOfItems
          }}
        >
          <div className="k-dropdown__content" tabIndex={expanded ? undefined : -1} style={styleContent}>
            {children}
          </div>
        </div>
      </NavLink>
    );
  }
}

KDropdown.description = "Componente de dropdown";
KDropdown.released = "v.0.1.27";
KDropdown.url = "Componentes/Moléculas/KDropdown";
KDropdown.element = "KDropdown";
KDropdown.orderElement = 4;

export default KDropdown;
