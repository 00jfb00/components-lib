import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";
import ListGroup from "react-bootstrap/ListGroup";
import { Button } from "react-bootstrap";
import KDropdown from "../KDropdown";
import KInputText from "../../atoms/KInputText";
import KIcon from "../../atoms/KIcon";

class KSelect extends Component {
  static defaultProps = {
    list: [],
    funcClick: () => {
    },
    numberOfItems: 5,
    onSelectItem: () => {
    },
    id: "k-menu-list",
    initialIndex: null,
    placeholder: "",
    icon: "chevron-down",
    required: false,
    messageError: undefined
  };

  static propTypes = {
    /** Id que será exibido na div * */
    id: PropTypes.string,
    /** Lista que será exibido no menu * */
    list: PropTypes.any,
    /** Número de ítens visíveis * */
    numberOfItems: PropTypes.number,
    /** Função de callback do click * */
    funcClick: PropTypes.func,
    /** Função de callback para objetos * */
    onSelectItem: PropTypes.func,
    /** Valor do índice de início ou -1 para nenhum valor* */
    initialIndex: PropTypes.number,
    /** Define o placeholder do KSelect* */
    placeholder: PropTypes.string,
    /** Define o icone do KSelect* */
    icon: PropTypes.string,
    /** Define se campo é obrigatório * */
    required: PropTypes.bool,
    /** Define a mensagem de erro que aparece quando o campo como required* */
    messageError: PropTypes.string
  };

  constructor(props) {
    super(props);
    const { initialIndex } = this.props;
    this.state = {
      selected: this.generateInitialValue(initialIndex)
    };
  }

  generateInitialValue = initialIndex => {
    if (initialIndex === null) {
      return "";
    }
    const { list } = this.props;
    if (typeof list[initialIndex] === "object") {
      return list[initialIndex].label;
    }

    return list[initialIndex];
  }

  generateListItem = (element, index) => {
    const { id } = this.props;

    return (
      <Button
        className="list-group-item list-group-item-action"
        key={`${id}-${index}`}
        id={`${id}-${index}`}
        onClick={() => {
          this.handlerSelect(element, index);
        }}
      >
        {typeof element === "object" ? element.label : element}
      </Button>
    );
  }

  handlerSelect = (element, index) => {
    this.setState({
      selected: typeof element === "object" ? element.label : element
    }, () => {
      this.props.funcClick(index);
      if (typeof element === "object") {
        element.index = index;
        this.props.onSelectItem(element);
      }
    });
  };

  UNSAFE_componentWillReceiveProps(nextProps) {
    this.setState(prevState => {
      let { selected } = prevState;
      if (nextProps.initialIndex !== null) {
        if (nextProps.initialIndex === -1) {
          selected = "";
        } else if (typeof nextProps.list[nextProps.initialIndex] === "object") {
          selected = nextProps.list[nextProps.initialIndex].label;
        } else {
          selected = nextProps.list[nextProps.initialIndex];
        }
      }

      return { selected };
    });
  }

  render() {
    const {
      id, list, numberOfItems, placeholder, icon, messageError, required
    } = this.props;
    const { selected } = this.state;

    return (
      <div id={id} className={`k-select ${required ? "required" : ""}`}>
        <Fragment>
          <KDropdown
            numberOfItems={numberOfItems}
            triggerComponent={(
              <KInputText
                placeholder={selected || placeholder}
                backgroundColor="#fff"
                rounded={false}
                disabled
                borderColor="#9A9A9A"
                iconColor="#9A9A9A"
                iconSize="1x"
                borderWidth={1}
                rightIcon={icon}
                onRightIconClick={() => {
                }}
              />
            )}
            contentPositionTop="38px"
          >
            <ListGroup className="k-select-list">
              {list.map((element, index) => (
                this.generateListItem(element, index)
              ))}
            </ListGroup>
          </KDropdown>
          {
            required && (
              <div className="error">
                <KIcon icon="exclamation-triangle" size="1x" />
                <div className="text">{messageError}</div>
              </div>
            )
          }
        </Fragment>
      </div>
    );
  }
}

KSelect.description = "Componemte select";
KSelect.released = "v.0.2.7";
KSelect.url = "Componentes/Moléculas/KSelect";
export default KSelect;
