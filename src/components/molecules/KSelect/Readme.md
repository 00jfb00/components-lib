Select:

```
<section>
  <KSelect id={'select-1'}  list={['1','2']} funcClick={(element) => console.log(`você clicou no elemento ${element}`)}/>
</section>
```
Select com erro:

```
<section>
  <KSelect required messageError="Selecione uma opção" id={'select-1'}  list={['1','2']} funcClick={(element) => console.log(`você clicou no elemento ${element}`)}/>
</section>
```
Initial Index:

```
// Lista de itens
const list = [
  "Bioquímica",
  "Construindo uma Carreira de Sucesso: Saúde",
  "Genética",
  "Ciências Morfofuncionais dos Sistemas Digestório Endócrino e Renal",
  "Ciências Morfofuncionais dos Sistemas Tegumentar Reprodutor e Locomotor"
];

<KSelect 
  id={'select-2'} 
  initialIndex={2}
  list={list}
  funcClick={
    (element) => console.log(`você clicou no elemento ${element}`)
  }
/>
```
With objects:

```
// Lista de itens
const list = [
  {label: "Bioquímica", shortname: "BIO_TESTE"},
  {label: "Construindo uma Carreira de Sucesso: Saúde"},
  {label: "Genética"},
  {label: "Ciências Morfofuncionais dos Sistemas Digestório Endócrino e Renal"},
  {label: "Ciências Morfofuncionais dos Sistemas Tegumentar Reprodutor e Locomotor"}
];

<KSelect 
  id={'select-3'} 
  initialIndex={2}
  list={list}
  onSelectItem = {element => console.log(element)}
/>
```
