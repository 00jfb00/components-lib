import React, { Component, Fragment } from "react";
import ReactDOM from "react-dom";

/** Um organismo principal que monta a estrutura do container e garante a responsividade. <br/>
 * Aceita como 'children' apenas 'k-header', 'k-side', 'k-drawer', 'k-body' e 'overlay items'.
 * Renderiza multiplos OverlayItems, KSide e KDrawer, mas apenas um KBody, um KHeader */
class KMain extends Component {
  constructor(props) {
    super(props);
    this.state = {
      top: 0,
      headerFixed: true,
      allowed: [
        { name: "KSide", order: 0 },
        { name: "KBody", order: 1 }
      ]
    };
  }

  componentDidMount() {
    this.setTop();
  }

  componentDidUpdate() {
    this.setTop();
  }

  setTop = () => {
    const children = this.prepareChildren();
    const hasHeader = children.find(elm => elm.type.element === "KHeader");
    if (hasHeader) {
      const headerDOM = ReactDOM.findDOMNode(this.refs.ref_header_main);
      if (this.state.headerFixed && this.refs.ref_header_main && this.refs.ref_header_main.props && this.refs.ref_header_main.props.fixed === false) {
        this.setState({ headerFixed: false });
      }
      if (headerDOM && headerDOM.offsetHeight !== 0 && this.state.top !== headerDOM.offsetHeight) {
        this.setState({ top: headerDOM.offsetHeight });
      }
    } else if (this.state.top !== 0) {
      this.setState({ top: 0 });
    }
  }

  prepareChildren = () => ((this.props.children === undefined) ? [] : (Array.isArray(this.props.children)) ? this.props.children : [this.props.children]);

  render() {
    const { allowed, top, headerFixed } = this.state;

    const children = this.prepareChildren();
    let foundKBody = false;

    const allowedElements = children.filter(i => {
      const bool = i.type.element !== "KBody" || (i.type.element === "KBody" && !foundKBody);
      foundKBody = foundKBody || i.type.element === "KBody";
      if (!bool) return false;

      return allowed.find(elm => elm.name === i.type.element);
    });

    const hasHeader = children.find(elm => elm.type.element === "KHeader");
    const hasDrawer = children.filter(elm => elm.type.element === "KDrawer");
    const hasOverlayItems = children.filter(elm => elm.type.element === "OverlayItems");

    return (
      <Fragment>
        <div
          id="k-main-header"
          className={`
            k-main-header 
            ${(headerFixed) ? "-is-fixed" : ""}`
          }
          style={{ height: (hasHeader) ? top : 0 }}
        >
          {(hasHeader) ? (React.cloneElement(hasHeader, { ref: "ref_header_main" })) : ""}
        </div>

        <div
          id="k-main"
          className="k-main"
          style={{
            top: (hasHeader && headerFixed) ? top : 0,
            height: (hasHeader && headerFixed) ? `calc(100vh - ${top}px)` : "100vh"
          }}
        >
          {
            allowedElements.map((item, index) => (React.cloneElement(item, {
              key: index,
              height: (hasHeader && headerFixed) ? top : null
            })))
          }
        </div>

        {hasDrawer}
        {hasOverlayItems}

      </Fragment>

    );
  }
}

KMain.description = "Container principal para ajustar layout e responsividade";
KMain.released = "v.0.1.22";
KMain.url = "Componentes/Organismos/KMain";
export default KMain;
