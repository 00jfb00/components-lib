KMain:
```
import KHeader from '../../molecules/KHeader';
import KSide from '../KSide';
import KBody from '../KBody';

<section>
  <KMain>
    <KHeader />
    <KHeader />
    <KSide width={280}><div style={{ backgroundColor: 'yellow', width: '100%', height: '200vh' }} /></KSide>
    <KBody><div style={{ backgroundColor: 'blue', width: '100%', height: '100%' }} /></KBody>
    <KBody><div style={{ backgroundColor: 'green', width: '100%', height: '100%' }} /></KBody>
    <KSide width={180}><div style={{ backgroundColor: 'red', width: '100%', height: '200vh' }} /></KSide>
  </KMain>
</section>
```

Como usar:
```
// Exemplo descrito na página de 'Uso' da biblioteca.
```

Renderizar seu  próprio componente: <br/>
Obs: Caso precise passar seu próprio componente de header ou drawer, <br/> 
antes de exportá-lo atribua a propriedade 'element' a classe para que o KMain possa fazer a renderização.

```
// AvaDrawerTree.element = "KDrawer"
// export default withRouter(AvaDrawer);

// AvaHeader.element = "KHeader"
// export default withRouter(AvaHeader);

// AvaHeader.element = "KSide"
// export default withRouter(AvaSidebar);

// AvaHeader.element = "OverlayItems"
// export default withRouter(AvaOverlayItems);
```


