import React, { Component } from "react";
import PropTypes from "prop-types";

/** Organismo principal que monta a estrutura do corpo de toda a aplicação.* */
class KBody extends Component {
  static defaultProps = {
    height: undefined,
    brand: "default",
    state: "primary"
  };

  static propTypes = {
    /** Altura padrão do corpo (Recebe automáticamente do KMain) * */
    height: PropTypes.any,
    /** Estado * */
    state: PropTypes.string,
    /** Marca * */
    brand: PropTypes.string
  };

  render() {
    const {
      children, height, state, brand
    } = this.props;

    return (
      <div
        ref="k_body"
        id="k-body"
        className={`k-body is-${state}-${brand}`}
        style={{
          width: "100%",
          height: (height) ? `calc(100vh - ${height}px)` : "100vh"
        }}
      >
        {children}
      </div>
    );
  }
}

KBody.description = "Corpo do projeto para ativação da responsividade";
KBody.released = "v.0.1.22";
KBody.url = "Componentes/Organismos/KBody";
KBody.element = "KBody";

export default KBody;
