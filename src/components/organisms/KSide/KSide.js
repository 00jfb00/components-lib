import React, { Component } from "react";
import PropTypes from "prop-types";

/** Organismo principal que monta a estrutura do SideBar.* */
class KSide extends Component {
  static defaultProps = {
    height: undefined,
    brand: "default",
    state: "primary"
  };

  static propTypes = {
    /** Largura padrão do sidebar (Recebe automáticamente do KMain) * */
    width: PropTypes.number.isRequired,
    /** Altura padrão do sidebar (Recebe automáticamente do KMain) * */
    height: PropTypes.any,
    /** Marca * */
    state: PropTypes.string,
    /** Estado * */
    brand: PropTypes.string
  };

  render() {
    const {
      children, width, height, state, brand
    } = this.props;

    return (
      <div
        ref="k-side"
        id="k-side"
        className={`k-side is-${state}-${brand}`}
        style={{
          width,
          minWidth: width,
          height: (height) ? `calc(100vh - ${height}px)` : "100vh"
        }}
      >
        {children}
      </div>
    );
  }
}

KSide.description = "Container para renderização do sidebar";
KSide.released = "v.0.1.22";
KSide.url = "Componentes/Organismos/KSide";
KSide.element = "KSide";

export default KSide;
