import React from "react";
import TestRenderer from "react-test-renderer";
import wait from "waait";
import KButton from "../components/atoms/KButton/KButton";

describe("KButton component", () => {
  it("Is the onClick method working?", () => {
    let click = false;
    const component = TestRenderer.create(
      <KButton
        onClick={() => {
          click = true;
        }}
      >
        Funcionou
      </KButton>,
    );
    const instance = component.getInstance();
    expect(click).toBe(false);
    expect(instance.props.children).toBeDefined();
    instance.handleClick();
    expect(click).toBe(true);
  });

  it("Is outined?", async () => {
    const component = TestRenderer.create(<KButton outline />);
    await wait(0);
    await component.getInstance().loadingPromise;
    expect(component.toJSON().props.className).toBe("p3 k-button is-primary-default is-outlined btn btn-primary btn-medium");
  });

  it("Is iconBackground?", async () => {
    const component = TestRenderer.create(<KButton iconBackground />);
    await wait(0);
    await component.getInstance().loadingPromise;
    expect(component.toJSON().props.className).toBe("p3 k-button is-primary-default is-iconBackground btn btn-primary btn-medium");
  });

  it("With icon?", async () => {
    const rightIcon = TestRenderer.create(<KButton rightIcon="home" />);
    await wait(0);
    await rightIcon.getInstance().loadingPromise;
    expect(rightIcon.toJSON().children[1].type).toBe("svg");

    const leftIcon = TestRenderer.create(<KButton leftIcon="home" />);
    await wait(0);
    await leftIcon.getInstance().loadingPromise;
    expect(leftIcon.toJSON().children[0].type).toBe("svg");
  });

  it("With icon only?", async () => {
    const rightIcon = TestRenderer.create(<KButton iconOnly rightIcon="home" />);
    await wait(0);
    await rightIcon.getInstance().loadingPromise;
    expect(rightIcon.toJSON().children[1].type).toBe("svg");

    const leftIcon = TestRenderer.create(<KButton iconOnly leftIcon="home" />);
    await wait(0);
    await leftIcon.getInstance().loadingPromise;
    expect(leftIcon.toJSON().children[0].type).toBe("svg");
  });

  it("Is border?", async () => {
    const component = TestRenderer.create(<KButton outline border={false} />);
    await wait(0);
    await component.getInstance().loadingPromise;
    expect(component.toJSON().props.className).toBe("p3 k-button is-primary-default is-outlined not-border btn btn-primary btn-medium");
  });

  it("Is button email?", async () => {
    const component = TestRenderer.create(<KButton noRadius />);
    await wait(0);
    await component.getInstance().loadingPromise;
    expect(component.toJSON().props.className).toBe("p3 k-button is-primary-default not-radius btn btn-primary btn-medium");
  });
});
