import React from "react";
import wait from "waait";
import { configure, mount } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import KVideoPlayerMdStrm from "../components/atoms/KVideoPlayerMdStrm";

configure({ adapter: new Adapter() });

describe("Is the KVideoPlayerMdStrm component working?", () => {
  beforeEach(() => {
    jest.setTimeout(20000);
  });

  afterEach(() => {
    document.getElementsByTagName("html")[0].innerHTML = "";
  });

  it("Is it rendering video-player?", async () => {
    const component = mount(
      <KVideoPlayerMdStrm
        onDuration={() => {}}
        onProgress={() => {}}
        onEnded={() => {}}
        autoPlay
        mediaId="5ed538951a5483074fda0173"
      />, { attachTo: document.body }
    );
    await wait(0);
    const instance = component.instance();
    await instance.loadingPromise;

    instance.player._isReady = true;
    instance.player._events.onTimeUpdate(1);
    instance.player.videoPlay();
    instance.player._isPlaying = true;
    instance.player._events.onTimeUpdate(2);
    instance.player._events.onTimeUpdate(3);
    instance.player._events.onPlay();
    instance.player._events.onVideoEnd();
    instance.handleOnVideoEnd();
    instance.props.onEnded({});
    instance.player._events.onSeeked(1);
  });

  it("Is it rendering video-player no callbacks?", async () => {
    const component = mount(
      <KVideoPlayerMdStrm
        autoPlay
        mediaId="youtube:IGQBtbKSVhY"
      />, { attachTo: document.body }
    );
    await wait(0);
    let instance = component.instance();
    await instance.loadingPromise;

    instance.player._isReady = true;
    instance.player._events.onTimeUpdate(1);
    instance.state.lastProgressCallback = 2;
    instance.player.videoPlay();
    instance.player._isPlaying = true;
    instance.player._events.onTimeUpdate(2);
    instance.player._events.onTimeUpdate(3);
    instance.player._events.onPlay();
    instance.player._events.onVideoEnd();
    instance.handleOnVideoEnd();
    instance.props.onEnded({});
    instance.player._events.onSeeked(1);
    component.update(
      <KVideoPlayerMdStrm
        width={640}
        height={360}
        mediaId="youtube:IGQBtbKSVhY"
      />, { attachTo: document.body }
    );
    await wait(0);
    instance = component.instance();
    await instance.loadingPromise;
  });

  it("Is it rendering error video-player?", async () => {
    const component = mount(
      <KVideoPlayerMdStrm
        autoPlay
        seek={246}
        mediaId={null}
      />
    );
    await wait(0);
    await component.instance().loadingPromise;
    expect(component).toBeDefined();
  });
});
