import React from "react";
import TestRenderer from "react-test-renderer";
import KMessage from "../components/molecules/KMessage/KMessage";

describe("Is the KConfirm component working?", () => {
  it("Is it rendering without props?", async () => {
    const component = TestRenderer.create(<KMessage />);
    expect(component).toBeDefined();
  });
  it("Is it rendering with all props?", async () => {
    const component = TestRenderer.create(
      <KMessage
        ClickChip={() => {}}
        listAttachments={[{
          _id: "5eb177555537670012445b91", fileName: "274eron%20(13)%20(1).pdf", status: "success", size: "25"
        }, { _id: "5eb177555537670012445b91", fileName: "teste2.pdf", status: "success" }]}
        title="Sejam bem-vindos ao Estudos Dirigidos!"
        src={`https://api.adorable.io/avatars/225/abost${new Date()}@addrable.png`}
        author="Hal Kemp Ferreira"
        discipline="Construindo uma Carreira de Sucesso: Saúde (ED_CSS)"
        date="feira, 35 de janeiro de 2, 25:72"
        message={<div>test</div>}
      />
    );
    expect(component).toBeDefined();
  });
});
