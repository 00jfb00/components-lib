import React from "react";
import TestRenderer from "react-test-renderer";
import KCard from "../components/atoms/KCard/KCard";

describe("Is the KCard component working?", () => {
  it("Is it rendering with custom styles?", async () => {
    const component = TestRenderer.create(<KCard style={{ height: "500px" }} />);
    expect(component.toJSON().props.style.height).toBe("500px");
  });

  it("Is it rendering with children?", async () => {
    const component = TestRenderer.create(
      <KCard>
        <div style={{ height: "500px" }} />
        <div style={{ height: "500px" }}>
          <p>Teste</p>
        </div>
      </KCard>,
    );
    expect(component.toJSON().children.length).toBe(1);
  });

  it("Is it rendering with border styles?", async () => {
    const component = TestRenderer.create(<KCard radius={10} />);
    expect(component.toJSON().props.style.borderRadius).toBe(10);
  });

  it("Is it rendering card with zoom?", async () => {
    const component = TestRenderer.create(<KCard zoom />);
    expect(component.toJSON().props.className.indexOf("has-zoom") >= 0).toBe(true);
  });

  it("Is it rendering card with shadow?", async () => {
    const component = TestRenderer.create(<KCard shadow={false} />);
    expect(component.toJSON().props.className.indexOf("has-shadow") >= 0).toBe(false);
  });

  it("Is it rendering card with hover effects?", async () => {
    const component = TestRenderer.create(<KCard hover />);
    expect(component.toJSON().props.className.indexOf("has-hover") >= 0).toBe(true);
  });

  it("Is it rendering card with micro contents?", async () => {
    const component = TestRenderer.create(<KCard hasMicroContent />);
    expect(component.toJSON().props.className.indexOf("hasMicroContent") >= 0).toBe(true);
  });

  it("Is it rendering card with subtitle?", async () => {
    const component = TestRenderer.create(<KCard subtitle="Teste" />);
    expect(component.toJSON().children[0].children[0].children[0]).toBe("Teste");
  });
});
