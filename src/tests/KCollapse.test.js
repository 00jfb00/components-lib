import React from "react";
import TestRenderer from "react-test-renderer";
import wait from "waait";
import KCollapse from "../components/atoms/KCollapse";

describe("KCollapse component", () => {
  it("Is the defaultValue classname?", async () => {
    const componentShow = TestRenderer.create(
      <KCollapse expanded>
        <div style={{ height: 100 }} />
      </KCollapse>,
    );
    await wait(0);
    await componentShow.getInstance().loadingPromise;
    expect(componentShow.toJSON().props.className).toBe("k-collapse");

    const componentHide = TestRenderer.create(
      <KCollapse>
        <div style={{ height: 100 }} />
      </KCollapse>,
    );
    await wait(0);
    await componentHide.getInstance().loadingPromise;
    expect(componentHide.toJSON().props.className).toBe("k-collapse");
  });
  it("Is the component hidding?", async () => {
    TestRenderer.create(
      <KCollapse expanded={false}>
        <div style={{ height: 100 }} />
      </KCollapse>,
    );
  });
});
