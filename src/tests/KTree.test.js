import React from "react";
import TestRenderer from "react-test-renderer";
import wait from "waait";
import { configure, mount } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import KTree from "../components/molecules/KTree";

configure({ adapter: new Adapter() });

describe("Is the KTree component working?", () => {
  it("Is it rendering content?", async () => {
    let changeValue = 1;
    let click = false;
    const component = TestRenderer.create(<KTree
      items={[
        {
          title: "Unidade 1",
          icon: ["fab", "youtube"],
          subitems: [{
            id: 1, title: "Why?", indicator: "1.0", active: true, percentage: 90
          }, {
            id: 2, title: "Porque pensar sobre ética?", indicator: "1.1", percentage: 90
          }],
          subtitle: "Ética e Política Brasileira"
        },
        {
          title: "Unidade 2",
          icon: ["fab", "google"],
          subitems: [{
            id: 1,
            title: "Why?",
            indicator: "1.0",
            onClick: () => {
            },
            percentage: 90
          }, {
            id: 2, title: "Porque pensar sobre ética?", indicator: "1.1", percentage: 90
          }],
          subtitle: "Ética e Política Brasileira"
        },
        {
          title: "Unidade 3",
          icon: ["fab", "apple"],
          subtitle: "Ética e Política Brasileira",
          onClick: () => {
            click = true;
          }
        }
      ]}
    />);
    const instance = component.getInstance();
    expect(click).toBe(false);
    const funcTest = () => {
      changeValue += 1;
      click = true;
    };
    instance.handleClick(2, funcTest);
    expect(changeValue).toBe(2);
    expect(click).toBe(true);
    expect(component.root.children[0].children.length).toBe(3);
    component.update(<KTree
      items={[
        {
          id: "oi",
          title: "Unidade 2",
          icon: ["fab", "youtube"],
          subitems: [{
            id: 1, title: "Why?", indicator: "1.0", active: true, percentage: 90
          }, {
            id: 2, title: "Porque pensar sobre ética?", indicator: "1.1", percentage: 90
          }],
          subtitle: "Ética e Política Brasileira"
        },
        {
          title: "Unidade 3",
          icon: ["fab", "google"],
          subitems: [{
            id: 1,
            title: "Why?",
            indicator: "1.0",
            onClick: () => {
            },
            percentage: 90
          }, {
            id: 2, title: "Porque pensar sobre ética?", indicator: "1.1", percentage: 90
          }],
          subtitle: "Ética e Política Brasileira"
        },
        {
          title: "Unidade 4",
          icon: ["fab", "apple"],
          subtitle: "Ética e Política Brasileira",
          onClick: () => {
          }
        }
      ]}
    />);
    await wait(0);
    await component.getInstance().loadingPromise;
    const evt = document.createEvent("Event");
    component.toJSON().children[0].children[0].props.onClick(evt);
  });
  it("test with enzyme?", async () => {
    const wrap = mount(<KTree
      items={[
        {
          title: "Unidade 1",
          icon: ["fab", "youtube"],
          subitems: [{
            id: 1, title: "Why?", indicator: "1.0", active: true, percentage: 90
          }, {
            id: 2, title: "Porque pensar sobre ética?", indicator: "1.1", percentage: 90
          }],
          subtitle: "Ética e Política Brasileira"
        },
        {
          title: "Unidade 2",
          icon: ["fab", "google"],
          subitems: [{
            id: 1,
            title: "Why?",
            indicator: "1.0",
            onClick: () => {
            },
            percentage: 90
          }, {
            id: 2, title: "Porque pensar sobre ética?", indicator: "1.1", percentage: 90
          }],
          subtitle: "Ética e Política Brasileira"
        },
        {
          title: "Unidade 3",
          icon: ["fab", "apple"],
          subtitle: "Ética e Política Brasileira",
          onClick: () => {
          }
        }
      ]}
    />);
    wrap.find("#tree-item0").at(0).simulate("click");
  });
});
