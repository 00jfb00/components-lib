import React from "react";
import TestRenderer from "react-test-renderer";
import KUrlView from "../components/atoms/KUrlView/KUrlView";

describe("Is the KUrlView component working?", () => {
  const urlContent = "https://s3.amazonaws.com/cm-kls-content/201502/INTERATIVAS_2_0/CIENCIAS_MOLECULARES_E_CELULARES/U1/LIVRO_UNICO.pdf";

  it("Is it rendering with url content ?", async () => {
    const component = TestRenderer.create(<KUrlView src={urlContent} />);
    expect(component).toBeDefined();
    expect(component.toJSON().props.className).toEqual("k-url-view");
    expect(component.toJSON().children[0].type).toEqual("iframe");
    expect(component.toJSON().children[0].props.className).toEqual(
      "k-url-view__content",
    );
    expect(component.toJSON().children[0].props.src).toEqual(urlContent);
  });
  it("Is it rendering without url content ?", async () => {
    const component = TestRenderer.create(<KUrlView />);
    expect(component).toBeDefined();
    expect(component.toJSON().props.className).toEqual("k-url-view");
    expect(component.toJSON().children[0].type).toEqual("span");
  });
});
