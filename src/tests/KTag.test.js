import React from "react";
import TestRenderer from "react-test-renderer";
import KTag from "../components/atoms/KTag/KTag";

describe("KTag component", () => {
  it("Loader title? - teste", () => {
    const component = TestRenderer.create(<KTag title="teste" />);
    const instance = component.getInstance();
    expect(instance.props.title).toBe("teste");
    // test defaults value
    expect(instance.props.textColor).toBe("#9A9A9A");
    expect(instance.props.bgColor).toBe("#DDDDDD");
    expect(instance.props.fontSize).toBe("12px");
  });
  it("get colors? -textColor='#FF001A' bgColor='#FFEFF0'", () => {
    const component = TestRenderer.create(
      <KTag title="teste" textColor="#FF001A" bgColor="#FFEFF0" />,
    );
    const instance = component.getInstance();
    expect(instance.props.title).toBe("teste");
    expect(instance.props.textColor).toBe("#FF001A");
    expect(instance.props.bgColor).toBe("#FFEFF0");
    // test defaults value
    expect(instance.props.fontSize).toBe("12px");
  });
  it("get fontSize? - fontSize='50px'", () => {
    const component = TestRenderer.create(
      <KTag title="teste" fontSize="50px" />,
    );
    const instance = component.getInstance();
    expect(instance.props.title).toBe("teste");
    expect(instance.props.fontSize).toBe("50px");
    // test defaults value
    expect(instance.props.textColor).toBe("#9A9A9A");
    expect(instance.props.bgColor).toBe("#DDDDDD");
  });
});
