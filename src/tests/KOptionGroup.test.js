import React from "react";
import TestRenderer from "react-test-renderer";
import { configure, mount } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import KOptionGroup from "../components/molecules/KOptionGroup/KOptionGroup";

configure({ adapter: new Adapter() });

describe("Is the KOptionGroup component working?", () => {
  it("Is it rendering content?", async () => {
    const component = TestRenderer.create(
      <KOptionGroup options={[{ content: "Teste" }, { content: "Teste" }]} />,
    );
    expect(component.root.children[0].children.length).toBe(2);
  });

  it("Is it rendering with list style?", async () => {
    const component = TestRenderer.create(
      <KOptionGroup lisStyleType="decimal" options={[{ content: "Teste" }, { content: "Teste" }]} />,
    );
    expect(component.root.children[0].children.length).toBe(2);
  });
  it("Is it rendering full component?", async () => {
    const component = TestRenderer.create(
      <KOptionGroup
        brand="anhanguera"
        options={[
          {
            content: (
              <p>
                Opção
                {" "}
                <strong>1</strong>
              </p>
            )
          },
          { content: "b", state: "success", selected: true },
          { content: "c", state: "info" },
          {
            content: (
              <p>
                Opção
                {" "}
                <strong>2</strong>
              </p>
            ),
            state: "warning"
          },
          { content: "e", state: "danger" }
        ]}
      />,
    );
    expect(component.root.children[0].children.length).toBe(5);
  });
  it("Is it rendering componentDidUpdate ?", async () => {
    const component = TestRenderer.create(
      <KOptionGroup
        brand="anhanguera"
        horizontal
        onChangeValue={() => {
        }}
        options={[
          {
            content: (
              <p>
                Horizontal
                {" "}
                <strong>1</strong>
              </p>
            )
          },
          { content: "Horizontal 2", state: "success" },
          { content: "Horizontal 3", state: "info" },
          {
            content: (
              <p>
                Horizontal
                {" "}
                <strong>4</strong>
              </p>
            ),
            state: "warning"
          },
          { content: "Horizontal 5", state: "danger" }
        ]}
      />,
    );
    const instance = component.getInstance();
    const prevState = {
      brand: "anhanguera",
      horizontal: true,
      onChangeValue: undefined,
      options: [
        { content: "Horizontal 1", selected: false },
        { content: "Horizontal 2", state: "success", selected: true },
        { content: "Horizontal 3", state: "info", selected: false },
        { content: [Object], state: "warning", selected: false },
        { content: "Horizontal 5", state: "danger", selected: false }
      ],
      highlight: true,
      showBorder: true,
      size: "1x",
      id: "k-option-group",
      radio: false,
      color: undefined,
      contentColor: undefined,
      borderColor: undefined,
      disable: false,
      ignoreState: false,
      multipleSelected: false,
      fontSize: "p3",
      listStyleType: "none"
    };
    instance.componentDidUpdate(prevState);
  });

  it("Is it rendering Horizontal ?", async () => {
    const component = mount(
      <KOptionGroup
        brand="anhanguera"
        horizontal
        onChangeValue={() => {
        }}
        options={[
          {
            content: (
              <p>
                Horizontal
                {" "}
                <strong>1</strong>
              </p>
            )
          },
          { content: "Horizontal 2", state: "success" },
          { content: "Horizontal 3", state: "info" },
          {
            content: (
              <p>
                Horizontal
                {" "}
                <strong>4</strong>
              </p>
            ),
            state: "warning"
          },
          { content: "Horizontal 5", state: "danger" }
        ]}
      />,
    );
    const instance = component.instance();
    // instance.handleClick();
    instance.onChangeSelected(1);
    expect(component.children().first().children().length).toBe(5);
    component.find("#radio-option-k-option-group-2").at(0).simulate("click");
  });

  it("Is it rendering full component with id?", async () => {
    const component = TestRenderer.create(
      <section>
        <KOptionGroup
          id="id"
          brand="anhanguera"
          options={[
            {
              content: (
                <p>
                  Opção
                  {" "}
                  <strong>1</strong>
                </p>
              )
            },
            {
              id: 1, content: "b", state: "success", selected: true
            },
            { id: 2, content: "c", state: "info" },
            {
              id: 3,
              content: (
                <p>
                  Opção
                  {" "}
                  <strong>2</strong>
                </p>
              ),
              state: "warning"
            },
            { id: 4, content: "e", state: "danger" }
          ]}
        />
      </section>,
    );
    expect(component.root.children[0].children.length).toBe(1);
  });
  it("Is it rendering with multipleSelected true ?", async () => {
    const component = TestRenderer.create(
      <KOptionGroup
        brand="anhanguera"
        horizontal
        multipleSelected
        onChangeValue={() => {
        }}
        options={[
          {
            content: (
              <p>
                Horizontal
                {" "}
                <strong>1</strong>
              </p>
            )
          },
          { content: "Horizontal 2", state: "success" },
          { content: "Horizontal 3", state: "info" },
          {
            content: (
              <p>
                Horizontal
                {" "}
                <strong>4</strong>
              </p>
            ),
            state: "warning"
          },
          { content: "Horizontal 5", state: "danger" }
        ]}
      />,
    );
    const instance = component.getInstance();
    // instance.handleClick();
    instance.onChangeSelected(1);
    // expect(component.root.children[0].children.length).toBe(5);
    // component.toJSON().children[1].props.onClick();
  });
  it("Is it rendering with disabled true ?", async () => {
    const component = TestRenderer.create(
      <KOptionGroup
        brand="anhanguera"
        horizontal
        multipleSelected
        onChangeValue={() => {
        }}
        options={[
          {
            content: (
              <p>
                Horizontal
                {" "}
                <strong>1</strong>
              </p>
            )
          },
          { content: "Horizontal 2", state: "success" },
          { content: "Horizontal 3", state: "info" },
          {
            content: (
              <p>
                Horizontal
                {" "}
                <strong>4</strong>
              </p>
            ),
            state: "warning"
          },
          { content: "Horizontal 5", state: "danger" }
        ]}
      />,
    );
    const instance = component.getInstance();
    instance.setState({ disable: true });
    // instance.handleClick();
    expect(instance.state.disable).toBe(true);
  });

  it("Is it rendering with disabled false ?", async () => {
    const component = TestRenderer.create(
      <KOptionGroup
        brand="anhanguera"
        horizontal
        multipleSelected
        onChangeValue={() => {
        }}
        options={[
          {
            content: (
              <p>
                Horizontal
                {" "}
                <strong>1</strong>
              </p>
            )
          },
          { content: "Horizontal 2", state: "success" },
          { content: "Horizontal 3", state: "info" },
          {
            content: (
              <p>
                Horizontal
                {" "}
                <strong>4</strong>
              </p>
            ),
            state: "warning"
          },
          { content: "Horizontal 5", state: "danger" }
        ]}
      />,
    );
    const instance = component.getInstance();
    instance.setState({ disable: false });
    // instance.handleClick();
    expect(instance.state.disable).toBe(false);
  });

  it("Is it rendering and return single value changed ?", async () => {
    let valueChanged = null;
    let component = TestRenderer.create(
      <KOptionGroup
        listStyleType="noone"
        options={[
          { content: "Opção 1" },
          { content: "Opção 2" }
        ]}
        onChangeValue={value => {
          valueChanged = value;
        }}
      />
    );
    let instance = component.getInstance();
    instance.onChangeSelected(1);
    expect(valueChanged).toBe(1);
    component = TestRenderer.create(
      <KOptionGroup
        options={[
          { content: "Opção 1" },
          { content: "Opção 2" }
        ]}
      />
    );
    instance = component.getInstance();
    instance.onChangeSelected();
  });
});
