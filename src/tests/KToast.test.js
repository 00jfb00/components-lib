import React from "react";
import TestRenderer from "react-test-renderer";
import wait from "waait";
import KToast from "../components/molecules/KToast/KToast";

describe("Is KToast component working?", () => {
  beforeEach(() => {
    jest.setTimeout(10000);
  });
  it("Is the component visible?", async () => {
    const component = TestRenderer.create(<KToast message="Teste" show />);
    await wait(0);
    expect(
      component.toJSON().children[0].props.className.indexOf("show") >= 0,
    ).toBe(true);
  });

  it("Is the component not visible?", async () => {
    const component = TestRenderer.create(<KToast message="Teste" />);
    await wait(0);
    expect(
      component.toJSON().children[0].props.className.indexOf("show") >= 0,
    ).toBe(false);
  });

  it("Is the component rendering printscreen?", async () => {
    const component = TestRenderer.create(<KToast onPrintScreen={() => {}} message="Teste" show />);
    const instance = component.getInstance();
    await wait(0);
    instance.handlePrintScreen();
    expect(
      component.toJSON().children[0].props.className.indexOf("show") >= 0,
    ).toBe(true);
  });

  it("Is the component rendering copy request?", async () => {
    const component = TestRenderer.create(<KToast onCopyRequest={() => {}} message="Teste" show />);
    const instance = component.getInstance();
    await wait(0);
    instance.handleCopyRequest();
    expect(
      component.toJSON().children[0].props.className.indexOf("show") >= 0,
    ).toBe(true);
  });

  it("Is the component auto hiding?", async () => {
    const component = TestRenderer.create(
      <KToast message="Teste" delay={500} show autoHide />,
    );
    await wait(0);
    expect(
      component.toJSON().children[0].props.className.indexOf("show") >= 0,
    ).toBe(true);
    await wait(1000);
    expect(
      component.toJSON().children[0].props.className.indexOf("show") >= 0,
    ).toBe(false);
  });

  it("Is the component auto hiding when changing props?", async () => {
    const component = TestRenderer.create(<KToast message="Teste" />);
    await wait(0);
    component.update(
      <KToast
        message="Teste"
        autoHide
        brand="default"
        delay={3000}
        icon="check-circle"
        onClose={() => {}}
        show
        state="success"
      />,
    );
    await wait(1000);
    expect(
      component.toJSON().children[0].props.className.indexOf("show") >= 0,
    ).toBe(true);
    await wait(3000);
    expect(
      component.toJSON().children[0].props.className.indexOf("show") >= 0,
    ).toBe(false);
  });
  it("Is the component auto hiding when changing props1?", async () => {
    const component = TestRenderer.create(<KToast message="Teste" />);
    await wait(0);
    component.update(
      <KToast
        message="Teste"
        autoHide
        brand="default"
        delay={3000}
        icon="check-circle"
        onClose={() => {}}
        show
        state="success"
      />,
    );
    await wait(1000);
    expect(
      component.toJSON().children[0].props.className.indexOf("show") >= 0,
    ).toBe(true);
    await wait(3000);
    expect(
      component.toJSON().children[0].props.className.indexOf("show") >= 0,
    ).toBe(false);
  });

  it("Is the component clearing interval when changing props?", async () => {
    const component = TestRenderer.create(
      <KToast message="Teste" show autoHide />,
    );
    await wait(0);
    component.update(
      <KToast
        message="Teste"
        autoHide
        brand="default"
        delay={3000}
        icon="check-circle"
        onClose={() => {}}
        show={false}
        state="success"
      />,
    );
    await wait(1000);
    expect(
      component.toJSON().children[0].props.className.indexOf("show") >= 0,
    ).toBe(false);
  });
});
