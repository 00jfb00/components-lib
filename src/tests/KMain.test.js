import React from "react";
import TestRenderer from "react-test-renderer";
import wait from "waait";
import KMain from "../components/organisms/KMain";
import KBody from "../components/organisms/KBody";
import KSide from "../components/organisms/KSide";
import KHeader from "../components/molecules/KHeader";

describe("KMain component", () => {
  it("Is the defaultValue classname?", async () => {
    let component = TestRenderer.create(
      <KMain>
        <KHeader
          fixed={false}
          iconMobile={["kci", "menu"]}
          colorIcon="white"
          sizeIcon="lg"
          expand="lg"
          variant="light"
          logo="https://s3-us-west-2.amazonaws.com/estudante-production/app/public/ckeditor_assets/pictures/128/original/anhanguera_full.svg"
          state="primary"
          responsiveHeight
          backgroundColor="#dddd"
        />
        <KSide width={280}>
          <div>SideBar</div>
        </KSide>
        <KBody>
          <p>Body</p>
        </KBody>
      </KMain>,
    );
    await wait(0);
    await component.getInstance().loadingPromise;
    expect(component.toJSON()[1].props.className).toBe("k-main");
    component = TestRenderer.create(
      <KMain />,
    );
    await wait(0);
    await component.getInstance().loadingPromise;
    expect(component.toJSON()[1].props.className).toBe("k-main");
  });

  it("Has filter sort with two bodys?", async () => {
    const component = TestRenderer.create(
      <KMain>
        <KBody>
          <p>Body</p>
        </KBody>
        <KSide width={280}>
          <div>SideBar</div>
        </KSide>
        <KBody>
          <p>Body</p>
        </KBody>
      </KMain>,
    );
    await wait(0);
    await component.getInstance().loadingPromise;
    const className = component.toJSON()[0].props.className.replace(/\s+/g, "");
    expect(className).toBe("k-main-header-is-fixed");
    expect(component.toJSON()[1].children.length).toBe(2);
    expect(component.toJSON()[1].children[1].props.id).toBe("k-side");
    expect(component.toJSON()[1].children[0].props.id).toBe("k-body");
  });

  it("Header fixed without side", async () => {
    const component = TestRenderer.create(
      <KMain>
        <KHeader
          logo="https://s3-us-west-2.amazonaws.com/estudante-production/app/public/ckeditor_assets/pictures/128/original/anhanguera_full.svg"
          bodyMiddle={<div>Conteúdo de input</div>}
          bodyRight={<div>Dados pessoais</div>}
        />
        <KBody>
          <p>Body</p>
        </KBody>
      </KMain>,
    );
    await wait(0);
    await component.getInstance().loadingPromise;

    const className = component.toJSON()[0].props.className.replace(/\s+/g, "");
    expect(className).toBe("k-main-header");

    expect(component.toJSON()[1].children.length).toBe(1);
    expect(component.toJSON()[1].children[0].props.id).toBe("k-body");

    await wait(0);
    await component.getInstance().loadingPromise;

    component.update(
      <KMain>
        <KHeader
          logo="https://s3-us-west-2.amazonaws.com/estudante-production/app/public/ckeditor_assets/pictures/128/original/anhanguera_full.svg"
          bodyMiddle={<div>Conteúdo de input</div>}
          bodyRight={<div>Dados pessoais</div>}
          style={{
            height: 100
          }}
        />
        <KBody>
          <p>Body</p>
        </KBody>
      </KMain>,
    );

    component.update(
      <KMain>
        <KBody>
          <p>Body</p>
        </KBody>
      </KMain>,
    );

    await wait(0);
    await component.getInstance().loadingPromise;
    const instance = component.getInstance();
    expect(instance.state.top).toBe(0);

    const prepareChildren = instance.prepareChildren();
    expect(prepareChildren.length).toBe(1);

    component.update(
      <KMain headerFixed={false} width={280} />,
    );

    await wait(0);
    await component.getInstance().loadingPromise;
    const instance2 = component.getInstance();
    expect(instance.state.top).toBe(0);

    const prepareChildren2 = instance2.prepareChildren();
    expect(prepareChildren2.length).toBe(0);
  });
});
