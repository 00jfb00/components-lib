import React from "react";
import TestRenderer from "react-test-renderer";
import wait from "waait";
import KOption from "../components/atoms/KOption/KOption";

describe("Is the KOption component working?", () => {
  it("Is it rendering when selected props is sent?", async () => {
    const component = TestRenderer.create(<KOption selected />);
    const instance = component.getInstance();
    expect(instance.state.selected).toBe(true);
  });

  it("Is it rendering content?", async () => {
    const component = TestRenderer.create(<KOption content="teste" />);
    expect(component.root.children[0].children.length).toBe(1);
  });
  it("Is it rendering?", async () => {
    const component = TestRenderer.create(
      <section>
        <KOption highlight={false} content={<p>Opção 1</p>} state="success" />
        <KOption content={<p>Opção 2</p>} state="info" />
        <KOption highlight={false} content={<p>Opção 3</p>} state="warning" />
        <KOption content={<p>Opção 4</p>} state="danger" />
      </section>,
    );
    expect(component.root.children[0].children.length).toBe(1);
  });
  it("execute function getStateIcon? info", async () => {
    const component = TestRenderer.create(
      <KOption highlight={false} content={<p>Opção 1</p>} state="info" />,
    );
    const instance = component.getInstance();
    instance.getStateIcon("info");
  });
  it("execute function getStateIcon? warning", async () => {
    const component = TestRenderer.create(
      <KOption highlight={false} content={<p>Opção 1</p>} state="warning" />,
    );
    const instance = component.getInstance();
    instance.getStateIcon("warning");
  });
  it("execute function getStateIcon? danger", async () => {
    const component = TestRenderer.create(
      <KOption highlight={false} content={<p>Opção 1</p>} state="danger" />,
    );
    const instance = component.getInstance();
    instance.getStateIcon("danger");
  });
  it("execute function getStateIcon? grey", async () => {
    const component = TestRenderer.create(
      <KOption
        highlight={false}
        showBorder={false}
        color="red"
        content={<p>Opção 1</p>}
        state="grey"
      />,
    );
    const instance = component.getInstance();
    instance.getStateIcon("grey");
  });
  it("test border", async () => {
    const component = TestRenderer.create(
      <KOption
        highlight={false}
        showBorder={false}
        color="red"
        content={<p>Opção 1</p>}
        state="danger"
      />,
    );
    const instance = component.getInstance();
    instance.getStateIcon("danger");
  });

  it("Is it rendering when disable props is sent?", async () => {
    let click = false;
    const component = TestRenderer.create(<KOption
      disable
      onChangeValue={() => {
        click = true;
      }}
    />);
    const instance = component.getInstance();
    expect(instance.state.disable).toBe(true);
    expect(instance.state.selected).toBe(false);
    instance.handleClick();
    expect(click).toBe(false);
  });

  it("Is it rendering when disable props is false?", async () => {
    let click = false;
    const component = TestRenderer.create(<KOption
      disable={false}
      onChangeValue={() => {
        click = true;
      }}
    />);
    const instance = component.getInstance();
    instance.handleClick();
    expect(click).toBe(true);
  });

  it("Is it rendering when message type is true props is sent?", async () => {
    const component = TestRenderer.create(<KOption
      id="1"
      disable
      ignoreState
    />);
    await wait(100);
    expect(component.root.findByProps({ id: "icon-check-1" })._fiber.pendingProps.icon).toStrictEqual(["far", "square"]);
  });

  it("Is it rendering when radio is sent?", async () => {
    const component = TestRenderer.create(<KOption
      id="1"
      radio
      state="danger"
      ignoreState={false}
    />);
    const instance = component.getInstance();
    instance.getStateIcon("danger");
  });

  it("Is it rendering when radio is ignoreState and colorSelected?", async () => {
    const component = TestRenderer.create(<KOption
      id="1"
      radio
      selected
      colorSelected="#F69120"
      state="danger"
      ignoreState={false}
    />);
    const instance = component.getInstance();
    instance.getStateIcon("danger");
  });

  it("Is it rendering when message type is true and selected is true props is sent?", async () => {
    const component = TestRenderer.create(<KOption
      id="1"
      disable
      radio
      ignoreState
    />);
    const instance = component.getInstance();
    instance.setState({ selected: true });
    await wait(100);
    expect(component.root.findByProps({ id: "icon-check-1" })._fiber.pendingProps.icon).toStrictEqual(["fas", "dot-circle"]);
  });

  it("execute function changeBackground and OutBackground? info", async () => {
    const component = TestRenderer.create(
      <KOption highlight={false} content={<p>Opção 1</p>} state="info" hoverBackground="#FFFAF4" />,
    );
    const instance = component.getInstance();
    const e = {
      currentTarget: {
        style: {
          backgroundColor: "red"
        },
        children: [{
          children: [
            {
              setAttribute: () => { console.log("Olá"); }
            }
          ]
        }]
      },
      target: {
        style: {
          backgroundColor: "transparent"
        }
      }
    };
    instance.changeBackground(e);
    instance.OutBackground(e);
    expect(e.currentTarget.style.backgroundColor).toBe("transparent");
  });
  it("execute function changeBackground? info", async () => {
    const component = TestRenderer.create(
      <KOption hoverBackground="#FFFAF4" showBorder={false} colorSelected="#F69120" content="Opção 1" color="#F69120" contentColor="#F69120" />
    );
    expect(component.toJSON().props.onMouseOver).toBeInstanceOf(Function);
    expect(component.toJSON().props.onMouseOut).toBeInstanceOf(Function);
  });
  it("execute function selected? info", async () => {
    const component = TestRenderer.create(
      <KOption hoverBackground="#FFFAF4" selected showBorder={false} colorSelected="#F69120" content="Opção 1" color="#F69120" contentColor="#F69120" />
    );
    const instance = component.getInstance();
    const e = {
      currentTarget: {
        style: {
          backgroundColor: "#FFFAF4"
        },
        children: [{
          children: [
            {
              setAttribute: () => { console.log("Olá"); }
            }
          ]
        }]
      },
      target: {
        style: {
          backgroundColor: "transparent"
        }
      }
    };
    instance.OutBackground(e);
    expect(e.currentTarget.style.backgroundColor).toBe("#FFFAF4");
  });
  it("execute function selected an colorSlected? info", async () => {
    const component = TestRenderer.create(
      <KOption hoverBackground="#FFFAF4" selected={false} showBorder={false} colorSelected="#F69120" content="Opção 1" contentColor="#F69120" />
    );
    const instance = component.getInstance();
    const e = {
      currentTarget: {
        style: {
          backgroundColor: "#FFFAF4"
        },
        children: [{
          children: [
            {
              setAttribute: () => { console.log("Olá"); }
            }
          ]
        }]
      },
      target: {
        style: {
          backgroundColor: "transparent"
        }
      }
    };
    instance.OutBackground(e);
    instance.changeBackground(e);
    expect(e.currentTarget.style.backgroundColor).toBe("#FFFAF4");
  });
  it("execute when color undefined? info", async () => {
    const component = TestRenderer.create(
      <KOption hoverBackground="#FFFAF4" selected={false} showBorder={false} colorSelected="#F69120" content="Opção 1" contentColor="#F69120" />
    );
    const instance = component.getInstance();
    const e = {
      currentTarget: {
        style: {
          backgroundColor: "#FFFAF4"
        },
        children: [{
          children: [
            {
              setAttribute: () => { console.log("Olá"); }
            }
          ]
        }]
      },
      target: {
        style: {
          backgroundColor: "transparent"
        }
      }
    };
    instance.OutBackground(e);
    instance.changeBackground(e);
    expect(e.currentTarget.style.backgroundColor).toBe("#FFFAF4");
  });
});
