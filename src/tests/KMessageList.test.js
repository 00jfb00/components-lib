import React from "react";
import TestRenderer from "react-test-renderer";
import KMessageList from "../components/molecules/KMessageList/KMessageList";

describe("KMessage List Teste -", () => {
  it("Is the property 'is New' working?", async () => {
    const component = TestRenderer.create(<KMessageList hasAttachment user="Usuário" date="17/05" id={1} isNew />);
    expect(component.root.findByProps({ className: "badgeMobile" })).not.toBe(undefined);
    expect(component.root.findByProps({ className: "badge" })).not.toBe(undefined);
    expect(component.root.findByProps({ id: "k-message-list-title-1" })
      .props.className.indexOf("is_new") >= 0).toBe(true);
  });
  it("Is the property 'is New' working when false?", async () => {
    const component = TestRenderer.create(<KMessageList id={1} user="Usuário" date="17/05" />);
    let badgeElement; let
      badgeMobileElement;
    try {
      badgeElement = component.root.findByProps({ className: "badgeMobile" });
      badgeMobileElement = component.root.findByProps({ className: "badgeMobile" });
    } catch (e) {
      expect(component.root.findByProps({ id: "k-message-list-title-1" })
        .props.className.indexOf("is_new") >= 0).toBe(false);
      expect(badgeElement).toBe(undefined);
      expect(badgeMobileElement).toBe(undefined);
    }
  });

  it("Is the property 'showImage' working?", async () => {
    const component = TestRenderer.create(<KMessageList user="Usuário" date="17/05" id={1} />);
    expect(component.toJSON().children[0].children[0].children[0].props.className).not.toBe(undefined);
  });
  it("Is the property 'showImage' working when false?", async () => {
    const component = TestRenderer.create(<KMessageList id={1} user="Usuário" date="17/05" showImage={false} />);
    let imageElement;
    try {
      imageElement = component.toJSON().children[0].children[0].children[0].props.className;
    } catch (e) {
      expect(imageElement).toBe(undefined);
    }
  });

  it("Is the property 'isActive' working?", async () => {
    const component = TestRenderer.create(<KMessageList id={1} user="Usuário" date="17/05" isActive />);
    expect(component.root.findByProps({ id: "k-message-list-container-1" })
      .props.className.indexOf("is_active") >= 0).toBe(true);
  });
  it("Is the property 'isActive' working when false?", async () => {
    const component = TestRenderer.create(<KMessageList id={1} user="Usuário" date="17/05" />);
    let element;
    try {
      element = component.root.findByProps({ id: "k-message-list-container-1" });
    } catch (e) {
      expect(element.props.className.indexOf("is_active") >= 0).toBe(false);
    }
  });

  it("Is the onClick method working?", () => {
    let click = false;
    const component = TestRenderer.create(
      <KMessageList
        onClick={() => {
          click = true;
        }}
        user="Usuário"
        date="17/05"
      />
    );
    const instance = component.getInstance();
    expect(click).toBe(false);
    instance.handleClick();
    expect(click).toBe(true);
  });

  it("Is the onClick method working when undefined?", () => {
    const click = false;
    const component = TestRenderer.create(
      <KMessageList user="Usuário" date="17/05" />
    );
    const instance = component.getInstance();
    expect(click).toBe(false);
    instance.handleClick();
    expect(click).toBe(false);
  });
});
