import React from "react";
import TestRenderer from "react-test-renderer";
import jsdom from "jsdom";
import KHeader from "../components/molecules/KHeader/KHeader";

describe("Is the KHeader component working?", () => {
  const dom = new jsdom.JSDOM("<div></div>");

  it("Is it rendering?", async () => {
    global.window = dom.window;
    global.window.innerWidth = 1024;
    const component = TestRenderer.create(
      <KHeader
        logo="https://s3-us-west-2.amazonaws.com/estudante-production/app/public/ckeditor_assets/pictures/128/original/anhanguera_full.svg"
      />,
    );
    expect(
      component
        .toJSON()
        .props.className.split(" ")
        .filter(element => element === "k-header").length,
    ).toEqual(1);
  });
  it("Is it going mobile?", async () => {
    global.window = dom.window;
    global.window.innerWidth = 500;
    const component = TestRenderer.create(
      <KHeader
        logo="https://s3-us-west-2.amazonaws.com/estudante-production/app/public/ckeditor_assets/pictures/128/original/anhanguera_full.svg"
        iconMobile={["kci", "menu"]}
      />,
    );
    const instance = component.getInstance();
    expect(instance.state.isMobile).toEqual(true);
  });

  it("Is it rendering full?", async () => {
    global.window = dom.window;
    global.window.innerWidth = 1024;
    const component = TestRenderer.create(
      <section>
        <KHeader
          fixed={false}
          iconMobile={["kci", "menu"]}
          colorIcon="white"
          sizeIcon="lg"
          expand="lg"
          variant="light"
          logo="https://s3-us-west-2.amazonaws.com/estudante-production/app/public/ckeditor_assets/pictures/128/original/anhanguera_full.svg"
          state="primary"
          responsiveHeight
          backgroundColor="#dddd"
        />
        <KHeader
          fixed
          iconMobile={["kci", "menu"]}
          colorIcon="white"
          sizeIcon="lg"
          expand="lg"
          variant="light"
          logo="https://s3-us-west-2.amazonaws.com/estudante-production/app/public/ckeditor_assets/pictures/128/original/anhanguera_full.svg"
          state="primary"
          responsiveHeight
          backgroundColor="#dddd"
        />
      </section>,
    );
    component.unmount();
  });
  it("Is it rendering 2 and click logo?", async () => {
    global.window = dom.window;
    global.window.innerWidth = 1024;
    let clickLogo = null;
    let component = TestRenderer.create(
      <KHeader
        logo="https://s3-us-west-2.amazonaws.com/estudante-production/app/public/ckeditor_assets/pictures/128/original/anhanguera_full.svg"
        bodyMiddle={<div>Conteúdo de input</div>}
        bodyRight={<div>Dados pessoais</div>}
        onClickLogo={() => {
          clickLogo = true;
        }}
      />,
    );
    let instance = component.getInstance();
    instance.handleWindowSizeChange();
    instance.handleClickLogo();
    component = TestRenderer.create(
      <KHeader
        logo="https://s3-us-west-2.amazonaws.com/estudante-production/app/public/ckeditor_assets/pictures/128/original/anhanguera_full.svg"
        bodyMiddle={<div>Conteúdo de input</div>}
        bodyRight={<div>Dados pessoais</div>}
      />,
    );
    instance = component.getInstance();
    instance.handleWindowSizeChange();
    instance.handleClickLogo();
    expect(clickLogo).toEqual(true);
  });
});
