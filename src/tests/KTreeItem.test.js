import React from "react";
import TestRenderer from "react-test-renderer";
import wait from "waait";
import KTreeItem from "../components/molecules/KTreeItem";

describe("Is the KTreeItem component working?", () => {
  it("Is it rendering subitems?", async () => {
    const component = TestRenderer.create(<KTreeItem
      title="Unidade 2"
      subitems={[{
        id: 1, title: "Why?", indicator: "1.0", active: true, percentage: 90
      }, {
        id: 2, title: "Porque pensar sobre ética?", indicator: "1.1", percentage: 90
      }]}
      subtitle="Cidadania e Direitos Humanos"
    />);
    expect(component.root.findByProps({ className: "k-tree-item-list" }).children[0].children.length).toBe(2);
  });
  it("Is it active?", async () => {
    const component = TestRenderer.create(<KTreeItem
      title="Unidade 2"
      subtitle="Cidadania e Direitos Humanos"
      active
    />);
    expect(component.root.children[0].children[0].props.className.indexOf("is-active") >= 0).toBe(true);
  });
  it("Is it cockpit?", async () => {
    const component = TestRenderer.create(<KTreeItem
      title="Unidade 2"
      cockpit
    />);
    await wait(0);
    await component.getInstance().loadingPromise;
    expect(component.root.children[0].children[0].props.className.indexOf("cockpit") >= 0).toBe(true);
  });
  it("Is it cockpit block active?", async () => {
    const component = TestRenderer.create(<KTreeItem
      title="Unidade 2"
      cockpit
      active
    />);
    await wait(0);
    await component.getInstance().loadingPromise;
    expect(component.root.children[0].children[0].props.className.indexOf("cockpit-block-active") >= 0).toBe(true);
  });
  it("Is it cockpit icon?", async () => {
    const component = TestRenderer.create(<KTreeItem
      title="Unidade 2"
      cockpit
      active
      icon={["kci", "arrow-circle-right"]}
      iconActive={["kci", "arrow-circle-left"]}
    />);
    await wait(0);
    await component.getInstance().loadingPromise;
    expect(component.root.children[0].children[0].props.className.indexOf("cockpit-block-active") >= 0).toBe(true);
  });
  it("Is it messageria?", async () => {
    const component = TestRenderer.create(<KTreeItem
      title="Unidade 2"
      mail
      badgeContent="3"
    />);
    await wait(0);
    await component.getInstance().loadingPromise;
    expect(component.root.children[0].children[0].props.className.indexOf("messageria") >= 0).toBe(true);
  });
  it("Is it messageria block active?", async () => {
    const component = TestRenderer.create(<KTreeItem
      title="Unidade 2"
      mail
      active
    />);
    await wait(0);
    await component.getInstance().loadingPromise;
    expect(component.root.children[0].children[0].props.className.indexOf("messageria-block-active") >= 0).toBe(true);
  });

  it("Is it not active?", async () => {
    const component = TestRenderer.create(<KTreeItem title="Unidade 2" subtitle="Cidadania e Direitos Humanos" />);
    expect(component.root.children[0].children[0].props.className.indexOf("is-active") >= 0).toBe(false);
  });
  it("Is it clickable?", async () => {
    let click = false;
    const component = TestRenderer.create(<KTreeItem
      title="Unidade 2"
      subtitle="Cidadania e Direitos Humanos"
      onClick={() => {
        click = true;
      }}
    />);
    const instance = component.getInstance();
    expect(click).toBe(false);
    instance.handleClick();
    expect(click).toBe(true);
  });
  it("Is it clickable when active?", async () => {
    let click = false;
    const component = TestRenderer.create(<KTreeItem
      title="Unidade 2"
      active
      subtitle="Cidadania e Direitos Humanos"
      onClick={() => {
        click = true;
      }}
    />);
    const instance = component.getInstance();
    expect(click).toBe(false);
    instance.handleClick();
    expect(click).toBe(true);
  });
  it("Are the subitems clickable?", async () => {
    let click = false;
    const component = TestRenderer.create(<KTreeItem
      title="Unidade 2"
      subitems={[{
        id: 1,
        title: "Porque pensar sobre ética?",
        indicator: "1.1",
        percentage: 10,
        onClick: () => {
          click = true;
        }
      }, {
        id: 2,
        title: "Porque pensar sobre ética?",
        indicator: "1.1",
        percentage: 90,
        onClick: () => {
          click = true;
        }
      }]}
      subtitle="Cidadania e Direitos Humanos"
    />);
    const instance = component.getInstance();
    expect(click).toBe(false);
    instance.handleClickSubItems(1, () => {
      click = true;
    });
    expect(click).toBe(true);
  });
  it("Are the subitems clickable when active?", async () => {
    let click = false;
    const component = TestRenderer.create(<KTreeItem
      title="Unidade 2"
      subitems={[{
        id: 1,
        title: "Porque pensar sobre ética?",
        indicator: "1.1",
        active: true,
        percentage: 10,
        onClick: () => {
          click = true;
        }
      }, {
        id: 2,
        title: "Porque pensar sobre ética?",
        indicator: "1.1",
        percentage: 90,
        onClick: () => {
          click = true;
        }
      }]}
      subtitle="Cidadania e Direitos Humanos"
    />);
    const instance = component.getInstance();
    expect(click).toBe(false);
    instance.handleClickSubItems(1, () => {
      click = true;
    });
    expect(click).toBe(true);
  });
  it("Is the element working with no subitems?", async () => {
    const component = TestRenderer.create(<KTreeItem
      title="Unidade 2"
      onClick={() => { }}
      active
      subitems={[{
        id: 2,
        title: "Porque pensar sobre ética?",
        indicator: "1.1",
        percentage: 90,
        onClick: () => { }
      }]}
      subtitle="Cidadania e Direitos Humanos"
    />);
    component.update(<KTreeItem
      title="Unidade 2"
      subitems={[]}
      subtitle="Cidadania e Direitos Humanos"
    />);
    const instance = component.getInstance();
    await wait(0);
    expect(instance.state.data.length).toBe(0);
  });
  it("Is the element not clickable when active?", async () => {
    let click = false;
    const component = TestRenderer.create(<KTreeItem
      title="Unidade 2"
      active
      onClick={() => { click = true; }}
      subitems={[{
        id: 1,
        title: "Porque pensar sobre ética?",
        indicator: "1.1",
        active: true,
        percentage: 10,
        onClick: () => {
          click = true;
        }
      }, {
        id: 2,
        title: "Porque pensar sobre ética?",
        indicator: "1.1",
        percentage: 90,
        onClick: () => {
          click = true;
        }
      }]}
      subtitle="Cidadania e Direitos Humanos"
    />);
    const instance = component.getInstance();
    expect(click).toBe(false);
    instance.handleClick();
    expect(click).toBe(false);
  });

  it("ComponentWillReceiveProps and handleClick()?", async () => {
    let click = false;
    const component = TestRenderer.create(<KTreeItem
      title="Unidade 2"
      onClick={() => {
        click = true;
      }}
      subitems={[{
        id: 1, title: "Why?", indicator: "1.0", percentage: 90
      }, {
        id: 2, title: "Porque pensar sobre ética?", indicator: "1.1", percentage: 90
      }]}
      subtitle="Cidadania e Direitos Humanos"
    />);
    await wait(0);
    await component.getInstance().loadingPromise;
    const instance = component.getInstance();

    instance.handleClick();
    expect(click).toBe(true);

    const kTreeSubItem = component.root.findByProps({ id: "tree-subitem0" });
    await wait(0);
    kTreeSubItem._fiber.pendingProps.onClick();
    expect(click).toBe(true);
    component.update(<KTreeItem
      active={false}
      onClick={() => {
        click = true;
      }}
      title="Unidade 2"
      subitems={[]}
      subtitle="Cidadania e Direitos Humanos"
    />);
    expect(instance.state.collapse).toBe(true);

    instance.handleClick({ target: { value: null } });
    expect(click).toBe(true);
  });
  it("render coockpit?", async () => {
    const component = TestRenderer.create(
      <KTreeItem
        cockpit
        active
        icon={["fas", "university"]}
        title="Biblioteca Virtual"
        iconColor={["#0185CD", "#000"]}
        subitems={[{ id: 1, title: "Biblioteca Virtual" }, { id: 2, title: "Acervo Virtual" }]}
      />,
    );
    await wait(0);
    await component.getInstance().loadingPromise;
    const instance = component.getInstance();
    expect(instance.state.collapse).toBe(true);
  });
  it("Is it session?", async () => {
    const component = TestRenderer.create(<KTreeItem
      title="Unidade 1"
      icon={["kci", "arrow-circle-right"]}
      iconActive={["kci", "arrow-circle-left"]}
      subtitle="Título da Unidade em Questão, Seguindo esse Padrão"
      mail
      subitems={[
        {
          id: 1,
          title: "PRAZO expira em 17/07",
          icon: ["fas", "caret-down"],
          subtitle: "Título da Sessão",
          indicator: "1.1",
          percentage: 0,
          active: false,
          cockpit: true,
          children: [
            {
              title: "Conteúdo",
              subtitle: "2 minutos de conteúdo",
              icon: ["fas", "file-alt"],
              iconColor: "#HEX",
              active: false,
              indicator: "1.1",
              cockpit: true,
              onClick: () => { console.log("esta clicavel01"); }
            }, {
              title: "Avaliação",
              subtitle: "Prazo ate 24/07",
              icon: ["fas", "calendar"],
              iconColor: "#HEX",
              active: false,
              indicator: "1.1",
              mail: true,
              onClick: () => { console.log("esta clicavel"); }
            }, {
              title: "Avaliação",
              subtitle: "Prazo ate 24/07",
              icon: ["fas", "calendar"],
              iconColor: "#HEX",
              active: false,
              indicator: "1.1",
              mail: false,
              onClick: () => { console.log("esta clicavel"); }
            }

          ]
        }
      ]}
      active
    />);
    expect(component.toJSON().children[0].props.className).toBe("k-tree-item-block    messageria  is-gradient-default  messageria-block-active nav-link");
  });
  it("Are the subitems children?", async () => {
    let click = false;
    const component = TestRenderer.create(<KTreeItem
      title="Unidade 2"
      subitems={[{
        id: 1,
        title: "Porque pensar sobre ética?",
        indicator: "1.1",
        percentage: 10,
        onClick: () => {
          click = true;
        },
        children: [
          {
            title: "Conteúdo",
            subtitle: "2 minutos de conteúdo",
            icon: ["fas", "file-alt"],
            iconColor: "#HEX",
            indicator: "1.1",
            onClick: () => { alert("esta clicavel01"); }
          }, {
            title: "Avaliação",
            subtitle: "Prazo ate 24/07",
            icon: ["fas", "calendar"],
            iconColor: "#HEX",
            indicator: "1.1",
            onClick: () => { alert("esta clicavel"); }
          }

        ]
      }, {
        id: 2,
        title: "Porque pensar sobre ética?",
        indicator: "1.1",
        percentage: 90,
        onClick: () => {
          click = true;
        }
      }]}
      subtitle="Cidadania e Direitos Humanos"
    />);
    const instance = component.getInstance();
    expect(click).toBe(false);
    instance.handleClickSubItems(0, () => {
      click = true;
    });
    expect(click).toBe(true);
  });
});
