import React from "react";
import TestRenderer from "react-test-renderer";
import { configure, mount, shallow } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import wait from "waait";
import KTextArea from "../components/atoms/KTextArea";

configure({ adapter: new Adapter() });

describe("KTextArea component", () => {
  it("Is the onchange method working?", () => {
    const wrapper = shallow(<KTextArea titleButton="Exit" text="blá blá" id={1} />);
    const instance = wrapper.instance();
    wrapper.find("#textarea-content-1").simulate("change", {
      target: {
        value: "Your new Value",
        scrollHeight: 60,
        rows: 2
      }
    });
    expect(instance.state.rows).toBe(2);
  });
  it("Is the onchange with scroll === null method working?", () => {
    const wrapper = shallow(<KTextArea titleButton="Exit" text="blá blá" id={1} />);
    const instance = wrapper.instance();
    wrapper.find("#textarea-content-1").simulate("change", { target: { value: "Your new Value", rows: 2 } });
    expect(instance.state.rows).toBe(1);
  });
  it("Is it rendering with text?", async () => {
    const component = TestRenderer.create(<KTextArea text="ola" id={2} />);
    expect(component.toJSON().children[0].children[0].children[0].children[0].props.defaultValue).toBe("ola");
  });
  it("UNSAFE_componentWillReceiveProps", async () => {
    const wrapper = mount(<KTextArea
      titleButton="Exit"
      text="blá blá"
      id={1}
      onClick={() => {
      }}
    />);
    wrapper.instance().UNSAFE_componentWillReceiveProps({ text: "ola" });
    await wait(0);
    expect(wrapper.find("#textarea-content-1").getElement().ref.current.value).toBe("blá blá");
    wrapper.unmount();
  });
  it("UNSAFE_componentWillReceiveProps with nextProps.text === undefined", async () => {
    const wrapper = mount(<KTextArea
      titleButton="Exit"
      text="blá blá"
      id={1}
      onClick={() => {
      }}
    />);
    wrapper.find("#textarea-content-1").getElement().ref.current = undefined;
    wrapper.instance().UNSAFE_componentWillReceiveProps({ text: "ola" });
    await wait(0);
    expect(wrapper.find("#textarea-content-1").getElement().ref.current).toBe(undefined);
  });
  it("UNSAFE_componentWillReceiveProps with nextProps.text", async () => {
    const wrapper = mount(<KTextArea
      titleButton="Exit"
      text="blá blá"
      id={1}
      onClick={() => {
      }}
    />);
    wrapper.find("#textarea-content-1").getElement().ref.current = undefined;
    wrapper.instance().UNSAFE_componentWillReceiveProps({ text: undefined });
    await wait(0);
    expect(wrapper.find("#textarea-content-1").getElement().ref.current).toBe(undefined);
  });
  it("Is the onClick method working?", async () => {
    let click = false;
    const component = TestRenderer.create(<KTextArea
      titleButton="Exit"
      text="blá blá"
      id={1}
      onClick={() => {
        click = true;
      }}
    />);
    component.getInstance().handleClick(() => {
      click = true;
    });
    expect(click).toBe(true);
  });
  it("Is the onClick method working with this.text_area.current == null?", async () => {
    const wrapper = mount(<KTextArea
      titleButton="Exit"
      text="blá blá"
      id={1}
      onClick={() => {
      }}
    />);
    wrapper.find("#textarea-content-1").getElement().ref.current = {};
    wrapper.instance().handleClick(() => {});
    await wait(0);
    expect(wrapper.find("#textarea-content-1").getElement().ref.current).toStrictEqual({});
  });
  it("Is the handleWindowSizeChange method working?", async () => {
    let click = false;
    const wrapper = mount(<KTextArea
      titleButton="Exit"
      text="blá blá"
      id={1}
      onClick={() => {
      }}
    />);
    wrapper.find("#textarea-content-1").getElement().ref.current = {};
    wrapper.instance().handleWindowSizeChange(click = true);
    await wait(0);
    expect(click).toBe(true);
  });
  it("Is the handleChange with isMobile true method working?", async () => {
    const wrapper = mount(<KTextArea
      titleButton="Exit"
      text="blá blá"
      id={1}
      onClick={() => {
      }}
    />);
    await wait(0);
    wrapper.instance().state.isMobile = true;
    wrapper.find("#textarea-content-1").simulate("change", {
      target: {
        value: "Your new Value",
        scrollHeight: 30,
        rows: 1
      }
    });
    const instance = wrapper.instance();
    expect(instance.state.rows).toBe(1);
  });
  it("Is the handleChange row 3with isMobile true method working?", async () => {
    const wrapper = mount(<KTextArea
      text="blá blá"
      id={1}
    />);
    await wait(0);
    wrapper.instance().state.isMobile = true;
    wrapper.find("#textarea-content-1").simulate("change", {
      target: {
        value: "Your new Value",
        scrollHeight: 60,
        rows: 2
      }
    });
    const instance = wrapper.instance();
    expect(instance.state.rows).toBe(2);
  });
  it("Is the option.block true method working?", async () => {
    const component = TestRenderer.create(<KTextArea
      options={[{
        id: 1, title: "Cancelar", border: false, emptyLockedButton: true, block: true
      }]}
      text=""
      id={2}
    />);
    expect(component.toJSON().children[0].children[0].children[1].children[0].props.className).toBe("disabled group-icon ");
  });
  it("Is the onclick method working?", async () => {
    let clik = false;
    const component = TestRenderer.create(<KTextArea
      options={[{
        id: 1, title: "Cancelar", border: false, emptyLockedButton: true, block: true, handleClickButton: () => {}
      }]}
      text=""
      id={2}
    />);
    await wait(100);
    const onclickutton = component.root.findByProps({ id: "confirm-send-1" });
    // eslint-disable-next-line no-return-assign
    onclickutton._fiber.pendingProps.onClick(clik = true);
    expect(clik).toBe(true);
  });

  it("Is the row > 1 method working?", async () => {
    const component = TestRenderer.create(<KTextArea
      options={[{
        id: 1, title: "Cancelar", border: false, emptyLockedButton: true, block: true, handleClickButton: () => {}
      }]}
      text="dsjfhsdkjfhskdjhfkjdshf"
      id={1}
    />);
    const textatera = component.root.findByProps({ id: "textarea-content-1" });
    textatera._fiber.pendingProps.onChange(
      {
        target: {
          value: "Your new Value",
          scrollHeight: 60,
          rows: 0
        }
      },
    );
    expect(component.toJSON().children[0].children[0].children[1].children[0].props.className).toBe("disabled group-icon --more-one-line");
  });
  it("Is the row  method working?", async () => {
    const component = TestRenderer.create(<KTextArea
      options={[{
        id: 1, title: "Cancelar", border: false, emptyLockedButton: false, block: true, handleClickButton: () => {}
      }]}
      text="dsjfhsdkjfhskdjhfkjdshf"
      id={1}
    />);
    const textatera = component.root.findByProps({ id: "textarea-content-1" });
    textatera._fiber.pendingProps.onChange(
      {
        target: {
          value: "Your new Value",
          scrollHeight: 60,
          rows: 0
        }
      },
    );
    expect(component.toJSON().children[0].children[0].children[1].children[0].props.className).toBe(" group-icon --more-one-line");
  });
  it("Is the border = true and empty = true  method working?", async () => {
    const component = TestRenderer.create(<KTextArea
      options={[{
        id: 1, title: "Cancelar", border: true, emptyLockedButton: true, block: true, handleClickButton: () => {}
      }]}
      text="dsjfhsdkjfhskdjhfkjdshf"
      id={1}
    />);
    const textatera = component.root.findByProps({ id: "textarea-content-1" });
    textatera._fiber.pendingProps.onChange(
      {
        target: {
          value: "Your new Value",
          scrollHeight: 60,
          rows: 0
        }
      },
    );
    expect(component.toJSON().children[0].children[0].children[1].children[0].props.className).toBe(" group-icon --more-one-line");
  });
});
