import React from "react";
import TestRenderer from "react-test-renderer";
import KText from "../components/atoms/KText/KText";

describe("KText component", () => {
  it("get text? - teste", () => {
    const component = TestRenderer.create(<KText text="teste" />);
    const instance = component.getInstance();
    expect(instance.props.text).toBe("teste");
    // test defaults value
    expect(instance.props.state).toBe("dark");
    expect(instance.props.brand).toBe("default");
    expect(instance.props.fontSize).toBe("20px");
    expect(instance.props.font).toBe("Nunito Sans");
  });
  it("get text colors? -textColor='#FF001A'", () => {
    const component = TestRenderer.create(<KText text="teste" textColor="#FF001A" />);
    const instance = component.getInstance();
    expect(instance.props.text).toBe("teste");
    expect(instance.props.textColor).toBe("#FF001A");
    // test defaults value
    expect(instance.props.state).toBe("dark");
    expect(instance.props.brand).toBe("default");
    expect(instance.props.fontSize).toBe("20px");
    expect(instance.props.font).toBe("Nunito Sans");
  });
  it("get fontSize? - fontSize='50px'", () => {
    const component = TestRenderer.create(<KText text="teste" fontSize="50px" />);
    const instance = component.getInstance();
    expect(instance.props.text).toBe("teste");
    expect(instance.props.fontSize).toBe("50px");
    // test defaults value
    expect(instance.props.state).toBe("dark");
    expect(instance.props.brand).toBe("default");
    expect(instance.props.font).toBe("Nunito Sans");
  });
  it("get font? - font='Arial'", () => {
    const component = TestRenderer.create(<KText text="teste" font="Arial" />);
    const instance = component.getInstance();
    expect(instance.props.text).toBe("teste");
    expect(instance.props.font).toBe("Arial");
    // test defaults value
    expect(instance.props.state).toBe("dark");
    expect(instance.props.brand).toBe("default");
    expect(instance.props.fontSize).toBe("20px");
  });
});
