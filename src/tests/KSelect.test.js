import React from "react";
import TestRenderer from "react-test-renderer";
import { mount } from "enzyme";
import wait from "waait";
import KSelect from "../components/molecules/KSelect";

describe("KSelect component", () => {
  it("Is the defaultValue classname?", async () => {
    const component = TestRenderer.create(
      <KSelect
        id="select"
        required
        list={["1", "2"]}
        funcClick={() => {}}
      />,
    );
    await wait(0);
    await component.getInstance().loadingPromise;
    expect(component.toJSON().props.className.indexOf("k-select") >= 0).toBe(true);
  });
  it("click select", async () => {
    const component = TestRenderer.create(
      <KSelect
        id="select"
        list={["1", "2"]}
        funcClick={() => {}}
        initialIndex={2}
      />,
    );
    await wait(1000);
    const instance = component.getInstance();
    await instance.loadingPromise;
    instance.handlerSelect();
    component.toJSON().children[0].children[1].children[0].children[0].children[0].props.onClick();
    component.toJSON().children[0].children[0].children[0].children[1].children[0].children[1].props.onClick();
  });
  it("sem funcClick", async () => {
    const component = TestRenderer.create(
      <KSelect
        id="select"
        list={["1", "2"]}
      />,
    );
    await wait(0);
    const instance = component.getInstance();
    instance.handlerSelect();
    component.toJSON().children[0].children[1].children[0].children[0].children[0].props.onClick();
  });
  it("componentWillReceiveProps with nextProps list", async () => {
    const list = [
      "Bioquímica",
      "Construindo uma Carreira de Sucesso: Saúde",
      "Genética",
      "Ciências Morfofuncionais dos Sistemas Digestório Endócrino e Renal",
      "Ciências Morfofuncionais dos Sistemas Tegumentar Reprodutor e Locomotor"
    ];
    const wrapper = mount(
      <KSelect
        id="select"
        initialIndex={2}
        list={[]}
      />
    );
    wrapper.setProps({ list });
    expect(wrapper.state().selected).toBe("Genética");
  });

  it("componentWillReceiveProps with nextProps initialIndex null", async () => {
    const list = [
      "Bioquímica",
      "Construindo uma Carreira de Sucesso: Saúde",
      "Genética",
      "Ciências Morfofuncionais dos Sistemas Digestório Endócrino e Renal",
      "Ciências Morfofuncionais dos Sistemas Tegumentar Reprodutor e Locomotor"
    ];
    const wrapper = mount(
      <KSelect
        id="select"
        initialIndex={2}
        list={list}
      />
    );
    wrapper.setProps({ initialIndex: null });
    expect(wrapper.state().selected).toBe("Genética");
  });
  it("componentWillReceiveProps with nextProps initialIndex -1", async () => {
    const list = [
      "Bioquímica",
      "Construindo uma Carreira de Sucesso: Saúde",
      "Genética",
      "Ciências Morfofuncionais dos Sistemas Digestório Endócrino e Renal",
      "Ciências Morfofuncionais dos Sistemas Tegumentar Reprodutor e Locomotor"
    ];
    const wrapper = mount(
      <KSelect
        id="select"
        initialIndex={2}
        list={list}
      />
    );
    wrapper.setProps({ initialIndex: -1 });
    expect(wrapper.state().selected).toBe("");
  });
  it("List with objects", async () => {
    const list = [
      { label: "Bioquímica", shortname: "BIO_TESTE" },
      { label: "Construindo uma Carreira de Sucesso: Saúde" },
      { label: "Genética" },
      { label: "Ciências Morfofuncionais dos Sistemas Digestório Endócrino e Renal" },
      { label: "Ciências Morfofuncionais dos Sistemas Tegumentar Reprodutor e Locomotor" }
    ];
    const wrapper = mount(
      <KSelect
        id="select"
        initialIndex={2}
        list={list}
      />
    );
    wrapper.setProps({ initialIndex: -1 });
    expect(wrapper.state().selected).toBe("");
    wrapper.setProps({ initialIndex: 2 });
    expect(wrapper.state().selected).toBe("Genética");
  });
  it("click select list with objects", async () => {
    const list = [
      { label: "Bioquímica", shortname: "BIO_TESTE" },
      { label: "Construindo uma Carreira de Sucesso: Saúde" },
      { label: "Genética" },
      { label: "Ciências Morfofuncionais dos Sistemas Digestório Endócrino e Renal" },
      { label: "Ciências Morfofuncionais dos Sistemas Tegumentar Reprodutor e Locomotor" }
    ];
    const component = TestRenderer.create(
      <KSelect
        id="select"
        list={list}
        funcClick={() => {}}
        initialIndex={2}
      />,
    );
    await wait(0);
    const instance = component.getInstance();
    instance.handlerSelect();
    component.toJSON().children[0].children[1].children[0].children[0].children[0].props.onClick();
  });
});
