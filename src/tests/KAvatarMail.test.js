import React from "react";
import TestRenderer from "react-test-renderer";
import wait from "waait";
import KAvatarMail from "../components/atoms/KAvatarMail";

describe("Is the KAvatarMail component working?", () => {
  it("Is it rendering?", async () => {
    const component = TestRenderer.create(
      <KAvatarMail
        fullName="Brad Pitt"
        style={{ height: "50px", width: "50px" }}
      />
    );
    await wait(0);
    await component.getInstance().loadingPromise;
    expect(component.toJSON().props.className).toEqual("k-avatar_mail is-primary-default");
  });

  it("Is it rendering with brand?", async () => {
    const component = TestRenderer.create(<KAvatarMail fullName="Brad Pitt da Silva" brand="anhanguera" />);
    await wait(0);
    await component.getInstance().loadingPromise;
    expect(component.toJSON().props.className).toEqual("k-avatar_mail is-primary-anhanguera");
  });
  it("Is it rendering with name null?", async () => {
    const component = TestRenderer.create(
      <KAvatarMail
        style={{ height: "50px", width: "50px" }}
      />
    );
    await wait(0);
    await component.getInstance().loadingPromise;
    expect(component.toJSON().props.className).toEqual("k-avatar_mail is-primary-default");
  });
});
