import React from "react";
import TestRenderer from "react-test-renderer";
import { configure, mount } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import KModal from "../components/molecules/KModal/KModal";

configure({ adapter: new Adapter() });

describe("Is the KConfirm component working?", () => {
  it("Is it rendering without props?", async () => {
    const component = TestRenderer.create(<KModal />);
    expect(component).toBeDefined();
  });
  it("Is it rendering with id?", async () => {
    const component = TestRenderer.create(<KModal id="1" />);
    expect(component).toBeDefined();
    const instance = component.getInstance();
    expect(instance.props.id).toBe("1");
  });
  it("Is it rendering with openModal?", async () => {
    const isOpen = false;
    const component = TestRenderer.create(
      <KModal id="1" openModal={isOpen} />,
    );
    expect(component).toBeDefined();
    const instance = component.getInstance();
    expect(instance.props.id).toBe("1");
    expect(instance.props.openModal).toBe(isOpen);
  });
  it("Is it rendering?", async () => {
    const isOpen = true;
    mount(
      <KModal
        id="1"
        openModal={isOpen}
        onHide={() => {
        }}
      />,
    );
  });
});
