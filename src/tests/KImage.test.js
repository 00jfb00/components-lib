import React from "react";
import TestRenderer from "react-test-renderer";
import wait from "waait";
import KImage from "../components/atoms/KImage/KImage";

describe("Is the KImage component working?", () => {
  const imageUrl = "https://s3-us-west-2.amazonaws.com/estudante-production/app/public/ckeditor_assets/pictures/128/original/anhanguera_full.svg";

  it("Is it rendering?", async () => {
    const component = TestRenderer.create(<KImage src={imageUrl} />);
    const instance = component.getInstance();
    expect(instance.state.propsClasses).toBeDefined();
    expect(component.toJSON().props.className).toEqual("k-image");
  });

  it("Is it rendering with class is-responsive?", async () => {
    const component = TestRenderer.create(
      <KImage src={imageUrl} responsive />,
    );
    const instance = component.getInstance();
    expect(instance.state.propsClasses).toBeDefined();
    expect(component.toJSON().props.className).toEqual("k-image is-responsive");
  });

  it("Is it rendering with class is-responsive-height?", async () => {
    const component = TestRenderer.create(
      <KImage src={imageUrl} responsiveHeight />,
    );
    const instance = component.getInstance();
    expect(instance.state.propsClasses).toBeDefined();
    expect(component.toJSON().props.className).toEqual(
      "k-image is-responsive-height",
    );
  });

  it("Is it rendering with custom class my-class?", async () => {
    const component = TestRenderer.create(
      <KImage src={imageUrl} customClass="my-class" />,
    );
    const instance = component.getInstance();
    expect(instance.state.propsClasses).toBeDefined();
    expect(component.toJSON().props.className).toEqual("k-image my-class");
  });

  it("Is it rendering with custom style max-width?", async () => {
    const component = TestRenderer.create(
      <KImage src={imageUrl} responsive maxWidth={100} />,
    );
    expect(component.toJSON().props.style.maxWidth).toEqual(100);
  });

  it("Is it rendering with custom style max-height?", async () => {
    const component = TestRenderer.create(
      <KImage src={imageUrl} responsive maxHeight={100} />,
    );
    expect(component.toJSON().props.style.maxHeight).toEqual(100);
  });

  it("Is it clicking?", async () => {
    let click = false;
    expect(click).toBe(false);
    let component = TestRenderer.create(
      <KImage src={imageUrl} onClick={() => { click = true; }} responsive maxHeight={100} />,
    );
    let instance = component.getInstance();
    instance.handleClick();
    await wait(0);
    expect(click).toBe(true);
    click = false;
    component = TestRenderer.create(
      <KImage src={imageUrl} responsive maxHeight={100} />,
    );
    instance = component.getInstance();
    instance.handleClick();
    await wait(0);
    expect(click).toBe(false);
  });
});
