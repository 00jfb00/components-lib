import React from "react";
import wait from "waait";
import { configure, mount } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import KVideoPlayer from "../components/atoms/KVideoPlayer";

configure({ adapter: new Adapter() });

describe("Is the KVideoPlayerMdStrm component working?", () => {
  const urlContent = "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4";

  it("Is it rendering video-player?", async () => {
    let clickDuration = false;
    let clickProgress = false;
    let clickEnded = false;
    expect(clickDuration).toBe(false);
    expect(clickProgress).toBe(false);
    expect(clickEnded).toBe(false);
    let component = mount(
      <KVideoPlayer
        playing
        url={urlContent}
      />
    );
    await wait(3000);
    await component.instance().loadingPromise;
    expect(component).toBeDefined();
    let instance = component.instance();
    instance.handleDuration();
    expect(clickDuration).toBe(false);
    instance.handleProgress();
    expect(clickProgress).toBe(false);
    instance.handleEnded();
    expect(clickEnded).toBe(false);
    component = mount(
      <KVideoPlayer
        onDuration={() => { clickDuration = true; }}
        onProgress={() => { clickProgress = true; }}
        onEnded={() => { clickEnded = true; }}
        playing
        url={urlContent}
      />
    );
    await wait(0);
    await component.instance().loadingPromise;
    instance = component.instance();
    instance.handleDuration();
    expect(clickDuration).toBe(true);
    instance.handleProgress();
    expect(clickProgress).toBe(true);
    instance.handleEnded();
    expect(clickEnded).toBe(true);
  });
});
