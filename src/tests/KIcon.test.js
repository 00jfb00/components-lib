import React from "react";
import TestRenderer from "react-test-renderer";
import KIcon from "../components/atoms/KIcon/KIcon";

describe("Is KIcon component working?", () => {
  it("Is the onClick method working?", async () => {
    let click = false;
    const component = TestRenderer.create(
      <KIcon
        icon="phone"
        rotate
        onClick={() => {
          click = true;
        }}
      />,
    );
    const instance = component.getInstance();
    expect(click).toBe(false);
    instance.handleClick();
    expect(click).toBe(true);
  });
  it("Is the onClick with null?", async () => {
    const component = TestRenderer.create(
      <KIcon icon="phone" rotate onClick={null} />,
    );
    const instance = component.getInstance();
    instance.handleClick();
    expect(component).toBeDefined();
  });
});
