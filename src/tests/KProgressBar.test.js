import React from "react";
import TestRenderer from "react-test-renderer";
import KProgressBar from "../components/atoms/KProgressBar/KProgressBar";

describe("Progress Bar component", () => {
  it("Is the style property 'percentage' working?", async () => {
    const component = TestRenderer.create(<KProgressBar percentage={30} />);
    const instance = component.getInstance();
    expect(instance.props.percentage).toBeDefined();
    expect(instance.props.percentage).toBe(30);
  });

  it("Is the style property 'animated' working?", async () => {
    const component = TestRenderer.create(<KProgressBar animated percentage={30} />);
    const instance = component.getInstance();
    expect(instance.props.animated).toBeDefined();
    expect(instance.props.animated).toBe(true);
  });

  it("Is the style property 'striped' working?", async () => {
    const component = TestRenderer.create(<KProgressBar striped percentage={30} />);
    const instance = component.getInstance();
    expect(instance.props.striped).toBeDefined();
    expect(instance.props.striped).toBe(true);
  });

  it("Is the style property 'striped' working 2?", async () => {
    const component = TestRenderer.create(<KProgressBar round percentage={30} />);
    const instance = component.getInstance();
    expect(instance.props.round).toBeDefined();
    expect(instance.props.round).toBe(true);
  });
});
