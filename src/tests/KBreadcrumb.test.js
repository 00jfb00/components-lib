import React from "react";
import TestRenderer from "react-test-renderer";
import KBreadcrumb from "../components/molecules/KBreadcrumb/KBreadcrumb";

describe("KBreadcrumb component", () => {
  it("get routes? - ['teste1','teste2']", () => {
    const component = TestRenderer.create(<KBreadcrumb routes={["teste1", "teste2"]} />);
    const instance = component.getInstance();
    expect(instance.props.routes[0]).toBe("teste1");
    // test defaults value
    expect(instance.props.icon).toBe("circle");
    expect(instance.props.fontSize).toBe("20px");
    expect(instance.props.font).toBe("Nunito Sans");
    expect(instance.props.fontWeight).toBe("regular");
    expect(instance.props.state).toBe("dark");
    expect(instance.props.brand).toBe("default");
    component.update(<KBreadcrumb routes={["teste3", "teste4"]} />);
    const instanceUpdate = component.getInstance();
    expect(instanceUpdate.props.routes[0]).toBe("teste3");
  });
  it("isMobile? - ['teste1','teste2']", () => {
    const component = TestRenderer.create(<KBreadcrumb routes={["teste1", "teste2"]} isMobile />);
    const instance = component.getInstance();
    expect(instance.props.routes[0]).toBe("teste1");
    // test defaults value
    expect(instance.props.icon).toBe("circle");
    expect(instance.props.fontSize).toBe("20px");
    expect(instance.props.font).toBe("Nunito Sans");
    expect(instance.props.fontWeight).toBe("regular");
    expect(instance.props.state).toBe("dark");
    expect(instance.props.brand).toBe("default");
  });
});
