import React from "react";
import TestRenderer from "react-test-renderer";
import KMenuItem from "../components/atoms/KMenuItem/KMenuItem";

describe("Is the KMenuItem component working?", () => {
  const title = "Ciência da Computação";
  const subtitle = "RA: 0291830390";

  it(`Is it rendering with title ${title}?`, async () => {
    const component = TestRenderer.create(<KMenuItem title={title} />);
    const instance = component.getInstance();
    expect(instance.handleClick()).toBe(undefined);
    expect(component).toBeDefined();
    expect(component.toJSON().children[0].children[0].children[0]).toEqual("Ciência da Computação");
  });

  it(`Is it rendering with title ${title} and subtitle ${subtitle}?`, async () => {
    const component = TestRenderer.create(<KMenuItem highlightTitle title={title} subtitle={subtitle} />);
    expect(component).toBeDefined();
    expect(component.toJSON().children[0].children[0].children[0]).toEqual("Ciência da Computação");
    expect(component.toJSON().children[0].children[1].children[0]).toEqual("RA: 0291830390");
    expect(component.toJSON().props.className.indexOf("k-menu-item") >= 0).toBe(true);
  });

  it(`Is it rendering with title ${title} and subtitle ${subtitle} and active prop?`, async () => {
    const component = TestRenderer.create(<KMenuItem title={title} subtitle={subtitle} active />);
    expect(component).toBeDefined();
    expect(component.toJSON().children[0].children[0].children[0]).toEqual("Ciência da Computação");
    expect(component.toJSON().children[0].children[1].children[0]).toEqual("RA: 0291830390");
    expect(component.toJSON().props.className.indexOf("k-menu-item") >= 0).toBe(true);
    expect(component.toJSON().props.className.indexOf("is-active") >= 0).toBe(true);
  });

  it(`Is it rendering with title ${title} and subtitle ${subtitle} and receive callback?`, async () => {
    let callBackEvent = false;
    const component = TestRenderer.create(
      <KMenuItem
        title={title}
        subtitle={subtitle}
        active
        hasBorder
        onClick={() => {
          callBackEvent = true;
        }}
      />,
    );
    const callback = component.toJSON().props.onClick;
    component.toJSON().props.onClick();
    expect(component).toBeDefined();
    expect(component.toJSON().children[0].children[0].children[0]).toEqual("Ciência da Computação");
    expect(component.toJSON().children[0].children[1].children[0]).toEqual("RA: 0291830390");
    expect(typeof (callback)).toEqual("function");
    expect(callBackEvent).toEqual(true);
  });

  it("Is it rendering with icon?", async () => {
    const component = TestRenderer.create(<KMenuItem title={title} active icon="check-circle" highlightTitle />);
    const instance = component.getInstance();
    expect(instance.props.icon).toEqual("check-circle");
  });
  it("Is it not rendering with icon?", async () => {
    const component = TestRenderer.create(<KMenuItem title={title} active={false} icon="check-circle" highlightTitle={false} />);
    const instance = component.getInstance();
    expect(instance.props.active).toEqual(false);
  });
  it("Is it rendering with highlightTitle?", async () => {
    const component = TestRenderer.create(<KMenuItem title={title} active={false} icon="check-circle" highlightTitle />);
    const instance = component.getInstance();
    expect(instance.props.active).toEqual(false);
  });
});
