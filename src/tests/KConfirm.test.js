import React from "react";
import TestRenderer from "react-test-renderer";
import { configure, mount } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import KConfirm from "../components/molecules/KConfirm/KConfirm";

configure({ adapter: new Adapter() });

describe("Is the KConfirm component working?", () => {
  it("Is it rendering without props?", async () => {
    const component = TestRenderer.create(<KConfirm />);
    expect(component).toBeDefined();
  });
  it("Is it rendering with id?", async () => {
    const component = TestRenderer.create(<KConfirm id="1" />);
    expect(component).toBeDefined();
    const instance = component.getInstance();
    expect(instance.props.id).toBe("1");
  });
  it("Is it rendering with openModal?", async () => {
    const isOpen = false;
    const component = TestRenderer.create(
      <KConfirm id="1" openModal={isOpen} />,
    );
    expect(component).toBeDefined();
    const instance = component.getInstance();
    expect(instance.props.id).toBe("1");
    expect(instance.props.openModal).toBe(isOpen);
  });
  it("Is it rendering?", async () => {
    const isOpen = true;
    mount(
      <KConfirm
        id="1"
        openModal={isOpen}
        onHide={() => {
        }}
        agree={() => {
        }}
        span="Atenção"
        msg="Editando sua resposta, você perdera os likes que recebeu nela."
        question="Tem certeza de que quer continuar?"
      />,
    );
  });
  it("Is it rendering with icon?", async () => {
    const isOpen = true;
    mount(
      <KConfirm
        openModal={isOpen}
        onHide={() => {
        }}
        agree={() => {
        }}
        icon="smile"
        iconColor="black"
        sizeIcon="2x"
        span="Sorria"
        msg="Você esta sendo filmado."
        question="Concorda com nossas filmagens?"
      />,
    );
  });
});
