import React from "react";
import TestRenderer from "react-test-renderer";
import KBadge from "../components/atoms/KBadge/KBadge";

describe("Is the KBadge component working?", () => {
  it("Is it rendering?", async () => {
    const component = TestRenderer.create(
      <KBadge>
        <div style={{ height: "50px", width: "50px", backgroundColor: "blue" }} />
      </KBadge>,
    );
    expect(component.toJSON().props.className).toBe("k-badge");
  });

  it("Is it hiding?", async () => {
    const component = TestRenderer.create(
      <KBadge hide>
        <div style={{ height: "50px", width: "50px", backgroundColor: "blue" }} />
      </KBadge>,
    );
    expect(component.toJSON().children.filter(element => element.props.name === "badge").length).toBe(0);
  });

  it("Is it changing position?", async () => {
    const component = TestRenderer.create(
      <KBadge border={false} rounded={false} position="bottomLeft">
        <div style={{ height: "50px", width: "50px", backgroundColor: "blue" }} />
      </KBadge>,
    );
    expect(component.toJSON().children.filter(element => element.props.className === "bottomLeft").length).toBe(1);
  });
});
