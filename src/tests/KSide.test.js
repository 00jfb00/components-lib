import React from "react";
import TestRenderer from "react-test-renderer";
import wait from "waait";
import KSide from "../components/organisms/KSide";

describe("KSide component", () => {
  it("Is the defaultValue classname?", async () => {
    const componentWithHeight = TestRenderer.create(<KSide width={280} height={26} />);
    await wait(0);
    await componentWithHeight.getInstance().loadingPromise;
    expect(componentWithHeight.toJSON().props.className.indexOf("k-side") >= 0).toBe(true);

    const component = TestRenderer.create(<KSide width={280} />);
    await wait(0);
    await component.getInstance().loadingPromise;
    expect(component.toJSON().props.className.indexOf("k-side") >= 0).toBe(true);
  });
});
