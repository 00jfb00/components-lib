import React from "react";
import TestRenderer from "react-test-renderer";
import KDonuts from "../components/molecules/KDonuts/KDonuts";

describe("Progress Bar Donuts", () => {
  it("Is the style property 'maxPoints' working?", async () => {
    const component = TestRenderer.create(<KDonuts maxPoints={14000} />);
    const instance = component.getInstance();
    expect(instance.props.maxPoints).toBe(14000);
  });

  it("Is the style property 'miniPoints' working?", async () => {
    const component = TestRenderer.create(<KDonuts animated miniPoints={8000} />);
    const instance = component.getInstance();
    expect(instance.props.miniPoints).toBe(8000);
  });

  it("Is the style property 'studentPoints' working?", async () => {
    const component = TestRenderer.create(<KDonuts striped studentPoints={7000} />);
    const instance = component.getInstance();
    expect(instance.props.studentPoints).toBe(7000);
  });

  it("Is the style property 'active' working?", async () => {
    const component = TestRenderer.create(<KDonuts striped maxPoints={14000} />);
    expect(component.toJSON().children[1].children[0].props.color).toBe("#212121");
  });

  it("Is the style property 'inactive' working?", async () => {
    const component = TestRenderer.create(<KDonuts striped maxPoints={0} />);
    expect(component.toJSON().children[1].children[0].props.color).toBe("#707070");
  });

  it("Is the style property 'ProgressBar' working?", async () => {
    const component = TestRenderer.create(<KDonuts striped maxPoints={14000} miniPoints={8000} studentPoints={7000} />);
    expect(component.toJSON().children[0].children[2].props.stroke).toBe("#fb9678");
    expect(component.toJSON().children[0].children[2].props.strokeDasharray).toBe("50 50");
  });

  it("Is the style property 'ProgressBarSucess' working?", async () => {
    const component = TestRenderer.create(<KDonuts striped maxPoints={14000} miniPoints={8000} studentPoints={9000} />);
    expect(component.toJSON().children[0].children[2].props.stroke).toBe("#3ac7b1");
    expect(component.toJSON().children[0].children[2].props.strokeDasharray).toBe("64 36");
  });

  it("Is autoApproved working?", async () => {
    const component = TestRenderer.create(
      <KDonuts
        miniPoints={8000}
        maxPoints={14000}
        studentPoints={9000}
        autoApproved={false}
      />
    );
    expect(component.toJSON().children[0].children[2].props.stroke).toBe("#fb9678");
  });

  it("Is autoApproved and approved working?", async () => {
    const component = TestRenderer.create(
      <KDonuts
        miniPoints={8000}
        maxPoints={14000}
        studentPoints={6000}
        autoApproved={false}
        approved
      />
    );
    expect(component.toJSON().children[0].children[2].props.stroke).toBe("#3ac7b1");
  });
});
