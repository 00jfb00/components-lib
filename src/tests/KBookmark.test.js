import React from "react";
import TestRenderer from "react-test-renderer";
import wait from "waait";
import KBookmark from "../components/atoms/KBookmark";

describe("KBookmark component", () => {
  it("Is the defaultValue classname?", async () => {
    const component = TestRenderer.create(<KBookmark />);
    await wait(0);
    await component.getInstance().loadingPromise;
    expect(component.toJSON().props.className).toBe("k-bookmark ");
  });

  it("Is show?", async () => {
    const component = TestRenderer.create(<KBookmark show icon="bookmark" />);
    await wait(0);
    await component.getInstance().loadingPromise;
    expect(component.toJSON().children[0].props.className).toBe("bookmark-icon ");
  });

  it("Accept className?", async () => {
    const component = TestRenderer.create(<KBookmark changeIcon className="-my-class" show />);
    await wait(0);
    await component.getInstance().loadingPromise;
    expect(component.toJSON().props.className).toBe("k-bookmark -my-class");
  });
});
