import React from "react";
import TestRenderer from "react-test-renderer";
import wait from "waait";
import KChipInput from "../components/molecules/KChipInput/KChipInput";

describe("Is KIcon component working?", () => {
  it("Is the onClick method working?", async () => {
    let click = null;
    const component = TestRenderer.create(<KChipInput
      options={[
        {
          id: 7,
          title: "Cancelar",
          border: true
        }]}
      rows="3"
      defaultValue="Teste de texto default"
    />);
    const instance = component.getInstance();
    instance.handleClick(() => { click = true; });
    await wait(0);
    expect(click).toBe(true);
  });
  it("Is the onClick undefined method working?", async () => {
    let click = null;
    const component = TestRenderer.create(<KChipInput
      options={[
        {
          id: 7,
          title: "Cancelar",
          border: true
        }]}
      defaultValue="Teste de texto default"
    />);
    const instance = component.getInstance();
    instance.handleClick(() => { click = true; });
    await wait(0);
    expect(click).toBe(true);
  });
  it("Is the onclickChip method working?", async () => {
    let deleteChip = null;
    const component = TestRenderer.create(<KChipInput
      options={[{ id: 1, title: "Cancelar", border: true }]}
      chips={[{ key: 1, label: "Indiara Beltrane" }, { key: 2, label: "Indiara Beltrane" }]}
      onClickDeleteChip={() => {
        deleteChip = false;
      }}
      defaultValue="Teste de texto default"
    />);
    const instance = component.getInstance();
    instance.onClickDeleteChip([{ key: 1, label: "Indiara Beltrane" }], deleteChip = true);
    await wait(0);
    expect(deleteChip).toBe(true);
  });

  it("Is the onclickChip method working with callback?", async () => {
    let deleteChip = null;
    const component = TestRenderer.create(<KChipInput
      options={[{ id: 1, title: "Cancelar", border: true }]}
      chips={[{ key: 1, label: "Indiara Beltrane" }, { key: 2, label: "Indiara Beltrane" }]}
      onClickDeleteChip={() => {
        deleteChip = false;
      }}
      defaultValue="Teste de texto default"
      deleteCallback={() => {}}
    />);
    const instance = component.getInstance();
    instance.onClickDeleteChip([{ key: 1, label: "Indiara Beltrane" }], deleteChip = true);
    await wait(0);
    expect(deleteChip).toBe(true);
  });

  it("Is the handleChange method working?", async () => {
    const component = TestRenderer.create(<KChipInput
      options={[{ id: 1, title: "Cancelar", border: true }]}
      chips={[{ key: 1, label: "Indiara Beltrane" }]}
      onClickDeleteChip={() => { }}
      defaultValue="Teste de texto default"
    />);
    const instance = component.getInstance();
    const event = { target: { innerText: "olá" } };
    instance.input.current = { innerText: "olá" };
    instance.handleChange(event);
    await wait(0);
    expect(instance.state.defaultValue).toBe("olá");
    expect(instance.input.current.innerText).toBe("olá");
  });
  it("Is the onKeyDown method working?", async () => {
    const component = TestRenderer.create(<KChipInput
      options={[{ id: 1, title: "Cancelar", border: true }]}
      chips={[{ key: 1, label: "Indiara Beltrane" }, { key: 2, label: "Beltrane" }]}
    />);
    const instance = component.getInstance();
    const event = { keyCode: 8 };
    instance.onKeyDown(event);
    await wait(1000);
    expect(instance.state.chips.length).toBe(1);
  });
  it("Is the onKeyDown null method working?", async () => {
    const component = TestRenderer.create(<KChipInput
      options={[{ id: 1, title: "Cancelar", border: true }]}
      chips={[{ key: 1, label: "Indiara Beltrane" }, { key: 2, label: "Beltrane" }]}
    />);
    const instance = component.getInstance();
    const event = { keyCode: 10 };
    instance.onKeyDown(event);
    await wait(1000);
    expect(instance.state.chips.length).toBe(2);
  });
  it("Is the onDeleteChip method working?", async () => {
    const component = TestRenderer.create(<KChipInput
      options={[{ id: 1, title: "Cancelar", border: true }]}
      chips={[{
        key: 1, label: "Indiara Beltrane", icon: ["kci", "close"], onDeleteChip: true
      }, {
        key: 3, label: "Indiara Beltrane", icon: ["kci", "close"], onDeleteChip: true
      }, {
        key: 2, label: "Indiara Beltrane", icon: ["kci", "close"], onDeleteChip: true
      }]}
      defaultValue="Teste de texto default"
    />);
    await wait(0);
    const instance = component.getInstance();
    component.root.findByProps({ id: "1" }).props.onClick();
    expect(instance.state.chips.length).toBe(2);
  });
  it("Is the emptyLockedButton method working?", async () => {
    const component = TestRenderer.create(<KChipInput
      buttonLeft
      options={[{
        id: 1, title: "Cancelar", border: true, emptyLockedButton: true
      }]}
      chips={[]}
    />);
    expect(component.toJSON().children[1].children[0].children[0].props.className).toBe("group-buttons disabled");
  });
  it("Is the emptyLockedButton undefined method working?", async () => {
    const component = TestRenderer.create(<KChipInput
      buttonLeft
      options={[{
        id: 1, title: "Cancelar", border: true, emptyLockedButton: true
      }]}
      chips={[{
        key: 1, label: "Indiara Beltrane", icon: ["kci", "close"], onDeleteChip: true
      }]}
      defaultValue=""
    />);
    expect(component.toJSON().children[1].children[0].children[0].props.className).toBe("group-buttons ");
  });
  it("Is the value == vazio method working?", async () => {
    const component = TestRenderer.create(<KChipInput
      options={[{
        id: 1, title: "Cancelar", border: true, emptyLockedButton: true
      }]}
      chips={[]}
      defaultValue=""
    />);
    expect(component.toJSON().children[1].children[0].children[0].props.className).toBe("group-buttons disabled");
  });
  it("Is the chip vazio method working?", async () => {
    const component = TestRenderer.create(<KChipInput
      buttonIsOff
      options={[{
        id: 1, title: "Cancelar", border: true, emptyLockedButton: true
      }]}
      defaultValue=""
    />);
    const instance = component.getInstance();
    expect(instance.state.chips.length).toBe(0);
  });
  it("Is the sem option method working?", async () => {
    const component = TestRenderer.create(<KChipInput
      buttonIsOff
      defaultValue=""
    />);
    const instance = component.getInstance();
    expect(instance.state.options).toBe(undefined);
  });
  it("Is the option.block true method working?", async () => {
    const component = TestRenderer.create(<KChipInput
      options={[{
        id: 1, title: "Cancelar", border: true, emptyLockedButton: true, block: true
      }]}
      buttonIsOff
      defaultValue=""
    />);
    expect(component.toJSON().children[1].children[0].children[0].props.className).toBe("group-buttons block");
  });
  it("Is the option.block false method working?", async () => {
    const component = TestRenderer.create(<KChipInput
      options={[{
        id: 1, title: "Cancelar", border: true, emptyLockedButton: true, block: false
      }]}
      buttonIsOff
      defaultValue=""
    />);
    expect(component.toJSON().children[1].children[0].children[0].props.className).toBe("group-buttons disabled");
  });
  it("Is the buttonLeft method working?", async () => {
    const component = TestRenderer.create(<KChipInput
      options={[{
        id: 1, title: "Cancelar", border: true, emptyLockedButton: true, block: false
      }]}
      buttonLeft
      buttonIsOff
      defaultValue=""
    />);
    expect(component.toJSON().children[1].children[0].children[0].props.className).toBe("group-buttons disabled");
  });
  it("Is the handleClickDelete method working?", async () => {
    const component = TestRenderer.create(<KChipInput
      options={[
        {
          id: 7,
          title: "Cancelar",
          border: true,
          delete: true,
          handleClickButton: () => {}
        }]}
      defaultValue="Teste de texto default"
    />);
    const instance = component.getInstance();
    instance.input.current = { innerText: "olá" };
    instance.handleClickDelete();
    await wait(0);
    expect(instance.input.current.innerText).toBe("");
  });
  it("Is the onclick props method working?", async () => {
    const component = TestRenderer.create(<KChipInput
      options={[
        {
          id: 7,
          title: "Cancelar",
          border: true,
          delete: true,
          handleClickButton: () => {}
        }]}
      defaultValue="Teste de texto default"
    />);
    await wait(10);
    const instance = component.getInstance();
    const option = component.root.findByProps({ id: "option-7" });
    instance.input.current = { innerText: "olá" };
    option._fiber.pendingProps.onClick();
  });
  it("Is the onclick in button delet method working?", async () => {
    const component = TestRenderer.create(<KChipInput
      options={[
        {
          id: 7,
          title: "Cancelar",
          border: true,
          handleClickButton: () => {}
        }]}
      defaultValue="Teste de texto default"
    />);
    await wait(10);
    const option = component.root.findByProps({ id: "option-7" });
    option._fiber.pendingProps.onClick();
  });
  it("Is the is the message in button delet method working?", async () => {
    const component = TestRenderer.create(<KChipInput
      options={[{ id: 1, title: "Cancelar", border: true }]}
      chips={[{
        key: 1, label: "Indiara Beltrane", icon: ["kci", "close"], onDeleteChip: true
      }, {
        key: 3, label: "Indiara Beltrane", icon: ["kci", "close"], onDeleteChip: true
      }, {
        key: 2, label: "Indiara Beltrane", icon: ["kci", "close"], onDeleteChip: true
      }]}
      defaultValue="Teste de texto default"
      message
    />);
    await wait(0);
    const instance = component.getInstance();
    const chip = component.root.findByProps({ id: "1" });
    chip._fiber.pendingProps.ClickChip();
    expect(instance.props.message).toBe(true);
  });
  it("Is the style with more one line method working?", async () => {
    const component = TestRenderer.create(<KChipInput
      options={[{ id: 1, title: "Cancelar", border: true }]}
      chips={[{
        key: 1, label: "Indiara Beltrane", icon: ["kci", "close"], onDeleteChip: true
      }]}
      defaultValue="Teste de texto default"
      rows="5"
      message
    />);
    const instance = component.getInstance();
    instance.setState({ oneLine: false });
    await wait(100);
    expect(component.toJSON().children[0].children[1].props.className).toBe("col-button icon-bottom ");
  });
  it("Is the style disable method working?", async () => {
    const component = TestRenderer.create(<KChipInput
      options={[{ id: 1, title: "Cancelar", border: true }]}
      chips={[{
        key: 1, label: "Indiara Beltrane", icon: ["kci", "close"], onDeleteChip: true
      }]}
      defaultValue="Teste de texto default"
      rows="5"
      message
      disabled
    />);
    const instance = component.getInstance();
    instance.setState({ oneLine: false });
    await wait(100);
    expect(component.toJSON().children[0].children[1].props.className).toBe("col-button icon-bottom icon-disabled");
  });
  it("Is the componentDidMount method working?", async () => {
    const component = TestRenderer.create(<KChipInput
      options={[{ id: 1, title: "Cancelar", border: true }]}
      chips={[{
        key: 1, label: "Indiara Beltrane", icon: ["kci", "close"], onDeleteChip: true
      }]}
      defaultValue="Teste de texto default"
      rows="5"
      message
    />);
    const instance = component.getInstance();
    instance.container.current = { offsetHeight: 53 };
    await wait(100);
    instance.componentDidMount();
  });
  it("Is the ClickIcon method working?", async () => {
    const component = TestRenderer.create(<KChipInput
      message
      onClickIcon={() => {}}
    />);
    await wait(100);
    const instance = component.getInstance();
    instance.handleClickIcon();
    component.toJSON().children[0].children[1].props.onClick();
  });
  it("Is the ClickIcon method working with button disable?", async () => {
    const component = TestRenderer.create(<KChipInput
      message
      disabled
      onClickIcon={() => {}}
    />);
    await wait(100);
    const instance = component.getInstance();
    instance.handleClickIcon();
    component.toJSON().children[0].children[1].props.onClick();
  });
  it("UNSAFE_componentWillReceiveProps", async () => {
    const component = TestRenderer.create(<KChipInput
      options={[{ id: 1, title: "Cancelar", border: true }]}
      chips={[{
        key: 1, label: "Indiara Beltrane", icon: ["kci", "close"], onDeleteChip: true
      }]}
      defaultValue="Teste de texto default"
      rows="5"
      message
      disabled
    />);
    component.update(<KChipInput
      options={[{ id: 1, title: "Cancelar", border: true }]}
      chips={[{
        key: 1, label: "Indiara Beltrane", icon: ["kci", "close"], onDeleteChip: true
      }, {
        key: 2, label: "Indiara Beltrane", icon: ["kci", "close"], onDeleteChip: true
      }]}
      defaultValue="Teste de texto default"
      rows="5"
      message
    />);
  });
  it("Is work without onChangValue?", async () => {
    const component = TestRenderer.create(<KChipInput
      options={[{ id: 1, title: "Cancelar", border: true }]}
      chips={[{ key: 1, label: "Indiara Beltrane" }]}
      onClickDeleteChip={() => { }}
      defaultValue="Teste de texto default"
    />);
    const instance = component.getInstance();
    const event = { target: { innerText: "olá" } };
    instance.input.current = { innerText: "olá" };
    instance.handleChange(event);
    await wait(0);
    expect(instance.state.defaultValue).toBe("olá");
    expect(instance.input.current.innerText).toBe("olá");
  });
  it("Is work with onChangValue?", async () => {
    const component = TestRenderer.create(<KChipInput
      options={[{ id: 1, title: "Cancelar", border: true }]}
      chips={[{ key: 1, label: "Indiara Beltrane" }]}
      onClickDeleteChip={() => { }}
      defaultValue="Teste de texto default"
      onChangeValue={() => {}}
    />);
    const instance = component.getInstance();
    const event = { target: { innerText: "olá" } };
    instance.input.current = { innerText: "olá" };
    instance.handleChange(event);
    await wait(0);
    expect(instance.state.defaultValue).toBe("olá");
    expect(instance.input.current.innerText).toBe("olá");
  });
});
