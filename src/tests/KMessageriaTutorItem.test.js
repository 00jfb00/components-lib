import React from "react";
import TestRenderer from "react-test-renderer";
import { configure } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import wait from "waait";
import KMessageriaTutorItem from "../components/molecules/KMessageriaTutorItem";

configure({ adapter: new Adapter() });

describe("Is the KMessageriaTutorItem component working?", () => {
  it("Is it rendering?", async () => {
    const component = TestRenderer.create(
      <KMessageriaTutorItem
        title="Daniel Aber Almeida"
        icon={["fas", "envelope"]}
        subtitle={["Sociedade e cidadania", "Metodologias Ativas", "Gestão Escolar"]}
      />,
    );
    await wait(0);
    await component.getInstance().loadingPromise;
    expect(component.toJSON().props.className).toBe("k-messageria-tutor");
  });

  it("Is it active?", async () => {
    const component = TestRenderer.create(
      <KMessageriaTutorItem
        title="Daniel Aber Almeida"
        icon={["fas", "envelope"]}
        subtitle={["Sociedade e cidadania", "Metodologias Ativas", "Gestão Escolar"]}
        active
      />
    );
    await wait(0);
    await component.getInstance().loadingPromise;
    expect(component.root.children[0].children[0].props.className.indexOf("is-active") >= 0).toBe(true);
  });

  it("Is it clicking content?", async () => {
    let click = false;
    let component = TestRenderer.create(
      <KMessageriaTutorItem
        title="Daniel Aber Almeida"
        icon={["fas", "envelope"]}
        subtitle={["Democracia e Cidadania"]}
        onClick={() => { click = true; }}
      />,
    );
    let instance = component.getInstance();
    await wait(0);
    await component.getInstance().loadingPromise;
    expect(click).toBe(false);
    instance.handleClick(0, () => {
      click = true;
    });
    expect(click).toBe(true);
    component = TestRenderer.create(
      <KMessageriaTutorItem
        title="Daniel Aber Almeida"
        icon={["fas", "envelope"]}
        subtitle={["Democracia e Cidadania"]}
      />,
    );
    instance = component.getInstance();
    await wait(0);
    await component.getInstance().loadingPromise;
    instance.handleClick();
  });
});
