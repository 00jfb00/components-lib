import React from "react";
import TestRenderer from "react-test-renderer";
import { JSDOM } from "jsdom";
import wait from "waait";
import KDrawer from "../components/molecules/KDrawer";

describe("KDrawer component", () => {
  const dom = new JSDOM("<!DOCTYPE html><html><head></head><body></body></html>");

  it("Is the defaultValue classname?", async () => {
    const component = TestRenderer.create(<KDrawer
      id="1"
      checkClosed={() => {
      }}
    />);
    await wait(0);
    await component.getInstance().loadingPromise;
    const className = component.toJSON().props.className.replace(/\s+/g, "");
    expect(className.indexOf("k-drawer-to-left") >= 0).toBe(true);
  });

  it("Is show?", async () => {
    const component = TestRenderer.create(<KDrawer
      id="1"
      checkClosed={() => {
      }}
      toDrawerRight
    />);
    await wait(0);
    await component.getInstance().loadingPromise;
    const className = component.toJSON().children[0].props.className.replace(/\s+/g, "");
    expect(className.indexOf("k-drawer-container-to-right") >= 0).toBe(true);
  });

  it("Accept className?", async () => {
    global.window = dom.window;
    global.window.innerWidth = 990;
    const component = TestRenderer.create(<KDrawer
      id="1"
      checkClosed={() => {
      }}
      showDrawer
    />);
    await wait(0);
    await component.getInstance().loadingPromise;
    const className = component.toJSON().props.className.replace(/\s+/g, "");

    expect(className.indexOf("k-drawer-to-left-is-showDrawer") >= 0).toBe(true);
  });

  it("Call handleWindowSizeChange?", async () => {
    global.window = dom.window;
    global.document = dom.window.document;

    const resizeEvent = document.createEvent("Event");
    resizeEvent.initEvent("resize", true, true);

    global.window.resizeTo = width => {
      global.window.innerWidth = width || global.window.innerWidth;
      global.window.innerHeight = width || global.window.innerHeight;
      global.window.dispatchEvent(resizeEvent);
    };

    const component = TestRenderer.create(<KDrawer
      id="1"
      checkClosed={() => {
      }}
      onlyMobile
      showDrawer
    />);
    await wait(0);
    await component.getInstance().loadingPromise;

    global.window.resizeTo(1769, 1000);
    let className = component.toJSON().props.className.replace(/\s+/g, "");
    expect(className.indexOf("k-drawer-to-left") >= 0).toBe(true);

    global.window.resizeTo(769, 1000);
    className = component.toJSON().props.className.replace(/\s+/g, "");
    expect(className.indexOf("k-drawer-to-left-is-showDrawer") >= 0).toBe(true);
  });

  it("Call handleWindowSizeChange 2?", async () => {
    const component = TestRenderer.create(<KDrawer
      id="1"
      checkClosed={() => {
      }}
      showDrawer
    />);
    await wait(0);
    await component.getInstance().loadingPromise;

    const instance = component.getInstance();

    instance.handleClick({
      target: {
        id: "k-drawer-1"
      }
    });

    instance.handleClick({
      target: {
        id: "k-drawer-2"
      }
    });

    const className = component.toJSON().props.className.replace(/\s+/g, "");
    expect(className.indexOf("k-drawer-to-left-is-showDrawer") >= 0).toBe(true);

    component.unmount();
  });
});
