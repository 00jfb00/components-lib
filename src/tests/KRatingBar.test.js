import React from "react";
import TestRenderer from "react-test-renderer";
import KRatingBar from "../components/molecules/KRatingBar/KRatingBar";

describe("P Rating Bar", () => {
  it("Is the style property 'maxPoints' working?", async () => {
    const component = TestRenderer.create(<KRatingBar maxPoints={14000} />);
    const instance = component.getInstance();
    expect(instance.props.maxPoints).toBe(14000);
  });

  it("Is the style property 'miniPoints' working?", async () => {
    const component = TestRenderer.create(<KRatingBar animated miniPoints={8000} />);
    const instance = component.getInstance();
    expect(instance.props.miniPoints).toBe(8000);
  });

  it("Is the style property 'studentPoints' working?", async () => {
    const component = TestRenderer.create(<KRatingBar striped studentPoints={7000} />);
    const instance = component.getInstance();
    expect(instance.props.studentPoints).toBe(7000);
  });

  it("Is the style property 'ProgressBar' working?", async () => {
    const component = TestRenderer.create(<KRatingBar striped maxPoints={14000} miniPoints={7000} studentPoints={8000} />);
    expect(component.toJSON().children[1].children[1].children[0].props.style.width).toBe("57%");
    expect(component.toJSON().children[1].children[0].props.style.left).toBe("50%");
  });

  it("Is the style property 'ProgressBarSucess' working?", async () => {
    const component = TestRenderer.create(<KRatingBar striped maxPoints={10000} miniPoints={5000} studentPoints={9000} />);
    expect(component.toJSON().children[1].children[1].children[0].props.style.width).toBe("90%");
    expect(component.toJSON().children[1].children[0].props.style.left).toBe("50%");
  });
});
