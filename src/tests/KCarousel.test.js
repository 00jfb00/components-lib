import React from "react";
import TestRenderer from "react-test-renderer";
import KCarousel from "../components/molecules/KCarousel/KCarousel";

describe("Is the KCarousel component working?", () => {
  it("Is it rendering?", () => {
    const component = TestRenderer.create(
      <KCarousel />
    );
    expect(component).toBeDefined();
  });

  it("Is it rendering with children?", () => {
    const component = TestRenderer.create(
      <KCarousel>
        <img src="https://i.imgur.com/Pnx3SeU.png" alt="teste" />
        <img src="https://i.imgur.com/Pnx3SeU.png" alt="teste" />
        <img src="https://i.imgur.com/Pnx3SeU.png" alt="teste" />
        <img src="https://i.imgur.com/Pnx3SeU.png" alt="teste" />
      </KCarousel>
    );
    const instance = component.getInstance();
    expect(instance.props.children.length).toBe(4);
  });

  it("Is it rendering with arrow color prop?", () => {
    const component = TestRenderer.create(
      <KCarousel arrowColor="black">
        <img src="https://i.imgur.com/Pnx3SeU.png" alt="teste" />
      </KCarousel>
    );
    const instance = component.getInstance();
    expect(instance.props.arrowColor).toBe("black");
  });

  it("Is it rendering with dots position prop as left?", () => {
    const component = TestRenderer.create(
      <KCarousel dotsPosition="left">
        <img src="https://i.imgur.com/Pnx3SeU.png" alt="teste" />
      </KCarousel>
    );
    const instance = component.getInstance();
    expect(instance.props.dotsPosition).toBe("left");
  });

  it("Is it rendering with show arrow prop as false?", () => {
    const component = TestRenderer.create(
      <KCarousel showArrows={false}>
        <img src="https://i.imgur.com/Pnx3SeU.png" alt="teste" />
      </KCarousel>
    );
    const instance = component.getInstance();
    expect(instance.props.showArrows).toBe(false);
  });

  it("Is it rendering with showDots prop as false?", () => {
    const component = TestRenderer.create(
      <KCarousel showDots={false}>
        <img src="https://i.imgur.com/Pnx3SeU.png" alt="teste" />
      </KCarousel>
    );
    const instance = component.getInstance();
    expect(instance.props.showDots).toBe(false);
  });

  it("Is it working prevItem function?", () => {
    const component = TestRenderer.create(
      <KCarousel>
        <img src="https://i.imgur.com/Pnx3SeU.png" alt="teste" />
        <img src="https://i.imgur.com/Pnx3SeU.png" alt="teste" />
        <img src="https://i.imgur.com/Pnx3SeU.png" alt="teste" />
        <img src="https://i.imgur.com/Pnx3SeU.png" alt="teste" />
      </KCarousel>
    );
    const instance = component.getInstance();
    instance.state.activeItem = 2;
    instance.prevItem();
  });

  it("Is it working prevItem function with first item?", () => {
    const component = TestRenderer.create(
      <KCarousel>
        <img src="https://i.imgur.com/Pnx3SeU.png" alt="teste" />
        <img src="https://i.imgur.com/Pnx3SeU.png" alt="teste" />
        <img src="https://i.imgur.com/Pnx3SeU.png" alt="teste" />
        <img src="https://i.imgur.com/Pnx3SeU.png" alt="teste" />
      </KCarousel>
    );
    const instance = component.getInstance();
    instance.state.activeItem = 0;
    instance.prevItem();
  });

  it("Is it working nextItem function with last item?", () => {
    const component = TestRenderer.create(
      <KCarousel>
        <img src="https://i.imgur.com/Pnx3SeU.png" alt="teste" />
        <img src="https://i.imgur.com/Pnx3SeU.png" alt="teste" />
        <img src="https://i.imgur.com/Pnx3SeU.png" alt="teste" />
        <img src="https://i.imgur.com/Pnx3SeU.png" alt="teste" />
      </KCarousel>
    );
    const instance = component.getInstance();
    instance.state.activeItem = 4;
    instance.nextItem();
  });

  it("Is it working nextItem function?", () => {
    const component = TestRenderer.create(
      <KCarousel>
        <img src="https://i.imgur.com/Pnx3SeU.png" alt="teste" />
        <img src="https://i.imgur.com/Pnx3SeU.png" alt="teste" />
        <img src="https://i.imgur.com/Pnx3SeU.png" alt="teste" />
        <img src="https://i.imgur.com/Pnx3SeU.png" alt="teste" />
      </KCarousel>
    );
    const instance = component.getInstance();
    instance.nextItem();
  });

  it("It is working click nextItem button (button on left)", () => {
    const component = TestRenderer.create(
      <KCarousel>
        <img src="https://i.imgur.com/Pnx3SeU.png" alt="teste" />
        <img src="https://i.imgur.com/Pnx3SeU.png" alt="teste" />
        <img src="https://i.imgur.com/Pnx3SeU.png" alt="teste" />
        <img src="https://i.imgur.com/Pnx3SeU.png" alt="teste" />
      </KCarousel>
    );
    component.toJSON().children[1].props.onClick();
  });

  it("It is working click prevItem button (button on right)", () => {
    const component = TestRenderer.create(
      <KCarousel>
        <img src="https://i.imgur.com/Pnx3SeU.png" alt="teste" />
        <img src="https://i.imgur.com/Pnx3SeU.png" alt="teste" />
        <img src="https://i.imgur.com/Pnx3SeU.png" alt="teste" />
        <img src="https://i.imgur.com/Pnx3SeU.png" alt="teste" />
      </KCarousel>
    );
    component.toJSON().children[2].props.onClick();
  });
});
