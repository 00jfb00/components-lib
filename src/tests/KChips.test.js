import React from "react";
import TestRenderer from "react-test-renderer";
import { mount } from "enzyme";
import KChip from "../components/atoms/KChip";

describe("Is KIcon component working?", () => {
  it("Is the onClick method working?", async () => {
    let click = false;
    const wrapper = mount(
      <KChip onClick={value => { click = value; }} id="1" fontSize="p3" state="primary" icon={["kci", "close"]} colorIcon="primary" sizeIcon="xs" hasRadius outlined chipText="Default Chip" />,
    );
    wrapper.find("#label-chip-1").getElement().ref.current.innerHTML = "oi";
    const instance = wrapper.instance();
    instance.handleClick();
    expect(click).toBe("oi");
  });
  it("Is the onClick with null?", async () => {
    const component = TestRenderer.create(
      <KChip onClick={null} />,
    );
    const instance = component.getInstance();
    instance.handleClick();
    expect(component).toBeDefined();
  });
  it("Is the state primary working?", async () => {
    const component = TestRenderer.create(
      <KChip fontSize="p3" chipText="Default Chip" state="primary" />,
    );
    expect(component.toJSON().props.className).toBe("k-chips chip p3 is-primary-default  ");
  });
  it("Is the hasRadius, outlined and colorBorder working?", async () => {
    const component = TestRenderer.create(
      <KChip fontSize="p3" hasRadius outlined colorBorder="#dddd" chipText="Default Chip" />,
    );
    expect(component.toJSON().children[0].props.className).toBe("chip-value hasRadius outlined colorBorder");
  });
  it("Is the iconLeft working?", async () => {
    const component = TestRenderer.create(
      <KChip fontSize="p3" state="primary" icon={["kci", "close"]} colorIcon="primary" sizeIcon="xs" hasRadius outlined leftIcon chipText="Default Chip" />,
    );
    expect(component.toJSON().children[0].children[0].props.className).toBe("icon-left");
  });
  it("Is the iconRigth working?", async () => {
    const component = TestRenderer.create(
      <KChip fontSize="p3" state="primary" icon={["kci", "close"]} colorIcon="primary" sizeIcon="xs" hasRadius outlined chipText="Default Chip" />,
    );
    expect(component.toJSON().children[0].children[1].props.className).toBe("icon");
  });
  it("Is the handleClickChip working?", async () => {
    const component = TestRenderer.create(
      <KChip ClickChip={() => {}} fontSize="p3" state="primary" icon={["kci", "close"]} colorIcon="primary" sizeIcon="xs" hasRadius outlined chipText="Default Chip" />,
    );
    const instance = component.getInstance();
    instance.handleClickChip();
  });
  it("Is the onClick working?", async () => {
    const component = TestRenderer.create(
      <KChip id={1} subText="2 mb" fontSize="p3" state="primary" icon={["kci", "close"]} colorIcon="primary" sizeIcon="xs" hasRadius outlined chipText="Default Chip" />,
    );
    const chip = component.root.findByProps({ id: "group-label-chip-1" });
    chip._fiber.pendingProps.onClick();
  });
});
