import React from "react";
import TestRenderer from "react-test-renderer";
import wait from "waait";
import KChat from "../components/molecules/KChat";

describe("Is the KChat component working?", () => {
  it("Is it rendering?", async () => {
    const component = TestRenderer.create(
      <KChat
        showCounter={false}
        style={{ height: "50px", width: "50px" }}
      />
    );
    await wait(0);
    await component.getInstance().loadingPromise;
    expect(component.toJSON().props.className).toEqual("k-chat");
  });

  it("Is it rendering with counter?", async () => {
    const component = TestRenderer.create(<KChat showCounter />);
    await wait(0);
    await component.getInstance().loadingPromise;
    expect(component.toJSON().children[0].props.className).toEqual("k-chat__counter");
  });
});
