import React from "react";
import TestRenderer from "react-test-renderer";
import KMessageriaBadge from "../components/atoms/KMessageriaBadge/KMessageriaBadge";

describe("Is the KBadge component working?", () => {
  it("Is it rendering?", async () => {
    const component = TestRenderer.create(
      <KMessageriaBadge>
        <div style={{ height: "50px", width: "50px", backgroundColor: "blue" }} />
      </KMessageriaBadge>,
    );
    expect(component.toJSON().props.className).toBe("k-messageria-badge");
  });

  it("Is it hiding?", async () => {
    const component = TestRenderer.create(
      <KMessageriaBadge hide>
        <div style={{ height: "50px", width: "50px", backgroundColor: "blue" }} />
      </KMessageriaBadge>,
    );
    expect(component.toJSON().children.filter(element => element.props.name === "badge").length).toBe(0);
  });

  it("Is it changing position?", async () => {
    const component = TestRenderer.create(
      <KMessageriaBadge border={false} rounded={false} position="center">
        <div style={{ height: "50px", width: "50px", backgroundColor: "blue" }} />
      </KMessageriaBadge>,
    );
    expect(component.toJSON().children[1].props.className.indexOf("center") >= 0).toBe(true);
  });
});
