import React from "react";
import TestRenderer from "react-test-renderer";
import wait from "waait";
import KAvatar from "../components/atoms/KAvatar/KAvatar";
import KImage from "../components/atoms/KImage";

describe("Is the KAvatar component working?", () => {
  const imageUrl = "https://content-static.upwork.com/uploads/2014/10/01073427/profilephoto1.jpg";

  it("Is it rendering with image default?", async () => {
    const component = TestRenderer.create(<KAvatar src={imageUrl} />);
    await wait(0);
    await component.getInstance().loadingPromise;
    expect(component.toJSON().props.className).toEqual("k-avatar");
  });

  it("Is it rendering with KImage?", async () => {
    const component = TestRenderer.create(<KAvatar src={imageUrl} />);
    await wait(0);
    await component.getInstance().loadingPromise;
    const testInstance = component.root;
    expect(testInstance.findByType(KImage)).toBeDefined();
  });

  it("Is it rendering with indicator?", async () => {
    const component = TestRenderer.create(<KAvatar src={imageUrl} showIndicator />);
    await wait(0);
    await component.getInstance().loadingPromise;
    expect(component.toJSON().children[0].props.className).toEqual("k-avatar__indicator sm");
  });

  it("Is it rendering with indicator and indicator background green?", async () => {
    const component = TestRenderer.create(<KAvatar src={imageUrl} showIndicator indicatorBackground="green" />);
    await wait(0);
    await component.getInstance().loadingPromise;
    expect(component.toJSON().children[0].props.style.backgroundColor).toEqual("green");
  });

  it("Is it rendering with indicator and indicator size md?", async () => {
    const component = TestRenderer.create(<KAvatar src={imageUrl} showIndicator indicatorSize="md" />);
    await wait(0);
    await component.getInstance().loadingPromise;
    expect(component.toJSON().children[0].props.className).toEqual("k-avatar__indicator md");
  });

  it("Is it rendering with indicator and indicator size lg?", async () => {
    const component = TestRenderer.create(<KAvatar src={imageUrl} showIndicator indicatorSize="lg" />);
    await wait(0);
    await component.getInstance().loadingPromise;
    expect(component.toJSON().children[0].props.className).toEqual("k-avatar__indicator lg");
  });

  it("Is it rendering with width and height de 100px?", async () => {
    const component = TestRenderer.create(<KAvatar src={imageUrl} width="100px" height="100px" />);
    await wait(0);
    await component.getInstance().loadingPromise;
    expect(component.toJSON().props.style.width).toEqual("100px");
    expect(component.toJSON().props.style.height).toEqual("100px");
  });
});
