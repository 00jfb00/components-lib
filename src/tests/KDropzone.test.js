import React from "react";
import TestRenderer from "react-test-renderer";
import { configure, mount } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import { Alert } from "react-bootstrap";
import KDropzone from "../components/molecules/KDropzone";

configure({ adapter: new Adapter() });

describe("Is it KDropzone component working?", () => {
  it("Is it rendering?", () => {
    const component = TestRenderer.create(
      <KDropzone />
    );
    expect(component).toBeDefined();
  });

  it("Is it receiving accept prop?", () => {
    const component = TestRenderer.create(
      <KDropzone accept=".pdf,.doc,.docx" />
    );
    const instance = component.getInstance();
    expect(instance.props.accept).toBe(".pdf,.doc,.docx");
  });

  it("Is it receiving multiple prop as true?", () => {
    const component = TestRenderer.create(
      <KDropzone multiple />
    );
    const instance = component.getInstance();
    expect(instance.props.multiple).toBe(true);
  });

  it("Is it receiving multiple prop as false?", () => {
    const component = TestRenderer.create(
      <KDropzone multiple={false} />
    );
    const instance = component.getInstance();
    expect(instance.props.multiple).toBe(false);
  });

  it("Is it receiving onSelectacceptedFiles prop as function and working?", () => {
    const testFunc = jest.fn();
    const component = TestRenderer.create(
      <KDropzone onSelectacceptedFiles={testFunc} />
    );
    const instance = component.getInstance();
    instance.props.onSelectacceptedFiles();
    expect(testFunc).toHaveBeenCalled();
  });

  it("Is it working onSelectacceptedFiles with default prop?", () => {
    const component = TestRenderer.create(
      <KDropzone />
    );
    const instance = component.getInstance();
    instance.props.onSelectacceptedFiles();
  });

  it("Is it onDragEnter function working?", () => {
    const testFunc = jest.fn();
    const component = TestRenderer.create(
      <KDropzone onSelectacceptedFiles={testFunc} />
    );
    const instance = component.getInstance();
    instance.onDragEnter();
  });

  it("Is it onDragLeave function working?", () => {
    const testFunc = jest.fn();
    const component = TestRenderer.create(
      <KDropzone onSelectacceptedFiles={testFunc} />
    );
    const instance = component.getInstance();
    instance.onDragLeave();
  });

  it("Is it onDrop function working?", () => {
    const testFunc = jest.fn();
    const component = TestRenderer.create(
      <KDropzone onSelectacceptedFiles={testFunc} />
    );
    const instance = component.getInstance();
    instance.onDrop();
  });

  it("Is it resetFiles function working?", () => {
    const testFunc = jest.fn();
    const component = TestRenderer.create(
      <KDropzone onSelectacceptedFiles={testFunc} />
    );
    const instance = component.getInstance();
    instance.resetFiles();
  });

  it("Is it onDrop function working with reject files?", () => {
    const testFunc = jest.fn();
    const component = TestRenderer.create(
      <KDropzone onSelectacceptedFiles={testFunc} />
    );
    const instance = component.getInstance();
    instance.onDrop([], ["Teste"]);
  });

  it("Is it onClose function of Alert Component working?", () => {
    const testFunc = jest.fn();
    const component = mount(<KDropzone onSelectacceptedFiles={testFunc} />);
    component.setState({ invalidFiles: true });
    component.find(Alert).first().props().onClose();
  });

  it("Is it working with mocked files?", () => {
    const testFunc = jest.fn();
    const component = TestRenderer.create(
      <KDropzone typeTeacher onSelectacceptedFiles={testFunc} />
    );
    const instance = component.getInstance();
    instance.onDrop([{ path: "teste.docx" }], []);
  });

  it("Is it substitute file button working?", () => {
    const testFunc = jest.fn();
    const component = mount(<KDropzone subTitleBold onSelectacceptedFiles={testFunc} />);
    component.setState({ files: ["Teste"] });
    component.find(".k-dropzone__status-text").at(0).simulate("click");
    component.instance().UNSAFE_componentWillReceiveProps({ deleteFiles: true });
  });

  it("UNSAFE_componentWillReceiveProps work?", async () => {
    const testFunc = jest.fn();
    const wrapper = mount(<KDropzone maxSize={0} messageAccept="teste" errorBottom onSelectacceptedFiles={testFunc} subTitleBold deleteFiles />);
    wrapper.setState({ invalidFiles: true });
    wrapper.setState({ files: ["Teste"] });
  });

  it("UNSAFE_componentWillReceiveProps work not error bottom?", async () => {
    const testFunc = jest.fn();
    const wrapper = mount(<KDropzone maxSize={0} typeTeacher messageAccept="teste" onSelectacceptedFiles={testFunc} subTitleBold deleteFiles />);
    wrapper.setState({ invalidFiles: true });
    wrapper.setState({ files: ["Teste"] });
    wrapper.instance().UNSAFE_componentWillReceiveProps({ deleteFiles: false });
  });

  it("UNSAFE_componentWillReceiveProps work typeTeacher?", async () => {
    const testFunc = jest.fn();
    const wrapper = mount(<KDropzone maxSize={0} typeTeacher errorBottom messageAccept="teste" onSelectacceptedFiles={testFunc} subTitleBold deleteFiles />);
    wrapper.setState({ invalidFiles: true });
    wrapper.setState({ files: ["Teste"] });
    wrapper.instance().UNSAFE_componentWillReceiveProps({ deleteFiles: false });
  });
});
