import React from "react";
import TestRenderer from "react-test-renderer";
import wait from "waait";
import KBalloon from "../components/molecules/KBalloon";

describe("Is the KBalloon component working?", () => {
  it("Is it rendering?", async () => {
    const component = TestRenderer.create(
      <KBalloon width="30px" height="30px" />
    );
    await wait(0);
    await component.getInstance().loadingPromise;
    expect(component.toJSON().props.className).toEqual("k-balloon");
  });

  it("Is it rendering with children?", async () => {
    const component = TestRenderer.create(
      <KBalloon>
        <strong>Nome: </strong>
        {" "}
Bart Simpson
      </KBalloon>
    );
    await wait(0);
    await component.getInstance().loadingPromise;
    expect(component.toJSON().props.className).toEqual("k-balloon");
  });
});
