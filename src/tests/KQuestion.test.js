import React from "react";
import TestRenderer from "react-test-renderer";
import wait from "waait";
import { mount } from "enzyme";
import NavLink from "react-bootstrap/NavLink";
import { Button } from "react-bootstrap";
import KQuestion from "../components/molecules/KQuestion";

describe("KQuestion component", () => {
  it("Is the defaultValue classname?", async () => {
    const component = TestRenderer.create(
      <KQuestion
        title="olá"
        options={[
          { content: "Este ava aqui", state: "warning" },
          { content: "Este ava aqui", state: "warning" }]
        }
        onChangeValue={() => {}}
      />,
    );

    await wait(0);
    await component.getInstance().loadingPromise;
    expect(component.toJSON().props.className.includes("k-question")).toBe(true);
  });
  it("pass id?", async () => {
    const component = TestRenderer.create(
      <KQuestion
        id="1"
        title="olá"
        options={[
          { content: "Este ava aqui", state: "warning" },
          { content: "Este ava aqui", state: "warning" }
        ]}
        onChangeValue={() => {}}
      />,
    );

    await wait(0);
    await component.getInstance().loadingPromise;
    expect(component.toJSON().props.className.includes("k-question")).toBe(true);
  });
  it("pass attachments?", async () => {
    const component = mount(
      <KQuestion
        id="1"
        title="olá"
        attachments={["a"]}
        options={[
          { content: "Este ava aqui", state: "warning" },
          { content: "Este ava aqui", state: "warning" }
        ]}
        onChangeValue={() => {}}
      />,
    );
    await wait(0);
    component.find("#button-1-attachments").at(0).simulate("click");
  });
  it("pass formulas?", async () => {
    const component = mount(
      <section>
        <NavLink id="a-teste">AUX</NavLink>
        <Button id="button-teste">AUX</Button>
        <KQuestion
          id="1"
          title="olá"
          formulas={["a", "b"]}
          options={[
            { content: "Este ava aqui", state: "warning" },
            { content: "Este ava aqui", state: "warning" }
          ]}
          onChangeValue={() => {}}
        />
      </section>,
    );

    await wait(0);
    component.find("#button-1-formulas").at(0).simulate("click");
    await wait(100);
    component.find("#k-question-zoom-image-modal-image-paginator-next").at(0).simulate("click");
    await wait(100);
    component.find("#k-question-zoom-image-modal-image-paginator-prev").at(0).simulate("click");
    await wait(100);
    component.find(".modal-backdrop").at(0).simulate("click");
    const instance = component.children().at(2).instance();
    instance.ignoreBackgroundActions();
    instance.setState({ isGalleryOpen: true }, () => {
      instance.accessibilityAuxiliars.auxA = ",a";
      instance.ignoreBackgroundActions();
      instance.setState({ isGalleryOpen: false });
    });
  });
});
