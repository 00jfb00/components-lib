import React from "react";
import TestRenderer from "react-test-renderer";
import wait from "waait";
import KBody from "../components/organisms/KBody";

describe("KBody component", () => {
  it("Is the defaultValue classname?", async () => {
    const componentWithHeight = TestRenderer.create(<KBody height={26} />);
    await wait(0);
    await componentWithHeight.getInstance().loadingPromise;
    expect(componentWithHeight.toJSON().props.className.indexOf("k-body") >= 0).toBe(true);

    const component = TestRenderer.create(<KBody />);
    await wait(0);
    await component.getInstance().loadingPromise;
    expect(component.toJSON().props.className.indexOf("k-body") >= 0).toBe(true);
  });
});
