import React from "react";
import TestRenderer from "react-test-renderer";
import wait from "waait";
import KTreeSubItem from "../components/molecules/KTreeSubItem";

describe("Is the KTreeSubItem component working?", () => {
  it("Is it rendering progress-bar?", async () => {
    const component = TestRenderer.create(<KTreeSubItem title="Unidade 2" percentage={10} />);
    await wait(0);
    await component.getInstance().loadingPromise;
    expect(component.root.findByProps({ className: "k-tree-subitem__percent-bar" })).toBeDefined();
  });
  it("Is it active and onClick default?", async () => {
    const component = TestRenderer.create(<KTreeSubItem title="Unidade 2" active />);
    await wait(0);
    await component.getInstance().loadingPromise;
    const instance = component.getInstance();
    expect(instance.handleClick()).toBe(undefined);
    expect(instance.handleClickIcon()).toBe(undefined);
    expect(component.root.children[0].props.className.indexOf("is-active") >= 0).toBe(true);
  });
  it("Is it active and onClick indicator?", async () => {
    const component = TestRenderer.create(<KTreeSubItem title="Unidade 2" indicator="1.1" active />);
    await wait(0);
    await component.getInstance().loadingPromise;
    const instance = component.getInstance();
    expect(instance.handleClick()).toBe(undefined);
    expect(instance.handleClickIcon()).toBe(undefined);
    expect(component.root.children[0].props.className.indexOf("is-active") >= 0).toBe(true);
  });
  it("Show indicator?", async () => {
    const component = TestRenderer.create(<KTreeSubItem title="Unidade 2" indicator="1.0" />);
    await wait(0);
    await component.getInstance().loadingPromise;
    expect(typeof component.root.findByProps({ className: "k-tree-subitem__indicator-wrapper" }).children[0].props.children === "string" && component.root.findByProps({ className: "k-tree-subitem__indicator-wrapper" }).children[0].props.children === "1.0").toBe(true);
  });

  it("Events click?", async () => {
    let click = null;
    let clickIcon = null;
    let clickLeftIcon = null;
    let event = null;
    const component = TestRenderer.create(<KTreeSubItem
      title="Unidade 2"
      isActive
      indicator="1.0"
      onClick={val => {
        event = val;
        click = true;
      }}
      onClickIcon={val => {
        event = val;
        clickIcon = true;
      }}
    />);
    await wait(0);
    await component.getInstance().loadingPromise;
    const instance = component.getInstance();

    instance.handleClick(true);
    await wait(0);
    expect(click).toBe(true);
    expect(event).toBe(true);

    instance.handleClickIcon(false);
    await wait(0);
    expect(event).toBe(false);
    expect(clickIcon).toBe(true);

    const componenActive = TestRenderer.create(<KTreeSubItem
      title="Unidade 2"
      active
      lefticon="download"
      icon="download"
      indicator="1.0"
      onClick={() => {
        clickLeftIcon = true;
      }}
    />);
    await wait(0);
    await componenActive.getInstance().loadingPromise;

    const lefticonActive = componenActive.root.findByProps({ className: "k-tree-subitem__lefticon-wrapper" });
    await wait(0);

    expect(lefticonActive._fiber.pendingProps.onClick).toBeInstanceOf(Function);
    lefticonActive._fiber.pendingProps.onClick();
    expect(clickLeftIcon).toBe(true);

    const componentInactive = TestRenderer.create(<KTreeSubItem
      title="Unidade 2"
      active={false}
      lefticon="download"
      indicator="1.0"
      onClick={() => {
        click = false;
      }}
    />);
    await wait(0);
    await componentInactive.getInstance().loadingPromise;

    const lefticon = componentInactive.root.findByProps({ className: "k-tree-subitem__lefticon-wrapper" });
    await wait(0);
    expect(lefticon._fiber.pendingProps.onClick).toBe(null);
  });
});
