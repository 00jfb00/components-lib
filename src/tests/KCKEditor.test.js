import React from "react";
import TestRenderer from "react-test-renderer";
import htmlToDraft from "html-to-draftjs";
import { ContentState, EditorState } from "draft-js";
import KCKEditor from "../components/molecules/KCKEditor/KCKEditor";

describe("KCKEditor test", () => {
  it("Is the component working?", async () => {
    const component = TestRenderer.create(
      <KCKEditor
        initialContent={"<p>Teste</p>"}
        isMobile={false}
      />
    );
    const instance = component.getInstance();
    expect(component.toJSON().props.className).toBe("k-ckeditor");

    const html = "<p>a</p>";
    const contentBlock = htmlToDraft(html);
    if (contentBlock) {
      const contentState = ContentState.createFromBlockArray(contentBlock.contentBlocks);
      const editorState = EditorState.createWithContent(contentState);
      instance.onEditorStateChange(editorState);
    }

    expect(component.toJSON().props.className).toBe("k-ckeditor");
    component.update(
      <KCKEditor
        initialContent={"<p>Teste</p>"}
        isMobile
      />
    );
    component.update(
      <KCKEditor
        initialContent=""
        isMobile
      />
    );
  });
});
