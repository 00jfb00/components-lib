import React from "react";
import TestRenderer from "react-test-renderer";
import wait from "waait";
import KTabs from "../components/molecules/KTabs";
import KTabsItem from "../components/molecules/KTabsItem";

describe("KTabs component", () => {
  it("Is the defaultValue classname?", async () => {
    const componentShow = TestRenderer.create(
      <KTabs index={3}>
        <KTabsItem>
          <div style={{ height: 100 }} />
        </KTabsItem>
        <KTabsItem>
          <div style={{ height: 100 }} />
        </KTabsItem>
        <KTabsItem>
          <div style={{ height: 100 }} />
        </KTabsItem>
      </KTabs>,
    );
    await wait(0);
    await componentShow.getInstance().loadingPromise;
    const class1 = componentShow.toJSON().children[2].props.className.replace(/\s+/g, "");
    expect(componentShow.toJSON().props.className).toBe("k-tabs");
    expect(class1).toBe("k-tab-item-item-showtab-child-item-2-next");

    componentShow.update(
      <KTabs index={2}>
        <KTabsItem>
          <div style={{ height: 100 }} />
        </KTabsItem>
        <KTabsItem>
          <div style={{ height: 100 }} />
        </KTabsItem>
        <KTabsItem>
          <div style={{ height: 100 }} />
        </KTabsItem>
      </KTabs>,
    );
    await wait(0);
    await componentShow.getInstance().loadingPromise;

    const class2 = componentShow.toJSON().children[1].props.className.replace(/\s+/g, "");
    const class3 = componentShow.toJSON().children[2].props.className.replace(/\s+/g, "");
    expect(class2).toBe("k-tab-item-item-showtab-child-item-1-previous");
    expect(class3).toBe("k-tab-item-item-hidetab-child-item-2-previous");

    componentShow.update(
      <KTabs index={1}>
        <KTabsItem>
          <div style={{ height: 100 }} />
        </KTabsItem>
      </KTabs>,
    );
    await wait(0);
    await componentShow.getInstance().loadingPromise;
    expect(componentShow.toJSON().props.className).toBe("k-tabs");

    componentShow.update(
      <KTabs index={1}>
        <KTabsItem>
          <div style={{ height: 120 }} />
        </KTabsItem>
      </KTabs>,
    );

    await wait(0);
    await componentShow.getInstance().loadingPromise;
    expect(componentShow.toJSON().props.className).toBe("k-tabs");

    componentShow.update(
      <KTabs id="1" index={1}>
        <KTabsItem id="2">
          <div style={{ height: 120 }} />
        </KTabsItem>
      </KTabs>,
    );

    await wait(0);
    await componentShow.getInstance().loadingPromise;
    expect(componentShow.toJSON().props.className).toBe("k-tabs");
  });
});
