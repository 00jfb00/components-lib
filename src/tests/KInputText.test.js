import React from "react";
import TestRenderer from "react-test-renderer";
import wait from "waait";
import KInputText from "../components/atoms/KInputText/KInputText";

describe("Is KInputText component working?", () => {
  it("Is the defaultValue property working?", async () => {
    const component = TestRenderer.create(<KInputText rounded={false} defaultValue="Teste" />);
    await wait(0);
    await component.getInstance().loadingPromise;
    const instance = component.getInstance();
    instance.handleFocus();
    instance.handleBlur();
    expect(component.toJSON().children[1].children[0].children[0].props.defaultValue).toBe("Teste");
  });

  it("Is the type property working?", async () => {
    const component = TestRenderer.create(<KInputText type="underline" defaultValue="Teste" />);
    await wait(0);
    await component.getInstance().loadingPromise;
    const instance = component.getInstance();
    instance.handleChange({ target: { value: "A" } });
    instance.handleFocus();
    instance.handleBlur();
    instance.handleChange({ target: { value: "" } });
    instance.handleFocus();
    instance.handleBlur();
    expect(component.toJSON().children[1].children[0].children[0].props.defaultValue).toBe("Teste");
  });

  it("Is the required property working?", async () => {
    const component = TestRenderer.create(<KInputText required defaultValue="Teste" />);
    await wait(0);
    await component.getInstance().loadingPromise;
    const instance = component.getInstance();
    instance.handleChange({ target: { value: "A" } });
    instance.handleFocus();
    instance.handleBlur();
    instance.handleChange({ target: { value: "" } });
    instance.handleFocus();
    instance.handleBlur();
    expect(component.toJSON()[0].children[1].children[0].children[0].props.defaultValue).toBe("Teste");
  });

  it("Is the required property working with type?", async () => {
    const component = TestRenderer.create(<KInputText type="underline" required defaultValue="Teste" />);
    await wait(0);
    await component.getInstance().loadingPromise;
    const instance = component.getInstance();
    instance.handleChange({ target: { value: "A" } });
    instance.handleFocus();
    instance.handleBlur();
    instance.handleChange({ target: { value: "" } });
    instance.handleFocus();
    instance.handleBlur();
    expect(component.toJSON()[0].children[1].children[0].children[0].props.defaultValue).toBe("Teste");
  });

  it("Is the left and right icons rendering?", async () => {
    const component = TestRenderer.create(<KInputText leftIcon={["far", "square"]} rightIcon={["fab", "github-alt"]} />);
    await wait(0);
    await component.getInstance().loadingPromise;
    expect(component.root.findAll(element => element.type === "svg").length).toBe(2);
  });

  it("Is handleLeftIconClick? and handleChange", async () => {
    const events = {
      left: false,
      right: false,
      change: false
    };

    const component = TestRenderer.create(
      <KInputText
        leftIcon={["far", "square"]}
        rightIcon={["fab", "github-alt"]}
        onLeftIconClick={() => {
          events.left = true;
        }}
        onRightIconClick={() => {
          events.right = true;
        }}
        onChangeValue={() => {
          events.change = true;
        }}
      />,
    );
    await wait(0);
    await component.getInstance().loadingPromise;

    const instance = component.getInstance();

    instance.handleLeftIconClick({
      target: null
    });

    instance.handleRightIconClick({
      target: null
    });

    component.toJSON().children[1].children[0].children[1].props.onChange(({ target: { value: "onChange" } }));

    expect(events.left).toBe(true);
    expect(events.right).toBe(true);
    expect(events.change).toBe(true);
  });
});
