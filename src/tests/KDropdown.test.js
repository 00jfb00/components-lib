import React from "react";
import TestRenderer from "react-test-renderer";
import { configure } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import KDropdown from "../components/molecules/KDropdown/KDropdown";

configure({ adapter: new Adapter() });

describe("Is the KDropdown component working?", () => {
  const imageUrl = "https://s3-us-west-2.amazonaws.com/estudante-production/app/public/ckeditor_assets/pictures/128/original/anhanguera_full.svg";
  const triggerComponent = <img alt="" src={imageUrl} />;

  it("Is it rendering without props?", async () => {
    const component = TestRenderer.create(<KDropdown triggerComponent={triggerComponent} />);
    expect(component).toBeDefined();
    expect(component.toJSON().children[0].children[0].type).toEqual("img");
    expect(component.toJSON().children[1].children[0].children).toEqual(null);
  });

  it("Is it rendering with trigger component image?", async () => {
    const component = TestRenderer.create(<KDropdown triggerComponent={triggerComponent} />);
    expect(component).toBeDefined();
    expect(component.toJSON().children[0].children[0].type).toEqual("img");
  });

  it("Is it rendering with trigger component image and child content div?", async () => {
    const component = TestRenderer.create(
      <KDropdown triggerComponent={triggerComponent}>
        <div>
          <h1>Conteúdo do Dropdown</h1>
          <ul>
            <li>Item 1</li>
            <li>Item 2</li>
            <li>Item 3</li>
            <li>Item 4</li>
          </ul>
        </div>
      </KDropdown>,
    );
    expect(component).toBeDefined();
    expect(component.toJSON().children[0].children[0].type).toEqual("img");
    expect(component.toJSON().children[1].children[0].children[0].type).toEqual("div");
  });

  it("Is it rendering content with custom style?", async () => {
    const component = TestRenderer.create(
      <KDropdown numberOfItems={2} triggerComponent={triggerComponent} styleContent={{ minWidth: "300px", minHeight: "150px" }} />,
    );
    expect(component).toBeDefined();
    expect(component.toJSON().children[1].children[0].props.className).toEqual("k-dropdown__content");
    expect(component.toJSON().children[1].children[0].props.style.minWidth).toEqual("300px");
    expect(component.toJSON().children[1].children[0].props.style.minHeight).toEqual("150px");
  });

  it("Is it rendering content with position top, left ou right?", async () => {
    const component = TestRenderer.create(
      <KDropdown
        triggerComponent={triggerComponent}
        contentPositionTop="50px"
        contentPositionLeft="100px"
        contentPositionRight="150px"
      />,
    );
    expect(component).toBeDefined();
    expect(component.toJSON().children[1].props.className).toEqual("k-dropdown__content-wrapper");
    expect(component.toJSON().children[1].props.style.top).toEqual("50px");
    expect(component.toJSON().children[1].props.style.left).toEqual("100px");
    expect(component.toJSON().children[1].props.style.right).toEqual("150px");
    const instance = component.getInstance();
    instance.handleClick();
    component.unmount();
  });

  it("test no changeProps", async () => {
    const event1 = {
      target: { className: "k-dropdown__trigger" }
    };
    const event2 = {
      target: { className: "" }
    };
    const event3 = {
      target: { className: "k-dropdown nav-link" }
    };
    const component = TestRenderer.create(
      <KDropdown
        triggerComponent={(<button type="button">Elemento acionador do Dropdown</button>)}
        styleContent={{ minWidth: "288px" }}
      >
        <div style={{
          backgroundColor: "#fff",
          padding: "10px",
          borderRadius: "4px",
          boxShadow: "5px 5px 25px 0px rgba(46, 61, 73, 0.2)"
        }}
        >
          <h1>Conteúdo do Dropdown</h1>
          <ul>
            <li>Item 1</li>
            <li>Item 2</li>
            <li>Item 3</li>
            <li>Item 4</li>
          </ul>
        </div>
      </KDropdown>,
    );
    expect(component).toBeDefined();
    const instance = component.getInstance();
    instance.handleDocumentClick(event1);
    instance.handleDocumentClick(event2);
    instance.handleDocumentClick(event3);
  });

  it("test changeProps", async () => {
    const event1 = {
      target: { className: "k-dropdown__trigger" }
    };
    const event2 = {
      target: { className: "" }
    };
    const component = TestRenderer.create(
      <KDropdown
        triggerComponent={(<button type="button">Elemento acionador do Dropdown</button>)}
        styleContent={{ minWidth: "288px" }}
        changeProps={() => {}}
      >
        <div style={{
          backgroundColor: "#fff",
          padding: "10px",
          borderRadius: "4px",
          boxShadow: "5px 5px 25px 0px rgba(46, 61, 73, 0.2)"
        }}
        >
          <h1>Conteúdo do Dropdown</h1>
          <ul>
            <li>Item 1</li>
            <li>Item 2</li>
            <li>Item 3</li>
            <li>Item 4</li>
          </ul>
        </div>
      </KDropdown>,
    );
    expect(component).toBeDefined();
    let instance = component.getInstance();
    instance.setState({ expanded: true });
    instance.handleDocumentClick(event1);
    instance.handleDocumentClick(event2);

    component.update(
      <KDropdown
        triggerComponent={(<button type="button">Elemento acionador do Dropdown</button>)}
        styleContent={{ minWidth: "288px" }}
      >
        <div style={{
          backgroundColor: "#fff",
          padding: "10px",
          borderRadius: "4px",
          boxShadow: "5px 5px 25px 0px rgba(46, 61, 73, 0.2)"
        }}
        >
          <h1>Conteúdo do Dropdown</h1>
          <ul>
            <li>Item 1</li>
            <li>Item 2</li>
            <li>Item 3</li>
            <li>Item 4</li>
          </ul>
        </div>
      </KDropdown>
    );

    instance = component.getInstance();
    instance.setState({ expanded: true });
    instance.handleDocumentClick(event1);
    instance.handleDocumentClick(event2);
  });
});
