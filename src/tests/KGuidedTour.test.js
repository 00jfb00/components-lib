import React from "react";
import { configure, mount } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import KGuidedTour from "../components/molecules/KGuidedTour";

configure({ adapter: new Adapter() });

describe("Is the KGuidedTour component working?", () => {
  it("Is it rendering?", async () => {
    const component = mount(<KGuidedTour
      isTourOpen={false}
      steps={[
        {
          selector: ".kguided-tour-float-baloon",
          title: "Titulo",
          position: "right",
          description: "Descrição do step"
        },
        {
          selector: ".kguided-tour-float-baloon",
          title: "Titulo",
          hideCloseButton: true,
          position: "right",
          description: "Descrição do step"
        }
      ]}
    />);
    expect(component.state().steps.length).toBe(2);
    component.setProps({ isTourOpen: true });
    component.find(".kguided-tour-float-baloon-prev-btn").at(0).simulate("click");
    component.find("#kguided-tour-float-baloon-close-btn-0").at(0).simulate("click");
    component.find(".kguided-tour-float-baloon-next-btn").at(0).simulate("click");
    component.find(".kguided-tour-float-baloon-next-btn").at(0).simulate("click");
    component.find(".kguided-tour-float-baloon-prev-btn").at(0).simulate("click");
    component.setProps({ isTourOpen: false });
    component.setProps({ hideStepIndicator: true });
    component.setProps({ hidePrevButton: true });
  });
});
