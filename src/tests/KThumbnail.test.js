import React from "react";
import TestRenderer from "react-test-renderer";
import jsdom from "jsdom";
import { configure, mount } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import KThumbnail from "../components/molecules/KThumbnail/KThumbnail";

configure({ adapter: new Adapter() });

describe("Is the KThumbnail component working?", () => {
  const dom = new jsdom.JSDOM("<div></div>");

  it("Is it rendering?", async () => {
    global = dom;
    const component = TestRenderer.create(<KThumbnail />);
    expect(component.toJSON().props.className.indexOf("k-thumbnail")).toBe(0);
  });

  it("Is it clickable?", async () => {
    global = dom;
    let click = false;
    const component = TestRenderer.create(
      <KThumbnail
        onClick={() => {
          click = true;
        }}
      />,
    );
    const instance = component.getInstance();
    expect(instance.props.onClick).toBeDefined();
    expect(typeof instance.props.onClick).toBe("function");
    expect(component.toJSON().children[0].children[0].props.className.indexOf("clickable") >= 0).toBe(true);
    expect(click).toBe(false);
    instance.handleClick();
    expect(click).toBe(true);
  });

  it("Is it hoverable?", async () => {
    global = dom;
    const component = TestRenderer.create(<KThumbnail hover onClick={() => {}} />);
    expect(component.toJSON().children[0].children[0].props.className.indexOf("has-animation") >= 0).toBe(true);
  });

  it("Is it responsive?", async () => {
    global = dom;
    const component = TestRenderer.create(<KThumbnail responsive />);
    expect(component.toJSON().props.className.indexOf("is-responsive") >= 0).toBe(true);
    component.update(<KThumbnail
      responsive
      hover
      src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQYKR8Of0RIBJVdYsXOMVlmODVMwAurznDMiiAA0Nils8k_v1PX"
    />);
    const instance = component.getInstance();
    instance.handleImageSizeChange();
  });

  it("Is it mounted with enzyme", async () => {
    const wrap = mount(
      <KThumbnail
        iconColor="red"
        src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQYKR8Of0RIBJVdYsXOMVlmODVMwAurznDMiiAA0Nils8k_v1PX"
        hover
        onClick={() => {
        }}
        maxHeight={80}
        maxWidth={80}
        responsive
        icon={["fab", "youtube"]}
        iconSize="sm"
        iconBackgroundColor="red"
      />,
    );
    wrap.update(<KThumbnail />);
  });
});
