import React from "react";
import TestRenderer from "react-test-renderer";
import KMessageMenuList from "../components/molecules/KMessageMenuList/KMessageMenuList";

describe("Is the KMenuList component working?", () => {
  it("Is it rendering without props?", async () => {
    const component = TestRenderer.create(<KMessageMenuList />);
    expect(component).toBeDefined();
  });
  it("Is it rendering with id?", async () => {
    const component = TestRenderer.create(<KMessageMenuList id="id" />);
    expect(component).toBeDefined();
    const instance = component.getInstance();
    expect(instance.props.id).toBe("id");
  });
  it("Is it rendering with title?", async () => {
    const component = TestRenderer.create(
      <KMessageMenuList id="id" title="menu" />,
    );
    expect(component).toBeDefined();
    const instance = component.getInstance();
    expect(instance.props.id).toBe("id");
    expect(instance.props.title).toBe("menu");
  });
  it("Is it rendering with list?", async () => {
    const component = TestRenderer.create(
      <KMessageMenuList id="id" title="menu" list={["item1", "item2"]} />,
    );
    expect(component).toBeDefined();
    const instance = component.getInstance();
    expect(instance.props.id).toBe("id");
    expect(instance.props.title).toBe("menu");
    expect(instance.props.list[0]).toBe("item1");
    expect(instance.props.list[1]).toBe("item2");
  });
  it("Is it clickable?", async () => {
    let component = TestRenderer.create(
      <KMessageMenuList
        id="id"
        title="menu"
        list={["item1", "item2", "item3"]}
        funcClick={() => {
        }}
      />,
    );
    let instance = component.getInstance();
    instance.handleClick();
    // This works but is ugly
    component.toJSON().children[0].children[0].children[0].props.onClick();
    const option = component.root.findByProps({ id: "id-1" });
    option.props.onClick();
    expect(instance.state.index).toBe(1);
    component = TestRenderer.create(
      <KMessageMenuList
        id="id"
        title="menu"
        list={["item1", "item2", "item3"]}
      />,
    );
    instance = component.getInstance();
    instance.handleFuncClick();
  });
});

