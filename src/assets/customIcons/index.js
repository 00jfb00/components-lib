const prefix = "kci";
const faForum = {
  prefix: "kci",
  iconName: "forum",
  icon: [
    5100,
    4096,
    [],
    "c001",
    "M4280 3834 c-104 -58 -227 -144 -501 -350 l-257 -193 -483 -4 c-359\n"
    + "-3 -495 -7 -527 -17 -48 -14 -132 -85 -167 -139 -35 -57 -64 -147 -84 -271\n"
    + "-32 -189 -36 -194 -170 -188 -74 3 -96 8 -162 39 -88 42 -203 118 -409 269\n"
    + "-274 201 -373 260 -434 260 -75 0 -86 -42 -86 -336 l0 -231 -282 -6 c-181 -4\n"
    + "-310 -11 -358 -20 -152 -30 -252 -80 -303 -154 -46 -66 -49 -151 -54 -1272\n"
    + "l-5 -1024 98 -99 99 -98 1150 0 c1044 0 1524 8 1690 26 52 6 65 12 106 51 105\n"
    + "98 148 234 156 491 l6 172 949 0 949 0 110 107 109 106 0 1072 0 1072 -84 77\n"
    + "c-48 42 -104 84 -128 93 -36 14 -96 17 -390 21 l-348 4 0 181 c-1 206 -13 361\n"
    + "-31 383 -21 26 -89 17 -159 -22z m-162 -708 l4 -134 411 -5 c227 -3 430 -10\n"
    + "452 -14 89 -18 94 -75 95 -1110 l0 -773 -890 0 -890 0 0 128 c-1 274 -20 972\n"
    + "-31 1067 -12 111 -25 149 -69 205 -77 97 -174 140 -376 165 -218 28 -234 37\n"
    + "-234 133 0 65 26 120 69 145 73 43 144 49 606 54 l439 5 131 101 c181 141 256\n"
    + "190 269 177 6 -6 12 -69 14 -144z m-2558 -640 c107 -82 211 -162 230 -178 l35\n"
    + "-28 542 0 543 0 0 -972 0 -971 -977 7 c-538 3 -1111 9 -1274 13 l-296 6 -7\n"
    + "291 c-3 160 -9 591 -12 959 l-7 667 502 0 501 0 0 186 c0 145 3 184 13 178 6\n"
    + "-4 100 -75 207 -158z"
  ]
};
const faMenu = {
  prefix: "kci",
  iconName: "menu",
  icon: [
    20,
    22,
    [],
    "c002",
    "M10,16.24H0V14H10v2.238ZM20,9.186H0V7H20V9.185ZM20,2.24H0V0H20V2.239Z"
  ]
};
const faClose = {
  prefix: "kci",
  iconName: "close",
  icon: [
    20,
    22,
    [],
    "c003",
    "M1.707,20,10,11.707,18.293,20,20,18.293,11.707,10,20,1.707,18.293,0,10,8.293,1.707,0,0,1.707,8.293,10,0,18.293Z"
  ]
};
const faArrowCircleRight = {
  prefix: "kci",
  iconName: "arrow-circle-right",
  icon: [
    512,
    512,
    [],
    "c004",
    "M8 256c0 137 111 248 248 248s248-111 248-248S393 8 256 8 8 119 8 256zm448 0c0 110.5-89.5 200-200 200S56 366.5 56 256 145.5 56 256 56s200 89.5 200 200zM266.9 126.1l121.4 121.4c4.7 4.7 4.7 12.3 0 17L266.9 385.9c-4.7 4.7-12.3 4.7-17 0l-19.6-19.6c-4.8-4.8-4.7-12.5.2-17.2l70.3-67.1H140c-6.6 0-12-5.4-12-12v-28c0-6.6 5.4-12 12-12h160.8l-70.3-67.1c-4.9-4.7-5-12.4-.2-17.2l19.6-19.6c4.7-4.7 12.3-4.7 17 0z"
  ]
};
const faArrowCircleLeft = {
  prefix: "kci",
  iconName: "arrow-circle-left",
  icon: [
    512,
    512,
    [],
    "c005",
    "M504 256C504 119 393 8 256 8S8 119 8 256s111 248 248 248 248-111 248-248zm-448 0c0-110.5 89.5-200 200-200s200 89.5 200 200-89.5 200-200 200S56 366.5 56 256zm189.1 129.9L123.7 264.5c-4.7-4.7-4.7-12.3 0-17l121.4-121.4c4.7-4.7 12.3-4.7 17 0l19.6 19.6c4.8 4.8 4.7 12.5-.2 17.2L211.2 230H372c6.6 0 12 5.4 12 12v28c0 6.6-5.4 12-12 12H211.2l70.3 67.1c4.9 4.7 5 12.4.2 17.2l-19.6 19.6c-4.7 4.7-12.3 4.7-17 0z"
  ]
};
const faArrowCircleDown = {
  prefix: "kci",
  iconName: "arrow-circle-down",
  icon: [
    512,
    512,
    [],
    "c006",
    "M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8zm0 448c-110.5 0-200-89.5-200-200S145.5 56 256 56s200 89.5 200 200-89.5 200-200 200zm129.9-206.1l-19.6-19.6c-4.8-4.8-12.5-4.7-17.2.2L282 300.8V140c0-6.6-5.4-12-12-12h-28c-6.6 0-12 5.4-12 12v160.8l-67.1-70.3c-4.7-4.9-12.4-5-17.2-.2l-19.6 19.6c-4.7 4.7-4.7 12.3 0 17l121.4 121.4c4.7 4.7 12.3 4.7 17 0l121.4-121.4c4.7-4.7 4.7-12.3 0-17z"
  ]
};
const faArrowCircleUp = {
  prefix: "kci",
  iconName: "arrow-circle-up",
  icon: [
    512,
    512,
    [],
    "c007",
    "M256 504c137 0 248-111 248-248S393 8 256 8 8 119 8 256s111 248 248 248zm0-448c110.5 0 200 89.5 200 200s-89.5 200-200 200S56 366.5 56 256 145.5 56 256 56zM126.1 245.1l121.4-121.4c4.7-4.7 12.3-4.7 17 0l121.4 121.4c4.7 4.7 4.7 12.3 0 17l-19.6 19.6c-4.8 4.8-12.5 4.7-17.2-.2L282 211.2V372c0 6.6-5.4 12-12 12h-28c-6.6 0-12-5.4-12-12V211.2l-67.1 70.3c-4.7 4.9-12.4 5-17.2.2l-19.6-19.6c-4.7-4.7-4.7-12.3 0-17z"
  ]
};

const faWaning = {
  prefix: "kci",
  iconName: "warning",
  icon: [
    512,
    512,
    [],
    "c008",
    "M270.2 160h35.5c3.4 0 6.1 2.8 6 6.2l-7.5 196c-.1 3.2-2.8 5.8-6 5.8h-20.5c-3.2 0-5.9-2.5-6-5.8l-7.5-196c-.1-3.4 2.6-6.2 6-6.2zM288 388c-15.5 0-28 12.5-28 28s12.5 28 28 28 28-12.5 28-28-12.5-28-28-28zm281.5 52L329.6 24c-18.4-32-64.7-32-83.2 0L6.5 440c-18.4 31.9 4.6 72 41.6 72H528c36.8 0 60-40 41.5-72zM528 480H48c-12.3 0-20-13.3-13.9-24l240-416c6.1-10.6 21.6-10.7 27.7 0l240 416c6.2 10.6-1.5 24-13.8 24z"
  ]
};

const faConecta = {
  prefix: "kci",
  iconName: "conecta",
  icon: [
    1300,
    1440,
    [],
    "c009",
    "M643 1450 c-236 -25 -430 -152 -523 -343 -57 -118 -73 -197 -73 -357\n"
    + "-1 -167 18 -261 78 -385 76 -158 205 -264 380 -311 90 -24 312 -30 405 -10\n"
    + "103 22 232 86 259 130 31 50 27 108 -12 191 -48 99 -58 105 -104 65 -20 -18\n"
    + "-67 -47 -103 -64 -60 -29 -73 -31 -170 -31 -95 0 -110 3 -156 27 -105 55 -156\n"
    + "148 -176 316 -14 122 0 212 47 311 35 72 81 116 152 146 117 49 295 11 420\n"
    + "-91 l42 -34 24 32 c62 83 84 186 53 247 -17 32 -92 86 -158 114 -92 39 -264\n"
    + "59 -385 47z\n"
    + "M690 882 c-54 -29 -80 -75 -80 -139 0 -87 41 -142 121 -163 72 -20\n"
    + "161 23 189 90 l12 30 99 0 99 0 0 40 0 40 -99 0 -99 0 -12 31 c-32 76 -153\n"
    + "113 -230 71z"
  ]
};
const faMinusCircle = {
  prefix: "kci",
  iconName: "minus-circle",
  icon: [
    512,
    512,
    [],
    "c010",
    "M140 274c-6.6 0-12-5.4-12-12v-12c0-6.6 5.4-12 12-12h232c6.6 0 12 5.4 12 12v12c0 6.6-5.4 12-12 12H140zm364-18c0 137-111 248-248 248S8 393 8 256 119 8 256 8s248 111 248 248zm-32 0c0-119.9-97.3-216-216-216-119.9 0-216 97.3-216 216 0 119.9 97.3 216 216 216 119.9 0 216-97.3 216-216z"
  ]
};
const faPlusCircle = {
  prefix: "kci",
  iconName: "plus-circle",
  icon: [
    512,
    512,
    [],
    "c011",
    "M384 250v12c0 6.6-5.4 12-12 12h-98v98c0 6.6-5.4 12-12 12h-12c-6.6 0-12-5.4-12-12v-98h-98c-6.6 0-12-5.4-12-12v-12c0-6.6 5.4-12 12-12h98v-98c0-6.6 5.4-12 12-12h12c6.6 0 12 5.4 12 12v98h98c6.6 0 12 5.4 12 12zm120 6c0 137-111 248-248 248S8 393 8 256 119 8 256 8s248 111 248 248zm-32 0c0-119.9-97.3-216-216-216-119.9 0-216 97.3-216 216 0 119.9 97.3 216 216 216 119.9 0 216-97.3 216-216z"
  ]
};

const faClipboardList = {
  prefix: "kci",
  iconName: "clipboard-list",
  icon: [
    512,
    512,
    [],
    "c011",
    "M280 240H168c-4.4 0-8 3.6-8 8v16c0 4.4 3.6 8 8 8h112c4.4 0 8-3.6 8-8v-16c0-4.4-3.6-8-8-8zm0 96H168c-4.4 0-8 3.6-8 8v16c0 4.4 3.6 8 8 8h112c4.4 0 8-3.6 8-8v-16c0-4.4-3.6-8-8-8zM112 232c-13.3 0-24 10.7-24 24s10.7 24 24 24 24-10.7 24-24-10.7-24-24-24zm0 96c-13.3 0-24 10.7-24 24s10.7 24 24 24 24-10.7 24-24-10.7-24-24-24zM336 64h-80c0-35.3-28.7-64-64-64s-64 28.7-64 64H48C21.5 64 0 85.5 0 112v352c0 26.5 21.5 48 48 48h288c26.5 0 48-21.5 48-48V112c0-26.5-21.5-48-48-48zM192 48c8.8 0 16 7.2 16 16s-7.2 16-16 16-16-7.2-16-16 7.2-16 16-16zm144 408c0 4.4-3.6 8-8 8H56c-4.4 0-8-3.6-8-8V120c0-4.4 3.6-8 8-8h40v32c0 8.8 7.2 16 16 16h160c8.8 0 16-7.2 16-16v-32h40c4.4 0 8 3.6 8 8v336z"
  ]
};

const _iconsCache = {
  faForum,
  faMenu,
  faClose,
  faArrowCircleRight,
  faArrowCircleLeft,
  faArrowCircleDown,
  faArrowCircleUp,
  faWaning,
  faConecta,
  faMinusCircle,
  faPlusCircle,
  faClipboardList
};

exports.kci = _iconsCache;
exports.prefix = prefix;
exports.faForum = faForum;
exports.faMenu = faMenu;
exports.faClose = faClose;
exports.faWaning = faWaning;
exports.faConecta = faConecta;
exports.faMinusCircle = faMinusCircle;
exports.faPlusCircle = faPlusCircle;
