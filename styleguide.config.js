const path = require("path");

const {
  createConfig, babel, css, sass, setOutput, match, file
} = require("webpack-blocks");

const pkg = require("./package.json");

module.exports = {
  title: "AVA 3.0 - Componentes",
  moduleAliases: {
    $PACKAGE_NAME: path.resolve(__dirname),
    [pkg.name]: path.resolve(__dirname),
    components: path.resolve(__dirname, "src/components")
  },
  require: [
    "./src/assets/scss/themes/ava_3.scss",
    "./docs/assets/docs.style.scss",
    "./docs/scripts/index.js"
  ],
  template: {
    favicon: "./docs/public/favicon.ico"
  },
  theme: {
    color: {
      link: "#FFF",
      linkHover: "#FFF",
      sidebarBackground: "#0185CD"
    },
    maxWidth: "100%",
    sidebarWidth: 240,
    fontFamily: {
      base: [
        "-apple-system",
        "BlinkMacSystemFont",
        "Segoe UI",
        "Roboto",
        "Oxygen",
        "Ubuntu",
        "Cantarell",
        "Fira Sans",
        "Droid Sans",
        "Helvetica Neue",
        "sans-serif"
      ],
      monospace: ["Consolas", "Liberation Mono", "Menlo", "monospace"]
    }
  },
  pagePerSection: true,
  sections: [
    {
      name: "Uso",
      content: "./docs/intro.md",
      sectionDepth: 1,
      exampleMode: "hide",
      usageMode: "hide"
    },
    {
      name: "Boas Práticas",
      content: "./docs/padroes-praticas.md",
      sectionDepth: 1,
      exampleMode: "hide",
      usageMode: "hide"
    },
    {
      name: "Padrões",
      content: "./docs/padroes-ava-lib.md",
      sectionDepth: 1,
      exampleMode: "hide",
      usageMode: "hide"
    },
    {
      name: "Tipografia",
      content: "./docs/typography.md",
      sectionDepth: 1,
      exampleMode: "expand",
      usageMode: "expand"
    },
    {
      name: "Componentes",
      content: "./docs/components.md",
      sectionDepth: 2,
      sections: [
        {
          name: "Átomos",
          content: "./docs/atoms.md",
          components: () => ["./src/components/atoms/**/K[A-Z]*.js"],
          sectionDepth: 2,
          exampleMode: "expand",
          usageMode: "expand"
        },
        {
          name: "Moléculas",
          content: "./docs/molecules.md",
          components: () => ["./src/components/molecules/**/K[A-Z]*.js"],
          sectionDepth: 2,
          exampleMode: "expand",
          usageMode: "expand"
        },
        {
          name: "Organismos",
          content: "./docs/organisms.md",
          components: () => ["./src/components/organisms/**/K[A-Z]*.js"],
          sectionDepth: 2,
          exampleMode: "expand",
          usageMode: "expand"
        }
      ]
    }
  ],
  webpackConfig: createConfig([
    setOutput("./build/bundle.js"),
    babel(),
    css(),
    match(["*.scss", "!*node_modules*"], [
      css(),
      sass(/* node-sass options */)
    ]),
    match(["*.gif", "*.jpg", "*.jpeg", "*.png", "*.svg", "*.webp"], [
      file()
    ])
  ])
};
