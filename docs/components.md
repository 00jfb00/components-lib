### Os componentes são as menores estruturas básicas de uma interface de usuário. A biblioteca segue os padrões do <code>atomic design</code> e temos demonstrações do uso desses elementos.
 
Tudo o que você vê aqui é editável no formato Markdown. 
A documentação que você vê abaixo é gerada automaticamente pelo React Styleguidist. 

## Overview

```
import Manager from './components';
<Manager show="all" />
```
