
Padrões e Boas Práticas no React.js

Regras Essenciais
------
*Categorias*: Desing de Componentes, Otimizaçãode desempenho, Nomenclatura

1.	Detalhar a definição de cada propertie do componente (tipo, obrigatoriedade, default)

2.	Componentes simples devem ser escritos como componentes puros

3.	**SEMPRE** usar a propriedade ````:key```` no ````map```` para otimização de desempenho (evite alterações desnecessárias no DOM)


Regras Altamente Recomendadas 
------
*Categorias*: Nomenclatura, Desing de Componentes

1.	**SEMPRE** criar um único componente por arquivo

2.	Nome dos componentes (.js) **SEMPRE** em ````Pascal Case```` (Ex: ConsumoDados.js)

3.	**NUNCA** exportar os componentes diretamente. **SEMPRE** crie um arquivo indexador (index.js) para cada componente

4.	Nomes de componentes fortemente acoplados **DEVEM** possuir como prefixo o nome do componente pai (Ex: SearchSidebar.js, SearchSidebarNavigation.js, SearchSidebarMenu.js)

5.	**NUNCA** utilizar abreviações para nomear componentes

6.	Propriedade dos componentes **SEMPRE** em ````camelCase```` (Ex: saldoConta)
