import React, { Component } from "react";
import PropTypes from "prop-types";
import KIcon from "../../src/components/atoms/KIcon";

class Manager extends Component {
  static defaultProps = {
    show: "all"
  };

  static propTypes = {
    /** Define a posição do badge (topLeft, topRight, bottomLeft, bottomRight) * */
    show: PropTypes.oneOf(["all", "atoms", "molecules", "organisms"])
  };

  constructor(props) {
    super(props);

    this.state = {
      components: [],
      label: "Componentes"
    };
  }

  getComponents = () => {
    const contexts = [];
    let label = "Componentes";
    switch (this.props.show) {
      case "all":
        contexts.push(require.context("../../src/components/atoms/", true, /index\.js$/));
        contexts.push(require.context("../../src/components/molecules/", true, /index\.js$/));
        contexts.push(require.context("../../src/components/organisms/", true, /index\.js$/));
        break;
      case "atoms":
        contexts.push(require.context("../../src/components/atoms/", true, /index\.js$/));
        label = "Átomos";
        break;
      case "molecules":
        contexts.push(require.context("../../src/components/molecules/", true, /index\.js$/));
        label = "Moléculas";
        break;
      case "organisms":
        contexts.push(require.context("../../src/components/organisms/", true, /index\.js$/));
        label = "Organismos";
        break;
      default: break;
    }

    const components = [];
    contexts.map(context => context.keys().map(key => components.push(context(key).default)));

    this.setState({
      components: components.sort((a, b) => {
        const nameA = a.name.toLowerCase();
        const nameB = b.name.toLowerCase();
        if (nameA < nameB) return -1;
        if (nameA > nameB) return 1;

        return 0;
      }),
      label
    });
  };

  UNSAFE_componentWillMount() {
    this.getComponents();
  }

  render() {
    const { label, components } = this.state;

    return (
      <div className="k-manager-components-status">
        <ul className="status-list">
          <li>
            <KIcon icon="check-circle" state="success" brand="default" size="1x" />
            &nbsp;Disponível
          </li>
          <li>
            <KIcon icon="eraser" state="info" brand="default" size="1x" />
            &nbsp;Em revisão
          </li>
          <li>
            <KIcon icon="exclamation-triangle" state="danger" brand="default" size="1x" />
            &nbsp;Depreciado
          </li>
          <li>
            <KIcon icon="pencil-alt" state="warning" brand="default" size="1x" />
            &nbsp;Protótipo
          </li>
        </ul>
        <table>
          <thead>
            <tr>
              <th>
                {" "}
                {label}
                {" "}
              </th>
              <th>Detalhes</th>
              <th>Released</th>
              <th>Status</th>
            </tr>
          </thead>
          <tbody>
            {
            components.map(item => (
              <tr key={item.name}>
                <td>
                  <code className="name">
                    <a href={`#/${item.url}`}>
                      {" "}
                      {item.name}
                      {" "}
                    </a>
                  </code>
                </td>
                <td>
                  { item.description }
                </td>
                <td>
                  { item.released }
                </td>
                <td style={{
                  display: "flex", justifyContent: "center", alignItems: "center", alignContent: "center"
                }}
                >
                  <KIcon icon="check-circle" state="success" brand="default" size="1x" />
                </td>
              </tr>
            ))
          }
          </tbody>
        </table>
      </div>
    );
  }
}

export default Manager;
