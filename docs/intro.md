
Biblioteca de componentes para React.js beaseda no react-bootstrap.

```
import package_json from '../package.json';
<>
    <hr />
    <h1>Versão v{package_json.version}</h1>
    <hr />
</>
```

### Adicionando o ava30-frontend-componentslib ao projeto
Obs: O NPM deve esta registrado para https://pkgs.dev.azure.com/kdop/_packaging/ava30/npm/registry/ <br> 
Isso pode ser feito adicionando o arquivo `.npmrc` na raiz do projeto com as seguintes informações:
````shell
registry=https://pkgs.dev.azure.com/kdop/_packaging/ava30/npm/registry/
always-auth=true
````

Após a configuração do .npmrc rodar o seguinte comando:
````shell
# Para adicionar o Ava 3.0-lib  ao projeto
$ npm install ava30-frontend-componentslib@latest
````

Obs: Talvez seja necessário gerar a chave de autenticação no KDop: <br/>
* Acessar: https://dev.azure.com/kdop/Plataformas%20Interativas/_packaging?_a=feed&feed=ava30 <br/>
* Clicar em "Connect to feed"
* Gerar as credenciais e colocá-las no seu .npmrc


````shell
registry=https://pkgs.dev.azure.com/kdop/_packaging/ava30/npm/registry/
always-auth=true

; Treat this auth token like a password.
; begin auth token
//pkgs.dev.azure.com/kdop/_packaging/ava30/npm/registry/:username=***
//pkgs.dev.azure.com/kdop/_packaging/ava30/npm/registry/:_password=***
//pkgs.dev.azure.com/kdop/_packaging/ava30/npm/registry/:email=***
//pkgs.dev.azure.com/kdop/_packaging/ava30/npm/:username=***
//pkgs.dev.azure.com/kdop/_packaging/ava30/npm/:_password=***
//pkgs.dev.azure.com/kdop/_packaging/ava30/npm/:email=***
; end auth token
````

Para utilizar nossos componentes aconselhamos envolvê-los com os componentes: 'k-main', 'k-side', 'k-body':

````shell
<KMain>
    <KHeader/>
    <KSide width={ 280 }>
        </k-sidebar>
    </KSide>
    <KBody>
        <Router>
            <Switch>
                <RouteLogged path="/" component={Home}/>
            </Switch>
        </Router>
    </KBody>
</KMain>
````


````shell
# Utilização de componente
$ import { KButton } from "ava30-frontend-componentslib";

# Importação do tema (.js || .sass)
$ import 'ava30-frontend-componentslib/src/theme.sass'
$ @import "~ava30-frontend-componentslib/src/theme";
````
